<?php
/*------------------------------------------------------------------------
# mod_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
class modvQuizcategoryHelper
{
	static function getItems($params)
	{
 
		 
		 	$app = JFactory::getApplication();
			$lang = JFactory::getLanguage();	
			
			$id = $params->def('id', 0);
			$ordering = $params->get('ordering');
			$limit = (int)$params->get('numberofcategory', 5);
			$db = JFactory::getDBO();
			$user = JFactory::getUser();
			$date = JFactory::getDate();

				//recent Played quizzes
				if($ordering==3){
				$query = 'select i.*,(SELECT count(cq.quiz_categoryid) from #__vquiz_quizzes as cq where cq.quiz_categoryid=i.id) as totalquizzes from #__vquiz_category as i left join ( select max(id) as id, quizzesid from #__vquiz_quizresult group by quizzesid) as r on r.categoryid=i.id';
				}else{
				$query = 'select i.*, count(i.id) as items , (SELECT count(cq.quiz_categoryid) from #__vquiz_quizzes as cq where cq.quiz_categoryid=i.id) as totalquizzes from #__vquiz_category as i left join #__vquiz_quizresult as r on r.categoryid=i.id';}


				$where = array();
				$where[] = 'i.published = 1';
				$where[] = 'i.id != 1';
				
				$where[] = 'i.access in ('.implode(',', $user->getAuthorisedViewLevels()).')';
								
				if($app->getLanguageFilter())	{
					$where[] = 'i.language in ('.$db->quote($lang->getTag()).', '.$db->Quote('*').')';
				}

				$query .= ' where ' . implode(' and ', $where);
				
				//	latest_quizzes	
				if($ordering==1)	{
					$query .= ' group by i.id order by created_date desc';
				}

				//most Played quizzes
				elseif($ordering==2)	{
					
					 $query .= ' group by i.id order by items desc';
				}

				//recent Played quizzes
				elseif($ordering==3)	{
					$query .= ' order by r.id desc';

				}

				//random played quizzes

				elseif($ordering==4)	{
					$query .= ' group by i.id ORDER BY RAND()';
				}

				$query .= ' limit '.$limit;

				$db->setQuery( $query );
				$items = $db->loadObjectList();
				//echo $db->getErrorMsg();
				return $items;

				

	}

 
}