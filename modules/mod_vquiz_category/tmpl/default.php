<?php
/*------------------------------------------------------------------------
# mod_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
?>

        <div class="category">
        <ul>
        <?php
        for($i=0;$i<count($items);$i++){ ?>	
        <li>
        <a href="<?php echo JRoute::_('index.php?option=com_vquiz&view=quizmanager&layout=quizzes&id='.$items[$i]->id); ?>">
        <?php if($showimage==1)
			if(!empty($items[$i]->photopath) and file_exists(JPATH_ROOT.'/media/com_vquiz/vquiz/images/photoupload/thumbs/'.'thumb_'.$items[$i]->photopath)){ 
			echo '<span class="th-image"><img src="'.JURI::root().'/media/com_vquiz/vquiz/images/photoupload/thumbs/'.'thumb_'.$items[$i]->photopath. '" alt=""/></span>'; 
			}else { echo '<span class="th-image"><img src="'.JURI::root().'/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1"/></span>';} 
		?>
		<spna class="quizzes-title"><?php echo $items[$i]->quiztitle;?></span>
		<?php if($nofquizzes==1){echo '<spna class="nofquizzes">('.$items[$i]->totalquizzes.')</span>';} ?></a>
        </li>
        <?php }	?>
        </ul>
        <?php if($viewallcategory) : ?>
        <div class="view-all">
        <a class="btn" href="<?php echo JRoute::_('index.php?option=com_vquiz&view=vquiz'); ?>">
		<?php echo JText::_('VIEW_ALL')?></a>
        </div>
		<?php endif; ?>
        </div>

 