<?php
/*-----------------------------------------------------------------------
# mod_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
require_once __DIR__ . '/helper.php';
$document = JFactory::getDocument();
$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_vquiz_category/assets/css/style.css');
$items = modvQuizcategoryHelper::getItems($params);
/*if($profile->load_jquery==1)	{
	$document->addScript('modules/mod_vquiz/assets/js/jquery.1.10.js');
}
$document->addScript('modules/mod_vquiz/assets/js/jquery.form.js');*/
if(count($items)) {
	$viewallcategory = $params->get('viewallcategory');
	$showimage = $params->get('showimage');
	$nofquizzes = $params->get('number_ofquizzes');
	require(JModuleHelper::getLayoutPath('mod_vquiz_category'));
}else	{
	echo JText::_('NO_QUIZ_CATEGORY_FOUND');

}

 