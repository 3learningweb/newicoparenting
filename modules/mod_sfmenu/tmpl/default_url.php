<?php
/**
 * @version		$Id: default_url.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="'.$item->anchor_css.'" ' : '';
$title = $item->anchor_title ? 'title="'.$item->anchor_title.'" ' : '';

$imgpath = JURI::base(true) . '/modules/mod_sfmenu/assets/link_opens_new_window.gif';
$onw = '<img src="'.$imgpath.'" alt="('.JText::_('JBROWSERTARGET_NEW').')"/>';

if ($item->menu_image) {
		$item->params->get('menu_text', 1 ) ? 
		$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'"width=93 height=35" /><span class="image-title">'.$item->title.'</span> ' :
		$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" />';
} 
else { $linktype = $item->title;
}

switch ($item->browserNav) :
	default:
	case 0:
?>
		<a class="menu_link_<?php echo $item->level;?>" title="<?php echo $item->title; ?>" <?php echo $class; ?>href="<?php echo $item->flink; ?>" <?php echo $title; ?>>
			<?php if($item->level == 2){ echo "<span class='submenu_icon'>•</span>"; } ?>
			<span><?php echo $linktype; ?></span>
		</a><?php
		break;
	case 1:
		// _blank
?>
		<!--<a class="menu_link_<?php echo $item->level;?>" title="<?php echo $item->title ."(".JText::_('JBROWSERTARGET_NEW').")"; ?>" <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><span><?php echo $linktype . $onw; ?></span></a>-->
		<a class="menu_link_<?php echo $item->level;?>" title="<?php echo $item->title ."(".JText::_('JBROWSERTARGET_NEW').")"; ?>" <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>>
			<span><?php echo $linktype ; ?></span>
			<?php if($item->level == 2){ echo "<span class='submenu_icon'>•</span>"; } ?>
		</a><?php
		break;
	case 2:
		// window.open
		$attribs = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'.$params->get('window_open');
?>
		<a class="menu_link_<?php echo $item->level;?>" <?php echo $class; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','<?php echo $attribs;?>');return false;" <?php echo $title; ?>>
			<?php if($item->level == 2){ echo "<span class='submenu_icon'>•</span>"; } ?>
			<span><?php echo $linktype; ?></span>
		</a><?php
		break;
endswitch;
