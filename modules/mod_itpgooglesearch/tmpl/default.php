<?php
/**
 * @package      ITPGoogleSearch
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor.iliev@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
 
defined('_JEXEC') or die; 

$menuid=$params->get('menuid');
?>
<div class="itp-gs<?php echo $moduleclass_sfx;?>">
	<div class="search">
		<form action="<?php echo JRoute::_('index.php?option=com_itpgooglesearch&view=search&Itemid='.$menuid); ?>" method="get" accept-charset="utf-8">
	       	<span>全文檢索</span>
	        <div class="search_block">
		        <input name="gsquery" type="text" class="inputbox" placeholder="<?php echo JText::_("MOD_ITPGOOGLESEARCH_SEARCH_FOR"); ?>" value="<?php echo $phrase; ?>" />
		        <?php if($params->get("searchButton")) {?>
				<input type="hidden" name="view" value="search">
				<input type="hidden" name="option" value="com_itpgooglesearch">
				<input type="hidden" name="Itemid" value="<?php echo $menuid; ?>">
		        <input type="submit" class="btn icon-search" />
		        <?php }?>
	        </div>
	    </form>
   </div>
</div>