<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

$menuid = $params->get('tag_id');
$title = $params->get('name');
$class_sfx = $params->get('moduleclass_sfx');

// load script code
ob_start();
require JModuleHelper::getLayoutPath('mod_menu', 'responsive_script');
$script = ob_get_contents();
ob_end_clean();

// load Javascript compressor library
// See https://github.com/rgrove/jsmin-php/
require_once JPATH_SITE . DIRECTORY_SEPARATOR . 'rd' . DIRECTORY_SEPARATOR . 'jsmin.php';

$script = str_replace('<script>', '', $script);
$script = JSMin::minify($script);

$document = &JFactory::getDocument();
$document->addScriptDeclaration($script);

// Note. It is important to remove spaces between elements.
$j = 0;



?>
<div id="open-<?php echo $menuid ?>" style="display: none;">
	<a href="javascript:void(0);" alt="<?php echo $title ?>" title="<?php echo $title ?>"><span><?php echo $title ?></span></a>
</div>
<ul class="menu<?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id')!=NULL) {
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
// echo "<pre>";
// print_r($list);
// echo "</pre>";
foreach ($list as $i => &$item) :
        $isEnd = (count($list) == $j + 1);
        $j++;

	$class = 'item-'.$item->id;
	if ($item->id == $active_id) {
		$class .= ' current';
	}

	if (in_array($item->id, $path)) {
		$class .= ' active';
	}
	elseif ($item->type == 'alias') {
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path)) {
			$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator') {
		$class .= ' separator';
	}

	if ($item->deeper) {
		$class .= ' deeper';
	}

	if ($item->parent) {
		$class .= ' parent';
	}
	
	// rene start
	if($item->level == "1") {
		$class .= ' mainlevel1';
	}
	
	if($item->level == "2") {
		$class .= ' mainlevel2';
	}

	if (!empty($class)) {
		$class = ' class="'.trim($class) .'"';
	}
	// rene end
	
	echo '<li'.$class.'>';
	
	//rene
	if($item->level == "1") {
		echo '<div class="itemlevel1">';
	}else{
		echo '<div class="itemlevel2">';
	}
	

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;
	
	//rene
	echo '</div>';
	
	// The next item is deeper.
	if ($item->deeper) {
		echo '<ul>';
	}
	// The next item is shallower.
	elseif ($item->shallower) {
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else {
		echo '</li>';
	}
	
endforeach;
?>
</ul>

<script language="JavaScript">
	// rene
	jQuery(document).ready(function() {
		jQuery("#remainmenu .parent").each(function(){
			jQuery(this).children().children("a").attr("href", "#");
		});		
		
		jQuery(".mainlevel2").hide();
		jQuery("#remainmenu .active").find("li").show();
		
		jQuery(".mainlevel1").on("click", function() {
			jQuery(".mainlevel1").removeClass("active");
			jQuery(this).addClass("active");
			jQuery(".mainlevel2").hide();
			jQuery(this).find("li").show();
		});
	});
</script>
