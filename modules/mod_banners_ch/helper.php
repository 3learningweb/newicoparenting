<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_banners
 *
 * @package     Joomla.Site
 * @subpackage  mod_banners
 * @since       1.5
 */
class ModBannersChHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getBanner(&$params)
	{
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__menu'));
		$query->where("id = '{$itemid}'");
		$db->setQuery($query);
		$menu = $db->loadObject();

		if($menu->parent_id == 1) {
			$menu_id = $menu->id; 
		}else{
			$menu_id = $menu->parent_id;
		}
		
		$baby	 = $params->get('baby_id');
		$edu	 = $params->get('edu_id');
		$parents = $params->get('parents_id');

		switch($menu_id) {
			case $baby :
				$banner = $params->get('baby_img');
				break;
			case $edu :
				$banner = $params->get('edu_img');
				break;
			case $parents :
				$banner = $params->get('parents_img');
				break;
			default:
				$banner = $params->get('ico_img');
		}

		return $banner ;
	}
}
