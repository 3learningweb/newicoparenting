<?php
/**
 * checklogin Module Entry Point
 * 
 * @package    
 * @subpackage 
 * @link http://www.efatek.com/
 * @license        
 * the module created by efatek.
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$app = JFactory::getApplication();

// Include the syndicate functions only once
//require_once( dirname(__FILE__).DIRECTORY_SEPARATOR.'helper.php' );
 
//$list = modcheckloginHelper::getList( $params );

$itemid = $params->get('menuid');
$is_check_group = $params->get('is_check_group');
$group_id = $params->get('group_id');

// 取得Menu Link
$menu = $app->getMenu();
$menu_link = $menu->getItem($itemid)->link;


// check user is login
$user =& JFactory::getUser();
$user_id = $user->get('id');

if ($user_id) {

	if ($is_check_group) {
		$groups = JAccess::getGroupsByUser($user_id, false);

		if (!in_array($group_id, $groups)) {		// 檢查是否為志工群組

			$msg = JText::_('MOD_CHECKLOGIN_NOTGROUP');
			JFactory::getApplication()->enqueueMessage($msg);


			$link = JRoute::_($menu_link. "&Itemid=". (int) $itemid);
			$app->redirect($link);
		}
	}

} else {	// 未登入
	$msg = JText::_('MOD_CHECKLOGIN_LOGIN');
	JFactory::getApplication()->enqueueMessage($msg);

	$link = JRoute::_($menu_link. "&Itemid=". (int) $itemid);
	
	$app->redirect($link);
}




//require( JModuleHelper::getLayoutPath( 'mod_checklogin' ) );
?>
