<?php
/**
 * @version		:  2011-07-07 05:07:21$
 * @author		 
 * @package		Tabs
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.html.parameter');

$document 	= &JFactory::getDocument();

// setting params
require JModuleHelper::getLayoutPath('mod_tabs', 'default_params');

// only load once scrpt & css
require_once JPATH_SITE.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'mod_tabs'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_assets.php';

// load tab script
ob_start();
require JModuleHelper::getLayoutPath('mod_tabs', 'default_script');
$script = ob_get_contents();
ob_end_clean();

// load Javascript compressor library
// See https://github.com/rgrove/jsmin-php/
require_once JPATH_SITE . DIRECTORY_SEPARATOR . 'rd' . DIRECTORY_SEPARATOR . 'jsmin.php';
$script = str_replace('<script>', '', $script);
$script = JSMin::minify($script);
$document->addScriptDeclaration($script);

// Instantiate title class 
$tabCls = array('tab-1st', 'tab-2nd');
?>
<div id="<?php echo $idTag; ?>">
	<div class="tab-box">
		<?php foreach($tabs as $i => $tab):
			$tab->params = new JRegistry( $tab->params );
			$mod_title = $tab->params->get('mod_title1', false);
			$tab->title = ($mod_title)? $mod_title: $tab->title;
		?>
		<div class="tab-item">
			<?php if ($mod_title != ""): ?>
    		<div class="tab-title tab-title<?php echo $classTag; ?> <?php echo (array_key_exists($i, $tabCls))?$tabCls[$i] : 'tab-'. ($i + 1) .'th' ?>">
    			<a class="tab-link tab-link<?php echo $classTag; ?>" href="javascript:void(0);"><span class="tab-link-span tab-link<?php echo $classTag; ?>"><?php echo $tab->title;?></span></a>
			</div>
			<?php endif; ?>
			<?php if($separator != '' && $i < (count($tabs) - 1) ): ?>
			<div class="tab-separator" style="display: none;"><?php echo $separator; ?></div>
			<?php endif; ?>
    		<div class="tab-content">
    			<?php echo $module->render($tab); ?>
    		</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>