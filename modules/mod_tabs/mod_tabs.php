<?php
/**
 * @version		:  2011-07-07 05:07:21$
 * @author		 
 * @package		Tabs
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
//require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'helper.php';

// Paging module source 
$tabs = JModuleHelper::getModules( $params->get('position') );

// Get JDocumentRendererModule Object
$doc = JFactory::getDocument();
$title = $module->title;
$module = $doc->loadRenderer('module');

// Get other params
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$separator = htmlspecialchars($params->get('separator', ''));
$idTag = htmlspecialchars($params->get('id_tag', ''));
$classTag = htmlspecialchars($params->get('moduleclass_sfx', ''));

require JModuleHelper::getLayoutPath('mod_tabs', $params->get('layout','default'));