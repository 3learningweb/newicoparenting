<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_leftmenu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php if(count($leftmenu) > 1) { ?>
<div class="mod_leftmenu">
	<ul class="nav menu<?php echo $class_sfx;?>">
		<?php 
			foreach ($leftmenu as $i => &$item)
			{
				$class = 'item-' . $item->id;
			
				if ($item->id == $active_id)
				{
					$class .= ' current';
				}
			
				if (in_array($item->id, $path))
				{
					$class .= ' active';
				}
				elseif ($item->type == 'alias')
				{
/*					$aliasToId = $item->params->get('aliasoptions');
		
					if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
					{
						$class .= ' active';
					}
					elseif (in_array($aliasToId, $path))
					{
						$class .= ' alias-parent-active';
					}*/
				}
			
				if ($item->type == 'separator')
				{
					$class .= ' divider';
				}
			
				if ($item->deeper)
				{
					$class .= ' deeper';
				}
			
				if ($item->parent_id == 1)
				{
					$class .= ' parent';
				}
			
				if (!empty($class))
				{
					$class = ' class="' . trim($class) . '"';
				}
			
				echo '<li' . $class . '>';
				
				// link
				$icon = "";
				if($item->parent_id != 1) {	$icon = "．"; }
				if($i == 0) { echo "<div>"; }
				switch($item->type) {
					case "component" :
						$router = $app::getRouter();

						if ($router->getMode() == JROUTER_MODE_SEF)
						{
							$item->link = 'index.php?Itemid=' . $item->id;

							if (isset($item->query['format']) && $app->get('sef_suffix'))
							{
								$item->link .= '&format=' . $item->query['format'];
							}
						}
						else
						{
							$item->link .= '&Itemid=' . $item->id;
						}
						echo "<a href='" . JRoute::_("{$item->link}&Itemid={$item->id}", false) ."' title='{$item->title}'>" . $icon . $item->title . "</a>";
						break;
					case "url" :
						echo "<a href='{$item->link}' title='{$item->title}'>" . $icon . $item->title . "</a>";
						break;
					case "alias" :
						$alias = json_decode($item->params, JSON_UNESCAPED_UNICODE);
						echo "<a href='" . JRoute::_("{$item->link}&Itemid={$alias['aliasoptions']}", false) ."' title='{$item->title}'>" . $icon . $item->title . "</a>";
						break;
				}
				if($i == 0) { echo "</div>"; }	
				
				// The next item is deeper.
				if ($item->parent_id == 1)
				{
					echo '<ul class="nav-child unstyled small">';
				}
				elseif ($item->shallower)
				{
					// The next item is shallower.
					echo '</li>';
					echo str_repeat('</ul></li>', $item->level_diff);
				}
				// The next item is on the same level.
				//if not the last one, echo separator.
				elseif (!$isEnd && $separator==1 ) {
					echo "<li class='separator'><span>．</span></li>";	
				} else {
					echo '</li>';
				}
			}
		?>
	</ul>
</div>
<?php } ?>
