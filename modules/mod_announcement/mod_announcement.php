<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_announcement
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$list = modAnnouncementHelper::getList($params);

$limit = (int) $params->get('limit');
$menuid = $params->get('menuid');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_announcement', $params->get('layout', 'default'));
