<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$h_title = $params->get('hit_title');
$h_content = $params->get('hit_content');
$youtube_arr = explode("v=", $params->get('hit_youtube'));
$h_youtube = $youtube_arr[1];
$menuid = $params->get('menuid');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_hit', $params->get('layout', 'default'));
