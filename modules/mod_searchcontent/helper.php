<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_hit
 *
 * @package     Joomla.Site
 * @subpackage  mod_hit
 * @since       1.5
 */
class ModSearchcontentHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getKeywordCount(&$params) {
		$app = JFactory::getApplication();
		$jinput = $app->input;
		$keyword = $jinput->getString('content_keyword', '');
		$keyword = htmlentities($keyword,ENT_QUOTES);
		if ($keyword == "") {
			return;
		}

		$db = & JFactory::getDBO();

		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__content_keyword'));
		$query->where('keyword = '. $db->quote($keyword));

		$db->setQuery($query);
		$row = $db->loadObject();

		if ($row) {		// 更新
			$query = $db->getQuery(true);

			// Fields to update.
			$fields = array(
				$db->quoteName('keyword') . ' = ' . $db->quote($keyword),
				$db->quoteName('count') . ' = ' . $db->quote($row->count + 1)
			);
			$conditions = array(
				$db->quoteName('id') . ' = '. $db->quote($row->id)
			);

			$query->update($db->quoteName('#__content_keyword'))->set($fields)->where($conditions);
		} else {							// 新增一筆新的
			$query = $db->getQuery(true);

			// Insert columns.
			$columns = array('keyword', 'count');
			$values = array($db->quote($keyword), 1);

			// Prepare the insert query.
			$query
				->insert($db->quoteName('#__content_keyword'))
				->columns($db->quoteName($columns))
				->values(implode(',', $values));

		}

		$db->setQuery($query);
		$db->execute();

		return ;
	}

	public static function &getKeywordList(&$params) {
		$limit = $params->get('limit', 6);

		$db = & JFactory::getDBO();

		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__content_keyword'));
		$query->order('count desc limit 0, '. $limit);

		$db->setQuery($query);

		return $db->loadObjectList();
	}
}
