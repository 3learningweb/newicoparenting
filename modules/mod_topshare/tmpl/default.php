<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_topshare
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$uri = JFactory::getURI();
$doc = JFactory::getDocument(); 
$page_title = $doc->getTitle();
?>
<div class="mod_topshare">
	<span>
		<!-- twitter -->
		<?php $href = "http://twitter.com/share?text=" . rawurlencode($page_title) . "&amp;url=" . rawurlencode($uri); ?>
		<a target="_blank" title="twitter" href="<?php echo $href; ?>">
			<img src="templates/ch/images/share/twitter.png" alt="twitter" />
		</a>
		
		<!-- facebook -->
		<?php $href = "http://www.facebook.com/share.php?u=" . rawurlencode($uri) . "&amp;t=" . rawurlencode($page_title); ?>		
		<a target="_blank" title="facebook" href="<?php echo $href; ?>">
			<img src="templates/ch/images/share/facebook.png" alt="facebook" />
		</a>
		
		<!-- google+ -->
		<a target="_blank" title="google+" href="https://plus.google.com/share?url=<?php echo rawurlencode($uri); ?>">
			<img src="templates/ch/images/share/google.png" alt="google" />
		</a>
		
		<!-- plurk -->
		<?php $href = "http://www.plurk.com/?qualifier=shares&amp;status=" . rawurlencode($uri) ."%20+(" . rawurlencode($page_title) .")"; ?>
		<a target="_blank" title="plurk" href="<?php echo $href; ?>">
			<img src="templates/ch/images/share/plurk.png" alt="plurk" />
		</a>
		
		<!-- weibo -->
		<a target="_blank" title="weibo" href="http://v.t.sina.com.cn/share/share.php?url=<?php echo rawurlencode($uri); ?>&title=<?php echo rawurlencode($page_title); ?>">
			<img src="templates/ch/images/share/weibo.png" alt="weibo" />
		</a>
	</span>
</div>
