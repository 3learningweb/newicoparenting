<?php
/*------------------------------------------------------------------------
# mod_vquiz_user - vQuiz User
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
class modvQuizuserHelper
{
	static function getItems($params)
	{
		
		$app = JFactory::getApplication();
		$lang = JFactory::getLanguage();		
		
		$limit = (int)$params->get('numberofuser', 5);	
		$ordering = $params->get('ordering');	
		$showlink = $params->get('showlink');
		$autodetect =(int)$params->get('autodetect',0);
		$selected_quizid =(int)$params->get('quizzesid',0);
		
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$date = JFactory::getDate();
		
		
		
					$query = 'select i.*, r.id as quizesid,r.quizzes_title as quiz_title ,u.name as username,x.profile_pic as userpic';

	
				//recent Played quizzes
				if($ordering==1){
				//$query .= ' from #__users as u left join #__vquiz_users as x on x.userid=u.id left join (select score,maxscore,quiz_spentdtime,max(id) as id, userid, avg(score*100/maxscore) as userscore,quizzesid from #__vquiz_quizresult group by userid) as i on i.userid=u.id  left join #__vquiz_quizzes as r on r.id=i.quizzesid ';
				$query .=' ,y.score as score,y.maxscore as maxscore ,y.quiz_spentdtime as quiz_spentdtime ';
				}
				//most Played 
				else if($ordering==2)
				$query .= ', i2.counts as counts';
				else
				{
				$query .= ' from #__users as u left join #__vquiz_quizresult as i on u.id=i.userid left join #__vquiz_quizzes as r on r.id=i.quizzesid  left join #__vquiz_users as x on x.userid=u.id';
				}

				$where = array();
				
				if($autodetect==1){
					
				     $checkview = JRequest::getVar('view', '');
				     $layout = JRequest::getCmd('layout', '');
				   
					 if($checkview=='quizmanager' and $layout==''){
					     $detect_quizid = JRequest::getInt('id', 0);
				         $where[] = 'r.id ='.$db->quote($detect_quizid);
					 }
				  else if($selected_quizid)
					{
						 $where[] = 'r.id ='.$db->quote($selected_quizid);
					}
					 
				}
				else if($selected_quizid)
				{
					 $where[] = 'r.id ='.$db->quote($selected_quizid);
				}
				
				$where[] = ' r.access in ('.implode(',', $user->getAuthorisedViewLevels()).')';
				$where[] = ' r.startpublish_date <= '.$db->quote($date->format('Y-m-d')).' and r.endpublish_date >= '.$db->quote($date->format('Y-m-d'));
				if($app->getLanguageFilter())	{

					$where[] = 'r.language in ('.$db->quote($lang->getTag()).', '.$db->Quote('*').')';

				}
				//recent Played  
				if($ordering==1)	{
					//$query .= ' left join (select max(id) as id, userid, avg(score*100/maxscore) as userscore,quizzesid from #__vquiz_quizresult group by userid) as i3 ON i3.quizzesid=r.id';
					$query .='  from #__users as u left join #__vquiz_users as x on x.userid=u.id left join (select max(id) as id, userid from #__vquiz_quizresult group by userid) as i ON i.userid=u.id left join #__vquiz_quizresult as y on y.id=i.id left join #__vquiz_quizzes as r on r.id=y.quizzesid';
					$orderby = ' order by i.id desc'; 
				}

				//most Played  
				elseif($ordering==2)	{
					//$query .= ' left join ( select userid, count(id) as counts from #__vquiz_quizresult group by userid ) as i2 on i2.userid=u.id';
					$orderby = ' order by counts desc';
				}
			    //	highest score	
				elseif($ordering==3)	{
					$orderby = ' order by userscore desc';
				}
				else
				$orderby = 'u.name asc';
				$query .= ' where ' . implode(' and ', $where);
				$query .= ' group by u.id';
				$query .= ''.$orderby.'';
			    $query .= ' limit '.$limit;

				$db->setQuery( $query );				

				$items = $db->loadObjectList();

 					return $items;
		

				/*$query = 'select i.*, r.id as quizesid,r.quizzes_title as quiz_title ,u.name as username, avg(i.score*100/i.maxscore) as userscore';
				
				if($ordering==2)
					$query .= ', i2.counts as counts';
				
				$query .= ' from #__users as u left join #__vquiz_quizresult as i on u.id=i.userid left join #__vquiz_quizzes as r on r.id=i.quizzesid ';
				
				$where = array();
				
				if($autodetect==1){
					
				     $checkview = JRequest::getVar('view', '');
				     $layout = JRequest::getCmd('layout', '');
				   
					 if($checkview=='quizmanager' and $layout==''){
					     $detect_quizid = JRequest::getInt('id', 0);
				         $where[] = 'r.id ='.$db->quote($detect_quizid);
					 }
				  else if($selected_quizid)
					{
						 $where[] = 'r.id ='.$db->quote($selected_quizid);
					}
					 
				}
				else if($selected_quizid)
				{
					 $where[] = 'r.id ='.$db->quote($selected_quizid);
				}
				
				$where[] = 'r.access in ('.implode(',', $user->getAuthorisedViewLevels()).')';
				
				$where[] = ' r.startpublish_date <= '.$db->quote($date->format('Y-m-d')).' and r.endpublish_date >= '.$db->quote($date->format('Y-m-d'));
				
				if($app->getLanguageFilter())	{
					$where[] = 'r.language in ('.$db->quote($lang->getTag()).', '.$db->Quote('*').')';
				}
				
				//recent Played  
				if($ordering==1)	{
					$orderby = ' order by i.id desc';
				}
				//most Played  
				elseif($ordering==2)	{
					
					$query .= ' left join ( select userid, count(id) as counts from #__vquiz_quizresult group by userid ) as i2 on i2.userid=u.id';
					
					$orderby = ' order by counts desc';
				}
			    //	highest score	
				elseif($ordering==3)	{

					$orderby = ' order by userscore desc';
				}
				else
					$orderby = 'u.name asc';
				
				
				$query .= ' where ' . implode(' and ', $where);
				
				$query .= ' group by u.id';
				
			    $query .= ' limit '.$limit;


				$db->setQuery( $query );
				$items = $db->loadObjectList();
				//echo $db->getErrorMsg();
				return $items;*/

				

	}



	



	 







}