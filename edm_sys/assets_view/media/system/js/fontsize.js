      jQuery(document).ready(function(){
          fzInitFromCookie();
          jQuery('#fzde').click(function () {
                  fzDecreaseFont();
          });
          jQuery('#fzre').click(function () {
                  fzResetFont();
          });
          jQuery('#fzin').click(function () {
                  fzIncreaseFont();
          });
          
          if(fzGetCookie('fzFontSize')==''){
        	  fzResetFont();
          }
      });
	  
      function fzGetCookie(Identifier) {
        var value = "";
        if (document.cookie) {
          var startIndex = document.cookie.indexOf(Identifier + "=");
          if(startIndex != -1) {
            var endIndex = document.cookie.indexOf(';', startIndex);
            if (endIndex == -1)
              endIndex = document.cookie.length;
            value = document.cookie.substring(startIndex + Identifier.length + 1, endIndex);
          }
        }
        return value;
      }
	  
      function fzSetCookie (Identifier, Value, Expires) {
        if(navigator.cookieEnabled) {
          var now = new Date();
          var expireValue = new Date(now.getTime() + Expires);
          var cookieValue = Identifier + "=" + Value + ";";
          if( (typeof(Expires) == "number") && (Expires > 0) )
            cookieValue += (" expires=" + expireValue.toGMTString() + ";");
          document.cookie = cookieValue;
        }
      }
	  
      var fzCalcFontSize = fzGetCookie('fzFontSize').replace('em','') ;
      if(fzCalcFontSize.length == ""){var fzCalcFontSize = 1.2;}
      var fzCalcLineHeight = fzGetCookie('fzLineHeight').replace('em','');
      if(fzCalcLineHeight.length == ""){var fzCalcLineHeight = 1.6;}
	  
      function fzInitFromCookie() {
        if( document.getElementById('content') != null && fzGetCookie('fzFontSize').length != "" && fzGetCookie('fzLineHeight').length != "" ) {
          document.getElementById('content').style.fontSize = fzGetCookie('fzFontSize');
          document.getElementById('content').style.lineHeight = fzGetCookie('fzLineHeight');
        }
      }
	  
      function fzIncreaseFont() {
        fzCalcFontSize = eval( fzCalcFontSize ) + eval( 0.1 ) ;
        tfzCalcLineHeight = eval( fzCalcLineHeight ) + eval(0.1);
        if( document.getElementById('content') == null){return;}
        document.getElementById('content').style.fontSize = fzCalcFontSize + 'em';
        document.getElementById('content').style.lineHeight = fzCalcLineHeight + 'em';
        fzSetCookie ('fzFontSize', fzCalcFontSize + 'em', 0);
        fzSetCookie ('fzLineHeight', fzCalcLineHeight + 'em', 0);
      }
	  
      function fzDecreaseFont() {
        if( ((fzCalcFontSize - 0.1) > 0.1) && ((fzCalcLineHeight - 0.1) > 0.1)) {
          fzCalcFontSize -= 0.1;
          fzCalcLineHeight -= 0.1;
        if( document.getElementById('content') == null){return;}
          document.getElementById('content').style.fontSize = fzCalcFontSize + 'em';
          document.getElementById('content').style.lineHeight = fzCalcLineHeight + 'em';
          fzSetCookie ('fzFontSize', fzCalcFontSize + 'em', 0);
          fzSetCookie ('fzLineHeight', fzCalcLineHeight +'em', 0);
        }
      }
	  
      function fzResetFont() {
        if( document.getElementById('content') == null){return;}
        document.getElementById('content').style.fontSize = 1.0 + 'em';
        document.getElementById('content').style.lineHeight = 1.4 + 'em';
        fzCalcFontSize = 1.0;
        fzCalcLineHeight = 1.4;
        fzSetCookie ('fzFontSize', "", 0);
        fzSetCookie ('fzLineHeight', "", 0);
      }
