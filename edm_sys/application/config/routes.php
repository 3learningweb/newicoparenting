<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index/reg';

$route['reg'] = 'index/reg';

$route['reg_finish'] = 'index/reg_finish';

$route['login'] = 'index/login';

$route['member_content'] = 'index/member_content';

$route['forget'] = 'index/forget';

$route['check'] = 'index/check';

$route['edm_print'] = 'index/edm_print';

$route['edm_list'] = 'index/edm_list';

$route['edm_list2'] = 'index/edm_list2';

$route['edm_detail'] = 'index/edm_detail';

$route['forget_finish'] = 'index/forget_finish';

$route['edm_pdf'] = 'index/edm_pdf';

$route['edm_pdf2'] = 'index/edm_pdf2';

$route['edm_print_c'] = 'index/edm_print_c';

$route['reg_c'] = 'index/reg_c';

$route['login_c'] = 'index/login_c';

$route['forget_c'] = 'index/forget_c';

$route['reg_finish_c'] = 'index/reg_finish_c';

$route['member_content_c'] = 'index/member_content_c';

$route['backend/member_list'] = 'backend/member_list';

$route['backend/edit_member'] = 'backend/edit_member';

$route['backend/newsletter_list'] = 'backend/newsletter_list';

$route['backend/edit_newsletter1'] = 'backend/edit_newsletter1';

$route['backend/edit_newsletter1-2'] = 'backend/edit_newsletter1_2';

$route['backend/edit_newsletter2'] = 'backend/edit_newsletter2';

$route['backend/newsletter_send'] = 'backend/newsletter_send';
$route['backend/resend_check_all'] = 'backend/resend_check_all';

$route['backend/edm_list_send'] = 'backend/edm_list_send';

$route['backend/newsletter_send'] = 'backend/newsletter_send';

$route['backend/edm_list_send_test'] = 'backend/edm_list_send_test';

$route['backend/newsletter_list_send'] = 'backend/newsletter_list_send';

$route['backend/newsletter_send_edm'] = 'backend/newsletter_send_edm';

$route['backend/edm_list_to_img'] = 'backend/edm_list_to_img';

$route['backend/resend_check_all_test'] = 'backend/resend_check_all_test';

$route['test'] = 'dompdf_test/index';

$route['test2'] = 'dompdf_test/pdf';

$route['test3'] = 'dompdf_test/test3';

$route['test4'] = 'dompdf_test/test4';

$route['tree1'] = 'tree/index';

$route['tree2'] = 'tree/tree2';

$route['tree3'] = 'tree/tree3';

$route['tree_img'] = 'tree/tree_img';

$route['tree4'] = 'tree/tree4';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
