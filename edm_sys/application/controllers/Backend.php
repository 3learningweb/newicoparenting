<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'pdfcrowd.php';
require 'Pdf.php';

class Backend extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $home = 'http://23.97.75.244/icoparenting/';
    public function index()
    {
        $this->load->view('welcome_message');
    }

    public function login($page = 'login')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $reqest = $this->input->post();
        if(!empty($reqest["view"]))
        {
            redirect(base_url().'index.php/t_view_class?class='.$reqest["view"], 'refresh');
        }
        $this->load->database('default');

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        //$this->load->view('templates/teacher_header', $data);
        $this->load->view('backend/' . $page, $data);
        //$this->load->view('templates/teacher_footer', $data);
    }

    public function home($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $reqest = $this->input->post();
        if(!empty($reqest["view"]))
        {
            redirect(base_url().'index.php/t_view_class?class='.$reqest["view"], 'refresh');
        }
        $this->load->database('default');

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_admin', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function new_firm($page = 'new_firm')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($reqest["view"]))
        {
            redirect(base_url().'index.php/t_view_class?class='.$reqest["view"], 'refresh');
        }
        $this->load->database('default');
        if(!empty($request["save"]))
        {
            $data_index=array("name"=>"廠商名稱","tel"=>"廠商電話");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {

                    echo '<script language="JavaScript"> alert("'.$value.'未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $insertdata = array();
            $insertdata["name"] =  $request["name"];
            $insertdata["tel"] =  $request["tel"];
            $insertdata["other"] =  $request["other"];
            $this->db->insert('firm', $insertdata);
            $this->db->close();
            redirect(base_url().'index.php/firm_list', 'refresh');
        }elseif(!empty($request["back"]))
        {
            redirect(base_url().'index.php/firm_list', 'refresh');
        }

        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function member_list($page = 'member_list')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($request["edit"]))
        {
            redirect(base_url().'index.php/backend/edit_member?id='.$request["edit"], 'refresh');
        }elseif(!empty($request["del"]))
        {
            $this->db->where('id',$request["del"]);
            $this->db->delete('i_member');
        }elseif(!empty($request["new"]))
        {
            redirect(base_url().'index.php/backend/member_list', 'refresh');
        }elseif(!empty($request["re_send"]))
        {
            redirect(base_url().'index.php/backend/resend_check_all', 'refresh');
        }
        $this->load->database('default');
        $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check');
        $this->db->from('i_member');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        //$this->db->where('org_to_stud.org', $org_id);
        $query = $this->db->get();
        $data["member"] = $query->result_array();
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function edit_member($page = 'edit_member')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["save"]))
        {
            $data_title = array("name","email","age","location","sex","edu");
            $user_data = array();
            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }
            $user_data["mail_check"] = $request["mail_check"];
            $user_data["edm"] = $request["edm"];

            $this->db->where('id', $request["save"]);
            $this->db->update('i_member', $user_data);

            $this->db->where('parent',$session["imember_uid"]);
            $this->db->delete('child');

            $user_data = array();
            $user_data["parent"] = $request["save"];

            for($i=0; $i<$request["birthday_num"]; $i++)
            {
                $user_data["birthday"] = $request["date".$i];
                $this->db->where('id', $request["save"]);
                $this->db->insert('child', $user_data);
            }
            redirect(base_url().'index.php/backend/member_list', 'refresh');
        }elseif(empty($get["id"]))
        {
            redirect(base_url().'index.php/backend/member_list', 'refresh');
        }
        $query = $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check')
            ->where('id', $get["id"])
            ->get('i_member');
        $data["user_data"] = $query->result_array();

        $query = $this->db->select('id, name')
            ->get('location');
        $data["location"] = $query->result_array();

        $query = $this->db->select('id, birthday')
            ->where('parent', $session["imember_uid"])
            ->get('child');
        $data["child"] = $query->result_array();

        $data["choose"] = "2";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function menu_list($page = 'menu_list')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($request["edit"]))
        {
            redirect(base_url().'index.php/backend/edit_menu?id='.$request["edit"], 'refresh');
        }elseif(!empty($request["del"]))
        {
            $this->db->where('id',$request["del"]);
            $this->db->delete('edm_menu');
        }elseif(!empty($request["new"]))
        {
            redirect(base_url().'index.php/backend/edit_menu', 'refresh');
        }elseif(!empty($request["sub"]))
        {
            redirect(base_url().'index.php/backend/menu_sub_list?p='.$request["sub"], 'refresh');
        }
        $this->load->database('default');
        $this->db->select('id, name, m_order, link');
        $this->db->where("parent",0);
        $this->db->order_by("m_order","asc");
        $this->db->from('edm_menu');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        //$this->db->where('org_to_stud.org', $org_id);
        $query = $this->db->get();
        $data["menu"] = $query->result_array();
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function edit_menu($page = 'edit_menu')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["save"]))
        {
            $data_title = array("name","m_order","link");
            $user_data = array();
            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }


            $this->db->where('id', $request["save"]);
            $this->db->update('edm_menu', $user_data);

            redirect(base_url().'index.php/backend/menu_list', 'refresh');
        }elseif(!empty($request["add"]))
        {
            $data_title = array("name","m_order","link");
            $user_data = array();
            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }
            $this->db->insert('edm_menu', $user_data);

            redirect(base_url().'index.php/backend/menu_list', 'refresh');
        }

        if(isset($get["id"]))
        {
            $query = $this->db->select('id, name, m_order, link')
                ->where('id', $get["id"])
                ->get('edm_menu');
            $data["menu"] = $query->result_array();
        }

        $data["choose"] = "2";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function menu_sub_list($page = 'menu_sub_list')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["edit"]))
        {
            redirect(base_url().'index.php/backend/edit_menu_sub?id='.$request["edit"], 'refresh');
        }elseif(!empty($request["del"]))
        {
            $this->db->where('id',$request["del"]);
            $this->db->delete('edm_menu');
        }elseif(!empty($request["new"]))
        {
            redirect(base_url().'index.php/backend/edit_menu_sub?p='.$request["parent"], 'refresh');
        }elseif(!empty($request["back"]))
        {
            redirect(base_url().'index.php/backend/menu_list', 'refresh');
        }
        $this->load->database('default');
        $this->db->select('id, name, m_order, link');
        $this->db->order_by("m_order","asc");
        $this->db->where("parent", $get["p"]);
        $this->db->from('edm_menu');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        //$this->db->where('org_to_stud.org', $org_id);
        $query = $this->db->get();
        $data["menu"] = $query->result_array();
        $data["parent"] = $get["p"];
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function edit_menu_sub($page = 'edit_menu_sub')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["save"]))
        {
            $data_title = array("name","m_order","link");
            $user_data = array();
            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }


            $this->db->where('id', $request["save"]);
            $this->db->update('edm_menu', $user_data);

            redirect(base_url().'index.php/backend/menu_sub_list?p='.$request["parent"], 'refresh');
        }elseif(!empty($request["add"]))
        {
            $data_title = array("name","m_order","link");
            $user_data = array();
            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }
            $user_data["parent"] = $request["parent"];
            $this->db->insert('edm_menu', $user_data);

            redirect(base_url().'index.php/backend/menu_sub_list?p='.$request["parent"], 'refresh');
        }

        if(isset($get["id"]))
        {
            $query = $this->db->select('id, name, m_order, link, parent')
                ->where('id', $get["id"])
                ->get('edm_menu');
            $data["menu"] = $query->result_array();
            $data["parent"] = $data["menu"][0]["parent"];
        }else
        {
            $data["parent"] = $get["p"];
        }

        $data["choose"] = "2";


        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_firm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function newsletter_list($page = 'newsletter_list')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($request["edit1"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1?id='.$request["edit1"], 'refresh');
        }elseif(!empty($request["edit2"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter2?id='.$request["edit2"], 'refresh');
        }elseif(!empty($request["del1"]))
        {
            $this->db->where('id',$request["del1"]);
            $this->db->delete('edm_main');
            $this->db->where('parent',$request["del1"]);
            $this->db->delete('edm_content1');
        }elseif(!empty($request["del2"]))
        {
            $this->db->where('id',$request["del2"]);
            $this->db->delete('edm_main');
            $this->db->where('parent',$request["del2"]);
            $this->db->delete('edm_content2');
        }elseif(!empty($request["new"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1', 'refresh');
        }elseif(!empty($request["new_special"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter2', 'refresh');
        }
        $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        for($i=0; $i< count($data["edm_main"]); $i++)
        {
            if($data["edm_main"][$i]["publish"] == "0000-00-00")
            {
                $data["edm_main"][$i]["publish_state"] = "未發行";
            }else
            {
                $data["edm_main"][$i]["publish_state"] = "已發行";
            }

        }
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function edit_newsletter1($page = 'edit_newsletter1')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["save"]))
        {
            $data_index=array("print"=>"刊別","title"=>"電子報主題","template"=>"版型");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $insertdata = array();
            if(!empty($_FILES["banner"])) {
                if ($_FILES["banner"]["error"] > 0) {
                    echo "Error: " . $_FILES["banner"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["banner"]["tmp_name"],"upload/".$time.$_FILES["banner"]["name"]);
                    $insertdata["banner"] =  $time.$_FILES["banner"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("Banner照片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            if(!empty($_FILES["list_banner"])) {
                if ($_FILES["list_banner"]["error"] > 0) {
                    echo "Error: " . $_FILES["banner"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_banner"]["tmp_name"],"upload/".$time.$_FILES["list_banner"]["name"]);
                    $insertdata["list_banner"] =  $time.$_FILES["list_banner"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("列表頁照片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            $insertdata["type"] =  1;
            $insertdata["print"] =  $request["print"];
            $insertdata["print_year"] =  $request["print_year"];
            $insertdata["print_img"] =  $request["print_img"];
            $insertdata["title"] =  $request["title"];
            $insertdata["publish"] =  "0000-00-00";
            $insertdata["template"] =  $request["template"];
            $insertdata["age_theme"] =  $request["age_theme"];
            $insertdata["pre_age"] =  $request["pre_age"];
            $this->db->insert('edm_main', $insertdata);
            $id = $this->db->insert_id();
            $this->db->close();
            redirect(base_url().'index.php/backend/edit_newsletter1-2?id='.$id.'&theme=1', 'refresh');
        }elseif(!empty($request["back"]))
        {
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }elseif(!empty($request["update"]))
        {
            $data_index=array("print"=>"刊別","title"=>"電子報主題","template"=>"版型");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data = array();
            if(!empty($_FILES["banner"])) {
                if ($_FILES["banner"]["error"] > 0) {
                   // echo "Error: " . $_FILES["banner"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["banner"]["tmp_name"],"upload/".$time.$_FILES["banner"]["name"]);
                    $data["banner"] =  $time.$_FILES["banner"]["name"];
                }

            }
            if(!empty($_FILES["list_banner"])) {
                if ($_FILES["list_banner"]["error"] > 0) {
                    echo "Error: " . $_FILES["banner"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_banner"]["tmp_name"],"upload/".$time.$_FILES["list_banner"]["name"]);
                    $data["list_banner"] =  $time.$_FILES["list_banner"]["name"];
                }

            }
            $data["print"] =  $request["print"];
            $data["title"] =  $request["title"];
            //$data["publish"] =  $request["publish"];
            $data["template"] =  $request["template"];
            $data["age_theme"] =  $request["age_theme"];
            $data["pre_age"] =  $request["pre_age"];
            $data["print_year"] =  $request["print_year"];
            $data["print_img"] =  $request["print_img"];
            $this->db->where('id',$request["update"]);
            $this->db->update('edm_main', $data);
            $this->db->close();
            redirect(base_url().'index.php/backend/edit_newsletter1-2?id='.$request["update"].'&theme=1', 'refresh');
        }
        $this->load->database('default');
        if(!empty($get["id"]))
        {
            $this->db->select('id, type, print, title, publish, banner, list_banner, template, age_theme, pre_age, create_date, modify_date, print_year, print_img');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();
        }
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm', $data);
        $this->load->view('backend/' . $page, $data);
       // $this->load->view('templates/backend_footer', $data);
    }

    public function edit_newsletter1_2($page = 'edit_newsletter1-2')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();

        if(!empty($request["save"]))
        {
            $data_index=array("list_title"=>"目錄頁標題","list_intro"=>"目錄頁簡介","title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $insertdata = array();
            if(!empty($_FILES["list_img"])) {
                if ($_FILES["list_img"]["error"] > 0) {
                    echo "Error: " . $_FILES["list_img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_img"]["tmp_name"],"upload/".$time.$_FILES["list_img"]["name"]);
                    $insertdata["list_img"] =  $time.$_FILES["list_img"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("目錄頁圖片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $insertdata["img"] =  $time.$_FILES["img"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("文章圖片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            if(!empty($_FILES["img2"])) {
                if ($_FILES["img2"]["error"] > 0) {
                    echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img2"]["tmp_name"],"upload/".$time.$_FILES["img2"]["name"]);
                    $insertdata["img2"] =  $time.$_FILES["img2"]["name"];
                }

            }
            if(!empty($_FILES["img3"])) {
                if ($_FILES["img3"]["error"] > 0) {
                    echo "Error: " . $_FILES["img3"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img3"]["tmp_name"],"upload/".$time.$_FILES["img3"]["name"]);
                    $insertdata["img3"] =  $time.$_FILES["img3"]["name"];
                }

            }
            $insertdata["theme"] =  $request["save"];
            $insertdata["parent"] =  $request["id"];
            $insertdata["list_title"] =  $request["list_title"];
            $insertdata["list_intro"] =  $request["list_intro"];
            $insertdata["title"] =  $request["title"];
            $insertdata["title2"] =  $request["title2"];
            $insertdata["title3"] =  $request["title3"];
            $insertdata["type"] =  $request["type"];
            $insertdata["content"] =  $request["edm_content"];
            $insertdata["content2"] =  $request["edm_content2"];
            $insertdata["suggestion"] =  $request["edm_suggestion"];
            $this->db->insert('edm_content1', $insertdata);
            $id = $this->db->insert_id();
            $this->db->close();
            redirect(base_url().'index.php/backend/edit_newsletter1-2?id='.$request["id"].'&theme='.$request["save"], 'refresh');
        }elseif(!empty($request["back"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1?id='.$request["id"], 'refresh');
        }elseif(!empty($request["update"]))
        {
            $data_index=array("list_title"=>"目錄頁標題","list_intro"=>"目錄頁簡介","title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data = array();
            if(!empty($_FILES["list_img"])) {
                if ($_FILES["list_img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["list_img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_img"]["tmp_name"],"upload/".$time.$_FILES["list_img"]["name"]);
                    $data["list_img"] =  $time.$_FILES["list_img"]["name"];
                }

            }
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $data["img"] =  $time.$_FILES["img"]["name"];
                }

            }
            if(!empty($_FILES["img2"])) {
                if ($_FILES["img2"]["error"] > 0) {
                    echo "Error2: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img2"]["tmp_name"],"upload/".$time.$_FILES["img2"]["name"]);
                    $data["img2"] =  $time.$_FILES["img2"]["name"];
                }

            }
            if(!empty($_FILES["img3"])) {
                if ($_FILES["img3"]["error"] > 0) {
                    echo "Error3: " . $_FILES["img3"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img3"]["tmp_name"],"upload/".$time.$_FILES["img3"]["name"]);
                    $data["img3"] =  $time.$_FILES["img3"]["name"];
                }

            }
            $data["theme"] =  $request["update"];
            $data["parent"] =  $request["id"];
            $data["list_title"] =  $request["list_title"];
            $data["list_intro"] =  $request["list_intro"];
            $data["title"] =  $request["title"];
            $data["title2"] =  $request["title2"];
            $data["title3"] =  $request["title3"];
            $data["type"] =  $request["type"];
            $data["content"] =  $request["edm_content"];
            $data["content2"] =  $request["edm_content2"];
            $data["suggestion"] =  $request["edm_suggestion"];
            $this->db->where('parent', $request["id"]);
            $this->db->where('theme', $request["update"]);
            $this->db->update('edm_content1', $data);
            $this->db->close();
            redirect(base_url().'index.php/backend/edit_newsletter1-2?id='.$request["id"].'&theme='.$request["update"], 'refresh');
        }elseif(!empty($request["save_back"]))
        {
            $data_index=array("list_title"=>"目錄頁標題","list_intro"=>"目錄頁簡介","title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $insertdata = array();
            if(!empty($_FILES["list_img"])) {
                if ($_FILES["list_img"]["error"] > 0) {
                    echo "Error: " . $_FILES["list_img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_img"]["tmp_name"],"upload/".$time.$_FILES["list_img"]["name"]);
                    $insertdata["list_img"] =  $time.$_FILES["list_img"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("目錄頁圖片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $insertdata["img"] =  $time.$_FILES["img"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("文章圖片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            $insertdata["theme"] =  $request["save_back"];
            $insertdata["parent"] =  $request["id"];
            $insertdata["list_title"] =  $request["list_title"];
            $insertdata["list_intro"] =  $request["list_intro"];
            $insertdata["title"] =  $request["title"];
            $insertdata["title2"] =  $request["title2"];
            $insertdata["title3"] =  $request["title3"];
            $insertdata["type"] =  $request["type"];
            $insertdata["content"] =  $request["edm_content"];
            $insertdata["suggestion"] =  $request["edm_suggestion"];
            $this->db->insert('edm_content1', $insertdata);
            $id = $this->db->insert_id();
            $this->db->close();
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }elseif(!empty($request["update_back"]))
        {
            $data_index=array("list_title"=>"目錄頁標題","list_intro"=>"目錄頁簡介","title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data = array();
            if(!empty($_FILES["list_img"])) {
                if ($_FILES["list_img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["list_img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["list_img"]["tmp_name"],"upload/".$time.$_FILES["list_img"]["name"]);
                    $data["list_img"] =  $time.$_FILES["list_img"]["name"];
                }

            }
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $data["img"] =  $time.$_FILES["img"]["name"];
                }

            }
            $data["theme"] =  $request["update_back"];
            $data["parent"] =  $request["id"];
            $data["list_title"] =  $request["list_title"];
            $data["list_intro"] =  $request["list_intro"];
            $data["title"] =  $request["title"];
            $data["title2"] =  $request["title2"];
            $data["title3"] =  $request["title3"];
            $data["type"] =  $request["type"];
            $data["content"] =  $request["edm_content"];
            $data["suggestion"] =  $request["edm_suggestion"];
            $this->db->where('parent', $request["id"]);
            $this->db->where('theme', $request["update_back"]);
            $this->db->update('edm_content1', $data);
            $this->db->close();
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }elseif(!empty($request["change"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1-2?id='.$request["id"].'&theme='.$request["change"], 'refresh');
        }elseif(!empty($request["all_to_pdf"]))
        {
            $this->all_to_pdf();
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }else if(!empty($request["view_page"]))
        {
            //redirect(base_url().'index.php/edm_list?id='.$request["id"].'&theme='.$request["view_page"], 'refresh');
            if($request["view_page"]>4)
            {
                $request["view_page"] = 1;
            }
            $url = base_url().'index.php/edm_list?id='.$request["id"].'&theme='.$request["view_page"];
            echo '<script language="JavaScript">  window.open(\''.$url.'\',\'_blank\');history.go(-1);</script>';
            //echo '<script language="JavaScript">  window.location="'.$url.'"; target="_blank";</script>';
        }else if(!empty($request["view_pdf"]))
        {
            if($request["view_pdf"]>4)
            {
                $request["view_pdf"] = 1;
            }
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["id"].'&theme='.$request["view_pdf"]);
            echo '<script language="JavaScript">  window.open(\'http://api.html2pdfrocket.com/pdf?value='.$url.'&apikey=55e32c22-1e15-4964-beeb-143443cde735\',\'_blank\');history.go(-1);</script>';
           // echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
        }



        $this->db->select('id, age_theme, pre_age');
        $this->db->where('id',$get["id"]);
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        if(!empty($data["edm_main"][0]))
        {
            $theme_arr = array(1=>"好家庭",2=>"好教養",3=>"好夫妻",4=>"好消息");
            $theme_arr_temp = array();
            array_push($theme_arr_temp,$theme_arr[$data["edm_main"][0]["age_theme"]]);
            for($i=1;$i<=count($theme_arr); $i++)
            {
                if($i!= $data["edm_main"][0]["age_theme"])
                {
                    array_push($theme_arr_temp,$theme_arr[$i]);
                }
            }
            $data["theme_arr"] = $theme_arr_temp;

        }else
        {
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }
        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, list_title, list_intro, list_img, title, title2, title3, type, img, content, content2, img2, img3, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->where('theme',$get["theme"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $data["content"] = $query->result_array();
            $data["theme"] = $get["theme"];
            $data["id"] = $get["id"];
        }else
        {
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }
        if($get["theme"]>4)
        {
            $theme = 1;
        }else
        {
            $theme = $get["theme"];
        }
        $url = urlencode(base_url().'index.php/edm_list2?id='.$get["id"].'&theme='.$theme);
        $data["url"] = $url;
        $data["url_pdf"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
        $data["url_view"] = base_url().'index.php/edm_list?id='.$get["id"].'&theme='.$theme;
        $data["choose"] = "1";
        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm', $data);
        $this->load->view('backend/' . $page, $data);
        //$this->load->view('templates/backend_footer', $data);
    }

    private function all_to_pdf()
    {
        $this->load->helper('url');
        $this->load->library('session');
        $data['base_url'] = base_url();
        $request = $this->input->post();
        $html = $this->output->get_output();
        for($i=1; $i<=4;$i++)
        {
            try
            {
                // create an API client instance
                $client = new Pdfcrowd("kennyshen", "682a2caffbeb01bbda124aff583e3233");

                $out_file = fopen(dirname(dirname(dirname(__FILE__))).'/assets_view/images/'.$request["id"]."_".$i.".pdf", 'w');
                $pdf = $client->convertURI(base_url().'index.php/edm_list?id='.$request["id"].'&theme='.$i, $out_file);

                // set HTTP response headers
                header("Content-Type: application/pdf");
                header("Cache-Control: max-age=0");
                header("Accept-Ranges: none");
                header("Content-Disposition: attachment; filename=\"edm.pdf\"");
                // send the generated PDF
            }
            catch(PdfcrowdException $why)
            {
                echo "Pdfcrowd Error: " . $why;
            }
        }

    }

    public function edit_newsletter2($page = 'edit_newsletter2')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["save"]))
        {
            $data_index=array("print"=>"刊別","publish"=>"發行日期","template"=>"版型");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data_index=array("title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $insertdata = array();
            $insertdata["type"] =  2;
            $insertdata["print"] =  $request["print"];
            $insertdata["publish"] =  $request["publish"];
            $insertdata["template"] =  $request["template"];
            $this->db->insert('edm_main', $insertdata);
            $id = $this->db->insert_id();

            $insertdata = array();
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $insertdata["img"] =  $time.$_FILES["img"]["name"];
                }

            }else
            {
                echo '<script language="JavaScript"> alert("文章圖片未上傳");history.go(-1); //回上一頁</script>';
                return;
            }
            $insertdata["parent"] =  $id;
            $insertdata["title"] =  $request["title"];
            $insertdata["type"] =  $request["type"];
            $insertdata["content"] =  $request["edm_content"];
            $insertdata["suggestion"] =  $request["edm_suggestion"];
            $this->db->insert('edm_content2', $insertdata);
            $this->db->close();
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }elseif(!empty($request["back"]))
        {
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }elseif(!empty($request["update"]))
        {
            $data_index=array("print"=>"刊別","publish"=>"發行日期","template"=>"版型");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data_index=array("title"=>"文章標題","type"=>"版型","edm_content"=>"文章內容","edm_suggestion"=>"文章建議");
            foreach($data_index as $key=>$value)
            {
                if(empty($request[$key]))
                {
                    echo '<script language="JavaScript"> alert("'.$value.'未填寫");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data = array();
            $data["print"] =  $request["print"];
            $data["publish"] =  $request["publish"];
            $data["template"] =  $request["template"];

            $this->db->where('id',$request["update"]);
            $this->db->update('edm_main', $data);
            $data = array();
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $data["img"] =  $time.$_FILES["img"]["name"];
                }

            }
            $data["parent"] =  $request["update"];
            $data["title"] =  $request["title"];
            $data["type"] =  $request["type"];
            $data["content"] =  $request["edm_content"];
            $data["suggestion"] =  $request["edm_suggestion"];
            $this->db->where('parent', $request["update"]);
            $this->db->update('edm_content2', $data);

            $this->db->close();
            redirect(base_url().'index.php/backend/newsletter_list', 'refresh');
        }
        $this->load->database('default');
        if(!empty($get["id"]))
        {
            $this->db->select('id, type, print, publish, template, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();
            $this->db->select('id, title, type, img, content, suggestion');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content2');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $data["content"] = $query->result_array();
        }
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm', $data);
        $this->load->view('backend/' . $page, $data);
        // $this->load->view('templates/backend_footer', $data);
    }

    private $send_num = 20;
    public function resend_check_all($page = 'edit_newsletter2')
    {
        ignore_user_abort();
        set_time_limit(0); // run script forever 程式執行時間不做限制.
        $get = $this->input->get();
        $this->load->database('default');
        $this->load->helper('url');
        $this->load->library('session');
        $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check');
        $this->db->from('i_member');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        $this->db->where('mail_check', 0);
        $query = $this->db->get();
        $member = $query->result_array();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "140.111.34.125";
        $config['smtp_port'] = "25";
        //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
        //$config['smtp_pass'] = "nss123456";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";


        if(!empty($get["num"]))
        {
            for($i=$get["num"]; $i<count($member) ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($member[$i]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[$i]["name"].'在iCoparenting註冊完成通知');
                $message_content = $member[$i]["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$member[$i]["id"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($get["num"]+$this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(600);
                    redirect(base_url().'index.php/backend/resend_check_all?num='.($i+1), 'refresh');
                }elseif($i ==(count($member)-1))
                {
                    $this->db->close(); //database close
                    redirect(base_url().'index.php/backend/resend_check_all', 'refresh');
                }
                sleep(2);
            }
        }else
        {
            for($i=0; $i<count($member) ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($member[$i]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[$i]["name"].'在iCoparenting註冊完成通知');
                $message_content = $member[$i]["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$member[$i]["id"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(600);
                    redirect(base_url().'index.php/backend/resend_check_all?num='.($i+1), 'refresh');
                }elseif($i ==(count($member)-1))
                {
                    $this->db->close(); //database close
                    redirect(base_url().'index.php/backend/resend_check_all', 'refresh');
                }
                sleep(2);
            }
        }

    }

    public function newsletter_send($page = 'newsletter_send')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($request["edit1"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1?id='.$request["edit1"], 'refresh');
        }elseif(!empty($request["edit2"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter2?id='.$request["edit2"], 'refresh');
        }elseif(!empty($request["del1"]))
        {
            $this->db->where('id',$request["del1"]);
            $this->db->delete('edm_main');
            $this->db->where('parent',$request["del1"]);
            $this->db->delete('edm_content1');
        }elseif(!empty($request["del2"]))
        {
            $this->db->where('id',$request["del2"]);
            $this->db->delete('edm_main');
            $this->db->where('parent',$request["del2"]);
            $this->db->delete('edm_content2');
        }elseif(!empty($request["new"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter1', 'refresh');
        }elseif(!empty($request["new_special"]))
        {
            redirect(base_url().'index.php/backend/edit_newsletter2', 'refresh');
        }
        $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        for($i=0; $i< count($data["edm_main"]); $i++)
        {
            if($data["edm_main"][$i]["publish"] == "0000-00-00")
            {
                echo $data["edm_main"][$i]["publish"];
                $data["edm_main"][$i]["publish_state"] = "未發行";
            }else
            {
                $data["edm_main"][$i]["publish_state"] = "已發行";
            }

        }
        $data["choose"] = "1";

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function edm_list_send($page = 'edm_list_send')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        //$this->load->view('pages/' . $page, $data);
        //$this->load->view('templates/footer', $data);
        $html=$this->load->view('pages/' . $page, $data, true);

        $this->load->database('default');
        $this->load->helper('url');
        $this->load->library('session');
        $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check');
        $this->db->from('i_member');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        $this->db->where('mail_check', 1);
        $query = $this->db->get();
        $member = $query->result_array();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "140.111.34.125";
        $config['smtp_port'] = "25";
        //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
        //$config['smtp_pass'] = "nss123456";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        foreach($member as $item)
        {
            $ci->email->initialize($config);
            $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
            //$list = array($item["email"]);
            $list = array("yanlin0718@gmail.com","chinfeng97@gmail.com");
            $ci->email->to($list);
            $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
            $ci->email->subject("和樂共親職".$data["edm_main"][0]["print"]."電子報_".$data["edm_main"][0]["title"]);
            $message_content = $html;
            //echo $message_content;
            //die();
            $ci->email->message($message_content);
            $ci->email->send();
            sleep(2);
        }
        $this->db->close(); //database close


    }

    public function edm_list_send_test($page = 'edm_list_send')
    {

        //$this -> load -> library('jobs');
        //$this -> jobs -> create('high', 'Backend', 'send_test_mail', array('10'), 'checkUsers','1');
        ignore_user_abort();
        set_time_limit(0); // run script forever 程式執行時間不做限制.
        $this->send_test_mail();

    }

    function send_test_mail()
    {
        $page = 'edm_list_send';
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }
        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        //$this->load->view('pages/' . $page, $data);
        //$this->load->view('templates/footer', $data);
        $html=$this->load->view('pages/' . $page, $data, true);

        $this->load->database('default');
        $this->load->helper('url');
        $this->load->library('session');
        $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check');
        $this->db->from('i_member');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        $this->db->where('mail_check', 1);
        $query = $this->db->get();
        $member = $query->result_array();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "140.111.34.125";
        $config['smtp_port'] = "25";
        //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
        //$config['smtp_pass'] = "nss123456";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        foreach($member as $item)
        {

            for($i=0; $i<1;$i++)
            {
                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                //$list = array($item["email"]);
                $list = array("yanlin0718@gmail.com");
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("和樂共親職電子報_");
                $message_content = $html;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                sleep(2);
            }
            for($i=0; $i<1;$i++)
            {
                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                //$list = array($item["email"]);
                $list = array("handyhsieh@iscom.com.tw");
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("和樂共親職電子報_");
                $message_content = $html;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                sleep(2);
            }

            for($i=0; $i<1;$i++)
            {
                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                //$list = array($item["email"]);
                $list = array("chinfeng97@gmail.com");
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("和樂共親職電子報_");
                $message_content = $html;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                sleep(2);
            }





            /*
            for($x=0; $x<20;$x++) {
                for ($i = 0; $i < 5; $i++) {
                    $ci->email->initialize($config);
                    $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                    //$list = array($item["email"]);
                    $list = array("kennyshen@hlmcoltd".$x.".com");
                    $ci->email->to($list);
                    $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                    $ci->email->subject("和樂共親職" . $data["edm_main"][0]["print"] . "電子報_" . $data["edm_main"][0]["title"]);
                    $message_content = $html;
                    //echo $message_content;
                    //die();
                    $ci->email->message($message_content);
                    $ci->email->send();
                    sleep(2);
                }
            }
            */

        }
        $this->db->close(); //database close
    }

    function fee_convert($fee)
    {
        $start = 0;
        $e500 = 0;
        $e1000 = 0;
        $start = strpos($fee,"+",0);
        while($start)
        {
            $e500++;
            $start = strpos($fee,"+",$start);
        }
        $start = strpos($fee,"*",0);
        while($start)
        {
            $e1000++;
            $start = strpos($fee,"*",$start);
        }
        if($start = strpos($fee,"+",0))
        {
            $real_fee = substr($fee,0,$start);
            $real_fee = (int)$real_fee + 500*$e500 + 1000*$e1000;
        }elseif($start = strpos($fee,"*",0))
        {
            $real_fee = substr($fee,0,$start);
            $real_fee = (int)$real_fee + 500*$e500 + 1000*$e1000;
        }else
        {
            $real_fee = $fee;
        }

        return $real_fee;
    }

    public function resend_check_all_test($page = 'edit_newsletter2')
    {
        ignore_user_abort();
        set_time_limit(0); // run script forever 程式執行時間不做限制.
        $get = $this->input->get();
        $this->load->database('default');
        $this->load->helper('url');
        $this->load->library('session');
        $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check');
        $this->db->from('i_member');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        $this->db->where('mail_check', 0);
        $query = $this->db->get();
        $member = $query->result_array();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "140.111.34.125";
        $config['smtp_port'] = "25";
        //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
        //$config['smtp_pass'] = "nss123456";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $test_number = 120;
        if(!empty($get["num"]))
        {
            for($i=$get["num"]; $i<$test_number ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array("kennyshen@hlmcoltd.com");
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[$i]["name"].'在iCoparenting註冊完成通知');
                $message_content = $member[$i]["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$member[$i]["id"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($get["num"]+$this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(300);
                    redirect(base_url().'index.php/backend/resend_check_all?num='.($i+1), 'refresh');
                }elseif($i ==($test_number-1))
                {
                    $this->db->close(); //database close
                    redirect(base_url().'index.php/backend/resend_check_all', 'refresh');
                }
                sleep(2);
            }
        }else
        {
            for($i=0; $i<$test_number ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array("kennyshen@hlmcoltd.com");
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[$i]["name"].'在iCoparenting註冊完成通知');
                $message_content = $member[$i]["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$member[$i]["id"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(300);
                    redirect(base_url().'index.php/backend/resend_check_all?num='.($i+1), 'refresh');
                }elseif($i ==($test_number-1))
                {
                    $this->db->close(); //database close
                    redirect(base_url().'index.php/backend/resend_check_all', 'refresh');
                }
                sleep(2);
            }
        }

    }

    public function newsletter_list_send($page = 'newsletter_list_send')
    {
        if (!file_exists(APPPATH . 'views/backend/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        /*
        if (isset($session["login"]) && $session["login"] == 2) {
            $data["login"] = $session["login"];
        } else {
            redirect(base_url() . 'index.php', 'refresh');
        }
        */
        $this->load->database('default');
        $request = $this->input->post();
        if(!empty($request["send1"]))
        {
            redirect(base_url().'index.php/backend/edm_list_to_img?id='.$request["send1"].'&theme=1', 'refresh');
        }elseif(!empty($request["send2"]))
        {
            redirect(base_url().'index.php/backend/edm_list_to_img?id='.$request["send2"], 'refresh');
        }
        $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        $data["choose"] = "1";
        for($i=0; $i< count($data["edm_main"]); $i++)
        {
            if($data["edm_main"][$i]["publish"] == "0000-00-00")
            {
                $data["edm_main"][$i]["publish_state"] = "未發行";
            }else
            {
                $data["edm_main"][$i]["publish_state"] = "已發行";
            }

        }

        $this->db->close(); //database close
        $data['base_url'] = base_url();
        $this->load->view('templates/backend_header', $data);
        $this->load->view('templates/backend_left_edm_send', $data);
        $this->load->view('backend/' . $page, $data);
        $this->load->view('templates/backend_footer', $data);
    }

    public function newsletter_send_edm($page = 'edm_list_send')
    {

        //$this -> load -> library('jobs');
        //$this -> jobs -> create('high', 'Backend', 'send_test_mail', array('10'), 'checkUsers','1');
        ignore_user_abort();
        set_time_limit(0); // run script forever 程式執行時間不做限制.
        $this->send_mail_edm();

    }

    function send_mail_edm()
    {
        $page = 'edm_list_send';
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }
        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }
        $data["time"] = "";
        if(!empty($get["time"]))
        {
            $data["time"] = $get["time"];
        }
        if(!empty($get["theme"]))
        {
            $data["theme"] = $get["theme"];
        }
        if(!empty($get["id"]))
        {
            $data["id"] = $get["id"];
        }

    /*
        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        */


        $this->load->database('default');
        $this->load->helper('url');
        $this->load->library('session');
        $this->db->select('i_member.id, i_member.acc, i_member.name, i_member.email, i_member.age, i_member.location, i_member.sex, i_member.edu,
         i_member.edm, i_member.mail_check, child.birthday');
        $this->db->from('i_member');
        $this->db->join('child', 'child.parent = i_member.id');
        $this->db->where('mail_check', 1);
        $query = $this->db->get();
        $member = $query->result_array();
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "140.111.34.125";
        $config['smtp_port'] = "25";
        //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
        //$config['smtp_pass'] = "nss123456";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->where('id',$get["id"]);
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        if($get["theme"] == 1)
        {
            $old_word = "(0-2)";
        }else if($get["theme"] == 2)
        {
            $old_word = "(3-4)";
        }else if($get["theme"] == 3)
        {
            $old_word = "(5-6)";
        }else if($get["theme"] == 4)
        {
            $old_word = "(7以上)";
        }
        $edm_title = $data["edm_main"][0]["print"].$old_word."_".$data["edm_main"][0]["title"];

        $del_id = array();
        for($i=0;$i<count($member); $i++)
        {
            date_default_timezone_set('Asia/Taipei');
            $today = explode("/",date(("Y/m/d"))) ;
            $birthday = explode("-",$member[$i]["birthday"]) ;
            //echo 'i='.$i.' '.$today[0].' - '.$birthday[0].' = '.($today[0]-$birthday[0]).'<br>';
            if(($today[0]-$birthday[0])<=2)
            {
                $theme = 1;
            }else if(($today[0]-$birthday[0])<=3)
            {
                $theme = 2;
            }else if(($today[0]-$birthday[0])<=5)
            {
                $theme = 3;
            }else{
                $theme = 4;
            }
            if($theme != $get["theme"])
            {
                $del_id[] = $i;
            }
        }
        foreach($del_id as $item)
        {
            unset($member[$item]);
        }
        $member_temp = array();
        $index = 0;
        foreach($member as $item)
        {
            $member_temp[$index] = $item;
            $index++;
        }
        $member = $member_temp;
        array_values($member);
        //var_dump($member);
        $data["home"] = $this->home;
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        //$this->load->view('pages/' . $page, $data);
        //$this->load->view('templates/footer', $data);
        $html=$this->load->view('pages/' . $page, $data, true);
        //die();
        if(!empty($get["num"]))
        {
            for($i=$get["num"]; $i<count($member) ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                //$list = array($item["email"]);
                $list = array($member[$i]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject($edm_title);
                $message_content = $html;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($get["num"]+$this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(600);
                    redirect(base_url().'index.php/backend/newsletter_send_edm?time='.$data["time"].'&num='.($i+1).'&theme='.$request["send"], 'refresh');
                }elseif($i ==(count($member)-1))
                {
                    $this->db->close(); //database close
                    redirect(base_url().'index.php/backend/newsletter_list_send', 'refresh');
                }
                sleep(2);
            }
        }else
        {
            for($i=0; $i<count($member) ; $i++)
            {

                $ci->email->initialize($config);
                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                //$list = array($item["email"]);
                $list = array($member[$i]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject($edm_title);
                $message_content = $html;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                if($i == ($this->send_num-1))
                {
                    $this->db->close(); //database close
                    sleep(600);
                    redirect(base_url().'index.php/backend/newsletter_send_edm?time='.$data["time"].'&num='.($i+1).'&theme='.$request["send"], 'refresh');
                }elseif($i ==(count($member)-1))
                {
                    $this->db->close(); //database close
                    //redirect(base_url().'index.php/backend/newsletter_list_send', 'refresh');
                }
                sleep(2);
            }
        }

        $this->db->close(); //database close
    }

    function edm_list_to_img($page = 'edm_list_to_img')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["send"]))
        {
            $user_data = array();
            $user_data["publish"] = date("Y-m-d");
            $this->db->where('id', $request["id"]);
            $this->db->update('edm_main', $user_data);
            if(empty($request['img_val']))
            {
                echo '<script language="JavaScript"> alert("圖片轉存失敗");history.go(-1); //回上一頁</script>';
                return;
            }
            $time = (int)microtime(true);
            $filteredData=substr($request['img_val'], strpos($request['img_val'], ",")+1);
            $unencodedData=base64_decode($filteredData);
            file_put_contents(dirname(dirname(dirname(__FILE__))).'/assets_view/images/'.$time.'img.png', $unencodedData);

            redirect(base_url().'index.php/backend/newsletter_send_edm?time='.$time.'&theme='.$request["send"].'&id='.$request["id"], 'refresh');
        }elseif(!empty($request["old"]))
        {
            redirect(base_url().'index.php/backend/edm_list_to_img?id='.$request["id"].'&theme='.$request["old"], 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date, print_year, print_img');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
            $data["id"] = $get["id"];
        }

        //計算預計人數
        $this->db->select('i_member.id, i_member.acc, i_member.name, i_member.email, i_member.age, i_member.location, i_member.sex, i_member.edu,
         i_member.edm, i_member.mail_check, child.birthday');
        $this->db->from('i_member');
        $this->db->join('child', 'child.parent = i_member.id');
        $this->db->where('mail_check', 1);
        $query = $this->db->get();
        $member = $query->result_array();
        $member_temp = array();
        for ($i=1; $i<=4; $i++)
        {
            $member_temp[$i] = array();
        }

        $del_id = array();
        for($i=0;$i<count($member); $i++)
        {
            date_default_timezone_set('Asia/Taipei');
            $today = explode("/",date(("Y/m/d"))) ;
            $birthday = explode("-",$member[$i]["birthday"]) ;
            //echo 'i='.$i.' '.$today[0].' - '.$birthday[0].' = '.($today[0]-$birthday[0]).'<br>';
            if(($today[0]-$birthday[0])<=2)
            {
                $theme = 1;
                $member_temp[1][] = $member[$i];
            }else if(($today[0]-$birthday[0])<=3)
            {
                $theme = 2;
                $member_temp[2][] = $member[$i];
            }else if(($today[0]-$birthday[0])<=5)
            {
                $theme = 3;
                $member_temp[3][] = $member[$i];
            }else{
                $theme = 4;
                $member_temp[4][] = $member[$i];
            }

        }

        $data["mamber_count"] = $member_temp;
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }


}