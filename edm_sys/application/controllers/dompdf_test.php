<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require 'pdfcrowd.php';
require 'Pdf.php';

class Dompdf_test extends CI_Controller {

	/**
	 * Example: DOMPDF 
	 *
	 * Documentation: 
	 * http://code.google.com/p/dompdf/wiki/Usage
	 *
	 */
    private $home = 'http://23.97.75.244/icoparenting/';
	public function index($page = 'edm_pdf') {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        //$this->load->view('pages/' . $page, $data);
        //$this->load->view('templates/footer', $data);
        $this->load->view('pages/edm_pdf2', $data);
		// Load all views as normal
		//$this->load->view('welcome_message');
		// Get output html

		$html = $this->output->get_output();
		/*
        try
        {
            // create an API client instance
            $client = new Pdfcrowd("kennyshen", "682a2caffbeb01bbda124aff583e3233");

            $out_file = fopen(dirname(__FILE__).'\pdf\a'.round(microtime(true) * 1000).".pdf", 'w');
            $pdf = $client->convertURI('http://23.97.75.244/icoparenting/edm_sys/index.php/edm_list?id=1&theme=1', $out_file);
            //$pdf = $client->convertURI('http://www.google.com', $out_file);

            // convert a web page and store the generated PDF into a $pdf variable
            //$pdf = $client->convertURI('http://180.176.96.58/demomode/ifamily/index.php/edm_list?id=1&theme=1', dirname(__FILE__).'\pdf\a.pdf');

            // set HTTP response headers
            header("Content-Type: application/pdf");
            header("Cache-Control: max-age=0");
            header("Accept-Ranges: none");
            header("Content-Disposition: attachment; filename=\"google_com.pdf\"");

            echo $pdf;
            // send the generated PDF
        }
        catch(PdfcrowdException $why)
        {
            echo "Pdfcrowd Error: " . $why;
        }
		*/

		// Load library
        /*
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf");*/

        $this->load->library('wkhtmltopdf');
        $array['path'] = '/mnt/website/edu001c/edm_sys/upload';
//Thats the absolute path to the temp folder.
        $wkhtmltopdf = new Wkhtmltopdf;
        $wkhtmltopdf->make($array);
        $wkhtmltopdf->setTitle("New Title");
        $wkhtmltopdf->setHtml("http://www.google.com");// $html can be a url or html content.
        $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "test_file.pdf");

        $this->load->library('wkhtmltopdf');
        $array['path'] ='/mnt/website/edu001c/edm_sys/upload';
//Thats the absolute path to the temp folder.
        $wkhtmltopdf = new Wkhtmltopdf;
        $wkhtmltopdf->make($array);
        $wkhtmltopdf->setTitle("New Title");
        $wkhtmltopdf->setHtml($html);// $html can be a url or html content.
        $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "test_file2.pdf");



	}

    public function pdf($page = 'edm_pdf') {
        $pdf = new Spatie\PdfToImage\Pdf(dirname(__FILE__).'\pdf\a1478423212973.pdf');
        $pdf->saveImage(dirname(__FILE__).'\pdf\a1478423212973.jpg');
    }

    public function test3($page = 'edm_pdf') {
        $this->load->library('wkhtmltopdf');
        $array['path'] = dirname(__FILE__);
//Thats the absolute path to the temp folder.
        $wkhtmltopdf = new Wkhtmltopdf;
        $wkhtmltopdf->make($array);
        $wkhtmltopdf->setTitle("New Title");
        $wkhtmltopdf->setHtml("http://www.google.com");// $html can be a url or html content.
        $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "file.pdf");
    }

    public function test4($page = 'edm_pdf') {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        //$this->load->view('pages/' . $page, $data);
        //$this->load->view('templates/footer', $data);
        $this->load->view('pages/edm_pdf3', $data);
        // Load all views as normal
        //$this->load->view('welcome_message');
        // Get output html

        $html = $this->output->get_output();

        // Load library
        /*
		$this->load->library('dompdf_gen');

		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf");*/

        $this->load->library('wkhtmltopdf');
        $array['path'] = dirname(__FILE__);
//Thats the absolute path to the temp folder.
        $wkhtmltopdf = new Wkhtmltopdf;
        $wkhtmltopdf->make($array);
        $wkhtmltopdf->setTitle("New Title");
        $wkhtmltopdf->setHtml($html);// $html can be a url or html content.
        $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "file.pdf");
    }

}
