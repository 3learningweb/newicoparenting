<?php
include("PDFGenerator.php");
class index extends CI_Controller
{
    private $home = 'https://icoparenting.moe.edu.tw/';
    public function reg($page = 'reg')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }


        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if (!empty($request["send"])) {
            $this->load->database('default');
            $query = $this->db->select('id, acc')->where('acc', $request["acc"])->get('i_member');
            if(empty($request["check_rule"]))
            {
                echo '<script language="JavaScript"> alert("您尚未同意隱私權條款");history.go(-1); //回上一頁</script>';
                return;
            }
            if (count($query->result_array())) {
                echo '<script language="JavaScript"> alert("帳號已經存在");history.go(-1); //回上一頁</script>';
                return;
            } else {
                $data_index = array("name" => "姓名", "acc" => "帳號", "email" => "電子信箱", "psd" => "密碼", "psd2" => "再次輸入密碼",);
                foreach ($data_index as $key => $value) {
                    if (empty($request[$key])) {
                        echo '<script language="JavaScript"> alert("' . $value . '未設定");history.go(-1); //回上一頁</script>';
                        return;
                    }
                }
                if ($request["psd"] != $request["psd2"]) {
                    echo '<script language="JavaScript"> alert("兩次密碼不同");history.go(-1); //回上一頁</script>';
                    return;
                }

                $insertdata = array();
                $insertdata["name"] = $request["name"];
                $insertdata["acc"] = $request["acc"];
                $insertdata["email"] = $request["email"];
                $insertdata["psd"] = $request["psd"];
                $insertdata["sex"] = $request["sex"];
                $insertdata["age"] = $request["age"];
                $insertdata["location"] = $request["location"];
                $insertdata["edu"] = $request["edu"];
                $insertdata["edm"] = 0;
                $insertdata["mail_check"] = 0;
                $this->db->insert('i_member', $insertdata);
                $id = $this->db->insert_id();

                if(!empty($request["birthday"]))
                {
                    $birthday = json_decode($request["birthday"]);
                    $insertdata = array();
                    $insertdata["parent"] = $id;
                    foreach($birthday as $value)
                    {
                        $insertdata["birthday"] = $value;
                        $this->db->insert('child', $insertdata);
                    }

                }
                $this->db->close();

                //send mail
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($request["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$request["acc"].'在iCoparenting註冊完成通知');
                $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$id;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                /*
                $session= array();
                $session["name"] =  $reqest["name"];
                $session["acc"] = $reqest["email"];
                $session["login"] = '1';
                $this->session->set_userdata($session);
                */
                //echo '<script type="text/javascript">alert(\'請至您的信箱收取驗證信件。\');</script>';
                redirect(base_url() . 'index.php/reg_finish', 'refresh');
                //redirect(base_url().'index.php', 'refresh');
                //var_dump($this->session->all_userdata());
            }
        }elseif(!empty($request["cancel"]))
        {
            redirect($this->home, 'refresh');
        }
        $query = $this->db->select('id, name')
            ->get('location');
        $data["location"] = $query->result_array();
        //$data = $this->get_menu($data);
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function reg_finish_c($page = 'reg_finish_c')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["home"]))
        {
            redirect($this->home, 'refresh');
        }
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }
	
	

    public function login( $page = "login")
    {
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if(isset($session["login"]))
        {
            $data["login"] = $session["login"];
        }else
        {
            $data["login"] = 0;
        }
        $this->load->helper('url');
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $request = $this->input->post();
        if(isset($request["reg"]) && $request["reg"] =1)
        {
            redirect(base_url().'index.php/reg', 'refresh');
        }else if(isset($request["login"]))
        {
            $this->load->database('default');
            $query = $this->db->select('id, name')->where('acc', $request["acc"])->where('psd', $request["psd"])->get('i_member');

            if(count($query->result_array()))
            {
                $res =  $query->result_array();
                //var_dump($res);
                $session_write= array();
                $session_write["imember_uid"] =  $res[0]["id"];
                $session_write["imember_acc"] = $request["acc"];
                $session_write["imember_login"] = 1;
                $this->session->set_userdata($session_write);
                redirect(base_url().'index.php/member_content', 'refresh');
            }else
            {
                echo '<script language="JavaScript"> alert("帳號密碼錯誤!");history.go(-1); //回上一頁</script>';
                return;
            }
            $this->db->close();
        }
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    private function get_menu($data)
    {
        $this->db->select('id, name, m_order, link');
        $this->db->where("parent",0);
        $this->db->order_by("m_order","asc");
        $this->db->from('edm_menu');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        //$this->db->where('org_to_stud.org', $org_id);
        $query = $this->db->get();
        $data["menu"] = $query->result_array();

        $this->db->select('id, name, m_order, link, parent');
        $this->db->where("parent !=",0);
        $this->db->order_by("m_order","asc");
        $this->db->from('edm_menu');
        //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
        //$this->db->where('org_to_stud.org', $org_id);
        $query = $this->db->get();
        $data["menu_all"] = $query->result_array();
        return $data;
    }
    public function member_content($page = 'member_content')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        //var_dump($this->session->all_userdata());
        if(!empty($get["c"]))
        {
            $this->session->unset_userdata('imember_uid');
            $this->session->unset_userdata('imember_acc');
            $this->session->unset_userdata('imember_login');
        }
        if (isset($session["imember_login"])) {
            $data["login"] = $session["imember_login"];
        } else {
            //$data["login"] = 0;
            redirect(base_url().'index.php/login', 'refresh');
        }
        $user_data = array();
        if(!empty($request["save"]))
        {
            if(!empty($request["psd"]))
            {
                if(!empty($request["psd2"]))
                {
                    if($request["psd"] != $request["psd2"])
                    {
                        echo '<script language="JavaScript"> alert("兩次密碼不同");history.go(-1); //回上一頁</script>';
                        return;
                    }else{
                        $user_data["psd"] = $request["psd"];
                    }
                }else{
                    echo '<script language="JavaScript"> alert("請再次輸入密碼");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data_index = array("name" => "姓名", "email" => "電子信箱");
            foreach ($data_index as $key => $value) {
                if (empty($request[$key])) {
                    echo '<script language="JavaScript"> alert("' . $value . '未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            //比對mail是否有修改
            $query = $this->db->select('id, email')->where('id', $session["imember_uid"])->get('i_member');
            $old_data = $query->result_array();
            if($request["email"] != $old_data[0]["email"] )
            {
                $mail_change = 1;
            }
            $data_title = array("name","email","age","location","sex","edu","edm");

            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }

            if(isset($mail_change))
            {
                $user_data["mail_check"] = 0;
            }

            $this->db->where('id', $session["imember_uid"]);
            $this->db->update('i_member', $user_data);

            $this->db->where('parent',$session["imember_uid"]);
            $this->db->delete('child');
            if(!empty($request["birthday"]))
            {
                $birthday = json_decode($request["birthday"]);
                $insertdata = array();
                $insertdata["parent"] = $session["imember_uid"];
                foreach($birthday as $value)
                {
                    $insertdata["birthday"] = $value;
                    $this->db->insert('child', $insertdata);
                }

            }

            if(isset($mail_change))
            {
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($request["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$request["name"].'在iCoparenting註冊完成通知');
                $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$session["imember_uid"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                echo '<script language="JavaScript"> alert("您的email資訊已更新，請至您的信箱收取驗證信件。"); //回上一頁</script>';
            }else
            {
                echo '<script language="JavaScript"> alert("您的資料已更新完成!"); //回上一頁</script>';
            }

            redirect($this->home, 'refresh');

        }elseif(!empty($request["send_email"]))
        {

            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "140.111.34.125";
            $config['smtp_port'] = "25";
            //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
            //$config['smtp_pass'] = "nss123456";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
            $list = array($request["email"]);
            $ci->email->to($list);
            $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
            $ci->email->subject("帳號".$request["name"].'在iCoparenting註冊完成通知');
            $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                base_url().'index.php/check?id='.$session["imember_uid"];
            //echo $message_content;
            //die();
            $ci->email->message($message_content);
            $ci->email->send();

        }


        $query = $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check')
            ->where('id', $session["imember_uid"])
            ->get('i_member');
        $data["user_data"] = $query->result_array();

        $query = $this->db->select('id, name')
            ->get('location');
        $data["location"] = $query->result_array();

        $query = $this->db->select('id, birthday')
            ->where('parent', $session["imember_uid"])
            ->get('child');
        $data["child"] = $query->result_array();

        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);

    }


    public function forget($page = 'forget')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        //database close
        if (!empty($request["send"])) {
            $this->load->database('default');
            $query = $this->db->select('id, acc, name, email')->where('acc', $request["acc"])->get('i_member');
            $member = $query->result_array();

            if (count($member)) {
                $new_psd = $this->RandomString();
                $user_data = array();
                $user_data["psd"] = $new_psd;
                $this->db->where('id', $member[0]["id"]);
                $this->db->update('i_member', $user_data);
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($member[0]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[0]["name"].'的密碼通知');
                $message_content = $member[0]["name"].'您好：<br>您在iCoparenting網站的會員密碼資訊如下。<br>帳號：'
                    .$member[0]["acc"].'<br>密碼：'.$new_psd
                    .'<br>建議您再次登入iCoparenting網站 <a href="'.base_url().'index.php/member_content?c=1">[電子報-會員專區]</a>變更您的密碼資訊，謝謝您。';
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                $this->db->close();
                redirect(base_url().'index.php/forget_finish?id='.$member[0]["id"], 'refresh');
            } else {
                echo '<script language="JavaScript"> alert("您輸入的帳號不存在");history.go(-1); //回上一頁</script>';
                redirect(base_url().'index.php/forget', 'refresh');
            }
        }
        $this->db->close();
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $randstring.$characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    public function check( $page = "check")
    {
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $this->load->helper('url');
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $get = $this->input->get();
        if(isset($get["id"]))
        {
            $this->load->database('default');
            $user_data = array();
            $user_data["mail_check"] = 1;
            $user_data["edm"] = 1;
            $this->db->where('id', $get["id"]);
            $this->db->update('i_member', $user_data);
            $this->db->close();
        }
        $this->load->library('session');
        $this->session->unset_userdata('imember_uid');
        $this->session->unset_userdata('imember_acc');
        $this->session->unset_userdata('imember_login');

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function forget_finish($page = 'forget_finish')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if($get["id"])
        {
            $this->load->database('default');
            $query = $this->db->select('id, acc, name, email')->where('id', $get["id"])->get('i_member');
            $member = $query->result_array();
            $data['member'] = $member[0];
        }


        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function edm_print($page = 'edm_print')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["view1"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view1"].'&theme=1', 'refresh');
        }else if(!empty($request["view2"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view2"].'&theme=2', 'refresh');
        }else if(!empty($request["view3"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view3"].'&theme=3', 'refresh');
        }else if(!empty($request["view4"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view4"].'&theme=4', 'refresh');
        }else if(!empty($request["view5"]))
        {

            redirect(base_url().'index.php/edm_detail?id='.$request["view5"], 'refresh');
        }else if(!empty($request["pdf1"]))
        {

            /*
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1');
            echo $url;
            $data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            */
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1');
            // echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';


            echo '<script language="JavaScript"> alert("0");//回上一頁</script>';
            $url = base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1';
            $output = shell_exec('/usr/local/bin/wkhtmltox/bin/wkhtmltopdf "'.$url.'" /mnt/website/edu001c/edm_sys/upload/test6.pdf');
            echo '<script language="JavaScript"> alert("'.$output.'");//回上一頁</script>';
            /*
            $html = $this->edm_list_html($request["pdf1"],1);
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */


        }else if(!empty($request["pdf2"]))
        {
            $url = base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1';
            echo '<script language="JavaScript"> alert("0");//回上一頁</script>';
            $output = shell_exec('/usr/local/bin/wkhtmltox/bin/wkhtmltopdf "'.$url.'" /mnt/website/edu001c/edm_sys/upload/test1.pdf');
            echo "<pre>$output</pre>";
            die();


            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';

            //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            /*
            $html = $this->edm_list_html($request["pdf2"],2);
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */

        }else if(!empty($request["pdf3"]))
        {

            echo '<script language="JavaScript"> alert("0");//回上一頁</script>';
            $get["theme"] = 3;
            if(!empty($request["pdf3"]) && !empty($get["theme"]))
            {
                $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
                $this->db->where('id',$request["pdf3"]);
                $this->db->from('edm_main');
                $query = $this->db->get();
                $data["edm_main"] = $query->result_array();

                $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
                $this->db->where('parent',$get["id"]);
                $this->db->from('edm_content1');
                //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
                //$this->db->where('org_to_stud.org', $org_id);
                $query = $this->db->get();
                $content = $query->result_array();
                $content_arr_temp = array();
                foreach($content as $item)
                {
                    if($item["theme"] == $get["theme"])
                    {

                        $content_arr_temp[1] = $item;
                    }elseif($item["theme"] == 5)
                    {

                        $content_arr_temp[2] = $item;
                    }elseif($item["theme"] == 6)
                    {

                        $content_arr_temp[3] = $item;
                    }elseif($item["theme"] == 7)
                    {

                        $content_arr_temp[4] = $item;
                    }
                }

                $content_arr = array();
                for($i=1; $i<=4; $i++)
                {
                    if($i < $get["theme"])
                    {
                        $content_arr[$i] = $content_arr_temp[($i+1)];
                    }else if($i == $get["theme"])
                    {
                        $content_arr[$i] = $content_arr_temp[1];
                    }else
                    {
                        $content_arr[$i] = $content_arr_temp[$i];
                    }
                }
                $data["content"] = $content_arr;
                $data["theme"] = $get["theme"];
            }
            $data["home"] = $this->home;
            $html=$this->load->view('pages/' . $page, $data, true);
            $this->load->library('wkhtmltopdf');
            $array['path'] ='/mnt/website/edu001c/edm_sys/upload';
//Thats the absolute path to the temp folder.
            $wkhtmltopdf = new Wkhtmltopdf;
            $wkhtmltopdf->make($array);
            $wkhtmltopdf->setTitle("edm_".$request["pdf1"].'_1');
            $wkhtmltopdf->setHtml($html);// $html can be a url or html content.
            $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "file.pdf");
            echo '<script language="JavaScript"> alert("1");//回上一頁</script>';
            /*
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf3"].'&theme=3');
            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
            $this->load->library('wkhtmltopdf');
            $array['path'] ='/mnt/website/edu001c/edm_sys/upload';
//Thats the absolute path to the temp folder.
            $wkhtmltopdf = new Wkhtmltopdf;
            $wkhtmltopdf->make($array);
            $wkhtmltopdf->setTitle("edm_".$request["pdf1"].'_1');
            $wkhtmltopdf->setHtml($url);// $html can be a url or html content.
            $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "file.pdf");
            echo '<script language="JavaScript"> alert("2");//回上一頁</script>';
            */
            //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            /*
            $html = $this->edm_list_html($request["pdf3"],3);
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */

        }else if(!empty($request["pdf4"]))
        {
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf4"].'&theme=4');
            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
            $this->load->library('wkhtmltopdf');
            $array['path'] ='/mnt/website/edu001c/edm_sys/upload';
//Thats the absolute path to the temp folder.
            $wkhtmltopdf = new Wkhtmltopdf;
            $wkhtmltopdf->make($array);
            $wkhtmltopdf->setTitle("edm_".$request["pdf1"].'_1');
            $wkhtmltopdf->setHtml($url);// $html can be a url or html content.
            $wkhtmltopdf->output(Wkhtmltopdf::MODE_DOWNLOAD, "file.pdf");


            //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';

            /*
                $html = $this->edm_list_html($request["pdf4"],4);
                //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
                $url = 'http://freehtmltopdf.com';
                $data = array(  'convert' => '',
                    'html' => $html,
                    'baseurl' => 'http://www.myhost.com');

                // use key 'http' even if you send the request to https://...
                $options = array(
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($data),
                    ),
                );
                $context  = stream_context_create($options);
                $result = file_get_contents($url, false, $context);

                // set the pdf data as download content:
                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename="webpage.pdf"');
                echo($result);
                return;
    */
        }else if(!empty($request["pdf5"]))
        {

            redirect(base_url().'index.php/edm_detail?id='.$request["pdf5"], 'refresh');
        }
        $this->db->select('id, type, print, title, publish, list_banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function edm_print_c($page = 'edm_print_c')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["view1"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view1"].'&theme=1', 'refresh');
        }else if(!empty($request["view2"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view2"].'&theme=2', 'refresh');
        }else if(!empty($request["view3"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view3"].'&theme=3', 'refresh');
        }else if(!empty($request["view4"]))
        {

            redirect(base_url().'index.php/edm_list?id='.$request["view4"].'&theme=4', 'refresh');
        }else if(!empty($request["view5"]))
        {

            redirect(base_url().'index.php/edm_detail?id='.$request["view5"], 'refresh');
        }else if(!empty($request["pdf1"]))
        {
            /*
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1');
            echo $url;
            $data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            */
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1');
            $apikey = '55e32c22-1e15-4964-beeb-143443cde735';
            $value = base_url().'index.php/edm_list2?id='.$request["pdf1"].'&theme=1'; // can aso be a url, starting with http..
            $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value));

// Output headers so that the file is downloaded rather than displayed
// Remember that header() must be called before any actual output is sent
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));

// Make the file a downloadable attachment - comment this out to show it directly inside the
// web browser.  Note that you can give the file any name you want, e.g. alias-name.pdf below:
            header('Content-Disposition: attachment; filename=' . 'edm.pdf' );
            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
            /*
            $html = $this->edm_list_html($request["pdf1"],1);
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */


        }else if(!empty($request["pdf2"]))
        {
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf2"].'&theme=2');
            $apikey = '55e32c22-1e15-4964-beeb-143443cde735';
            $value = base_url().'index.php/edm_list2?id='.$request["pdf2"].'&theme=2'; // can aso be a url, starting with http..
            $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value));

// Output headers so that the file is downloaded rather than displayed
// Remember that header() must be called before any actual output is sent
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));

// Make the file a downloadable attachment - comment this out to show it directly inside the
// web browser.  Note that you can give the file any name you want, e.g. alias-name.pdf below:
            header('Content-Disposition: attachment; filename=' . 'edm.pdf' );
            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
            //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            /*
            $html = $this->edm_list_html($request["pdf2"],2);
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */

        }else if(!empty($request["pdf3"]))
        {
            $url = urlencode(base_url().'index.php/edm_list2?id='.$request["pdf3"].'&theme=3');
            $apikey = '55e32c22-1e15-4964-beeb-143443cde735';
            $value = base_url().'index.php/edm_list2?id='.$request["pdf3"].'&theme=3'; // can aso be a url, starting with http..
            $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value));

// Output headers so that the file is downloaded rather than displayed
// Remember that header() must be called before any actual output is sent
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));

// Make the file a downloadable attachment - comment this out to show it directly inside the
// web browser.  Note that you can give the file any name you want, e.g. alias-name.pdf below:
            header('Content-Disposition: attachment; filename=' . 'edm.pdf' );
            //echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
            //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';
            /*
            $html = $this->edm_list_html($request["pdf3"],3);
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */

        }else if(!empty($request["pdf4"]))
        {

            echo '<script language="JavaScript"> alert("pdf4"); //回上一頁</script>';
            $url = urlencode('http://www.google.com');
            $apikey = '55e32c22-1e15-4964-beeb-143443cde735';
            $value ='http://www.google.com'; // can aso be a url, starting with http..
            $result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value));

// Output headers so that the file is downloaded rather than displayed
// Remember that header() must be called before any actual output is sent
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));

// Make the file a downloadable attachment - comment this out to show it directly inside the
// web browser.  Note that you can give the file any name you want, e.g. alias-name.pdf below:
            header('Content-Disposition: attachment; filename=' . 'edm.pdf' );

           // echo '<script language="JavaScript">  window.location="http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de"; target="_blank";</script>';
        //$data["pdf_url"] = 'http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de';

        /*
            $html = $this->edm_list_html($request["pdf4"],4);
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
*/
        }else if(!empty($request["pdf5"]))
        {

            redirect(base_url().'index.php/edm_detail?id='.$request["pdf5"], 'refresh');
        }
        $this->db->select('id, type, print, title, publish, list_banner, template, age_theme, pre_age, create_date, modify_date');
        $this->db->where('publish !=',"0000-00-00");
        $this->db->from('edm_main');
        $query = $this->db->get();
        $data["edm_main"] = $query->result_array();
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function edm_list($page = 'edm_list')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date, print_year, print_img');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, title2, title3, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.service_id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                 {
                     $content_arr[$i] = $content_arr_temp[1];
                 }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
            $data["id"] = $get["id"];
        }
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }



    private function edm_list_html($id,$theme)
    {

        $page = 'edm_list';
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($id) && !empty($theme))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$id);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, title2, title3, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$id);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.service_id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $theme)
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $theme;
            $data["id"] = $id;
        }
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
        return $this->output->get_output();

    }

    public function edm_list2($page = 'edm_list2')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date, print_year, print_img');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, title2, title3, type, img, img2, img3, content, content2, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.service_id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            //var_dump($content[0]["theme"]);
            for($i=1; $i<=4; $i++)
            {
                if($i < $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $data["edm_main"][0]["age_theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
            $data["id"] = $get["id"];
        }
        $data["home"] = $this->home;
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function edm_detail($page = 'edm_detail')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["back"]))
        {
            redirect(base_url().'index.php/edm_print', 'refresh');
        }

        if(!empty($get["id"])) {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id', $get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, title, type, img, content, suggestion');
            $this->db->where('parent', $get["id"]);
            $this->db->from('edm_content2');
            $query = $this->db->get();
            $data["content"] =  $query->result_array();
        }else if(!empty($get["theme_id"]))
        {
            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('id', $get["theme_id"]);
            $this->db->from('edm_content1');
            $query = $this->db->get();
            $data["content"] =  $query->result_array();
        }

        $this->db->close(); //database close
        $data["home"] = $this->home;
        $data['base_url'] = base_url();
        //$this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function edm_pdf($page = 'edm_pdf')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["home"]))
        {
            redirect($this->home, 'refresh');
        }
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            for($i=1; $i<=4; $i++)
            {
                if($i < $get["theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $get["theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $html=$this->load->view('pages/' . $page, $data, true);
        $this->db->close(); //database close
/*
        //this the the PDF filename that user will get to download
        $pdfFilePath = "output_pdf_name.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //set font
        //$this->m_pdf->pdf->SetAutoFont('AUTOFONT_ALL');
        $this->m_pdf->pdf->SetDisplayMode('fullpage');
        $this->m_pdf->showWatermarkText = true;
        //$this->m_pdf->SetAutoFont(AUTOFONT_ALL);

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
*/

        $this->load->library("Pdf");
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        //$pdf->Image(base_url().'pdf' ,30, 40, 75, 113, 'JPG', '', '', true, 300, '');
        // Add a page
        $pdf->AddPage();
        //$html = "<h1>Test Page</h1>";
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output();

    }

    public function edm_pdf2($page = 'edm_pdf2')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if(!empty($request["home"]))
        {
            redirect($this->home, 'refresh');
        }
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        if(!empty($get["id"]) && !empty($get["theme"]))
        {
            $this->db->select('id, type, print, title, publish, banner, template, age_theme, pre_age, create_date, modify_date');
            $this->db->where('id',$get["id"]);
            $this->db->from('edm_main');
            $query = $this->db->get();
            $data["edm_main"] = $query->result_array();

            $this->db->select('id, list_title, list_intro, list_img, title, type, img, content, suggestion, theme');
            $this->db->where('parent',$get["id"]);
            $this->db->from('edm_content1');
            //$this->db->join('org_to_stud', 'org_to_stud.student = student_info.id');
            //$this->db->where('org_to_stud.org', $org_id);
            $query = $this->db->get();
            $content = $query->result_array();
            $content_arr_temp = array();
            foreach($content as $item)
            {
                if($item["theme"] == $get["theme"])
                {

                    $content_arr_temp[1] = $item;
                }elseif($item["theme"] == 5)
                {

                    $content_arr_temp[2] = $item;
                }elseif($item["theme"] == 6)
                {

                    $content_arr_temp[3] = $item;
                }elseif($item["theme"] == 7)
                {

                    $content_arr_temp[4] = $item;
                }
            }

            $content_arr = array();
            for($i=1; $i<=4; $i++)
            {
                if($i < $get["theme"])
                {
                    $content_arr[$i] = $content_arr_temp[($i+1)];
                }else if($i == $get["theme"])
                {
                    $content_arr[$i] = $content_arr_temp[1];
                }else
                {
                    $content_arr[$i] = $content_arr_temp[$i];
                }
            }
            $data["content"] = $content_arr;
            $data["theme"] = $get["theme"];
        }
        $data["home"] = $this->home;
        $html=$this->load->view('pages/' . $page, $data, true);
        $this->db->close(); //database close
        /*
                //this the the PDF filename that user will get to download
                $pdfFilePath = "output_pdf_name.pdf";

                //load mPDF library
                $this->load->library('m_pdf');

                //set font
                //$this->m_pdf->pdf->SetAutoFont('AUTOFONT_ALL');
                $this->m_pdf->pdf->SetDisplayMode('fullpage');
                $this->m_pdf->showWatermarkText = true;
                //$this->m_pdf->SetAutoFont(AUTOFONT_ALL);

                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
        */

        $this->load->library("Pdf");
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        //$pdf->Image(base_url().'pdf' ,30, 40, 75, 113, 'JPG', '', '', true, 300, '');
        // Add a page
        $pdf->AddPage();
        //$html = "<h1>Test Page</h1>";
        $pdf->writeHTML($html, true, false, true, false, '');
        //$pdf->Output();
        $time = (int)microtime(true);
        $pdf_name ='D:/upupw2/htdocs/demomode/ifamily/pdf/'.$time.'.pdf';
        $pdf->Output($pdf_name, 'F');
        //$file = $pdf->Output('temp.pdf','F');

        //将pdf文档转换成图片
        $this->load->library('image_lib');

        $config = array(
            'image_library' => 'imagemagick',
            'library_path' => '~/bin',
            'source_image' => $pdf_name,
            'new_image' => 'D:/upupw2/htdocs/demomode/ifamily/pdf/'.$time.'.jpg',
            'maintain_ratio' => true,
            'width' => 980,
            'quality' => '90%',
        );

        $this->image_lib->initialize( $config );

        if ( $this->image_lib->resize( ) ) {
            $this->image_lib->clear( );
        }
        /*
        $im = new imagick($pdf_name);
        $im->setImageFormat( "jpg" );
        $img_name = time().'.jpg';
        $im->setSize(800,600);
        $fileHandle = fopen("D:/upupw2/htdocs/demomode/ifamily/pdf/'.$time.'.jpg", "w");
        $im->writeImageFile($fileHandle);
        $im->clear();
        $im->destroy();
        */


    }

    public function reg_c($page = 'reg_c')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }


        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        if (!empty($request["send"])) {
            $this->load->database('default');
            $query = $this->db->select('id, acc')->where('acc', $request["acc"])->get('i_member');
            if(empty($request["check_rule"]))
            {
                echo '<script language="JavaScript"> alert("您尚未同意隱私權條款");history.go(-1); //回上一頁</script>';
                return;
            }
            if (count($query->result_array())) {
                echo '<script language="JavaScript"> alert("帳號已經存在");history.go(-1); //回上一頁</script>';
                return;
            } else {
                $data_index = array("name" => "姓名", "acc" => "帳號", "email" => "電子信箱", "psd" => "密碼", "psd2" => "再次輸入密碼",);
                foreach ($data_index as $key => $value) {
                    if (empty($request[$key])) {
                        echo '<script language="JavaScript"> alert("' . $value . '未設定");history.go(-1); //回上一頁</script>';
                        return;
                    }
                }
                if ($request["psd"] != $request["psd2"]) {
                    echo '<script language="JavaScript"> alert("兩次密碼不同");history.go(-1); //回上一頁</script>';
                    return;
                }

                $insertdata = array();
                $insertdata["name"] = $request["name"];
                $insertdata["acc"] = $request["acc"];
                $insertdata["email"] = $request["email"];
                $insertdata["psd"] = $request["psd"];
                $insertdata["sex"] = $request["sex"];
                $insertdata["age"] = $request["age"];
                $insertdata["location"] = $request["location"];
                $insertdata["edu"] = $request["edu"];
                $insertdata["edm"] = 0;
                $insertdata["mail_check"] = 0;
                $this->db->insert('i_member', $insertdata);
                $id = $this->db->insert_id();

                if(!empty($request["birthday"]))
                {
                    $birthday = json_decode($request["birthday"]);
                    $insertdata = array();
                    $insertdata["parent"] = $id;
                    foreach($birthday as $value)
                    {
                        $insertdata["birthday"] = $value;
                        $this->db->insert('child', $insertdata);
                    }

                }
                $this->db->close();

                //send mail
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($request["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$request["acc"].'在iCoparenting註冊完成通知');
                $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$id;
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();

                /*
                $session= array();
                $session["name"] =  $reqest["name"];
                $session["acc"] = $reqest["email"];
                $session["login"] = '1';
                $this->session->set_userdata($session);
                */
                //echo '<script type="text/javascript">alert(\'請至您的信箱收取驗證信件。\');</script>';
                redirect(base_url() . 'index.php/reg_finish_c', 'refresh');
                //redirect(base_url().'index.php', 'refresh');
                //var_dump($this->session->all_userdata());
            }
        }elseif(!empty($request["cancel"]))
        {
            redirect($this->home, 'refresh');
        }
        $query = $this->db->select('id, name')
            ->get('location');
        $data["location"] = $query->result_array();
        //$data = $this->get_menu($data);
        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function login_c( $page = "login_c")
    {
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if(isset($session["login"]))
        {
            $data["login"] = $session["login"];
        }else
        {
            $data["login"] = 0;
        }
        $this->load->helper('url');
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $request = $this->input->post();
        if(isset($request["reg"]) && $request["reg"] =1)
        {
            redirect(base_url().'index.php/reg_c', 'refresh');
        }else if(isset($request["login"]))
        {
            $this->load->database('default');
            $query = $this->db->select('id, name')->where('acc', $request["acc"])->where('psd', $request["psd"])->get('i_member');

            if(count($query->result_array()))
            {
                $res =  $query->result_array();
                //var_dump($res);
                $session_write= array();
                $session_write["imember_uid"] =  $res[0]["id"];
                $session_write["imember_acc"] = $request["acc"];
                $session_write["imember_login"] = 1;
                $this->session->set_userdata($session_write);
                redirect(base_url().'index.php/member_content_c', 'refresh');
            }else
            {
                echo '<script language="JavaScript"> alert("帳號密碼錯誤!");history.go(-1); //回上一頁</script>';
                return;
            }
            $this->db->close();
        }
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function forget_c($page = 'forget_c')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        //var_dump($this->session->all_userdata());
        if (isset($session["login"])) {
            $data["login"] = $session["login"];
        } else {
            $data["login"] = 0;
        }

        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        //database close
        if (!empty($request["send"])) {
            $this->load->database('default');
            $query = $this->db->select('id, acc, name, email')->where('acc', $request["acc"])->get('i_member');
            $member = $query->result_array();

            if (count($member)) {
                $new_psd = $this->RandomString();
                $user_data = array();
                $user_data["psd"] = $new_psd;
                $this->db->where('id', $member[0]["id"]);
                $this->db->update('i_member', $user_data);
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($member[0]["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$member[0]["name"].'的密碼通知');
                $message_content = $member[0]["name"].'您好：<br>您在iCoparenting網站的會員密碼資訊如下。<br>帳號：'
                    .$member[0]["acc"].'<br>密碼：'.$new_psd
                    .'<br>建議您再次登入iCoparenting網站 <a href="'.base_url().'index.php/member_content?c=1">[電子報-會員專區]</a>變更您的密碼資訊，謝謝您。';
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                $this->db->close();
                redirect(base_url().'index.php/forget_finish?id='.$member[0]["id"], 'refresh');
            } else {
                echo '<script language="JavaScript"> alert("您輸入的帳號不存在");history.go(-1); //回上一頁</script>';
                redirect(base_url().'index.php/forget_c', 'refresh');
            }
        }
        $this->db->close();
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);


    }

    public function member_content_c($page = 'member_content_c')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->library('session');
        $session = $this->session->all_userdata();
        $url = "";
        $this->load->helper('url');
        $data['url'] = $url;

        $this->load->database('default');
        $request = $this->input->post();
        $get = $this->input->get();
        //var_dump($this->session->all_userdata());
        if(!empty($get["c"]))
        {
            $this->session->unset_userdata('imember_uid');
            $this->session->unset_userdata('imember_acc');
            $this->session->unset_userdata('imember_login');
        }
        if (isset($session["imember_login"])) {
            $data["login"] = $session["imember_login"];
        } else {
            //$data["login"] = 0;
            redirect(base_url().'index.php/login_c', 'refresh');
        }
        $user_data = array();
        if(!empty($request["save"]))
        {
            if(!empty($request["psd"]))
            {
                if(!empty($request["psd2"]))
                {
                    if($request["psd"] != $request["psd2"])
                    {
                        echo '<script language="JavaScript"> alert("兩次密碼不同");history.go(-1); //回上一頁</script>';
                        return;
                    }else{
                        $user_data["psd"] = $request["psd"];
                    }
                }else{
                    echo '<script language="JavaScript"> alert("請再次輸入密碼");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            $data_index = array("name" => "姓名", "email" => "電子信箱");
            foreach ($data_index as $key => $value) {
                if (empty($request[$key])) {
                    echo '<script language="JavaScript"> alert("' . $value . '未設定");history.go(-1); //回上一頁</script>';
                    return;
                }
            }
            //比對mail是否有修改
            $query = $this->db->select('id, email')->where('id', $session["imember_uid"])->get('i_member');
            $old_data = $query->result_array();
            if($request["email"] != $old_data[0]["email"] )
            {
                $mail_change = 1;
            }
            $data_title = array("name","email","age","location","sex","edu","edm");

            foreach($data_title as $item)
            {
                if(!empty($request[$item]))
                {
                    $user_data[$item] = $request[$item];
                }
            }

            if(isset($mail_change))
            {
                $user_data["mail_check"] = 0;
            }

            $this->db->where('id', $session["imember_uid"]);
            $this->db->update('i_member', $user_data);

            $this->db->where('parent',$session["imember_uid"]);
            $this->db->delete('child');
            if(!empty($request["birthday"]))
            {
                $birthday = json_decode($request["birthday"]);
                $insertdata = array();
                $insertdata["parent"] = $session["imember_uid"];
                foreach($birthday as $value)
                {
                    $insertdata["birthday"] = $value;
                    $this->db->insert('child', $insertdata);
                }

            }

            if(isset($mail_change))
            {
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "140.111.34.125";
                $config['smtp_port'] = "25";
                //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
                //$config['smtp_pass'] = "nss123456";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);

                $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
                $list = array($request["email"]);
                $ci->email->to($list);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $ci->email->subject("帳號".$request["name"].'在iCoparenting註冊完成通知');
                $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                    base_url().'index.php/check?id='.$session["imember_uid"];
                //echo $message_content;
                //die();
                $ci->email->message($message_content);
                $ci->email->send();
                echo '<script language="JavaScript"> alert("您的email資訊已更新，請至您的信箱收取驗證信件。"); //回上一頁</script>';
            }else
            {
                echo '<script language="JavaScript"> alert("您的資料已更新完成!"); //回上一頁</script>';
            }

            redirect($this->home, 'refresh');

        }elseif(!empty($request["send_email"]))
        {

            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "140.111.34.125";
            $config['smtp_port'] = "25";
            //$config['smtp_user'] = "ePaper@mail.moe.gov.tw";
            //$config['smtp_pass'] = "nss123456";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            $ci->email->from('ePaper@mail.moe.gov.tw', 'iCoparenting網站管理員');
            $list = array($request["email"]);
            $ci->email->to($list);
            $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
            $ci->email->subject("帳號".$request["name"].'在iCoparenting註冊完成通知');
            $message_content = $request["name"].'您好：<br>感謝您在iCoparenting網站註冊，請務必開啟以下連結網址，確認您的電子信箱是正確的，日後網站將定期發送電子報給您，感謝配合。<br>您可以複製下列連結，貼上於瀏覽器網址列：<br>'.
                base_url().'index.php/check?id='.$session["imember_uid"];
            //echo $message_content;
            //die();
            $ci->email->message($message_content);
            $ci->email->send();

        }


        $query = $this->db->select('id, acc, name, email, age, location, sex, edu, edm, mail_check')
            ->where('id', $session["imember_uid"])
            ->get('i_member');
        $data["user_data"] = $query->result_array();

        $query = $this->db->select('id, name')
            ->get('location');
        $data["location"] = $query->result_array();

        $query = $this->db->select('id, birthday')
            ->where('parent', $session["imember_uid"])
            ->get('child');
        $data["child"] = $query->result_array();

        $this->db->close(); //database close
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('templates/header_c', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);

    }
}
?>