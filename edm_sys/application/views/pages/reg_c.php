<!-- Ming content -->
<form action="?" method="post">
<div class="memberM">
    <h1>電子報會員註冊</h1>
    <div class="bottomM"> </div>
    <ul>
        <li>
            <div class="titleM"><span>*</span>姓名</div>
            <div class="typeM"><input placeholder="-- 請填寫真實姓名 --" type="text" name="name"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>帳號名稱</div>
            <div class="typeM"><input placeholder="-- 會員登入之帳號以英文或數字填寫 --" type="text" name="acc"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>密碼</div>
            <div class="typeM"><input placeholder="-- 請填寫6~12位的英文或數字 -- " type="password" name="psd"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>再次輸入密碼</div>
            <div class="typeM"><input type="password" name="psd2"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>電子郵件信箱</div>
            <div class="typeM"><input type="text" name="email"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>性別</div>
            <div class="typeM">
                <select name="sex">
                   <?php
                        echo ' <option value="1" >男</option>';
                        echo ' <option value="2" >女</option>';
                    ?>
                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>年齡</div>
            <div class="typeM">

                <select name="age">
                   <?php 
                    echo ' <option value="1" >19 歲及以下 </option>';
                    echo ' <option value="2" >20 歲-24 歲 </option>';
                    echo ' <option value="3" >25 歲-29 歲</option>';
                    echo ' <option value="4" >30 歲-34 歲 </option>';
                    echo ' <option value="5" >35 歲-39 歲 </option>';
                    echo ' <option value="6" >40 歲-44 歲 </option>';
                    echo ' <option value="7" >45 歲-49 歲 </option>';
                    echo ' <option value="8" >50 歲-54 歲</option>';
                    echo ' <option value="9" >55 歲-59 歲 </option>';
                    echo ' <option value="10" >60 歲-64歲 </option>';
                    echo ' <option value="11" >65 歲及以上</option>';
                    ?>
                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>教育程度</div>
            <div class="typeM">

                <select name="edu">
                   <?php 
                        echo ' <option value="1" >無或自修 </option>';
                        echo ' <option value="2" >自修或小學</option>';
                        echo ' <option value="3" >國中/初中/初職</option>';
                        echo ' <option value="4" >高中/高職/士官學校</option>';
                        echo ' <option value="5" >五專/二專/三專 /軍警專修班/軍警官學校</option>';
                        echo ' <option value="6" >大學（含技術學院與科技大學）</option>';
                        echo ' <option value="7" >碩士</option>';
                        echo ' <option value="8" >博士</option>';
                        echo ' <option value="9" >其他</option>';
                    ;
                    ?>

                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>居住地</div>
            <div class="typeM">
                <select name="location">
               <?php 
                if(!empty($location)) {
                    foreach ($location as $item) {
                        echo '<option value=' . $item["id"] . '>' . $item["name"] . '</option>';
                    }
                }

                ?>
                </select>
            </div>
        </li>
        <li id="child">
           <?php 
            if(!empty($child))
            {
                foreach($child as $key=>$item)
                {
                    echo '<div><div class="titleM child_num">第'.($key+1).'個孩子的生日</div><div class="type3M birthday">'.$item["birthday"].'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';
                }
            }
            ?>
        </li>
        <li>
            <div class="titleM">選擇孩子的生日</div>

            <div class="typeM">
                <select id="year" style="width: 40%;">
                   <?php 
                    for($i=1950; $i<=2016; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'年</option>';
                    }
                    ?>
                </select>
                <select id="month" style="width: 28%;">
                   <?php 
                    for($i=1; $i<=12; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'月</option>';
                    }

                    ?>
                </select>
                <select id="day" style="width: 28%;">
                   <?php 
                    for($i=1; $i<=31; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'日</option>';
                    }

                    ?>
                </select>
            </div>
            <div class="titleM">&nbsp;</div>
            <div class="type3M"><p style="color: red;">選擇生日後，請按右方[+]鍵儲存</p></div>
            <div class="type4M"><div onclick="add(this)"><button onclick="return false;">＋</button></div></div>
            <input type="hidden" name="birthday" id="birthday" value="">
            </li>

        <li>
            <div class="titleM"><span>*</span>隱私權政策</div>
            <div class="typeM">
                <label style="font-size: 18px;"><input type="checkbox" name="check_rule" value="1" style="width: 10%;">我同意下列條款</label>
                <textarea style="width: 100%; height: 300px;">
                    非常歡迎您光臨「iCoparenting-和樂共親職學習網」(以下簡稱本網站)，為了讓您能夠安心的使用本網站的各項服務與資訊，特此向您說明本網站的隱私權保護政策，以保障您的權益，請您詳閱下列內容：
一、隱私權保護政策的適用範圍
隱私權保護政策內容，包括本網站如何處理在您使用網站服務時收集到的相關資料。隱私權保護政策不適用於本網站以外的相關連結網站，也不適用於非本網站所委託或參與管理的人員。

二、資料的蒐集與使用方式
為了在本網站上提供您最佳的互動性服務，可能會紀錄相關操作資料，其範圍如下：
◆	本網站在您使用服務信箱、辯論留言等互動性功能時，會保留您所提供的名稱、電子郵件地址、使用時間等。
◆	於使用各項測驗時，伺服器會自行記錄相關行徑，包括您使用連線設備的IP位址、使用時間、瀏覽及點選資料記錄等，做為我們增進網站服務的參考依據，此記錄為內部應用，決不對外公布。
◆	為提供精確的服務，我們會將收集的問卷調查內容進行統計與分析，分析結果之統計數據或說明文字呈現，除供內部研究外，我們會視需要公佈統計數據及說明文字，但不涉及特定個人之資料。

三、資料之保護
本網站主機均設有防火牆、防毒系統等相關的各項資訊安全設備及必要的安全防護措施，加以保護網站及相關資料採用嚴格的保護措施，只由經過授權的人員才能接觸您相關資料。

四、網站對外的相關連結
本網站的網頁提供其他網站的網路連結，您也可經由本網站所提供的連結，點選進入其他網站。但該連結網站不適用本網站的隱私權保護政策，您必須參考該連結網站中的隱私權保護政策。

五、隱私權保護政策之修正
本網站隱私權保護政策將因應需求隨時進行修正，修正後的條款將刊登於網站上。
                </textarea></div>
        </li>
        <li>
            <div class="titleM noneM">&nbsp;</div>
            <div class="type7M"><button name="send" value="1" onclick="birthday_collect()">註冊</button><button name="cancel" value="1">取消</button></div>
        </li>
    </ul>

</div>
    </form>
<!-- END content -->


		</div>
</div>
<script>
    function add(item)
    {
        var picker = document.getElementById("education_datepicker");
        var year = document.getElementById("year");
        var month = document.getElementById("month");
        var day = document.getElementById("day");
        /*
        if( picker.value != "--請選擇日期並按下[增加]符號 --")
        {
            var child_num = document.getElementsByClassName("child_num");
            var str = "第"+(child_num.length+1)+"個孩子的生日";
            var child = document.getElementById("child");
            child.innerHTML = child.innerHTML + '<div><div class="titleM child_num">'+str+'</div><div class="type3M birthday">'+picker.value+'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';
            picker.value = "--請選擇日期並按下[增加]符號 --";
        }
        */
        var child_num = document.getElementsByClassName("child_num");
        var str = "第"+(child_num.length+1)+"個孩子的生日";
        var child = document.getElementById("child");
        var date = year.value;
        if(month.value < 10)
        {
            date = date+"/0"+month.value;
        }else
        {
            date = date+"/"+month.value;
        }
        if(day.value < 10)
        {
            date = date+"/0"+day.value;
        }else
        {
            date = date+"/"+day.value;
        }
        child.innerHTML = child.innerHTML + '<div><div class="titleM child_num">'+str+'</div><div class="type3M birthday">'+date+'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';
        return false;
    }

    function del(item)
    {
        var itme_parent = item.parentNode.parentNode;
        itme_parent.parentNode.removeChild(itme_parent);
        var child_num = document.getElementsByClassName("child_num");
        for(var i=0; i<child_num.length; i++)
        {
            child_num[i].innerHTML = "第"+(i+1)+"個孩子的生日";
        }
        return false;
    }

    function birthday_collect()
    {
        var birthday = document.getElementsByClassName("birthday");
        var b_array = [];
        for(var i=0; i<birthday.length; i++)
        {
            //alert(birthday[i].innerHTML);
            b_array.push(birthday[i].innerHTML);
        }
        document.getElementById("birthday").value = JSON.stringify(b_array);
        //alert(document.getElementById("birthday").value);
    }
</script>
<script>
    $(function() {
        $( "#education_date" ).datepicker();
    });
</script>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>
