<!-- Ming content -->
<form action="?" method="post">
<div class="memberM">
    <h1>會員專區</h1>
    <div class="bottomM"> </div>
    <ul>
        <li>
            <div class="titleM"><span>*</span>姓名</div>
            <div class="typeM"><input placeholder="" type="text" name="name" value="<?php  if(!empty($user_data)){ echo  $user_data[0]["name"];}?>"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>帳號</div>
            <div class="typeM"><input placeholder="" type="text" value="<?php  if(!empty($user_data)){ echo  $user_data[0]["acc"];}?>" disabled></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>email</div>
            <div class="typeM"><input placeholder="E-mail" type="text" name="email" value="<?php  if(!empty($user_data)){ echo  $user_data[0]["email"];}?>"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>密碼</div>
            <div class="typeM"><input type="password" name="psd" placeholder="(若有需要更新密碼請輸入)" ></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>再次確認密碼</div>
            <div class="typeM"><input type="password" name="psd2" placeholder="(若有需要更新密碼請輸入)"></div>
        </li>
        <li>
            <div class="titleM"><span>*</span>年齡</div>
            <div class="typeM">
                <select name="age">
                    <?php  if(!empty($user_data)){
                        if($user_data[0]["age"] == 1)
                        {
                            echo ' <option value="1" selected>19 歲及以下 </option>';
                        }else
                        {
                            echo ' <option value="1" >19 歲及以下 </option>';
                        }
                        if($user_data[0]["age"] == 2)
                        {
                            echo ' <option value="2" selected>20 歲-24 歲 </option>';
                        }else
                        {
                            echo ' <option value="2" >20 歲-24 歲 </option>';
                        }
                        if($user_data[0]["age"] == 3)
                        {
                            echo ' <option value="3" selected>25 歲-29 歲</option>';
                        }else
                        {
                            echo ' <option value="3" >25 歲-29 歲</option>';
                        }
                        if($user_data[0]["age"] == 4)
                        {
                            echo ' <option value="4" selected>30 歲-34 歲 </option>';
                        }else
                        {
                            echo ' <option value="4" >30 歲-34 歲 </option>';
                        }
                        if($user_data[0]["age"] == 5)
                        {
                            echo ' <option value="5" selected>35 歲-39 歲 </option>';
                        }else
                        {
                            echo ' <option value="5" >35 歲-39 歲 </option>';
                        }
                        if($user_data[0]["age"] == 6)
                        {
                            echo ' <option value="6" selected>40 歲-44 歲 </option>';
                        }else
                        {
                            echo ' <option value="6" >40 歲-44 歲 </option>';
                        }
                        if($user_data[0]["age"] == 7)
                        {
                            echo ' <option value="7" selected>45 歲-49 歲 </option>';
                        }else
                        {
                            echo ' <option value="7" >45 歲-49 歲 </option>';
                        }
                        if($user_data[0]["age"] == 8)
                        {
                            echo ' <option value="8" selected>50 歲-54 歲</option>';
                        }else
                        {
                            echo ' <option value="8" >50 歲-54 歲</option>';
                        }
                        if($user_data[0]["age"] == 9)
                        {
                            echo ' <option value="9" selected>55 歲-59 歲 </option>';
                        }else
                        {
                            echo ' <option value="9" >55 歲-59 歲 </option>';
                        }
                        if($user_data[0]["age"] == 10)
                        {
                            echo ' <option value="10" selected>60 歲-64歲 </option>';
                        }else
                        {
                            echo ' <option value="10" >60 歲-64歲 </option>';
                        }
                        if($user_data[0]["age"] == 11)
                        {
                            echo ' <option value="11" selected>65 歲及以上</option>';
                        }else
                        {
                            echo ' <option value="11" >65 歲及以上</option>';
                        }
                    }?>
                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>居住地點</div>
            <div class="typeM">
                <select name="location">
                    <?php 
                        if(!empty($user_data[0]["location"]))
                        {
                            foreach($location as $item)
                            {
                                if($user_data[0]["location"] == $item["id"])
                                {
                                    echo '<option value='.$item["id"].' selected >'.$item["name"].'</option>';
                                }else
                                {
                                    echo '<option value='.$item["id"].'>'.$item["name"].'</option>';
                                }

                            }
                        }else
                        {
                            foreach($location as $item)
                            {
                                echo '<option value='.$item["id"].'>'.$item["name"].'</option>';
                            }
                        }

                    ?>

                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>性別</div>
            <div class="typeM">
                <select name="sex">
                    <?php  if(!empty($user_data)){
                        if($user_data[0]["sex"] == 1)
                        {
                            echo ' <option value="1" selected>男</option>';
                        }else
                        {
                            echo ' <option value="1" >男</option>';
                        }
                        if($user_data[0]["sex"] == 2)
                        {
                            echo ' <option value="2" selected>女</option>';
                        }else
                        {
                            echo ' <option value="2" >女</option>';
                        }
                    }?>
                </select>
            </div>
        </li>
        <li>
            <div class="titleM"><span>*</span>教育程度</div>
            <div class="typeM">
                <select name="edu">
                    <?php  if(!empty($user_data)){
                        if($user_data[0]["edu"] == 1)
                        {
                            echo ' <option value="1" selected>無或自修 </option>';
                        }else
                        {
                            echo ' <option value="1" >無或自修 </option>';
                        }
                        if($user_data[0]["edu"] == 2)
                        {
                            echo ' <option value="2" selected>自修或小學</option>';
                        }else
                        {
                            echo ' <option value="2" >自修或小學</option>';
                        }
                        if($user_data[0]["edu"] == 3)
                        {
                            echo ' <option value="3" selected>國中/初中/初職</option>';
                        }else
                        {
                            echo ' <option value="3" >國中/初中/初職</option>';
                        }
                        if($user_data[0]["edu"] == 4)
                        {
                            echo ' <option value="4" selected>高中/高職/士官學校</option>';
                        }else
                        {
                            echo ' <option value="4" >高中/高職/士官學校</option>';
                        }
                        if($user_data[0]["edu"] == 5)
                        {
                            echo ' <option value="5" selected>五專/二專/三專 /軍警專修班/軍警官學校</option>';
                        }else
                        {
                            echo ' <option value="5" >五專/二專/三專 /軍警專修班/軍警官學校</option>';
                        }
                        if($user_data[0]["edu"] == 6)
                        {
                            echo ' <option value="6" selected>大學（含技術學院與科技大學）</option>';
                        }else
                        {
                            echo ' <option value="6" >大學（含技術學院與科技大學）</option>';
                        }
                        if($user_data[0]["edu"] == 7)
                        {
                            echo ' <option value="7" selected>碩士</option>';
                        }else
                        {
                            echo ' <option value="7" >碩士</option>';
                        }
                        if($user_data[0]["edu"] == 8)
                        {
                            echo ' <option value="8" selected>博士</option>';
                        }else
                        {
                            echo ' <option value="8" >博士</option>';
                        }
                        if($user_data[0]["edu"] == 9)
                        {
                            echo ' <option value="9" selected>其他</option>';
                        }else
                        {
                            echo ' <option value="9" >其他</option>';
                        }
                    }?>
                </select>
            </div>
        </li>
        <li id="child">
            <?php 
            if(!empty($child))
            {
                foreach($child as $key=>$item)
                {
                    echo '<div><div class="titleM child_num">第'.($key+1).'個孩子的生日</div><div class="type3M birthday">'.$item["birthday"].'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';
                }
            }
            ?>
        </li>
        <!--
        <li>
            <div class="titleM">輸入孩子的生日</div>
            <div class="type3M"><input id="education_datepicker" value="--請選擇日期並按下[增加]符號 --"></div>
            <div class="type4M"><div onclick="add(this)"><button onclick="return false;">＋</button></div></div>
        </li>
        -->
        <li>
            <div class="titleM">選擇孩子的生日</div>

            <div class="typeM">
                <select id="year" style="width: 40%;">
                    <?php 
                    for($i=1950; $i<=2016; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'年</option>';
                    }
                    ?>
                </select>
                <select id="month" style="width: 28%;">
                    <?php 
                    for($i=1; $i<=12; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'月</option>';
                    }

                    ?>
                </select>
                <select id="day" style="width: 28%;">
                    <?php 
                    for($i=1; $i<=31; $i++)
                    {
                        echo ' <option value="'.$i.'" >'.$i.'日</option>';
                    }

                    ?>
                </select>
            </div>
            <div class="titleM">&nbsp;</div>
            <div class="type3M"><p style="color: red;">選擇生日後，請按右方[+]鍵儲存</p></div>
            <div class="type4M"><div onclick="add(this)"><button onclick="return false;">＋</button></div></div>
        </li>

        <li <?php  if($user_data[0]["mail_check"] == 1){ echo  'style="display:none;"';}?>>
            <div class="titleM">會員狀態</div>
            <div class="type5M" style="margin-top: 6px;">
                <?php if($user_data[0]["mail_check"] == 2)
                {
                    ?>
                    <span style="font-size: 15px; margin-right: 5px;" >已寄送確認信</span><button name="send_email" value="1">再次寄發確認信</button>
                <?php
                }else
                {
                    ?>
                    <span style="font-size: 15px; margin-right: 5px;" >非正式會員</span><button name="send_email" value="1">寄發正式會員確認信</button>
                <?php
                }?>

            </div>
        </li>
        <li <?php  if($user_data[0]["mail_check"] == 0 || $user_data[0]["mail_check"] == 2){ echo  'style="display:none;"';}?>>
            <div class="titleM">會員狀態</div>
            <div class="type5M" style="margin-top: 6px;">
               <span style="font-size: 15px; margin-right: 5px;" >正式會員</span>
            </div>
            <input type="hidden" name="mail_check" value="<?php echo $user_data[0]["mail_check"];?>">
        </li>
        <li <?php  if($user_data[0]["mail_check"] == 0 || $user_data[0]["mail_check"] == 2){ echo  'style="display:none;"';}?>>
            <div class="titleM">訂閱電子報</div>
            <div class="type5M">
                <label><input type="radio" name="edm" value="1" <?php  if(!empty($user_data) && $user_data[0]["edm"] == 1){ echo  "checked";}?>><p>是</p></label>
                <label><input type="radio" name="edm" value="2" <?php  if(!empty($user_data) && $user_data[0]["edm"] == 2){ echo  "checked";}?>><p>否<p></label>
            </div>
        </li>
        <li>
            <div class="titleM noneM">&nbsp;</div>
            <div class="type6M"><button name="save" value="1" onclick="birthday_collect()">儲存</button></div>
        </li>
    </ul>
    <input type="hidden" name="birthday" id="birthday" value="123">
</div>
    </form>
<!-- END content -->


		</div>
</div>
<script>
    function add(item)
    {
        var picker = document.getElementById("education_datepicker");
        var year = document.getElementById("year");
        var month = document.getElementById("month");
        var day = document.getElementById("day");
        /*
         if( picker.value != "--請選擇日期並按下[增加]符號 --")
         {
         var child_num = document.getElementsByClassName("child_num");
         var str = "第"+(child_num.length+1)+"個孩子的生日";
         var child = document.getElementById("child");
         child.innerHTML = child.innerHTML + '<div><div class="titleM child_num">'+str+'</div><div class="type3M birthday">'+picker.value+'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';
         picker.value = "--請選擇日期並按下[增加]符號 --";
         }
         */
        var child_num = document.getElementsByClassName("child_num");
        var str = "第"+(child_num.length+1)+"個孩子的生日";
        var child = document.getElementById("child");
        var date = year.value;
        if(month.value < 10)
        {
            date = date+"/0"+month.value;
        }else
        {
            date = date+"/"+month.value;
        }
        if(day.value < 10)
        {
            date = date+"/0"+day.value;
        }else
        {
            date = date+"/"+day.value;
        }
        child.innerHTML = child.innerHTML + '<div><div class="titleM child_num">'+str+'</div><div class="type3M birthday">'+date+'</div> <div class="type4M"><div onclick="del(this)"><button onclick="return false;">-</button></div></div></div>';

        return false;
    }

    function del(item)
    {
        var itme_parent = item.parentNode.parentNode;
        itme_parent.parentNode.removeChild(itme_parent);
        var child_num = document.getElementsByClassName("child_num");
        for(var i=0; i<child_num.length; i++)
        {
            child_num[i].innerHTML = "第"+(i+1)+"個孩子的生日";
        }

        return false;
    }

    function birthday_collect()
    {
        var birthday = document.getElementsByClassName("birthday");
        var b_array = [];
        //alert(birthday.length);
        for(var i=0; i<birthday.length; i++)
        {
            //alert(birthday[i].innerHTML);
            b_array.push(birthday[i].innerHTML);
        }
        document.getElementById("birthday").value = JSON.stringify(b_array);
        //alert(document.getElementById("birthday").value);
    }
</script>
<script>
    $(function() {
        $( "#education_date" ).datepicker();
    });
</script>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>