<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iCoparenting" />
    <title>iCoparenting</title>
    <link href="<?php echo $base_url;?>index.php" rel="canonical" />
    <link href="<?php echo $base_url;?>assets_view/indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="<?php echo $base_url;?>assets_view/index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/reset.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/css/tabs.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />
    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/system/js/caption.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/ch/js/template.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/respond.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>

    <!-- Ming -->
    <script src="<?php echo $base_url;?>assets_view/js/jquery-1.8.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/jquery-ui.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/action.js"></script>
    <script>
        $(function() {
            $( "#education_datepicker" ).datepicker();
        });
    </script>
    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/html5shiv.js"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/html5.js"></script>
    <![endif]-->


</head>
<body class="epaperM">
<div class="epaperContentM">
    <a href="" id="goTop"><img src="<?php echo $base_url;?>assets_view/images/top.png"></a>
    <header>
        <div class="headerA">
            <h1>
                <img src="<?php echo $base_url;?>assets_view/images/logoWords.png" alt="和樂共識親">
            </h1>
            <div class="shareIcons">
                <a href="http://twitter.com/share?text=i%20CoParenting&url=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?id='.$id.'&theme='.$theme);?>" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconT.png" alt="">
                </a>
                <a href="http://www.facebook.com/share.php?u=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?id='.$id.'&theme='.$theme);?>" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconF.png" alt="">
                </a>
                <a href="https://plus.google.com/share?url=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?id='.$id.'&theme='.$theme);?>" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconG.png" alt="">
                </a>
                <a href="http://www.plurk.com/?qualifier=shares&status=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?id='.$id.'&theme='.$theme);?>" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconP.png" alt="">
                </a>
                <a href="http://v.t.sina.com.cn/share/share.php?url=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?id='.$id.'&theme='.$theme);?>" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconW.png" alt="">
                </a>
            </div>
					<div class="navMobile">
						<img src="<?php echo $base_url;?>assets_view/images/navMBM.png">
        </div>
				</div>
				<nav class="navMobileOpen">
					<a href="">育兒預備</a>
					<a href="">保保的健康成長</a>
					<a href="">教養孩子有辦法</a>
					<a href="">爸媽加油站</a>
					<a href="">各期電子報</a>
					<div class="shareIconsM">
						<a href="" target="_blank">
							<img src="<?php echo $base_url;?>assets_view/images/iconT.png" alt="">
						</a>
						<a href="" target="_blank">
							<img src="<?php echo $base_url;?>assets_view/images/iconF.png" alt="">
						</a>
						<a href="" target="_blank">
							<img src="<?php echo $base_url;?>assets_view/images/iconG.png" alt="">
						</a>
						<a href="" target="_blank">
							<img src="<?php echo $base_url;?>assets_view/images/iconP.png" alt="">
						</a>
						<a href="" target="_blank">
							<img src="<?php echo $base_url;?>assets_view/images/iconW.png" alt="">
						</a>
					</div>
				</nav>
        <div class="headerB">
            <a href="">
                <img src="<?php echo $base_url;?>assets_view/images/logoM.png">
            </a>
            <nav class="epaperNav">
                <a href="<?php echo $home."育兒預備/傳家寶貝";?>" style="font-weight: bold;">育兒預備</a>
                <a href="<?php echo $home."教養孩子有方法/育兒困擾";?>" style="font-weight: bold;">教養孩子有辦法</a>
                <a href="<?php echo $home."寶寶的健康成長/寶寶的出生紀錄";?>" style="font-weight: bold;">寶寶的健康成長</a>
                <a href="<?php echo $home."爸媽加油站/育兒的甜蜜與負擔";?>" style="font-weight: bold;">爸媽加油站</a>
                <a href="<?php echo $base_url."index.php/edm_print";?>" style="font-weight: bold;">各期電子報</a>
            </nav>
        </div>
        <div class="headerC">
            <div class="headerCa">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if(isset($edm_main[0]["print_img"])){echo "print_".$edm_main[0]["print_img"];}else{ echo "print_1";}?>.png">
                <p><?php if(isset($edm_main[0]["print_year"])){echo $edm_main[0]["print_year"];}?></p>
            </div>
            <div class="headerCb">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 1){echo "plantA_1";}else{ echo "plantA_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 2){echo "plantB_1";}else{ echo "plantB_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 3){echo "plantC_1";}else{ echo "plantC_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 4){echo "plantD_1";}else{ echo "plantD_0";}?>.png">
            </div>
            <div class="headerCc">
                <img src="<?php echo $base_url;?>assets_view/images/haederAnimal.png">
                <p><span style=" font-size: 22px; font-weight:bold;"><?php echo $edm_main[0]["title"];?></span></p>
            </div>
        </div>
    </header>
    <section class="bannerM">
        <img src="<?php echo $base_url;?>upload/<?php echo $edm_main[0]["banner"];?>">
    </section>
    <section class="epaperLFM">
        <section class="epaperListM">
            <ul>
                <li class="gfamily"><a href="#div1">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gfamily.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[1]["list_title"];?></p>
                                <p><?php echo $content[1]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[1]["list_img"];?>">
                        </div>
                    </a>
                </li>
                <li class="gcouple"><a href="#div2">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gteach.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[2]["list_title"];?></p>
                                <p><?php echo $content[2]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[2]["list_img"];?>">
                        </div>
                    </a>
                </li>
                <li class="gteach"><a href="#div3">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gcouple.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[3]["list_title"];?></p>
                                <p><?php echo $content[3]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[3]["list_img"];?>">
                        </div>
                    </a>
                </li>
            </ul>
        </section>
        <footer>
            <img src="<?php echo $base_url;?>assets_view/images/logoB.png">
        </footer>
    </section>
</div>
<div style="height: 10px;"></div>
<?php if(!empty($content) && $content[1]["type"] != 4 && $content[1]["type"] != 5) {?>
<div class="epaperDetailM">
    <a name="div1"></a>
    <h1>
        <img src="<?php echo $base_url;?>assets_view/images/familyDetailT.png" alt="好教養">
    </h1>
    <section class="epaperDetailA">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
    </section>
    <section class="epaperDetailB">
        <h2><?php if(!empty($content)){echo $content[1]["title"];}?></h2>
        <?php if(!empty($content) && $content[1]["type"] == 3){?>
            <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>"></p>
        <?php }?>
        <p>
            <?php if(!empty($content) && ($content[1]["type"] == 1 || $content[1]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>"  style="float:<?php if($content[1]["type"] == 1){echo "left";}else{echo "right";} ?>;"><?php }?><?php if(!empty($content)){echo $content[1]["content"];}?>
        </p>
        <div class="epaperDetailBa" style="padding-left: 10px;">
            <p style="padding-top: 20px;"><?php if(!empty($content)){echo $content[1]["suggestion"];}?></p>
            <div class="epaperDetailBaPast">
                <img src="<?php echo $base_url;?>assets_view/images/past.png">
            </div>
        </div>
    </section>
    <section class="epaperDetailC">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
    </section>
    <section class="epaperDetailBottom">
        <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
    </section>
</div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div1"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/familyDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[1]["title"];}?></h2>
            <div class="epaperDetailBL">

                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[1]["title2"];}?></h3>
                <p>
                    <?php if($content[1]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[1]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[1]["title3"];}?></h3>
                <p>
                    <?php if($content[1]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[1]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR" style="padding-left: 10px;">
                <h2></h2>
                <p>
                    <?php if($content[1]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[1]["suggestion"];}?>
                </p>
            </div>
        </section>

        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>
<br>
<?php if(!empty($content) && $content[2]["type"] != 4  && $content[2]["type"] != 5) {?>
<div class="epaperDetailM">
    <a name="div2"></a>
    <h1>
        <img src="<?php echo $base_url;?>assets_view/images/teachDetailT.png" alt="好教養">
    </h1>
    <section class="epaperDetailA">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
    </section>
    <section class="epaperDetailB">
        <h2><?php if(!empty($content)){echo $content[2]["title"];}?></h2>
        <?php if(!empty($content) && $content[2]["type"] == 3){?>
            <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>"></p>
        <?php }?>
        <p>
           <?php if(!empty($content) && ($content[2]["type"] == 1 || $content[2]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>"  style="float:<?php if($content[2]["type"] == 1){echo "left";}else{echo "right";} ?>;"><?php }?><?php if(!empty($content)){echo $content[2]["content"];}?>
        </p>
        <div class="epaperDetailBa" style="padding-left: 10px;">
            <p style="padding-top: 20px;"><?php if(!empty($content)){echo $content[2]["suggestion"];}?></p>
            <div class="epaperDetailBaPast">
                <img src="<?php echo $base_url;?>assets_view/images/past.png">
            </div>
        </div>
    </section>
    <section class="epaperDetailC">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
    </section>
    <section class="epaperDetailBottom">
        <img src="<?php echo $base_url;?>assets_view/images/coupleDetailB.png">
    </section>
</div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div2"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[2]["title"];}?></h2>
            <div class="epaperDetailBL">

                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title2"];}?></h3>
                <p>
                    <?php if($content[2]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[2]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title3"];}?></h3>
                <p>
                    <?php if($content[2]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[2]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR" style="padding-left: 10px;">
                <h2></h2>
                <p>
                    <?php if($content[2]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[2]["suggestion"];}?>
                </p>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>
<br>
<?php if(!empty($content) && $content[3]["type"] != 4  && $content[3]["type"] != 5) {?>
<div class="epaperDetailM">
    <a name="div3"></a>
    <h1>
        <img src="<?php echo $base_url;?>assets_view/images/coupleDetailT.png" alt="好教養">
    </h1>
    <section class="epaperDetailA">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
    </section>
    <section class="epaperDetailB">
        <h2><?php if(!empty($content)){echo $content[3]["title"];}?></h2>
        <?php if(!empty($content) && $content[3]["type"] == 3){?>
            <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>"></p>
        <?php }?>
        <p>
            <?php if(!empty($content) && ($content[3]["type"] == 1 || $content[3]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>"  style="float:<?php if($content[3]["type"] == 1){echo "left";}else{echo "right";} ?>;"><?php }?><?php if(!empty($content)){echo $content[3]["content"];}?>
        </p>
        <div class="epaperDetailBa" style="padding-left: 10px;">
            <p style="padding-top: 20px;"><?php if(!empty($content)){echo $content[3]["suggestion"];}?></p>
            <div class="epaperDetailBaPast">
                <img src="<?php echo $base_url;?>assets_view/images/past.png">
            </div>
        </div>
    </section>
    <section class="epaperDetailC">
        <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
    </section>
    <section class="epaperDetailBottom">
        <img src="<?php echo $base_url;?>assets_view/images/familyDetailB.png">
    </section>
</div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div3"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/coupleDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[3]["title"];}?></h2>
            <div class="epaperDetailBL">
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title2"];}?></h3>
                <p>
                  <?php if($content[3]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[3]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title3"];}?></h3>
                <p>
                    <?php if($content[3]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[3]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR"  style="padding-left: 10px;">
                <h2></h2>
                <p>
                    <?php if($content[3]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[3]["suggestion"];}?>
                </p>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url;?>assets_view/js/app.js"></script>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>