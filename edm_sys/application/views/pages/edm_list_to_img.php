<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iMyfamily" />
    <title>iMyfamily</title>
    <link href="<?php echo $base_url;?>assets_view/index.html" rel="canonical" />
    <link href="<?php echo $base_url;?>assets_view/indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="<?php echo $base_url;?>assets_view/index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/reset.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/css/tabs.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />
    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/system/js/caption.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/ch/js/template.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/respond.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>

    <!-- Ming -->
    <script src="<?php echo $base_url;?>assets_view/js/jquery-1.8.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/jquery-ui.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/action.js"></script>
    <script>
        $(function() {
            $( "#education_datepicker" ).datepicker();
        });
    </script>
    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/html5shiv.js"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo $base_url;?>assets_view/js/html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo $base_url;?>assets_view/js/jquery.plugin.html2canvas.js"></script>
    <!-- -->



</head>
<body class="epaperM">

<div style="text-align: center;">
    <form action="?" method="post" enctype="multipart/form-data">

        <table style="width: 80%; margin: 0 auto;">
            <tr style="border-style: solid;">
                <td>年紀</td>
                <td>預計寄送量</td>
                <td>寄送失敗量</td>
                <td>寄送時間</td>
                <td>功能</td>
            </tr>
            <tr style="border-style: solid;">
                <td>0-2歲</td>
                <td><?php echo count($mamber_count[1]);?></td>
                <td>0</td>
                <td> <?php
                    if($edm_main[0]["publish"] == "0000-00-00")
                    {
                        ?>
                            尚未寄送
                        <?php
                    }else
                    {
                        echo $edm_main[0]["publish"];
                    }
                    ?></td>
                <td>
                    <?php
                    if($edm_main[0]["publish"] == "0000-00-00") {
                        if($theme == 1)
                        {
                          ?>
                            <button name="send" value="<?php echo $theme;?>"  onclick="capture();">轉存圖片並且進入排程</button>
                    <?php
                        }else
                        {
                            ?>
                            <button name="old" value="1"  onclick="capture();">預覽畫面</button>
                            <?php
                        }
                    }
                    ?>

                </td>
            </tr>
            <tr style="border-style: solid;">
                <td>2-3歲</td>
                <td><?php echo count($mamber_count[2]);?></td>
                <td>0</td>
                <td><?php
                    if($edm_main[0]["publish"] == "0000-00-00")
                    {
                        ?>
                        尚未寄送
                        <?php
                    }else
                    {
                        echo $edm_main[0]["publish"];
                    }
                    ?></td>
                <td>
                    <?php
                    if($edm_main[0]["publish"] == "0000-00-00") {
                        if ($theme == 2) {
                            ?>
                            <button name="send" value="<?php echo $theme; ?>" onclick="capture();">轉存圖片並且進入排程</button>
                            <?php
                        } else {
                            ?>
                            <button name="old" value="2" onclick="capture();">預覽畫面</button>
                            <?php
                        }
                    }
                    ?>

                </td>
            </tr>
            <tr style="border-style: solid;">
                <td>4-5歲</td>
                <td><?php echo count($mamber_count[3]);?></td>
                <td>0</td>
                <td><?php
                    if($edm_main[0]["publish"] == "0000-00-00")
                    {
                        ?>
                        尚未寄送
                        <?php
                    }else
                    {
                        echo $edm_main[0]["publish"];
                    }
                    ?></td>
                <td>
                    <?php
                    if($edm_main[0]["publish"] == "0000-00-00") {
                        if ($theme == 3) {
                            ?>
                            <button name="send" value="<?php echo $theme; ?>" onclick="capture();">轉存圖片並且進入排程</button>
                            <?php
                        } else {
                            ?>
                            <button name="old" value="3" onclick="capture();">預覽畫面</button>
                            <?php
                        }
                    }
                    ?>

                </td>
            </tr>
            <tr style="border-style: solid;">
                <td>6-7歲</td>
                <td><?php echo count($mamber_count[4]);?></td>
                <td>0</td>
                <td><?php
                    if($edm_main[0]["publish"] == "0000-00-00")
                    {
                        ?>
                        尚未寄送
                        <?php
                    }else
                    {
                        echo $edm_main[0]["publish"];
                    }
                    ?></td>
                <td>
                    <?php
                    if($edm_main[0]["publish"] == "0000-00-00") {
                        if ($theme == 4) {
                            ?>
                            <button name="send" value="<?php echo $theme; ?>" onclick="capture();">轉存圖片並且進入排程</button>
                            <?php
                        } else {
                            ?>
                            <button name="old" value="4" onclick="capture();">預覽畫面</button>
                            <?php
                        }
                    }
                    ?>
                </td>
            </tr>
        </table>
        <p>目前您預覽的是：
            <?php
                if($theme == 1)
                {
                    echo '0-2歲 EDM內容';
                }else if($theme == 2)
                {
                    echo '2-3歲 EDM內容';
                }else if($theme == 3)
                {
                    echo '4-5歲 EDM內容';
                }else if($theme == 4)
                {
                    echo '7歲以上 EDM內容';
                }
            ?>
        </p>
        <p><button name="back" value="1"  onclick="javascript:location.href='<?=$base_url?>index.php/backend/newsletter_list_send'; return false;">返回列表</button></p>
        <input type="hidden" name="img_val" id="img_val" value="" />
        <input type="hidden" name="id" value="<?php echo $id;?>" />
        <input type="hidden" name="theme" value="<?php echo $theme;?>" />
    </form>
</div>
<div  id="target">
    <div class="epaperContentM">
        <header>
            <div class="headerA">
                <h1>
                    <img src="<?php echo $base_url;?>assets_view/images/logoWords.png" alt="和樂共識親">
                </h1>
                <div class="shareIcons">
                    <a href="" target="_blank">
                        <img src="<?php echo $base_url;?>assets_view/images/iconT.png" alt="">
                    </a>
                    <a href="" target="_blank">
                        <img src="<?php echo $base_url;?>assets_view/images/iconF.png" alt="">
                    </a>
                    <a href="" target="_blank">
                        <img src="<?php echo $base_url;?>assets_view/images/iconG.png" alt="">
                    </a>
                    <a href="" target="_blank">
                        <img src="<?php echo $base_url;?>assets_view/images/iconP.png" alt="">
                    </a>
                    <a href="" target="_blank">
                        <img src="<?php echo $base_url;?>assets_view/images/iconW.png" alt="">
                    </a>
                </div>
            </div>
            <div class="headerB">
                <a href="">
                    <img src="<?php echo $base_url;?>assets_view/images/logoM.png">
                </a>
                <nav class="epaperNav">
                    <a href="<?php echo $home."育兒預備/生育價值";?>" style="font-weight: bold;">育兒預備</a>
                    <a href="<?php echo $home."教養孩子有方法/育兒困擾";?>" style="font-weight: bold;">教養孩子有辦法</a>
                    <a href="<?php echo $home."寶寶的健康成長/寶寶的出生紀錄";?>" style="font-weight: bold;">寶寶的健康成長</a>
                    <a href="<?php echo $home."爸媽加油站/育兒的甜蜜與負擔";?>" style="font-weight: bold;">爸媽加油站</a>
                    <a href="<?php echo $base_url."index.php/edm_print";?>" style="font-weight: bold;">各期電子報</a>
                </nav>
            </div>
            <div class="headerC">
                <div class="headerCa">
                    <img src="<?php echo $base_url;?>assets_view/images/<?php if(isset($edm_main[0]["print_img"])){echo "print_".$edm_main[0]["print_img"];}else{ echo "print_1";}?>.png">
                    <p><?php if(isset($edm_main[0]["print_year"])){echo $edm_main[0]["print_year"];}?></p>
                </div>
                <div class="headerCb">
                    <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 1){echo "plantA_1";}else{ echo "plantA_0";}?>.png">
                    <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 2){echo "plantB_1";}else{ echo "plantB_0";}?>.png">
                    <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 3){echo "plantC_1";}else{ echo "plantC_0";}?>.png">
                    <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 4){echo "plantD_1";}else{ echo "plantD_0";}?>.png">
                </div>
                <div class="headerCc">
                    <img src="<?php echo $base_url;?>assets_view/images/haederAnimal.png">
                    <p><span><?php echo $edm_main[0]["title"];?></span></p>
                </div>
            </div>
        </header>
        <section class="bannerM">
            <img src="<?php echo $base_url;?>upload/<?php echo $edm_main[0]["banner"];?>">
        </section>
        <section class="epaperLFM">
            <section class="epaperListM">
                <ul>
                    <li class="gfamily"><a href="" target="_blank">
                            <div class="epaperListA">
                                <img src="<?php echo $base_url;?>assets_view/images/gfamily.png">
                            </div>
                            <div class="epaperListB">
                                <div>
                                    <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[1]["list_title"];?></p>
                                    <?php
                                        $length = floor(mb_strlen($content[1]["list_intro"])/31 + 1);
                                        for($i=0; $i<$length; $i++)
                                        {
                                            echo "<p>".mb_substr($content[1]["list_intro"],(31*$i),31, 'UTF-8')."</p>";

                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="epaperListC">
                                <img src="<?php echo $base_url;?>upload/<?php echo $content[1]["list_img"];?>">
                            </div>
                        </a>
                    </li>
                    <li class="gcouple"><a href="" target="_blank">
                            <div class="epaperListA">
                                <img src="<?php echo $base_url;?>assets_view/images/gcouple.png">
                            </div>
                            <div class="epaperListB">
                                <div>
                                    <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[2]["list_title"];?></p>
                                    <?php
                                    $length = floor(mb_strlen($content[2]["list_intro"])/31 + 1);
                                    for($i=0; $i<$length; $i++)
                                    {
                                        echo "<p>".mb_substr($content[2]["list_intro"],(31*$i),31, 'UTF-8')."</p>";

                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="epaperListC">
                                <img src="<?php echo $base_url;?>upload/<?php echo $content[2]["list_img"];?>">
                            </div>
                        </a>
                    </li>
                    <li class="gteach"><a href="" target="_blank">
                            <div class="epaperListA">
                                <img src="<?php echo $base_url;?>assets_view/images/gteach.png">
                            </div>
                            <div class="epaperListB">
                                <div>
                                    <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[3]["list_title"];?></p>
                                    <?php
                                    $length = floor(mb_strlen($content[3]["list_intro"])/31 + 1);
                                    for($i=0; $i<$length; $i++)
                                    {
                                        echo "<p>".mb_substr($content[3]["list_intro"],(31*$i),31, 'UTF-8')."</p>";

                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="epaperListC">
                                <img src="<?php echo $base_url;?>upload/<?php echo $content[3]["list_img"];?>">
                            </div>
                        </a>
                    </li>
                </ul>
            </section>
            <footer>
                <img src="<?php echo $base_url;?>assets_view/images/logoB.png">
            </footer>
        </section>
    </div>
</div>
<script>
    function capture() {
        $('#target').html2canvas({
            onrendered: function (canvas) {
                //Set hidden field's value to image data (base-64 string)
                $('#img_val').val(canvas.toDataURL("image/png"));
                //Submit the form manually
                document.getElementById("myForm").submit();
            }
        });
    }

</script>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>