<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iMyfamily" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="/templates/system/js/html5shiv.js"></script>
    <script src="/media/jui/js/html5.js"></script>
    <![endif]-->


</head>
<body style="width:980px;background:#fff;">
<table cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tr>
        <td colspan="3"><img src="<?echo $base_url;?>assets_view/images/headPdf1.jpg" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:center;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listAM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="border:1px dashed #7d8e48; vertical-align:top;">
            <p style="color: #506816;">三大原則，和孩子好好溝通</p>
            <p style="color: #1f1f1f;">1. 先緩和「情緒」，再討論「事情」<br>
                2. 先表達「關心」，再詢問「原因」<br>
                3. 先「聽」再「說」，讓孩子把話說完<br>
                育兒100問，讓你教養孩子有辦法！</p>
        </td>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:left;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listApicM.png" border="0" style="display:block;">
        </td>
    </tr>
    <tr>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:center;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listBM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="border:1px dashed #7d8e48; vertical-align:top;">
            <p style="color: #506816;">三大原則，和孩子好好溝通</p>
            <p style="color: #1f1f1f;">1. 先緩和「情緒」，再討論「事情」<br>
                2. 先表達「關心」，再詢問「原因」<br>
                3. 先「聽」再「說」，讓孩子把話說完<br>
                育兒100問，讓你教養孩子有辦法！</p>
        </td>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:left;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listApicM.png" border="0" style="display:block;">
        </td>
    </tr>
    <tr>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:center;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listCM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="border:1px dashed #7d8e48; vertical-align:top;">
            <p style="color: #506816;">三大原則，和孩子好好溝通</p>
            <p style="color: #1f1f1f;">1. 先緩和「情緒」，再討論「事情」<br>
                2. 先表達「關心」，再詢問「原因」<br>
                3. 先「聽」再「說」，讓孩子把話說完<br>
                育兒100問，讓你教養孩子有辦法！</p>
        </td>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:left;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listApicM.png" border="0" style="display:block;">
        </td>
    </tr>
    <tr>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:center;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listDM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="border:1px dashed #7d8e48; vertical-align:top;">
            <p style="color: #506816;">三大原則，和孩子好好溝通</p>
            <p style="color: #1f1f1f;">1. 先緩和「情緒」，再討論「事情」<br>
                2. 先表達「關心」，再詢問「原因」<br>
                3. 先「聽」再「說」，讓孩子把話說完<br>
                育兒100問，讓你教養孩子有辦法！</p>
        </td>
        <td width="30%" style="border:1px dashed #7d8e48; text-align:left;">
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listApicM.png" border="0" style="display:block;">
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <!-- Detail1 -->
    <tr>
        <td colspan="3"><img src="<?echo $base_url;?>assets_view/images/newsMPdf.jpg" border="0" style="display:block;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff;border:1px dashed #7d8e48; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;">寶寶發展里程碑</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="margin-left:20px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="margin-top:20px;padding:0 15px;color: #7aa21e;margin-bottom: 15px;letter-spacing: 2px;">發展沒有關鍵期</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr style="background: #afd55c;padding: 15px 25px;margin-top: 15px;">
                    <td colspan="3">
                        <table>
                            <tr>
                                <td><p style="margin-top:20px;padding:0 15px;color: #e86398;margin-bottom: 15px;letter-spacing: 2px;">從容接受寶寶探索世界的方式</p></td>
                            </tr>
                            <tr>
                                <td><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <!-- Detail2 -->
    <tr>
        <td colspan="3"><img src="<?echo $base_url;?>assets_view/images/familyMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff;border:1px dashed #7d8e48; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;">寶寶發展里程碑</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="margin-left:20px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="margin-top:20px;padding:0 15px;color: #7aa21e;margin-bottom: 15px;letter-spacing: 2px;">發展沒有關鍵期</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr style="background: #afd55c;padding: 15px 25px;margin-top: 15px;">
                    <td colspan="3">
                        <table>
                            <tr>
                                <td><p style="margin-top:20px;padding:0 15px;color: #e86398;margin-bottom: 15px;letter-spacing: 2px;">從容接受寶寶探索世界的方式</p></td>
                            </tr>
                            <tr>
                                <td><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <!-- Detail3 -->
    <tr>
        <td colspan="3"><img src="<?echo $base_url;?>assets_view/images/teachMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff;border:1px dashed #7d8e48; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;">寶寶發展里程碑</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="width:100%;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="margin-top:20px;padding:0 15px;color: #7aa21e;margin-bottom: 15px;letter-spacing: 2px;">發展沒有關鍵期</p></td>
                </tr>
                <tr>
                    <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                </tr>
                <tr style="background: #afd55c;padding: 15px 25px;margin-top: 15px;">
                    <td colspan="3">
                        <table>
                            <tr>
                                <td><p style="margin-top:20px;padding:0 15px;color: #e86398;margin-bottom: 15px;letter-spacing: 2px;">從容接受寶寶探索世界的方式</p></td>
                            </tr>
                            <tr>
                                <td><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <!-- Detail4 -->
    <tr>
        <td colspan="3"><img src="<?echo $base_url;?>assets_view/images/wifeMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#fff;border:1px dashed #7d8e48; margin:0;padding:0;">
                <tr>
                    <td colspan="3">
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;">寶寶發展里程碑</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width:70%;word-break:break-all;">
                        <table>
                            <tr>
                                <td>
                                    <p style="margin-top:20px;padding:0 15px;color: #7aa21e;margin-bottom: 15px;letter-spacing: 2px;">發展沒有關鍵期</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="display:block;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="margin-top:20px;padding:0 15px;color: #7aa21e;margin-bottom: 15px;letter-spacing: 2px;">發展沒有關鍵期</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="display:block;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width:30%;vertical-align:top;background: #afd55c;padding: 15px 25px;margin-top: 15px;word-break:break-all;">
                        <table>
                            <tr>
                                <td>
                                    <p style="margin-top:20px;padding:0 15px;color: #012d2c;margin-bottom: 15px;letter-spacing: 2px;text-align: center;">發展沒有關鍵期</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?echo $base_url;?>assets_view/images/detailPic.png" border="0" style="display:block;max-width: 100%;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><p style="color: #666666;padding:0 15px;">沒文字可以複製沒文字可以複製沒文字</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3" style="text-align:center;">
            <img width="50px" src="<?echo $base_url;?>assets_view/images/logoM.png">
            和樂共親職
            <span>&#174;</span>
        </td>
    </tr>
</table>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>