<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iCoparenting" />
    <title>iCoparenting</title>
    <link href="<?php echo $base_url;?>index.php" rel="canonical" />
    <link href="<?php echo $base_url;?>assets_view/indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="<?php echo $base_url;?>assets_view/index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style2.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->


    <!-- Ming -->

    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->




</head>
<body class="epaperM" style="font-family: PMingLiU; font-weight: normal;">
<div class="epaperContentM">
    <a href="" id="goTop"><img src="<?php echo $base_url;?>assets_view/images/top.png"></a>
    <header>
        <div class="headerA">
            <h1>
                <img src="<?php echo $base_url;?>assets_view/images/logoWords.png" alt="和樂共識親">
            </h1>
            <div class="shareIcons">
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconT.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconF.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconG.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconP.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconW.png" alt="">
                </a>
            </div>
            <div class="navMobile">
                <img src="<?php echo $base_url;?>assets_view/images/navMBM.png">
            </div>
        </div>
        <nav class="navMobileOpen">
            <a href="">育兒預備</a>
            <a href="">保保的健康成長</a>
            <a href="">教養孩子有辦法</a>
            <a href="">爸媽加油站</a>
            <a href="">各期電子報</a>
            <div class="shareIconsM">
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconT.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconF.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconG.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconP.png" alt="">
                </a>
                <a href="" target="_blank">
                    <img src="<?php echo $base_url;?>assets_view/images/iconW.png" alt="">
                </a>
            </div>
        </nav>
        <div class="headerB">
            <a href="">
                <img src="<?php echo $base_url;?>assets_view/images/logoM.png">
            </a>
            <nav class="epaperNav">
                <a href="<?php echo "https://icoparenting.moe.edu.tw/%E8%82%B2%E5%85%92%E9%A0%90%E5%82%99/%E5%82%B3%E5%AE%B6%E5%AF%B6%E8%B2%9D";?>" style="font-weight: bold;">育兒預備</a>
                <a href="<?php echo "https://icoparenting.moe.edu.tw/%E6%95%99%E9%A4%8A%E5%AD%A9%E5%AD%90%E6%9C%89%E6%96%B9%E6%B3%95/%E8%82%B2%E5%85%92%E5%9B%B0%E6%93%BE";?>" style="font-weight: bold;">教養孩子有辦法</a>
                <a href="<?php echo "https://icoparenting.moe.edu.tw/%E5%AF%B6%E5%AF%B6%E7%9A%84%E5%81%A5%E5%BA%B7%E6%88%90%E9%95%B7/%E5%AF%B6%E5%AF%B6%E7%9A%84%E5%87%BA%E7%94%9F%E7%B4%80%E9%8C%84";?>" style="font-weight: bold;">寶寶的健康成長</a>
                <a href="<?php echo "https://icoparenting.moe.edu.tw/%E7%88%B8%E5%AA%BD%E5%8A%A0%E6%B2%B9%E7%AB%99/%E8%82%B2%E5%85%92%E7%9A%84%E7%94%9C%E8%9C%9C%E8%88%87%E8%B2%A0%E6%93%94";?>" style="font-weight: bold;">爸媽加油站</a>
                <a href="<?php echo "https://icoparenting.moe.edu.tw/%E9%9B%BB%E5%AD%90%E5%A0%B1/%E5%90%84%E6%9C%9F%E9%9B%BB%E5%AD%90%E5%A0%B1";?>" style="font-weight: bold;">各期電子報</a>
            </nav>
        </div>
        <div class="headerC">
            <div class="headerCa">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if(isset($edm_main[0]["print_img"])){echo "print_".$edm_main[0]["print_img"];}else{ echo "print_1";}?>.png">
                <p><?php if(isset($edm_main[0]["print_year"])){echo $edm_main[0]["print_year"];}?></p>
            </div>
            <div class="headerCb">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 1){echo "plantA_1";}else{ echo "plantA_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 2){echo "plantB_1";}else{ echo "plantB_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 3){echo "plantC_1";}else{ echo "plantC_0";}?>.png">
                <img src="<?php echo $base_url;?>assets_view/images/<?php if($theme == 4){echo "plantD_1";}else{ echo "plantD_0";}?>.png">
            </div>
            <div class="headerCc">
                <img src="<?php echo $base_url;?>assets_view/images/haederAnimal.png">
                <p><span style=" font-size: 22px; font-weight:bold;"><?php echo $edm_main[0]["title"];?></span></p>
            </div>
        </div>
    </header>
    <section class="bannerM">
        <img src="<?php echo $base_url;?>upload/<?php echo $edm_main[0]["banner"];?>">
    </section>
    <section class="epaperLFM">
        <section class="epaperListM">
            <ul>
                <li class="gfamily">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gfamily.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[1]["list_title"];?></p>
                                <p><?php echo $content[1]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[1]["list_img"];?>">
                        </div>

                </li>
                <li class="gcouple">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gteach.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[2]["list_title"];?></p>
                                <p><?php echo $content[2]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[2]["list_img"];?>">
                        </div>

                </li>
                <li class="gteach">
                        <div class="epaperListA">
                            <img src="<?php echo $base_url;?>assets_view/images/gcouple.png">
                        </div>
                        <div class="epaperListB">
                            <div>
                                <p class="epaperListTitle" style="font-weight: bold;"><?php echo $content[3]["list_title"];?></p>
                                <p><?php echo $content[3]["list_intro"];?></p>
                            </div>
                        </div>
                        <div class="epaperListC">
                            <img src="<?php echo $base_url;?>upload/<?php echo $content[3]["list_img"];?>">
                        </div>

                </li>
            </ul>
        </section>
        <footer>
            <img src="<?php echo $base_url;?>assets_view/images/logoB.png">
        </footer>
    </section>
</div>
<div style="height: 10px;"></div>
<?php if(!empty($content) && $content[1]["type"] != 4 && $content[1]["type"] != 5) {?>
    <div class="epaperDetailM">
        <a name="div1"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/familyDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[1]["title"];}?></h2>
            <?php if(!empty($content) && $content[1]["type"] == 3){?>
                <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>"></p>
            <?php }?>
            <p style="padding-right: 30px;">
                <?php if(!empty($content) && ($content[1]["type"] == 1 || $content[1]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>"  style="float:<?php if($content[1]["type"] == 1){echo "left";}else{echo "right";} ?>;padding-left: 0px; margin-right: 10px;"><?php }?><?php if(!empty($content)){echo $content[1]["content"];}?>
            </p>
            <div class="epaperDetailBa" style="padding-left: 10px;">
                <p style="padding-right: 30px;padding-top: 20px;"><?php if(!empty($content)){echo $content[1]["suggestion"];}?></p>
                <div class="epaperDetailBaPast">
                    <img src="<?php echo $base_url;?>assets_view/images/past.png">
                </div>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div1"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/familyDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[1]["title"];}?></h2>
            <div class="epaperDetailBL">

                <?php if(!empty($content)){echo "<h3 style=\"font-size: 20px;color: #88ba7d; font-weight: bold;\">".$content[1]["title2"]."</h3>";}?>
                <p style="padding-right: 30px;">
                    <?php if($content[1]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[1]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[1]["title3"];}?></h3>
                <p style="padding-right: 30px;">
                    <?php if($content[1]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[1]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR" style="padding-left: 10px;">
                <h2></h2>
                <p style="padding-right: 10px;">
                    <?php if($content[1]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[1]["suggestion"];}?>
                </p>
            </div>
        </section>

        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>
<br>
<?php if(!empty($content) && $content[2]["type"] != 4 && $content[2]["type"] != 5) {?>
    <div class="epaperDetailM">
        <a name="div2"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[2]["title"];}?></h2>
            <?php if(!empty($content) && $content[2]["type"] == 3){?>
                <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>"></p>
            <?php }?>
            <p style="padding-right: 30px;">
                <?php if(!empty($content) && ($content[2]["type"] == 1 || $content[2]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>"  style="float:<?php if($content[2]["type"] == 1){echo "left";}else{echo "right";} ?>;padding-left: 0px; margin-right: 10px;"><?php }?><?php if(!empty($content)){echo $content[2]["content"];}?>
            </p>
            <div class="epaperDetailBa" style="padding-left: 10px;">
                <p style="padding-right: 30px;padding-top: 20px;"><?php if(!empty($content)){echo $content[2]["suggestion"];}?></p>
                <div class="epaperDetailBaPast">
                    <img src="<?php echo $base_url;?>assets_view/images/past.png">
                </div>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/coupleDetailB.png">
        </section>
    </div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div2"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[2]["title"];}?></h2>
            <div class="epaperDetailBL">

                    <?php if(!empty($content)){echo "<h3 style=\"font-size: 20px;color: #88ba7d; font-weight: bold;\">".$content[2]["title2"]."</h3>";}?>
                <p style="padding-right: 30px;">
                    <?php if($content[2]["type"] ==4 ) {?><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[2]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title3"];}?></h3>
                <p style="padding-right: 30px;">
                    <?php if($content[2]["type"] ==4 ) {?><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[2]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR" style="padding-left: 10px;">
                <h2></h2>
                <p style="padding-right: 10px;">
                    <?php if($content[2]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[2]["suggestion"];}?>
                </p>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>
<br>
<?php if(!empty($content) && $content[3]["type"] != 4 && $content[3]["type"] != 5) {?>
    <div class="epaperDetailM">
        <a name="div3"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/coupleDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[3]["title"];}?></h2>
            <?php if(!empty($content) && $content[3]["type"] == 3){?>
                <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>"></p>
            <?php }?>
            <p style="padding-right: 30px;">
                <?php if(!empty($content) && ($content[3]["type"] == 1 || $content[3]["type"] == 2)){?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>"  style="float:<?php if($content[3]["type"] == 1){echo "left";}else{echo "right";} ?>;padding-left: 0px; margin-right: 10px;"><?php }?><?php if(!empty($content)){echo $content[3]["content"];}?>
            </p>
            <div class="epaperDetailBa" style="padding-left: 10px;">
                <p style="padding-right: 30px;padding-top: 20px;"><?php if(!empty($content)){echo $content[3]["suggestion"];}?></p>
                <div class="epaperDetailBaPast">
                    <img src="<?php echo $base_url;?>assets_view/images/past.png">
                </div>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/familyDetailB.png">
        </section>
    </div>
<?php }else { ?>
    <div class="epaperDetailM">
        <a name="div3"></a>
        <h1>
            <img src="<?php echo $base_url;?>assets_view/images/coupleDetailT.png" alt="好教養">
        </h1>
        <section class="epaperDetailA">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg1.png">
        </section>
        <section class="epaperDetailB">
            <h2><?php if(!empty($content)){echo $content[3]["title"];}?></h2>
            <div class="epaperDetailBL">
                <?php if(!empty($content)){echo "<h3 style=\"font-size: 20px;color: #88ba7d; font-weight: bold;\">".$content[3]["title2"]."</h3>";}?>
                <p style="padding-right: 30px;">
                    <?php if($content[3]["type"] ==4 ) {?>  <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>" style="float:right;"><?php } ?><?php if(!empty($content)){echo $content[3]["content"];}?>
                </p>
                <h3 style="font-size: 20px;color: #88ba7d; font-weight: bold;"><?php if(!empty($content)){echo $content[2]["title3"];}?></h3>
                <p style="padding-right: 30px;">
                    <?php if($content[3]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img2"];}?>" style="float:left;"><?php } ?><?php if(!empty($content)){echo $content[3]["content2"];}?>
                </p>
            </div>
            <div class="epaperDetailBR"  style="padding-left: 10px;">
                <h2></h2>
                <p style="padding-right: 10px;">
                    <?php if($content[3]["type"] ==4 ) {?> <img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img3"];}?>"><?php } ?><?php if(!empty($content)){echo $content[3]["suggestion"];}?>
                </p>
            </div>
        </section>
        <section class="epaperDetailC">
            <img src="<?php echo $base_url;?>assets_view/images/detailBg3.png">
        </section>
        <section class="epaperDetailBottom">
            <img src="<?php echo $base_url;?>assets_view/images/teachDetailB.png">
        </section>
    </div>
    <?php
}
?>



</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>