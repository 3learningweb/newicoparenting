<!-- Ming content -->
<div class="memberM">
    <h1>恭喜您通過驗證</h1>
    <div class="bottomM"> </div>
    <ul>
        <li>
            <div class="type8M"  style="font-size: 16px;">系統已收到您的確認回覆，謝謝您。<br>請進入<a href="https://icoparenting.moe.edu.tw/%E9%9B%BB%E5%AD%90%E5%A0%B1/%E5%90%84%E6%9C%9F%E9%9B%BB%E5%AD%90%E5%A0%B1"> [各期電子報] </a>瀏覽各期電子報。</div>
        </li>
    </ul>

</div>
<!-- END content -->


<!-- Footer -->
<div class="footer clear" id="footer_home">
    <div class="foot_main">
        <ul class="nav menu _footermenu" id="footermenu">
            <li class='separator'><span>|</span></li><li class="item-203">		<a href="%89%88%e6%ac%8a%e8%aa%aa%e6%98%8e.html" >版權說明</a><li class='separator'><span>|</span></li><li class="item-204">		<a href="%9a%b1%e7%a7%81%e6%ac%8a%e6%94%bf%e7%ad%96.html" >隱私權政策</a><li class='separator'><span>|</span></li></ul>



        <div class="custom_footerintro"  >
            <p>建議使用瀏覽器Google Chrome、Firefox或IE10以上版本，瀏覽解析度1024x768以上，以獲得最佳瀏覽模式</p></div>

        <div class="footergroup" style="background-image: url('filesys/images/footer/footer_img.jpg');"></div>

    </div>
</div>			</div>
</div>

</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>