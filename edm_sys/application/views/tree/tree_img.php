<?php

/**
 * Jcrop image cropping plugin for jQuery
 * Example cropping script
 * @copyright 2008-2009 Kelly Hallman
 * More info: http://deepliquid.com/content/Jcrop_Implementation_Theory.html
 */



// If not a POST request, display page below:

?><!DOCTYPE html>
<html lang="en">
<head>
    <title>Live Cropping Demo</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?php echo $base_url;?>tree_asset/js/jquery.min.js"></script>
    <script src="<?php echo $base_url;?>tree_asset/js/jquery.Jcrop.js"></script>
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/demo_files/main.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/demo_files/demos.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/css/jquery.Jcrop.css" type="text/css" />

    <script type="text/javascript">

        $(function(){

            $('#cropbox').Jcrop({
                aspectRatio: 1,
                onSelect: updateCoords
            });

        });

        function updateCoords(c)
        {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
        };

        function checkCoords()
        {
            if (parseInt($('#w').val())) return true;
            alert('請選擇區域後再上傳');
            return false;
        };

    </script>
    <style type="text/css">
        #target {
            background-color: #ccc;
            width: 500px;
            height: 330px;
            font-size: 24px;
            display: block;
        }


    </style>

</head>
<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div class="jc-demo-box">
                <form action="<?php echo $base_url;?>index.php/tree_img" method="post"  enctype="multipart/form-data">
                    <div class="control-group">

                        <div class="controls">
                            <span style="color: #0000cc">步驟一： </span><input class="input-file uniform_on" id="fileInput" type="file" name="img">
                        </div>
                        <label class="control-label" for="fileInput"><span style="color:crimson;padding-left: 60px;">檔案名稱須為英文，檔案格式須為jpg，檔案最佳尺寸為150X150像素。<br><br></span></label>
                        <div class="controls">
                            <span style="color: #0000cc">步驟二： </span><button name="upload" value="<?php echo $id; ?>">上傳</button>
                        </div>
                        <label class="control-label" for="fileInput"><span style="color:crimson;padding-left: 60px;">上傳後，請拖曳滑鼠選取您要擷取的照片區域。</span></label>

                </form>
                <form action="<?php echo $base_url;?>index.php/tree_img" method="post" onsubmit="return checkCoords();">

                <!-- This is the image we're attaching Jcrop to -->
                <img src="<?php if(isset($img)){ echo $img;}else{echo $base_url."tree_asset/sample.jpg";}?>" id="cropbox" />
                    <br><br>

                <!-- This is the form that our event handler fills -->

                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <span style="color: #0000cc">步驟三： </span><button name="crop" value="<?php echo $id; ?>">送出圖片</button>
                </form>



            </div>
        </div>
    </div>
</div>
</body>

</html>