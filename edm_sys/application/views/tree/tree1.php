<!DOCTYPE html>
<!-- saved from url=(0104)<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9 -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<base href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9">--><base href=".">

    <meta name="generator" content="iMyfamily">
    <title>家庭樹 | iMyfamily</title>
    <link href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9?view=familytree" rel="canonical">
    <link href="<?php echo $home; ?>templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/reset.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/site.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/layout.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/site(1).css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/layout(1).css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/style(1).css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/w1000.css" type="text/css" media="only screen and (min-width: 1000px)">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/w650.css" type="text/css" media="only screen and (max-width: 649px)">
    <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/superfish.css" type="text/css">
    <script async="" src="<?php echo $base_url;?>tree_asset/analytics.js.下載"></script><script src="<?php echo $base_url;?>tree_asset/jquery.min.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/jquery-noconflict.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/jquery-migrate.min.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/bootstrap.min.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/template.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/respond.min.js.下載" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>tree_asset/superfish.js.下載" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('.hasTooltip').tooltip({"html": true,"container": "body"});
        });
        ;(function(window,$){var $mainmenu;var $mainmainmenu;var $sfmenu;var $menuLinks;$(function(){$sfmenu=$('ul.sf-menu');$menuLinks=$('.menu_link_1');$submenu_warpper=$('.submenu_warpper');$sfmenu.superfish({autoArrows:false,dropShadows:false});$mainmenu=$('#open-mainmenu');$mainmainmenu=$('.mainmainmenu');$mainmenu.bind('click',function(){$('#topmenu').hide();$mainmainmenu.toggle({speed:500});});initMenuState();$(document).bind('responsive',initMenuState);});var initMenuState=function(){$mainmainmenu.show();$sfmenu.find('ul').css('position','absolute');$menuLinks.unbind('click');}})(window,jQuery);
        ;(function(window,$){$(function(){var remainmenu=$('#remainmenu');var openremainmenu=$('#open-remainmenu');openremainmenu.bind('click',function(){if(remainmenu.is(':hidden')){$('.menu').hide();}
            remainmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){remainmenu.hide();}else{remainmenu.show();}}})(window,jQuery);
        ;(function(window,$){$(function(){var retopmenu=$('#retopmenu');var openretopmenu=$('#open-retopmenu');openretopmenu.bind('click',function(){if(retopmenu.is(':hidden')){$('.menu').hide();}
            retopmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){retopmenu.hide();}else{retopmenu.show();}}})(window,jQuery);
    </script>


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="/imyfamily/templates/system/js/html5shiv.js"></script>
    <script src="/media/jui/js/html5.js"></script>
    <![endif]-->

    <script>
        jQuery(document).ready(function() {
            jQuery("#retopmenu").append(jQuery(".itp-gs_research"));
            jQuery("#retopmenu").append(jQuery(".mod_retopshare"));
        });
    </script>




    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72465755-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Universal Google Analytics Plugin by PB Web Development -->


</head>
<body class="com_postcards view-familytree no-layout no-task itemid-146">
<!-- Body -->
<div class="all">
    <div class="container">
        <script>

            jQuery(document).ready(function() {
                jQuery(".listtable tbody tr:nth-child(even)").addClass("even");
                jQuery(".listtable tbody tr:nth-child(odd)").addClass("odd");

                jQuery(".datatable thead th:nth-child(even)").addClass("even");
                jQuery(".datatable thead th:nth-child(odd)").addClass("odd");
                jQuery(".datatable tbody tr:nth-child(even)").addClass("even");
                jQuery(".datatable tbody tr:nth-child(odd)").addClass("odd");
                jQuery(".datatable tbody tr td:nth-child(even)").addClass("even");
                jQuery(".datatable tbody tr td:nth-child(odd)").addClass("odd");
            });
        </script>

        <div class="theme_top">
            <!-- Header -->
            <div class="header">
                <div class="header-logo">


                    <div class="custom">
                        <p><a href="<?php echo $home; ?>index.php"><img src="<?php echo $base_url;?>tree_asset/logo.png" alt="logo"></a></p></div>

                </div>
                <div class="header-menu">
                    <ul class="nav menu_topmenu" id="topmenu">
                        <li class="item-126">		<a href="<?php echo $home; ?>">首頁</a></li><li class="separator"><span>．</span></li><li class="item-314">		<a href="<?php echo $home; ?>%E7%B6%B2%E7%AB%99%E5%85%AC%E5%91%8A">網站公告</a></li><li class="separator"><span>．</span></li><li class="item-128">		<a href="https://ilove.moe.edu.tw/" target="_blank">iLove</a></li><li class="separator"><span>．</span></li><li class="item-129">		<a href="https://icoparenting.moe.edu.tw/" target="_blank">iCoparenting</a></li><li class="separator last_separator"><span>．</span></li></ul>
                    <div id="open-retopmenu" style="display: none;">
                        <a href="javascript:void(0);" alt="上方選單" title="上方選單"><span>上方選單</span></a>
                    </div>
                    <ul class="menu" id="retopmenu">
                        <li class="item-126 mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>">首頁</a></div></li><li class="item-314 mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E7%B6%B2%E7%AB%99%E5%85%AC%E5%91%8A">網站公告</a></div></li><li class="item-128 mainlevel1"><div class="itemlevel1">		<a href="https://ilove.moe.edu.tw/" target="_blank">iLove</a></div></li><li class="item-129 mainlevel1"><div class="itemlevel1">		<a href="https://icoparenting.moe.edu.tw/" target="_blank">iCoparenting</a></div></li><div class="itp-gs_research">
                            <div class="search">
                                <form action="<?php echo $home; ?>%E5%85%A8%E6%96%87%E6%AA%A2%E7%B4%A2" method="get" accept-charset="utf-8">
                                    <span>全文檢索</span>
                                    <div class="search_block">
                                        <input name="gsquery" type="text" class="inputbox" placeholder="Search for..." value="">
                                        <input type="hidden" name="view" value="search">
                                        <input type="hidden" name="option" value="com_itpgooglesearch">
                                        <input type="hidden" name="Itemid" value="252">
                                        <input type="submit" class="btn icon-search">
                                    </div>
                                </form>
                            </div>
                        </div><div class="mod_retopshare">
	<span>
		<!-- twitter -->
				<a target="_blank" title="twitter" href="http://twitter.com/share?text=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily&amp;url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9">
                    <img src="<?php echo $base_url;?>tree_asset/twitter.png" alt="twitter">
                </a>

        <!-- facebook -->

		<a target="_blank" title="facebook" href="http://www.facebook.com/share.php?u=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9&amp;t=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily">
            <img src="<?php echo $base_url;?>tree_asset/facebook.png" alt="facebook">
        </a>

        <!-- google+ -->
		<a target="_blank" title="google+" href="https://plus.google.com/share?url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9">
            <img src="<?php echo $base_url;?>tree_asset/google.png" alt="google">
        </a>

        <!-- plurk -->
				<a target="_blank" title="plurk" href="http://www.plurk.com/?qualifier=shares&amp;status=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9%20+(%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily)">
                    <img src="<?php echo $base_url;?>tree_asset/plurk.png" alt="plurk">
                </a>

        <!-- weibo -->
		<a target="_blank" title="weibo" href="http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9&amp;title=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily">
            <img src="<?php echo $base_url;?>tree_asset/weibo.png" alt="weibo">
        </a>
	</span>
                        </div></ul>

                    <script language="JavaScript">
                        // rene
                        jQuery(document).ready(function() {
                            jQuery("#remainmenu .parent").each(function(){
                                jQuery(this).children().children("a").attr("href", "#");
                            });

                            jQuery(".mainlevel2").hide();
                            jQuery("#remainmenu .active").find("li").show();

                            jQuery(".mainlevel1").on("click", function() {
                                jQuery(".mainlevel1").removeClass("active");
                                jQuery(this).addClass("active");
                                jQuery(".mainlevel2").hide();
                                jQuery(this).find("li").show();
                            });
                        });
                    </script>

                    <div class="itp-gs_search">
                        <div class="search">
                            <form action="<?php echo $home; ?>%E5%85%A8%E6%96%87%E6%AA%A2%E7%B4%A2" method="get" accept-charset="utf-8">
                                <span>全文檢索</span>
                                <div class="search_block">
                                    <input name="gsquery" type="text" class="inputbox" placeholder="Search for..." value="">
                                    <input type="hidden" name="view" value="search">
                                    <input type="hidden" name="option" value="com_itpgooglesearch">
                                    <input type="hidden" name="Itemid" value="252">
                                    <input type="submit" class="btn icon-search">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mod_topshare">
	<span>
		<!-- twitter -->
				<a target="_blank" title="twitter" href="http://twitter.com/share?text=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily&amp;url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9">
                    <img src="<?php echo $base_url;?>tree_asset/twitter.png" alt="twitter">
                </a>

        <!-- facebook -->

		<a target="_blank" title="facebook" href="http://www.facebook.com/share.php?u=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9&amp;t=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily">
            <img src="<?php echo $base_url;?>tree_asset/facebook.png" alt="facebook">
        </a>

        <!-- google+ -->
		<a target="_blank" title="google+" href="https://plus.google.com/share?url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9">
            <img src="<?php echo $base_url;?>tree_asset/google.png" alt="google">
        </a>

        <!-- plurk -->
				<a target="_blank" title="plurk" href="http://www.plurk.com/?qualifier=shares&amp;status=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9%20+(%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily)">
                    <img src="<?php echo $base_url;?>tree_asset/plurk.png" alt="plurk">
                </a>

        <!-- weibo -->
		<a target="_blank" title="weibo" href="http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2F180.176.96.58%2Fimyfamily%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E7%25B4%2580%25E9%258C%2584%25E7%25B0%25BF%2F%25E5%25AE%25B6%25E5%25BA%25AD%25E6%25A8%25B9&amp;title=%E5%AE%B6%E5%BA%AD%E6%A8%B9%20%7C%20iMyfamily">
            <img src="<?php echo $base_url;?>tree_asset/weibo.png" alt="weibo">
        </a>
	</span>
                    </div>


                </div>
            </div>

            <div class="mainmenu">

                <ul class="sf-menu sf-js-enabled menu" id="mainmenu">

                    <li id="item-134" class="parent">		<a class="menu_link_1" title="" href="<?php echo $home; ?>%E6%B4%BB%E5%8B%95%E5%B0%88%E5%8D%80">
                            <span>活動專區</span>
                        </a><ul class="submenu_warpper" style="float: none; width: 220px; position: absolute; display: none; visibility: hidden;"><div class="submenu_table"><div class="twoblock"><div class="submenu_list"><span id="level_tow_145" class="submenu_1">		<a class="menu_link_2" title="活動列表" href="<?php echo $home; ?>%E6%B4%BB%E5%8B%95%E5%B0%88%E5%8D%80/%E6%B4%BB%E5%8B%95%E5%88%97%E8%A1%A8">
                                                <span class="submenu_icon">•</span>			<span>活動列表</span>
                                            </a></span></div></div></div></ul></li><li id="item-135_now" class="active parent">		<a class="menu_link_1" title="家庭紀錄簿" href="<?php echo $home; ?>tree/index.php/tree1">
                            <span>家庭紀錄簿</span>
                        </a><ul class="submenu_warpper" style="float: none; width: 220px; position: absolute; display: none; visibility: hidden;"><div class="submenu_table"><div class="twoblock"><div class="submenu_list"><span id="level_tow_146" class="current active submenu_1">	<a class="menu_link_2" title="家庭樹" href="<?php echo $home; ?>tree/index.php/tree1">
                                                <span class="submenu_icon">•</span>			<span>家庭樹</span>
                                            </></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_148" class="submenu_1">		<a class="menu_link_2" title="祝福賀卡" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E7%A5%9D%E7%A6%8F%E8%B3%80%E5%8D%A1">
                                                <span class="submenu_icon">•</span>			<span>祝福賀卡</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_149" class="submenu_1">		<a class="menu_link_2" title="回憶珠寶盒" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%9B%9E%E6%86%B6%E7%8F%A0%E5%AF%B6%E7%9B%92">
                                                <span class="submenu_icon">•</span>			<span>回憶珠寶盒</span>
                                            </a></span></div></div></div></ul></li><li id="item-136" class="parent">		<a class="menu_link_1" title="家人關係" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E8%80%81%E4%BA%BA%E5%8D%B0%E8%B1%A1%E6%8B%BC%E5%9C%96">
                            <span>家人關係</span>
                        </a><ul class="submenu_warpper" style="float: none; width: 220px; position: absolute; display: none; visibility: hidden;"><div class="submenu_table"><div class="twoblock"><div class="submenu_list"><span id="level_tow_273" class="submenu_1">		<a class="menu_link_2" title="男女大不同？" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E7%94%B7%E5%A5%B3%E5%A4%A7%E4%B8%8D%E5%90%8C%EF%BC%9F">
                                                <span class="submenu_icon">•</span>			<span>男女大不同？</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_270" class="submenu_1">		<a class="menu_link_2" title="家庭三溫暖" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E5%AE%B6%E5%BA%AD%E4%B8%89%E6%BA%AB%E6%9A%96">
                                                <span class="submenu_icon">•</span>			<span>家庭三溫暖</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_151" class="submenu_1">		<a class="menu_link_2" title="老人印象拼圖" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E8%80%81%E4%BA%BA%E5%8D%B0%E8%B1%A1%E6%8B%BC%E5%9C%96">
                                                <span class="submenu_icon">•</span>			<span>老人印象拼圖</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_152" class="submenu_1">		<a class="menu_link_2" title="大手牽小手" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E5%A4%A7%E6%89%8B%E7%89%BD%E5%B0%8F%E6%89%8B">
                                                <span class="submenu_icon">•</span>			<span>大手牽小手</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_154" class="submenu_1">		<a class="menu_link_2" title="結婚紀念日" href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E7%B5%90%E5%A9%9A%E7%B4%80%E5%BF%B5%E6%97%A5">
                                                <span class="submenu_icon">•</span>			<span>結婚紀念日</span>
                                            </a></span></div></div></div></ul></li><li id="item-137" class="parent">		<a class="menu_link_1" title="" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95">
                            <span>家庭活動</span>
                        </a><ul class="submenu_warpper" style="float: none; width: 220px; position: absolute; display: none; visibility: hidden;"><div class="submenu_table"><div class="twoblock"><div class="submenu_list"><span id="level_tow_165" class="submenu_1">		<a class="menu_link_2" title="家庭聚會卡" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E5%AE%B6%E5%BA%AD%E8%81%9A%E6%9C%83%E5%8D%A1">
                                                <span class="submenu_icon">•</span>			<span>家庭聚會卡</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_256" class="submenu_1">		<a class="menu_link_2" title="家庭存款簿" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E5%AE%B6%E5%BA%AD%E5%AD%98%E6%AC%BE%E7%B0%BF">
                                                <span class="submenu_icon">•</span>			<span>家庭存款簿</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_171" class="submenu_1">		<a class="menu_link_2" title="線上抓周" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E7%B7%9A%E4%B8%8A%E6%8A%93%E5%91%A8">
                                                <span class="submenu_icon">•</span>			<span>線上抓周</span>
                                            </a></span></div></div></div></ul></li><li id="item-138" class="parent">		<a class="menu_link_1" title="" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8">
                            <span>家庭圖書館</span>
                        </a><ul class="submenu_warpper" style="float: none; width: 220px; position: absolute; display: none; visibility: hidden;"><div class="submenu_table"><div class="twoblock"><div class="submenu_list"><span id="level_tow_316" class="submenu_1">		<a class="menu_link_2" title="給關心家庭生活的你 (2)" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%97%9C%E5%BF%83%E5%AE%B6%E5%BA%AD%E7%94%9F%E6%B4%BB%E7%9A%84%E4%BD%A0-2">
                                                <span class="submenu_icon">•</span>			<span>給關心家庭生活的你 (2)</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_302" class="submenu_1">		<a class="menu_link_2" title="給想要成家的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%83%B3%E8%A6%81%E6%88%90%E5%AE%B6%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給想要成家的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_303" class="submenu_1">		<a class="menu_link_2" title="給成為好丈夫/妻子的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E5%A5%BD%E4%B8%88%E5%A4%AB-%E5%A6%BB%E5%AD%90%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給成為好丈夫/妻子的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_304" class="submenu_1">		<a class="menu_link_2" title="給成為爸爸媽媽的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E7%88%B8%E7%88%B8%E5%AA%BD%E5%AA%BD%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給成為爸爸媽媽的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_305" class="submenu_1">		<a class="menu_link_2" title="給成為好兒女的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E5%A5%BD%E5%85%92%E5%A5%B3%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給成為好兒女的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_306" class="submenu_1">		<a class="menu_link_2" title="給成為爺爺/奶奶的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E7%88%BA%E7%88%BA-%E5%A5%B6%E5%A5%B6%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給成為爺爺/奶奶的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_307" class="submenu_1">		<a class="menu_link_2" title="給邁向黃金年代的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%82%81%E5%90%91%E9%BB%83%E9%87%91%E5%B9%B4%E4%BB%A3%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給邁向黃金年代的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_308" class="submenu_1">		<a class="menu_link_2" title="給專業人員的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E5%B0%88%E6%A5%AD%E4%BA%BA%E5%93%A1%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給專業人員的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_178" class="submenu_1">		<a class="menu_link_2" title="給談戀愛的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E8%AB%87%E6%88%80%E6%84%9B%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給談戀愛的你</span>
                                            </a></span></div></div><div class="twoblock"><div class="submenu_list"><span id="level_tow_315" class="submenu_1">		<a class="menu_link_2" title="給關心家庭生活的你" href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%97%9C%E5%BF%83%E5%AE%B6%E5%BA%AD%E7%94%9F%E6%B4%BB%E7%9A%84%E4%BD%A0">
                                                <span class="submenu_icon">•</span>			<span>給關心家庭生活的你</span>
                                            </a></span></div></div></div></ul></li></ul>

                <div id="open-remainmenu" style="display: none;">
                    <a href="javascript:void(0);" alt="主要選單" title="主要選單"><span>主要選單</span></a>
                </div>
                <ul class="menu" id="remainmenu">
                    <li class="item-134 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9#">活動專區</a></div><ul><li class="item-145 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E6%B4%BB%E5%8B%95%E5%B0%88%E5%8D%80/%E6%B4%BB%E5%8B%95%E5%88%97%E8%A1%A8">活動列表</a></div></li></ul></li><li class="item-135 active deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9#">家庭紀錄簿</a></div><ul><li class="item-146 current active mainlevel2" style="display: list-item;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9">家庭樹</a></div></li><li class="item-148 mainlevel2" style="display: list-item;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E7%A5%9D%E7%A6%8F%E8%B3%80%E5%8D%A1">祝福賀卡</a></div></li><li class="item-149 mainlevel2" style="display: list-item;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%9B%9E%E6%86%B6%E7%8F%A0%E5%AF%B6%E7%9B%92">回憶珠寶盒</a></div></li></ul></li><li class="item-136 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9#">家人關係</a></div><ul><li class="item-273 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E7%94%B7%E5%A5%B3%E5%A4%A7%E4%B8%8D%E5%90%8C%EF%BC%9F">男女大不同？</a></div></li><li class="item-270 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E5%AE%B6%E5%BA%AD%E4%B8%89%E6%BA%AB%E6%9A%96">家庭三溫暖</a></div></li><li class="item-151 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E8%80%81%E4%BA%BA%E5%8D%B0%E8%B1%A1%E6%8B%BC%E5%9C%96">老人印象拼圖</a></div></li><li class="item-152 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E5%A4%A7%E6%89%8B%E7%89%BD%E5%B0%8F%E6%89%8B">大手牽小手</a></div></li><li class="item-154 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E7%B5%90%E5%A9%9A%E7%B4%80%E5%BF%B5%E6%97%A5">結婚紀念日</a></div></li></ul></li><li class="item-137 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9#">家庭活動</a></div><ul><li class="item-165 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E5%AE%B6%E5%BA%AD%E8%81%9A%E6%9C%83%E5%8D%A1">家庭聚會卡</a></div></li><li class="item-256 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E5%AE%B6%E5%BA%AD%E5%AD%98%E6%AC%BE%E7%B0%BF">家庭存款簿</a></div></li><li class="item-171 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E6%B4%BB%E5%8B%95/%E7%B7%9A%E4%B8%8A%E6%8A%93%E5%91%A8">線上抓周</a></div></li></ul></li><li class="item-138 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9#">家庭圖書館</a></div><ul><li class="item-316 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%97%9C%E5%BF%83%E5%AE%B6%E5%BA%AD%E7%94%9F%E6%B4%BB%E7%9A%84%E4%BD%A0-2">給關心家庭生活的你 (2)</a></div></li><li class="item-302 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%83%B3%E8%A6%81%E6%88%90%E5%AE%B6%E7%9A%84%E4%BD%A0">給想要成家的你</a></div></li><li class="item-303 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E5%A5%BD%E4%B8%88%E5%A4%AB-%E5%A6%BB%E5%AD%90%E7%9A%84%E4%BD%A0">給成為好丈夫/妻子的你</a></div></li><li class="item-304 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E7%88%B8%E7%88%B8%E5%AA%BD%E5%AA%BD%E7%9A%84%E4%BD%A0">給成為爸爸媽媽的你</a></div></li><li class="item-305 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E5%A5%BD%E5%85%92%E5%A5%B3%E7%9A%84%E4%BD%A0">給成為好兒女的你</a></div></li><li class="item-306 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E6%88%90%E7%82%BA%E7%88%BA%E7%88%BA-%E5%A5%B6%E5%A5%B6%E7%9A%84%E4%BD%A0">給成為爺爺/奶奶的你</a></div></li><li class="item-307 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%82%81%E5%90%91%E9%BB%83%E9%87%91%E5%B9%B4%E4%BB%A3%E7%9A%84%E4%BD%A0">給邁向黃金年代的你</a></div></li><li class="item-308 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E5%B0%88%E6%A5%AD%E4%BA%BA%E5%93%A1%E7%9A%84%E4%BD%A0">給專業人員的你</a></div></li><li class="item-178 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E8%AB%87%E6%88%80%E6%84%9B%E7%9A%84%E4%BD%A0">給談戀愛的你</a></div></li><li class="item-315 mainlevel2" style="display: none;"><div class="itemlevel2"><span class="submenu_icon">•</span>		<a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8/%E7%B5%A6%E9%97%9C%E5%BF%83%E5%AE%B6%E5%BA%AD%E7%94%9F%E6%B4%BB%E7%9A%84%E4%BD%A0">給關心家庭生活的你</a></div></li></ul></li></ul>

                <script language="JavaScript">
                    // rene
                    jQuery(document).ready(function() {
                        jQuery("#remainmenu .parent").each(function(){
                            jQuery(this).children().children("a").attr("href", "#");
                        });

                        jQuery(".mainlevel2").hide();
                        jQuery("#remainmenu .active").find("li").show();

                        jQuery(".mainlevel1").on("click", function() {
                            jQuery(".mainlevel1").removeClass("active");
                            jQuery(this).addClass("active");
                            jQuery(".mainlevel2").hide();
                            jQuery(this).find("li").show();
                        });
                    });
                </script>

            </div>
            <div class="head_banner" id="tmpl_banner">

                <div class="bannergroup" style="background-image: url('https://imyfamily.moe.edu.tw/filesys/images/banner/imy.jpg');"></div>

            </div>
        </div>

        <div class="contentarea" id="contentarea_block">
            <div class="contentarea_middle">


                <div class="custom_quicklink">
                    <div class="quick_link">
                        <div class="time_block">
                            <div class="quiz_time time quick_left">
                                <div class="icon"><a href="<?php echo $home; ?>%E7%B7%9A%E4%B8%8A%E6%B8%AC%E9%A9%97-%E6%AA%A2%E6%A0%B8"><img src="<?php echo $base_url;?>tree_asset/quick_quiz.png" alt="線上測驗/檢核"></a></div>
                            </div>
                            <div class="voting_time time quick_right">
                                <div class="icon"><a href="<?php echo $home; ?>%E5%AE%B6%E4%BA%BA%E9%97%9C%E4%BF%82/%E7%94%B7%E5%A5%B3%E5%A4%A7%E4%B8%8D%E5%90%8C%EF%BC%9F?category=1"><img src="<?php echo $base_url;?>tree_asset/quick_voting.png" alt="線上調查/投票"></a></div>
                            </div>
                            <div class="forum_time time quick_left">
                                <div class="icon"><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E5%9C%96%E6%9B%B8%E9%A4%A8"><img src="<?php echo $base_url;?>tree_asset/quick_forum.png" alt="主題討論"></a></div>
                            </div>
                            <div class="game_time last_time quick_right">
                                <div class="icon"><a href="<?php echo $home; ?>%E4%BA%92%E5%8B%95%E9%81%8A%E6%88%B2"><img src="<?php echo $base_url;?>tree_asset/quick_game.png" alt="互動遊戲"></a></div>
                            </div>
                        </div>
                    </div></div>

            </div>
            <div class="contentarea inner">
                <div id="left_menu">
                    <script type="text/javascript">
                        var dir = "<?php echo $home; ?>";
                        var moodzt = "0";
                        var http_request = false;
                        var delay = 0;

                        jQuery(document).ready(function () {
                            // for category tab 切換頁籤投票時，把已投票判斷歸零
                            jQuery(".tab").click(function() {
                                moodzt = "0";
                            });
                        });

                        function makeRequest(url, functionName, httpType, sendData)
                        {
                            http_request = false;
                            if(!httpType) httpType = "GET";
                            if(window.XMLHttpRequest) {  // Non-IE...
                                http_request = new XMLHttpRequest();
                                if(http_request.overrideMimeType) {
                                    http_request.overrideMimeType('text/plain');
                                }
                            } else if(window.ActiveXObject) { // IE
                                try{
                                    http_request = new ActiveXObject("Msxml2.XMLHTTP");
                                } catch (e) {
                                    try{
                                        http_request = new ActiveXObject("Microsoft.XMLHTTP");
                                    } catch(e) {}
                                }
                            }

                            if(!http_request) {
                                alert("Cannot send an XMLHTTP request");
                                return false;
                            }

                            var changefunc = "http_request.onreadystatechange = " + functionName;
                            eval (changefunc);
                            http_request.open(httpType, url, true);
                            http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            http_request.send(sendData);
                        }

                        function $()
                        {
                            var elements = new Array();

                            for(var i=0; i<arguments.length; i++) {
                                var element = arguments[i];
                                if(typeof element == 'string')
                                    element = document.getElementById(element);

                                if(arguments.length == 1)
                                    return element;

                                elements.push(element)
                            }

                            return elements;
                        }

                        jQuery(document).on("click", "#mood_link", function() {
                            var mood_id = jQuery(this).attr("class");
                            var infoid = jQuery(this).parent().parent().find("#infoid").val();
                            var classid = jQuery(this).parent().parent().find("#classid").val();

                            remood(infoid, classid); // 第一次投票(資料庫還未建資料)使用

                            if(moodzt == "1") {
                                alert("您已經投過票，請勿重複投票。");
                            } else {
                                //alert("投票成功");  // sleep
                                setTimeout(function(){
                                    delay = 1;
                                    url = dir + "plugins/content/mood/xinqing.php?action=mood&classid="+classid+"&id="+infoid+"&typee="+mood_id+"&m=" + Math.random();
                                    makeRequest(url, 'return_review1', 'GET', '');
                                    moodzt = "1";
                                },500);
                            }

                        });

                        function remood(infoid, classid)
                        {
                            url = dir + "plugins/content/mood/xinqing.php?action=show&id="+infoid+"&classid="+classid+"&m=" + Math.random();
                            makeRequest(url, 'return_review1', 'GET', '');
                        }

                        function return_review1(ajax)
                        {
                            if(http_request.readyState == 4) {
                                if(http_request.status == 200) {
                                    var str_error_num = http_request.responseText;
                                    // alert(str_error_num);
                                    if(str_error_num == "error") {
                                        alert("訊息不存在");
                                    } else if(str_error_num == 0) {
                                        alert("您已經投過票，請勿重複投票。");
                                    } else {
                                        if(delay == 1){
                                            moodinner(str_error_num);
                                        }
                                    }
                                } else {
                                    alert('There was a problem with the request.');
                                }
                            }
                        }

                        function moodinner(moodtext)
                        {
                            var tab = jQuery(".tab.active").find("a").attr("class");
                            if(tab) {
                                var tab_index = tab.split("_");
                            }

                            var imga = dir+"/plugins/content/mood/images/pre_02.gif";
                            var imgb = dir+"/plugins/content/mood/images/pre_01.gif";
                            var color1 = "#666666";
                            var color2 = "#EB610E";
                            var heightz = "80";
                            var hmax = 0;
                            var hmaxpx = 0;
                            var heightarr = new Array();
                            var moodarr = moodtext.split(",");
                            var moodzs = 0;

                            for(k=0; k<8; k++) {
                                moodarr[k] = parseInt(moodarr[k]);
                                moodzs += moodarr[k];
                            }

                            for(i=0; i<8; i++) {
                                heightarr[i] = Math.round(moodarr[i]/moodzs*heightz);
                                if(heightarr[i]<1) heightarr[i]=1;
                                if(moodarr[i]>hmaxpx) {
                                    hmaxpx = moodarr[i];
                                }
                            }

                            var lang = document.getElementById("lang").value;
                            if(lang == "zh-TW"){
                                if(tab) {
                                    jQuery(".tab_content_"+tab_index[1]+" #moodtitle").html("<span style='color: #555555;'>目前已有<font color='#FF0000'>"+moodzs+"</font>人發表心情</span>");
                                }else{
                                    jQuery("#moodtitle").html("<span style='color: #555555;'>目前已有<font color='#FF0000'>"+moodzs+"</font>人發表心情</span>");
                                }
                            }else{

                            }

                            for(j=0; j<8; j++) {
                                var num = (moodarr[j]/moodzs)*100;

                                if(moodarr[j] == hmaxpx && moodarr[j] != 0) {
//			百分比顯示
                                    if(tab) {
                                        jQuery(".tab_content_"+tab_index[1]+" #moodinfo"+j).html("<span style='color: "+color2+";'>"+num.toFixed(1)+"%</span>");
                                    }else{
                                        jQuery("#moodinfo"+j).html("<span style='color: "+color2+";'>"+num.toFixed(1)+"%</span>");
                                    }
                                } else {
//			百分比顯示
                                    if(tab) {
                                        jQuery(".tab_content_"+tab_index[1]+" #moodinfo"+j).html("<span style='color: "+color1+";'>"+num.toFixed(1)+"%</span>");
                                    }else{
                                        jQuery("#moodinfo"+j).html("<span style='color: "+color1+";'>"+num.toFixed(1)+"%</span>");
                                    }
                                }
                            }
                        }

                    </script><div class="newsflash">
                        <span class="hit_name">本月主打</span>

                        <h5 class="newsflash-title">
                            屬於你我的親子心動時光		</h5>




                        <p><a href="https://icoparenting.moe.edu.tw/3C3T"><strong style="font-size: 11px;"><span style="font-size: 14pt; color: #290891;">2016年國際家庭日 善用3C，幸福3T</span></strong></a></p>
                        <p><a href="https://icoparenting.moe.edu.tw/event"><strong><span style="font-size: 14pt; color: #a40a23;">『999個親子心動時光』相片徵文活動</span></strong></a></p>
                        <p>&nbsp;</p>
                        <p><span style="font-size: 12pt;">快快上傳親子間美好時光的照片，</span></p>
                        <p><span style="font-size: 12pt;">寫下屬於你的心情與感動!</span></p>
                        <p>&nbsp;</p>
                        <p><a href="https://icoparenting.moe.edu.tw/event"><img src="<?php echo $base_url;?>tree_asset/logo(1).png" alt="logo" width="351" height="114"></a></p>
                        <p>&nbsp;</p>
                        <p><span style="text-decoration: underline;"><strong style="font-size: 11px;"><a href="https://icoparenting.moe.edu.tw/event"><span style="font-size: 14pt;">現在就投稿去~</span></a></strong><a href="https://icoparenting.moe.edu.tw/event" style="font-size: 11px;"></a></span></p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                    <div class="mod_leftmenu">
                        <ul class="nav menu_leftmenu">
                            <li class="item-135 active parent"><div><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9" title="家庭紀錄簿">家庭紀錄簿</a></div><ul class="nav-child unstyled small"><li class="item-146 current active"><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9" title="家庭樹">．家庭樹</a></li><li class="item-148"><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E7%A5%9D%E7%A6%8F%E8%B3%80%E5%8D%A1" title="祝福賀卡">．祝福賀卡</a></li><li class="item-149"><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%9B%9E%E6%86%B6%E7%8F%A0%E5%AF%B6%E7%9B%92" title="回憶珠寶盒">．回憶珠寶盒</a></li>	</ul>
                            </li></ul></div>


                    <div class="custom_iquick">
                        <div class="iquick_link">
                            <div class="iquick_icon"><a href="https://icoparenting.moe.edu.tw/" target="_blank"><span>iCoparenting</span></a></div>
                        </div></div>


                    <div class="custom_iquick">
                        <div class="iquick_link">
                            <div class="iquick_icon"><a href="https://ilove.moe.edu.tw/index.php" target="_blank"><span>iLove</span></a></div>
                        </div></div>

                </div>
                <div id="content">

                    <ul class="breadcrumb">
                        <li class="active"><span class="divider"></span></li><li><a href="<?php echo $home; ?>" class="pathway">首頁</a><span class="divider">&gt;</span></li><li><a href="<?php echo $home; ?>%E5%AE%B6%E5%BA%AD%E7%B4%80%E9%8C%84%E7%B0%BF/%E5%AE%B6%E5%BA%AD%E6%A8%B9" class="pathway">家庭紀錄簿</a><span class="divider">&gt;</span></li><li class="active"><span>家庭樹</span></li></ul>

                    <div id="content_title">
                        <div class="mod_sectiontitle">
                            <div class="section_title">家庭紀錄簿</div>
                        </div>
                        <div id="content_block">
                            <div id="system-message-container">
                            </div>


                            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                            <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/reset(1).css">
                            <link rel="stylesheet" href="<?php echo $base_url;?>tree_asset/tree2.css">

                            <div class=" choiceBox choiceBoxBg">
                                <div class="choiceBoxL" style="padding-top: 180px;">
                                    <div class="title">
                                        <h1>家庭樹</h1>
                                        <p>家庭猶如大樹，每個成員都代表一個個豐盛的果實。<br>
                                            家人之間的關心與互動，是促進大樹成長茁壯的最佳養分。<br>
                                            請找出家人的照片，製作出屬於自家的家庭樹。</p>
                                    </div>
                                    <div class="pictures picturesSample" style="margin-top: 30px;">
                                        <div class="head">
                                            <p style="background:rgba(0, 0, 0, 0);"> </p>
                                            <img src="<?php echo $base_url;?>tree_asset/blank.png">
                                        </div>
                                        <div class="head">
                                            <p>爺爺</p>
                                            <img src="<?php echo $base_url;?>tree_asset/爺爺.png">
                                        </div>
                                        <div class="head">
                                            <p>奶奶</p>
                                            <img src="<?php echo $base_url;?>tree_asset/奶奶.png">
                                        </div>
                                        <div class="head">
                                            <p style="background:rgba(0, 0, 0, 0);"> </p>
                                            <img src="<?php echo $base_url;?>tree_asset/blank.png">
                                        </div>
                                        <div class="head">
                                            <p style="background:rgba(0, 0, 0, 0);"> </p>
                                            <img src="<?php echo $base_url;?>tree_asset/blank.png">
                                        </div>
                                        <div class="head">
                                            <p>爸爸</p>
                                            <img src="<?php echo $base_url;?>tree_asset/爸爸.png">
                                        </div>
                                        <div class="head">
                                            <p>媽媽</p>
                                            <img src="<?php echo $base_url;?>tree_asset/媽媽.png">
                                        </div>
                                        <div class="head">
                                            <p style="background:rgba(0, 0, 0, 0);"> </p>
                                            <img src="<?php echo $base_url;?>tree_asset/blank.png">
                                        </div>
                                        <div class="head">
                                            <p>姐姐</p>
                                            <img src="<?php echo $base_url;?>tree_asset/姐姐.png">
                                        </div>
                                        <div class="head">
                                            <p>我</p>
                                            <img src="<?php echo $base_url;?>tree_asset/我.png">
                                        </div>
                                        <div class="head">
                                            <p>弟弟</p>
                                            <img src="<?php echo $base_url;?>tree_asset/弟弟.png">
                                        </div>
                                        <div class="head">
                                            <p style="background:rgba(0, 0, 0, 0);"> </p>
                                            <img src="<?php echo $base_url;?>tree_asset/blank.png">
                                        </div>
                                    </div>
                                    <img src="<?php echo $base_url;?>tree_asset/tree_3.jpg" width="454" >
                                    <p class="sampleTitle">家庭樹範例圖</p>
                                </div>
                                <form action="<?php echo $base_url;?>index.php/tree1" method="post">
                                    <div class="choiceBoxR2" style="">
                                        <h2 class="pc_display">請選擇您喜歡的<br>家庭樹主題</h2>
                                        <h2 class="phone_display">請選擇您喜歡的家庭樹主題</h2>
                                        <!--<div class="gimg_blocks">-->
                                        <div class="choiceBoxR2Sub">
                                            <label for="cat_id_188">
                                                <img alt="背景一" src="<?php echo $base_url;?>tree_asset/tree_3.jpg" class="choiceBoxR2Img">
                                                <input type="radio" id="cat_id_188" name="cat_id" value="<?php echo $base_url;?>tree_asset/tree_3.jpg">背景一              </label>
                                        </div>
                                        <!--</div>-->
                                        <!--<div class="gimg_blocks">-->
                                        <div class="choiceBoxR2Sub">
                                            <label for="cat_id_189">
                                                <img alt="背景二" src="<?php echo $base_url;?>tree_asset/tree_2.jpg" class="choiceBoxR2Img">
                                                <input type="radio" id="cat_id_189" name="cat_id" value="<?php echo $base_url;?>tree_asset/tree_2.jpg">背景二              </label>
                                        </div>
                                        <!--</div>-->
                                        <!--<div class="gimg_blocks">-->
                                        <div class="choiceBoxR2Sub">
                                            <label for="cat_id_190">
                                                <img alt="背景三" src="<?php echo $base_url;?>tree_asset/tree_1.jpg" class="choiceBoxR2Img">
                                                <input type="radio" id="cat_id_190" name="cat_id" value="<?php echo $base_url;?>tree_asset/tree_1.jpg">背景三              </label>
                                        </div>
                                        <!--</div>-->
                                        <button style="width:100px;" name="next" value="1">下一步</button>
                                    </div>
                                </form>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="return">
                    <div class="return_back">
                        <div class="return_inner">
                            <span class="icon-undo"></span>
                            <a href="javascript:history.go(-1)"><span>返回上一頁</span></a>
                            <noscript>您的瀏覽器不支援script程式碼。請使用 "Backspace" 按鍵回到上一頁。</noscript>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="footer clear" id="footer_tmpl">
            <div class="foot_main">
                <ul class="nav menu _footermenu" id="footermenu">
                    <li class="separator"><span>|</span></li><li class="item-203">		<a href="<?php echo $home; ?>%E7%89%88%E6%AC%8A%E8%AA%AA%E6%98%8E">版權說明</a></li><li class="separator"><span>|</span></li><li class="item-204">		<a href="<?php echo $home; ?>%E9%9A%B1%E7%A7%81%E6%AC%8A%E6%94%BF%E7%AD%96">隱私權政策</a></li><li class="separator"><span>|</span></li></ul>



                <div class="custom_footerintro">
                    <p>建議使用瀏覽器Google Chrome、Firefox或IE10以上版本，瀏覽解析度1024x768以上，以獲得最佳瀏覽模式</p></div>

                <div class="footergroup" style="background-image: url(&#39;/imyfamily/filesys/images/footer/footer_img.jpg&#39;);"></div>

            </div>
        </div>			</div>
</div>



</body></html>