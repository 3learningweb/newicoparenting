<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iCoparenting" />
    <title>iCoparenting</title>
    <link href="index.html" rel="canonical" />
    <link href="indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/reset.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/css/tabs.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />
    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/system/js/caption.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/ch/js/template.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/respond.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(window).on('load',  function() {
            new JCaption('img.caption');
        });
        jQuery(document).ready(function(){
            jQuery('.hasTooltip').tooltip({"html": true,"container": "body"});
        });
        jQuery(document).ready(function(){$=jQuery;var tabs_num=jQuery('#tabs_announcement .tab-title').length;jQuery('#tabs_announcement .tab-separator').each(function(index,obj){var offset=0+((index+1)*120.5);var top=8;jQuery(obj).css('left',offset+'px');jQuery(obj).css('top',top+'px');});jQuery('#tabs_announcement .tab-link').each(function(index,obj){jQuery(obj).bind('click focus',function(){jQuery('#tabs_announcement  .tab-link').removeClass("active");jQuery(this).addClass("active");jQuery('#tabs_announcement .tab-content').hide().eq(index).show();});});jQuery('#tabs_announcement .tab-link').eq(0).click();$('#tabs_announcement .tab-link').mouseover(function(){$(this).addClass('hover');}).mouseout(function(){$(this).removeClass('hover');});});
        ;(function(window,$){var $mainmenu;var $mainmainmenu;var $sfmenu;var $menuLinks;$(function(){$sfmenu=$('ul.sf-menu');$menuLinks=$('.menu_link_1');$submenu_warpper=$('.submenu_warpper');$sfmenu.superfish({autoArrows:false,dropShadows:false});$mainmenu=$('#open-mainmenu');$mainmainmenu=$('.mainmainmenu');$mainmenu.bind('click',function(){$('#topmenu').hide();$mainmainmenu.toggle({speed:500});});initMenuState();$(document).bind('responsive',initMenuState);});var initMenuState=function(){$mainmainmenu.show();$sfmenu.find('ul').css('position','absolute');$menuLinks.unbind('click');}})(window,jQuery);
        ;(function(window,$){$(function(){var remainmenu=$('#remainmenu');var openremainmenu=$('#open-remainmenu');openremainmenu.bind('click',function(){if(remainmenu.is(':hidden')){$('.menu').hide();}
            remainmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){remainmenu.hide();}else{remainmenu.show();}}})(window,jQuery);
        ;(function(window,$){$(function(){var retopmenu=$('#retopmenu');var openretopmenu=$('#open-retopmenu');openretopmenu.bind('click',function(){if(retopmenu.is(':hidden')){$('.menu').hide();}
            retopmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){retopmenu.hide();}else{retopmenu.show();}}})(window,jQuery);
    </script>
    <!-- Ming -->
    <script src="<?php echo $base_url;?>assets_view/js/jquery-1.8.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#education_datepicker" ).datepicker();
        });
    </script>
    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/html5shiv.js"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/html5.js"></script>
    <![endif]-->

    <script>
        jQuery(document).ready(function() {
            jQuery("#retopmenu").append(jQuery(".itp-gs_research"));
            jQuery("#retopmenu").append(jQuery(".mod_retopshare"));
        });
    </script>




    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72465755-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Universal Google Analytics Plugin by PB Web Development -->


</head>
<body class="com_content view-featured no-layout no-task itemid-126">
<!-- Body -->
<div class="all">
    <div class="container">
        <div class="theme_top">
            <!-- Header -->
            <div class="header">
                <div class="header-logo">


                    <div class="custom"  >
                        <p><a href="<?php echo $home;?>index.php"><img src="<?php echo $base_url;?>assets_view/filesys/images/system/logo.png" alt="logo" /></a></p></div>

                </div>
                <div class="header-menu">
                    <ul class="nav menu_topmenu" id="topmenu">
                        <li class="item-126 current active">		<a href="<?php echo $home;?>/" >首頁</a><li class='separator'><span>．</span></li><li class="item-307">		<a href="<?php echo $home;?>網站公告" >網站公告</a><li class='separator'><span>．</span></li><li class="item-128">		<a href="https://ilove.moe.edu.tw" target="_blank" >iLove</a><li class='separator'><span>．</span></li><li class="item-129">		<a href="https://imyfamily.moe.edu.tw/" target="_blank" >iMyfamily</a><li class='separator last_separator'><span>．</span></li></ul>
                    <div id="open-retopmenu" style="display: none;">
                        <a href="javascript:void(0);" alt="上方選單" title="上方選單"><span>上方選單</span></a>
                    </div>
                    <ul class="menu" id="retopmenu">
                        <li class="item-126 current active mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>/" >首頁</a></div></li><li class="item-307 mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>/網站公告" >網站公告</a></div></li><li class="item-128 mainlevel1"><div class="itemlevel1">		<a href="https://ilove.moe.edu.tw" target="_blank" >iLove</a></div></li><li class="item-129 mainlevel1"><div class="itemlevel1">		<a href="https://imyfamily.moe.edu.tw/" target="_blank" >iMyfamily</a></div></li></ul>

                    <script language="JavaScript">
                        // rene
                        jQuery(document).ready(function() {
                            jQuery("#remainmenu .parent").each(function(){
                                jQuery(this).children().children("a").attr("href", "#");
                            });

                            jQuery(".mainlevel2").hide();
                            jQuery("#remainmenu .active").find("li").show();

                            jQuery(".mainlevel1").on("click", function() {
                                jQuery(".mainlevel1").removeClass("active");
                                jQuery(this).addClass("active");
                                jQuery(".mainlevel2").hide();
                                jQuery(this).find("li").show();
                            });
                        });
                    </script>

                    <div class="itp-gs_search">
                        <div class="search">
                            <form action="<?php echo $home;?>全文檢索" method="get" accept-charset="utf-8">
                                <span>全文檢索</span>
                                <div class="search_block">
                                    <input name="gsquery" type="text" class="inputbox" placeholder="Search for..." value="" />
                                    <input type="hidden" name="view" value="search">
                                    <input type="hidden" name="option" value="com_itpgooglesearch">
                                    <input type="hidden" name="Itemid" value="252">
                                    <input type="submit" class="btn icon-search" />
                                </div>
                            </form>
                        </div>
                    </div><div class="itp-gs_research">
                        <div class="search">
                            <form action="<?php echo $home;?>全文檢索" method="get" accept-charset="utf-8">
                                <span>全文檢索</span>
                                <div class="search_block">
                                    <input name="gsquery" type="text" class="inputbox" placeholder="Search for..." value="" />
                                    <input type="hidden" name="view" value="search">
                                    <input type="hidden" name="option" value="com_itpgooglesearch">
                                    <input type="hidden" name="Itemid" value="252">
                                    <input type="submit" class="btn icon-search" />
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mod_topshare">
	<span>
		<!-- twitter -->
				<a target="_blank" title="twitter" href="http://twitter.com/share?text=i%20CoParenting&amp;url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F">
                    <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/twitter.png" alt="twitter" />
                </a>

        <!-- facebook -->

		<a target="_blank" title="facebook" href="http://www.facebook.com/share.php?u=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F&amp;t=i%20CoParenting">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/facebook.png" alt="facebook" />
        </a>

        <!-- google+ -->
		<a target="_blank" title="google+" href="https://plus.google.com/share?url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/google.png" alt="google" />
        </a>

        <!-- plurk -->
				<a target="_blank" title="plurk" href="http://www.plurk.com/?qualifier=shares&amp;status=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F%20+(i%20CoParenting)">
                    <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/plurk.png" alt="plurk" />
                </a>

        <!-- weibo -->
		<a target="_blank" title="weibo" href="http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F&title=i%20CoParenting">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/weibo.png" alt="weibo" />
        </a>
	</span>
                    </div>
                    <div class="mod_retopshare">
	<span>
		<!-- twitter -->
				<a target="_blank" title="twitter" href="http://twitter.com/share?text=i%20CoParenting&amp;url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F">
                    <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/twitter.png" alt="twitter" />
                </a>

        <!-- facebook -->

		<a target="_blank" title="facebook" href="http://www.facebook.com/share.php?u=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F&amp;t=i%20CoParenting">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/facebook.png" alt="facebook" />
        </a>

        <!-- google+ -->
		<a target="_blank" title="google+" href="https://plus.google.com/share?url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/google.png" alt="google" />
        </a>

        <!-- plurk -->
				<a target="_blank" title="plurk" href="http://www.plurk.com/?qualifier=shares&amp;status=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F%20+(i%20CoParenting)">
                    <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/plurk.png" alt="plurk" />
                </a>

        <!-- weibo -->
		<a target="_blank" title="weibo" href="http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2F23.97.75.244%2Ficoparenting%2F&title=i%20CoParenting">
            <img src="<?php echo $base_url;?>assets_view/templates/ch/images/share/weibo.png" alt="weibo" />
        </a>
	</span>
                    </div>

                </div>
            </div>

            <div class="mainmenu">

                <ul class="sf-menu sf-js-enabled menu" id="mainmenu">

                    <!--
                    <?php
                        foreach ($menu as $item)
                        {
                            ?>
                                <li id="item-134" class="parent">
                                <a class="menu_link_1" title="<?php echo $item["name"];?>" href="<?php echo $item["link"];?>" >
                                    <span><?php echo $item["name"];?></span>
                                </a>
                                    <ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'>
                                        <div class='submenu_table'>

                                        <?php
                                        foreach ($menu_all as $item2)
                                        {
                                            if($item2["parent"] == $item["id"])
                                            {
                                            ?>
                                            <div class='twoblock'>
                                                <div class='submenu_list'>
                                                    <span id="level_tow_145" class="submenu_1">
                                                        <a class="menu_link_2" title="<?php echo $item2["name"];?>" href="<?php echo $item2["link"];?>" >
                                                            <span class='submenu_icon'>•</span>			<span><?php echo $item2["name"];?></span>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                            <?
                                            }
                                        }
                                            ?>
                                        </div>
                                    </ul>
                                </li>

                        <?
                            }
                        ?>



                    -->




	<li id="item-134" class="parent">		<a class="menu_link_1" title="活動專區" href="/活動專區/活動列表" >
						<span>活動專區</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_145" class="submenu_1">		<a class="menu_link_2" title="活動列表" href="/活動專區/活動列表" >
			<span class='submenu_icon'>•</span>			<span>活動列表</span>
		</a></span></div></div></div></ul></li><li id="item-135" class="parent">		<a class="menu_link_1" title="育兒預備" href="/育兒預備/傳家寶貝" >
						<span>育兒預備</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_320" class="submenu_1">		<a class="menu_link_2" title="傳家寶貝" href="/育兒預備/傳家寶貝" >
			<span class='submenu_icon'>•</span>			<span>傳家寶貝</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_148" class="submenu_1">		<a class="menu_link_2" title="甜蜜天使窩" href="/育兒預備/生育預備-家庭環境" >
			<span class='submenu_icon'>•</span>			<span>甜蜜天使窩</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_149" class="submenu_1">		<a class="menu_link_2" title="資源盤點與規劃" href="/育兒預備/生育預備-家庭資源盤點與規劃" >
			<span class='submenu_icon'>•</span>			<span>資源盤點與規劃</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_256" class="submenu_1">		<a class="menu_link_2" title="愛的成長紀錄" href="/育兒預備/孕期紀錄" >
			<span class='submenu_icon'>•</span>			<span>愛的成長紀錄</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_270" class="submenu_1">		<a class="menu_link_2" title="幸福待產包" href="/育兒預備/幸福待產包" >
			<span class='submenu_icon'>•</span>			<span>幸福待產包</span>
		</a></span></div></div></div></ul></li><li id="item-137" class="parent">		<a class="menu_link_1" title="教養孩子有方法" href="/教養孩子有方法/育兒困擾" >
						<span>教養孩子有方法</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_156" class="submenu_1">		<a class="menu_link_2" title="育兒有妙方" href="/教養孩子有方法/育兒困擾" >
			<span class='submenu_icon'>•</span>			<span>育兒有妙方</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_160" class="submenu_1">		<a class="menu_link_2" title="寶寶的氣質與教養" href="/教養孩子有方法/氣質與教養" >
			<span class='submenu_icon'>•</span>			<span>寶寶的氣質與教養</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_171" class="submenu_1">		<a class="menu_link_2" title="親子共讀趣" href="/教養孩子有方法/共讀方法-技巧" >
			<span class='submenu_icon'>•</span>			<span>親子共讀趣</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_319" class="submenu_1">		<a class="menu_link_2" title="網路上的育兒幫手" href="/教養孩子有方法/網路上的育兒幫手" >
			<span class='submenu_icon'>•</span>			<span>網路上的育兒幫手</span>
		</a></span></div></div></div></ul></li><li id="item-136" class="parent">		<a class="menu_link_1" title="寶寶的健康成長" href="/寶寶的健康成長/寶寶的出生紀錄" >
						<span>寶寶的健康成長</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_151" class="submenu_1">		<a class="menu_link_2" title="寶寶的成長里程碑" href="/寶寶的健康成長/寶寶的出生紀錄" >
			<span class='submenu_icon'>•</span>			<span>寶寶的成長里程碑</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_152" class="submenu_1">		<a class="menu_link_2" title="寶寶的發展檢核" href="/寶寶的健康成長/寶寶發展檢核表-早療" >
			<span class='submenu_icon'>•</span>			<span>寶寶的發展檢核</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_154" class="submenu_1">		<a class="menu_link_2" title="家庭急救包" href="/寶寶的健康成長/家庭急救包" >
			<span class='submenu_icon'>•</span>			<span>家庭急救包</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_273" class="submenu_1">		<a class="menu_link_2" title="體驗活動" href="/寶寶的健康成長/親子活動" >
			<span class='submenu_icon'>•</span>			<span>體驗活動</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_340" class="submenu_1">		<a class="menu_link_2" title="親子野餐趣" href="/寶寶的健康成長/親子野餐趣" >
			<span class='submenu_icon'>•</span>			<span>親子野餐趣</span>
		</a></span></div></div></div></ul></li><li id="item-138" class="parent">		<a class="menu_link_1" title="爸媽加油站" href="/爸媽加油站/育兒的甜蜜與負擔" >
						<span>爸媽加油站</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_165" class="submenu_1">		<a class="menu_link_2" title="育兒的甜蜜與負擔" href="/爸媽加油站/育兒的甜蜜與負擔" >
			<span class='submenu_icon'>•</span>			<span>育兒的甜蜜與負擔</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_178" class="submenu_1">		<a class="menu_link_2" title="協力共親職" href="/爸媽加油站/協力共親職" >
			<span class='submenu_icon'>•</span>			<span>協力共親職</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_169" class="submenu_1">		<a class="menu_link_2" title="幸福辯論賽" href="/爸媽加油站/幸福辯論賽" >
			<span class='submenu_icon'>•</span>			<span>幸福辯論賽</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_325" class="submenu_1">		<a class="menu_link_2" title=" 3C家族零距離" href="/爸媽加油站/3c家族零距離" >
			<span class='submenu_icon'>•</span>			<span> 3C家族零距離</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_326" class="submenu_1">		<a class="menu_link_2" title="協力育兒減家務" href="/爸媽加油站/協力育兒減家務" >
			<span class='submenu_icon'>•</span>			<span>協力育兒減家務</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_337" class="submenu_1">		<a class="menu_link_2" title="增溫性福指數" href="/爸媽加油站/增溫性福指數" >
			<span class='submenu_icon'>•</span>			<span>增溫性福指數</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_338" class="submenu_1">		<a class="menu_link_2" title="攜手解除壓力源" href="/爸媽加油站/攜手解除壓力源" >
			<span class='submenu_icon'>•</span>			<span>攜手解除壓力源</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_339" class="submenu_1">		<a class="menu_link_2" title="話解衝突" href="/爸媽加油站/話解衝突" >
			<span class='submenu_icon'>•</span>			<span>話解衝突</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_341" class="submenu_1">		<a class="menu_link_2" title="親子依附知多少" href="/爸媽加油站/親子依附知多少" >
			<span class='submenu_icon'>•</span>			<span>親子依附知多少</span>
		</a></span></div></div></div></ul></li><li id="item-316" class="parent">		<a class="menu_link_1" title="　電子報　" href="/edm_sys/index.php/edm_print" >
						<span>　電子報　</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_146" class="submenu_1">		<a class="menu_link_2" title="訂閱電子報" href="/edm_sys/index.php/reg" >
			<span class='submenu_icon'>•</span>			<span>訂閱電子報</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_318" class="submenu_1">		<a class="menu_link_2" title="各期電子報" href="/edm_sys/index.php/edm_print" >
			<span class='submenu_icon'>•</span>			<span>各期電子報</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_317" class="submenu_1">		<a class="menu_link_2" title="會員專區" href="/edm_sys/index.php/member_content" >
			<span class='submenu_icon'>•</span>			<span>會員專區</span>
		</a></span></div></div></div></ul></li><li id="item-144" class="parent">		<a class="menu_link_1" title="" href="/相關資源" >
						<span>相關資源</span>
		</a><ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'><div class='submenu_table'><div class='twoblock'><div class='submenu_list'><span id="level_tow_206" class="submenu_1">		<a class="menu_link_2" title="婚姻教育與親職教育相關資源" href="/相關資源/婚姻教育與親職教育相關資源" >
			<span class='submenu_icon'>•</span>			<span>婚姻教育與親職教育相關資源</span>
		</a></span></div></div><div class='twoblock'><div class='submenu_list'><span id="level_tow_277" class="submenu_1">		<a class="menu_link_2" title="政府部門相關資源" href="/相關資源/政府部門相關資源" >
			<span class='submenu_icon'>•</span>			<span>政府部門相關資源</span>
		</a></span></div></div></div></ul></li>

                </ul>

                <div id="open-remainmenu" style="display: none;">
                    <a href="javascript:void(0);" alt="主要選單" title="主要選單"><span>主要選單</span></a>
                </div>
                <ul class="menu" id="remainmenu">
                    <li class="item-134 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>活動專區/活動列表" >活動專區</a></div><ul><li class="item-145 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>活動專區/活動列表" >活動列表</a></div></li></ul></li><li class="item-135 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>育兒預備/生育價值" >育兒預備</a></div><ul><li class="item-146 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>育兒預備/生育價值" >傳家寶貝</a></div></li><li class="item-148 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>育兒預備/生育預備-家庭環境" >甜蜜天使窩</a></div></li><li class="item-149 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>育兒預備/生育預備-家庭資源盤點與規劃" >資源盤點與規劃</a></div></li><li class="item-256 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>育兒預備/孕期紀錄" >愛的成長紀錄</a></div></li><li class="item-270 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>育兒預備/幸福待產包" >幸福待產包</a></div></li></ul></li><li class="item-137 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>教養孩子有方法/育兒困擾" >教養孩子有方法</a></div><ul><li class="item-156 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>教養孩子有方法/育兒困擾" >育兒有妙方</a></div></li><li class="item-160 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>教養孩子有方法/氣質與教養" >寶寶的氣質與教養</a></div></li><li class="item-321 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>教養孩子有方法/氣質與教養-2" >網路上的育兒幫手</a></div></li><li class="item-171 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>教養孩子有方法/共讀方法-技巧" >親子共讀趣</a></div></li></ul></li><li class="item-136 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>寶寶的健康成長/寶寶的出生紀錄" >寶寶的健康成長</a></div><ul><li class="item-151 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>寶寶的健康成長/寶寶的出生紀錄" >寶寶的成長里程碑</a></div></li><li class="item-152 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>寶寶的健康成長/寶寶發展檢核表-早療" >寶寶的發展檢核</a></div></li><li class="item-154 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>寶寶的健康成長/家庭急救包" >家庭急救包</a></div></li><li class="item-273 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>寶寶的健康成長/親子活動" >體驗活動</a></div></li></ul></li><li class="item-138 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>爸媽加油站/育兒的甜蜜與負擔" >爸媽加油站</a></div><ul><li class="item-165 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>爸媽加油站/育兒的甜蜜與負擔" >育兒的甜蜜與負擔</a></div></li><li class="item-178 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>爸媽加油站/協力共親職" >協力共親職</a></div></li><li class="item-169 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>爸媽加油站/幸福辯論賽" >幸福辯論賽</a></div></li></ul></li><li class="item-315 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $base_url;?>index.php/edm_print" >　電子報　</a></div><ul><li class="item-316 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $base_url;?>index.php/reg" >訂閱電子報</a></div></li><li class="item-318 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $base_url;?>index.php/edm_print" >各期電子報</a></div></li><li class="item-317 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $base_url;?>index.php/member_content" >會員專區</a></div></li></ul></li><li class="item-144 deeper parent mainlevel1"><div class="itemlevel1">		<a href="<?php echo $home;?>相關資源" >相關資源</a></div><ul><li class="item-206 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>相關資源/婚姻教育與親職教育相關資源" >婚姻教育與親職教育相關資源</a></div></li><li class="item-277 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span>		<a href="<?php echo $home;?>相關資源/政府部門相關資源" >政府部門相關資源</a></div></li></ul></li></ul>

                <script language="JavaScript">
                    // rene
                    jQuery(document).ready(function() {
                        jQuery("#remainmenu .parent").each(function(){
                            jQuery(this).children().children("a").attr("href", "#");
                        });

                        jQuery(".mainlevel2").hide();
                        jQuery("#remainmenu .active").find("li").show();

                        jQuery(".mainlevel1").on("click", function() {
                            jQuery(".mainlevel1").removeClass("active");
                            jQuery(this).addClass("active");
                            jQuery(".mainlevel2").hide();
                            jQuery(this).find("li").show();
                        });
                    });
                </script>

            </div>

        </div>