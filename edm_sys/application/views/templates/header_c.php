<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iCoparenting" />
    <title>iCoparenting</title>
    <link href="index.html" rel="canonical" />
    <link href="indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/reset.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/css/tabs.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />
    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/system/js/caption.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/ch/js/template.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/respond.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(window).on('load',  function() {
            new JCaption('img.caption');
        });
        jQuery(document).ready(function(){
            jQuery('.hasTooltip').tooltip({"html": true,"container": "body"});
        });
        jQuery(document).ready(function(){$=jQuery;var tabs_num=jQuery('#tabs_announcement .tab-title').length;jQuery('#tabs_announcement .tab-separator').each(function(index,obj){var offset=0+((index+1)*120.5);var top=8;jQuery(obj).css('left',offset+'px');jQuery(obj).css('top',top+'px');});jQuery('#tabs_announcement .tab-link').each(function(index,obj){jQuery(obj).bind('click focus',function(){jQuery('#tabs_announcement  .tab-link').removeClass("active");jQuery(this).addClass("active");jQuery('#tabs_announcement .tab-content').hide().eq(index).show();});});jQuery('#tabs_announcement .tab-link').eq(0).click();$('#tabs_announcement .tab-link').mouseover(function(){$(this).addClass('hover');}).mouseout(function(){$(this).removeClass('hover');});});
        ;(function(window,$){var $mainmenu;var $mainmainmenu;var $sfmenu;var $menuLinks;$(function(){$sfmenu=$('ul.sf-menu');$menuLinks=$('.menu_link_1');$submenu_warpper=$('.submenu_warpper');$sfmenu.superfish({autoArrows:false,dropShadows:false});$mainmenu=$('#open-mainmenu');$mainmainmenu=$('.mainmainmenu');$mainmenu.bind('click',function(){$('#topmenu').hide();$mainmainmenu.toggle({speed:500});});initMenuState();$(document).bind('responsive',initMenuState);});var initMenuState=function(){$mainmainmenu.show();$sfmenu.find('ul').css('position','absolute');$menuLinks.unbind('click');}})(window,jQuery);
        ;(function(window,$){$(function(){var remainmenu=$('#remainmenu');var openremainmenu=$('#open-remainmenu');openremainmenu.bind('click',function(){if(remainmenu.is(':hidden')){$('.menu').hide();}
            remainmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){remainmenu.hide();}else{remainmenu.show();}}})(window,jQuery);
        ;(function(window,$){$(function(){var retopmenu=$('#retopmenu');var openretopmenu=$('#open-retopmenu');openretopmenu.bind('click',function(){if(retopmenu.is(':hidden')){$('.menu').hide();}
            retopmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){retopmenu.hide();}else{retopmenu.show();}}})(window,jQuery);
    </script>
    <!-- Ming -->
    <script src="<?php echo $base_url;?>assets_view/js/jquery-1.8.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#education_datepicker" ).datepicker();
        });
    </script>
    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/html5shiv.js"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/html5.js"></script>
    <![endif]-->

    <script>
        jQuery(document).ready(function() {
            jQuery("#retopmenu").append(jQuery(".itp-gs_research"));
            jQuery("#retopmenu").append(jQuery(".mod_retopshare"));
        });
    </script>




    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72465755-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Universal Google Analytics Plugin by PB Web Development -->


</head>
<body class="com_content view-featured no-layout no-task itemid-126" style="background: none !important;">
<!-- Body -->
<div class="all" style="width: 100%; border-top: none;">
    <div class="container" style="padding-top: 10px;">
