<!--/span-->
<div class="span9" id="content">
    <form action="?" method="post">
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Bootstrap dataTables with Toolbar</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <a href="#"><button class="btn btn-success" name="re_send" value="1">全部重新寄送發驗證信 <i class="icon-plus icon-white"></i></button></a>
                    </div>
                    <div class="btn-group pull-right">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Print</a></li>
                            <li><a href="#">Save as PDF</a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul>
                    </div>
                </div>

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                    <thead>
                    <tr>
                        <th>員工ID</th>
                        <th>帳號</th>
                        <th>姓名</th>
                        <th>性別</th>
                        <th>年齡</th>
                        <th>會員狀態</th>
                        <th>電子報</th>
                        <th>功能</th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php 
                        if(!empty($empolyee))
                        {
                            foreach($empolyee as $item)
                            {
                                echo '<tr class="odd gradeX">';
                                echo '<td>'.$item["id"].'</td>';
                                echo '<td>'.$item["name"].'</td>';
                                echo '<td>'.$item["mobile"].'</td>';

                                echo '<td class="center"> <button style="width: 100%;" name="edit" value="'.$item["id"].'">瀏覽</button><button style="width: 100%;" name="del" value="'.$item["id"].'">刪除</button></td>';
                                echo '</tr>';
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- /block -->
    <!--/.fluid-container-->

</div>
        </form>