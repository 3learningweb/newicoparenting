<div class="span9" id="content">
    <form action="?" method="post">
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">會員管理</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">


                    <form class="form-horizontal">
                        <fieldset>
                            <legend>編輯會員</legend>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">選單ID </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" value="<?php if(isset($menu)){echo $menu[0]["id"]; }?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">選單名稱 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead" name="name"  data-provide="typeahead" data-items="4" value="<?php if(isset($menu)){echo $menu[0]["name"]; }?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">超連結 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead" name="link"  data-provide="typeahead" data-items="4" value="<?php if(isset($menu)){echo $menu[0]["link"]; }?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">排序 </label>
                                <div class="controls">
                                    <input type="number" class="span6" id="typeahead" name="m_order"  data-provide="typeahead" data-items="4" value="<?php if(isset($menu)){echo $menu[0]["m_order"]; }?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="hidden" name="parent" value="<?php echo $parent ?>">
                                <button class="btn btn-primary" name="<?php if(isset($menu)){echo "save"; }else{ echo "add";}?>" value="<?php if(isset($menu)){echo $menu[0]["id"]; }else{ echo "1";}?>">儲存</button>
                            </div>
                        </fieldset>
                    </form>
                    </table>
                </div>
            </div>
            <link href="<?php echo $base_url;?>vendors/datepicker.css" rel="stylesheet" media="screen">
            <link href="<?php echo $base_url;?>vendors/uniform.default.css" rel="stylesheet" media="screen">
            <link href="<?php echo $base_url;?>vendors/chosen.min.css" rel="stylesheet" media="screen">

            <link href="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

            <script src="<?php echo $base_url;?>vendors/jquery-1.9.1.js"></script>
            <script src="<?php echo $base_url;?>bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/jquery.uniform.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/chosen.jquery.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/bootstrap-datepicker.js"></script>

            <script src="<?php echo $base_url;?>vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
            <script src="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

            <script src="<?php echo $base_url;?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

            <script type="text/javascript" src="<?php echo $base_url;?>vendors/jquery-validation/dist/jquery.validate.min.js"></script>
            <script src="<?php echo $base_url;?>assets/form-validation.js"></script>

            <script src="<?php echo $base_url;?>assets/scripts.js"></script>
            <script>

                jQuery(document).ready(function() {
                    FormValidation.init();
                });


                $(function() {
                    $(".datepicker").datepicker();
                    $(".uniform_on").uniform();
                    $(".chzn-select").chosen();
                    $('.textarea').wysihtml5();

                    $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index+1;
                        var $percent = ($current/$total) * 100;
                        $('#rootwizard').find('.bar').css({width:$percent+'%'});
                        // If it's the last tab then hide the last button and show the finish instead
                        if($current >= $total) {
                            $('#rootwizard').find('.pager .next').hide();
                            $('#rootwizard').find('.pager .finish').show();
                            $('#rootwizard').find('.pager .finish').removeClass('disabled');
                        } else {
                            $('#rootwizard').find('.pager .next').show();
                            $('#rootwizard').find('.pager .finish').hide();
                        }
                    }});
                    $('#rootwizard .finish').click(function() {
                        alert('Finished!, Starting over!');
                        $('#rootwizard').find("a[href*='tab1']").trigger('click');
                    });
                });
            </script>
        </div>
        <!-- /block -->
    </div>
        </form>
</div>
</div>