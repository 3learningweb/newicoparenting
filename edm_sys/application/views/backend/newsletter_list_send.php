        <!--/span-->
        <div class="span9" id="content">

            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <form action="?" method="post">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">電子報管理</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                            <div class="table-toolbar">

                            </div>

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                                <thead>
                                <tr>
                                    <th>電子報ID</th>
                                    <th>報別</th>
                                    <th>刊別</th>
                                    <th>電子報名稱</th>
                                    <th>發刊日期</th>
                                    <th>新增日期</th>
                                    <th>修改日期</th>
                                    <th>狀態</th>
                                    <th>功能</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($edm_main))
                                        {
                                            foreach($edm_main as $item)
                                            { ?>
                                                <tr class="odd gradeX">
                                                <td><?php echo $item["id"];?></td>
                                                <td><?php  if($item["type"] == 1){echo "季刊";}elseif($item["type"] == 2){echo "特別刊";};?></td>
                                                <td><?php echo $item["print"];?></td>
                                                <td><?php echo $item["title"];?></td>
                                                <td class="center"><?php echo $item["publish"];?></td>
                                                <td class="center"><?php echo $item["create_date"];?></td>
                                                <td class="center"><?php echo $item["modify_date"];?></td>
                                                <td class="center"><?php echo $item["publish_state"];?></td>
                                                <td class="center"><button name="<?php  if($item["type"] == 1){echo "send1";}elseif($item["type"] == 2){echo "send2";};?>" value="<?php echo $item["id"];?>">發送</button>
                                                </tr>
                                        <?php     }
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                        </form>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
