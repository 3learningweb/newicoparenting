<div class="span9" id="content">




    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">電子報管理</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">


                    <form class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend>編輯電子報</legend>

                            <div class="form-actions">

                                <button class="btn <?php if($theme == 1){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="1" <?php if($edm_main[0]["pre_age"] == 1){echo 'style="color:yellow;"';}?>><?php echo $theme_arr[0];?>1</button>
                                <button class="btn <?php if($theme == 2){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="2" <?php if($edm_main[0]["pre_age"] == 2){echo 'style="color:yellow;"';}?>><?php echo $theme_arr[0];?>2</button>
                                <button class="btn <?php if($theme == 3){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="3" <?php if($edm_main[0]["pre_age"] == 3){echo 'style="color:yellow;"';}?>><?php echo $theme_arr[0];?>3</button>
                                <button class="btn <?php if($theme == 4){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="4" <?php if($edm_main[0]["pre_age"] == 4){echo 'style="color:yellow;"';}?>><?php echo $theme_arr[0];?>4</button>
                                <button class="btn <?php if($theme == 5){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="5"><?php echo $theme_arr[1];?></button>
                                <button class="btn <?php if($theme == 6){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="6"><?php echo $theme_arr[2];?></button>
                                <!--<button class="btn <?php if($theme == 7){echo "btn-danger";} else{ echo "btn-primary";}?>" name="change" value="7"><?php echo $theme_arr[3];?></button>-->
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">目錄頁標題 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="list_title" value="<?php if(!empty($content[0]["list_title"])){echo $content[0]["list_title"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">目錄頁簡述 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="list_intro" value="<?php if(!empty($content[0]["list_intro"])){echo $content[0]["list_intro"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="fileInput">目錄頁圖片</label>
                                <div class="controls">
                                    <?php 
                                        if(!empty($content[0]["list_img"]))
                                        {
                                            echo '<img src="'.$base_url.'upload/'.$content[0]["list_img"].'" style="max-width: 50%;">';
                                        }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput" type="file" name="list_img">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">文章標題 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="title" value="<?php if(!empty($content[0]["title"])){echo $content[0]["title"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="typeahead">文章格式 </label>
                                <div class="controls">
                                    <label>
                                        <input type="radio" id="optionsCheckbox2" name="type" value="1" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==1){echo "checked";}?> onclick="showfortemp4(this)">
                                        左圖繞文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="2" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==2){echo "checked";}?> onclick="showfortemp4(this)">
                                        右圖繞文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="3" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==3){echo "checked";}?> onclick="showfortemp4(this)">
                                        上圖下文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="4" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==4){echo "checked";}?> onclick="showfortemp4(this)">
                                        3圖繞文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="5" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==5){echo "checked";}?> onclick="showfortemp4(this)">
                                        右左版型
                                        <input type="radio" id="optionsCheckbox2" name="type" value="6" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==6){echo "checked";}?> onclick="showfortemp4(this)">
                                        上下版型
                                    </label>
                                </div>
                            </div>
                            <div class="control-group"  id="temp56_item1" style="display: <?php if(!empty($content[0]["type"]) && ($content[0]["type"]==5 || $content[0]["type"]==6)){echo "none";}?>;">
                                <label class="control-label" for="fileInput">文章照片</label>
                                <div class="controls">
                                    <?php 
                                    if(!empty($content[0]["img"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$content[0]["img"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput" type="file" name="img">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group"  id="temp4_item1" style="display: <?php if(!empty($content[0]["type"]) && ($content[0]["type"]!=4 || $content[0]["type"]==5 || $content[0]["type"]==6)){echo "none";}?>;">
                                <label class="control-label" for="fileInput">文章照片2</label>
                                <div class="controls">
                                    <?php
                                    if(!empty($content[0]["img2"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$content[0]["img2"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput2" type="file" name="img2">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group"  id="temp4_item2" style="display: <?php if(!empty($content[0]["type"]) && ($content[0]["type"]!=4 || $content[0]["type"]==5 || $content[0]["type"]==6)){echo "none";}?>;">
                                <label class="control-label" for="fileInput">文章照片3</label>
                                <div class="controls">
                                    <?php
                                    if(!empty($content[0]["img3"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$content[0]["img3"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput3" type="file" name="img3">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group" id="temp56_item2"  style="display: <?php if(!empty($content[0]["type"]) && ($content[0]["type"]==5 || $content[0]["type"]==6)){echo "none";}?>;">
                                <label class="control-label" for="typeahead" >文章副標題1 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="title2" value="<?php if(!empty($content[0]["title2"])){echo $content[0]["title2"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">內文</label>
                                <div class="controls">
                                    <textarea id="edm_content" name="edm_content" style="width: 600px; height: 200px"><?php if(!empty($content[0]["content"])){echo $content[0]["content"];}?></textarea>
                                </div>
                            </div>
                            <div class="control-group"  id="temp56_item3" style="display: <?php if(!empty($content[0]["type"]) && ($content[0]["type"]!=5 || $content[0]["type"]!=6)){echo "none";}?>;">
                                <label class="control-label" for="typeahead">文章副標題2 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="title3" value="<?php if(!empty($content[0]["title3"])){echo $content[0]["title3"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group" id="temp4_item3" style="display: <?php if(!empty($content[0]["type"]) && $content[0]["type"]!=4){echo "none";}?>;">
                                <label class="control-label" for="select01">內文2</label>
                                <div class="controls">
                                    <textarea id="edm_content2" name="edm_content2" style="width: 600px; height: 200px"><?php if(!empty($content[0]["content2"])){echo $content[0]["content2"];}?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">建議方法</label>
                                <div class="controls">
                                    <textarea  id="edm_suggestion" name="edm_suggestion" style="width: 600px; height: 200px"><?php if(!empty($content[0]["suggestion"])){echo $content[0]["suggestion"];}?></textarea>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <button class="btn btn-primary" name="<?php if(!empty($content[0])){echo "update";}else{echo "save";}?>" value="<?php echo $theme;?>">暫存</button>
                                <button class="btn" name="back" value="1">不儲存 回上一步</button>
                                <button class="btn" name="<?php if(!empty($content[0])){echo "update_back";}else{echo "save_back";}?>" value="<?php echo $theme;?>">儲存 回電子報管理</button>
                               <!-- <button class="btn" name="all_to_pdf" value="1">轉換為PDF檔案</button> -->
                                <a href="<?php echo $url_view; ?>" target="_blank"> <span class="btn">預覽網頁版</span></a>
                                <a href="<?php
                                //$url = urlencode(base_url().'index.php/edm_list2?id='.$item["id"].'&theme=2');
                                echo 'http://api.html2pdfrocket.com/pdf?value='.$url.'&apikey=0bfe9070-1c86-4cd3-99ce-4a2e1e4f5fe8';
                                ?>" target="_blank"> <span class="btn">預覽PDF版</span></a>

                            </div>
                        </fieldset>
                    </form>
                    </table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
</div>
<hr>
<footer>
    <p></p>
</footer>
</div>
<!--/.fluid-container-->
<link href="<?php echo $base_url;?>vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/chosen.min.css" rel="stylesheet" media="screen">

<link href="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

<script src="<?php echo $base_url;?>vendors/jquery-1.9.1.js"></script>
<script src="<?php echo $base_url;?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $base_url;?>vendors/jquery.uniform.min.js"></script>
<script src="<?php echo $base_url;?>vendors/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url;?>vendors/bootstrap-datepicker.js"></script>

<script src="<?php echo $base_url;?>vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

<script src="<?php echo $base_url;?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url;?>vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo $base_url;?>assets/form-validation.js"></script>

<script src="<?php echo $base_url;?>assets/scripts.js"></script>
<script>

    jQuery(document).ready(function() {
        FormValidation.init();
    });


    $(function() {
        $(".datepicker").datepicker();
        $(".uniform_on").uniform();
        $(".chzn-select").chosen();
        $('.textarea').wysihtml5();

        $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.bar').css({width:$percent+'%'});
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }});
        $('#rootwizard .finish').click(function() {
            alert('Finished!, Starting over!');
            $('#rootwizard').find("a[href*='tab1']").trigger('click');
        });
    });
</script>
<script src="<?php echo $base_url;?>ckeditor/ckeditor.js"></script>
<script src="<?php echo $base_url;?>ckfinder/ckfinder.js"></script>
<link rel="stylesheet" href="<?php echo $base_url;?>ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
<script src="<?php echo $base_url;?>ckeditor/samples/js/sample.js"></script>

<script>
    initSample();

    function fill_textarea(){
        var content = document.getElementById("edm_content");
        var suggestion = document.getElementById("edm_suggestion");
        var data = CKEDITOR.instances.edm_content.getData();
        content.innerHTML = data;
        alert(content.value);
        var data = CKEDITOR.instances.edm_suggestion.getData();
        suggestion.value = data;
    }

    function showfortemp4(item)
    {
        var temp4_item1 =  document.getElementById("temp4_item1");
        var temp4_item2 =  document.getElementById("temp4_item2");
        var temp4_item3 =  document.getElementById("temp4_item3");
        var temp56_item1 =  document.getElementById("temp56_item1");
        var temp56_item2 =  document.getElementById("temp56_item2");
        var temp56_item3 =  document.getElementById("temp56_item3");
        if(item.value == 4)
        {
            temp4_item1.style.display = "block";
            temp4_item2.style.display = "block";
            temp4_item3.style.display = "block";
            temp56_item1.style.display = "block";
            temp56_item2.style.display = "block";
            temp56_item3.style.display = "block";
        }else if(item.value == 5 || item.value == 6)
        {
            temp4_item1.style.display = "none";
            temp4_item2.style.display = "none";
            temp4_item3.style.display = "none";
            temp56_item1.style.display = "none";
            temp56_item2.style.display = "none";
            temp56_item3.style.display = "none";
        }
        else
        {
            temp4_item1.style.display = "none";
            temp4_item2.style.display = "none";
            temp4_item3.style.display = "none";
            temp56_item1.style.display = "block";
            temp56_item2.style.display = "block";
            temp56_item3.style.display = "block";
        }
    }
</script>

<script>
    CKFinder.setupCKEditor();
    CKEDITOR.replace( 'edm_content', {
        toolbar: 'Full',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
    CKEDITOR.replace( 'edm_content2', {
        toolbar: 'Full',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
    CKEDITOR.replace( 'edm_suggestion', {
        toolbar: 'Full',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>
</body>

</html>

