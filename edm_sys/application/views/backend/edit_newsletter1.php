<div class="span9" id="content">

    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">電子報管理</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">


                    <form class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend>編輯電子報</legend>
                            <div class="control-group">
                                <label class="control-label" for="typeahead" >刊別 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="print" value="<?php if(!empty($edm_main[0]["print"])){echo $edm_main[0]["print"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead" >刊別圖片 </label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="print_img">
                                        <option value="1" <?php if(!empty($edm_main[0]["print_img"]) && $edm_main[0]["print_img"]==1){echo "selected";}?>>春</option>
                                        <option value="2" <?php if(!empty($edm_main[0]["print_img"]) && $edm_main[0]["print_img"]==2){echo "selected";}?>>夏</option>
                                        <option value="3" <?php if(!empty($edm_main[0]["print_img"]) && $edm_main[0]["print_img"]==3){echo "selected";}?>>秋</option>
                                        <option value="4" <?php if(!empty($edm_main[0]["print_img"]) && $edm_main[0]["print_img"]==4){echo "selected";}?>>冬</option>
                                        <option value="5" <?php if(!empty($edm_main[0]["print_img"]) && $edm_main[0]["print_img"]==5){echo "selected";}?>>特別刊</option>
                                    </select>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead" >刊別年分 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="print_year" value="<?php if(!empty($edm_main[0]["print_year"])){echo $edm_main[0]["print_year"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">電子報主題 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="title" value="<?php if(!empty($edm_main[0]["title"])){echo $edm_main[0]["title"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="control-group" style="display: none;">
                                <label class="control-label" for="date01">發行日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" name="publish" value="<?php if(!empty($edm_main[0]["publish"])){echo $edm_main[0]["publish"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="fileInput">列表頁圖片上傳</label>
                                <div class="controls">
                                    <?php
                                    if(!empty($edm_main[0]["list_banner"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$edm_main[0]["list_banner"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput" type="file" name="list_banner">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="fileInput">Banner上傳</label>
                                <div class="controls">
                                    <?php 
                                    if(!empty($edm_main[0]["banner"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$edm_main[0]["banner"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput" type="file" name="banner">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group" style="display: none;">
                                <label class="control-label" for="typeahead">選擇版型 </label>
                                <div class="controls">
                                    <label>
                                        <input type="radio" id="optionsCheckbox2" name="template" value="1" <?php if(!empty($edm_main[0]["template"]) && $edm_main[0]["template"]==1){echo "checked";}else if(empty($edm_main)){ echo  "checked";}?>>
                                        版型A
                                        <input type="radio" id="optionsCheckbox2" name="template" value="2" <?php if(!empty($edm_main[0]["template"]) && $edm_main[0]["template"]==2){echo "checked";}?>>
                                        版型B
                                    </label>
                                </div>
                            </div>
                            <!--
                            <div class="control-group">
                                <label class="control-label" for="select01">選擇本期主題</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select">
                                        <option>主題A</option>
                                        <option selected>主題B</option>
                                    </select>
                                </div>
                            </div>
                            -->
                            <div class="control-group">
                                <label class="control-label" for="select01">選擇年齡分層主題</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="age_theme">
                                        <option value="1" <?php if(!empty($edm_main[0]["age_theme"]) && $edm_main[0]["age_theme"]==1){echo "selected";}?>>好家庭</option>
                                        <option value="2" <?php if(!empty($edm_main[0]["age_theme"]) && $edm_main[0]["age_theme"]==2){echo "selected";}?>>好教養</option>
                                        <option value="3" <?php if(!empty($edm_main[0]["age_theme"]) && $edm_main[0]["age_theme"]==3){echo "selected";}?>>好夫妻</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">預設年齡段</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="pre_age">
                                        <option value="1" <?php if(!empty($edm_main[0]["pre_age"]) && $edm_main[0]["pre_age"]==1){echo "selected";}?>>0-2歲</option>
                                        <option value="2" <?php if(!empty($edm_main[0]["pre_age"]) && $edm_main[0]["pre_age"]==2){echo "selected";}?>>2-4歲</option>
                                        <option value="3" <?php if(!empty($edm_main[0]["pre_age"]) && $edm_main[0]["pre_age"]==3){echo "selected";}?>>4-6歲</option>
                                        <option value="4" <?php if(!empty($edm_main[0]["pre_age"]) && $edm_main[0]["pre_age"]==4){echo "selected";}?>>7歲以上</option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">新增日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="<?php if(!empty($edm_main[0]["create_date"])){echo $edm_main[0]["create_date"];}?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">修改日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="<?php if(!empty($edm_main[0]["modify_date"])){echo $edm_main[0]["modify_date"];}?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-primary"  name="<?php if(!empty($edm_main[0])){echo "update";}else{echo "save";}?>" value="<?php if(!empty($edm_main[0])){echo $edm_main[0]["id"];}else{echo "1";}?>">儲存 下一步</button>
                                <button class="btn"  name="back" value="1">返回</button>
                            </div>
                        </fieldset>
                    </form>
                    </table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
</div>
<hr>
<footer>
    <p></p>
</footer>
</div>
<!--/.fluid-container-->
<link href="<?php echo $base_url;?>vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/chosen.min.css" rel="stylesheet" media="screen">

<link href="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

<script src="<?php echo $base_url;?>vendors/jquery-1.9.1.js"></script>
<script src="<?php echo $base_url;?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $base_url;?>vendors/jquery.uniform.min.js"></script>
<script src="<?php echo $base_url;?>vendors/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url;?>vendors/bootstrap-datepicker.js"></script>

<script src="<?php echo $base_url;?>vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

<script src="<?php echo $base_url;?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url;?>vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo $base_url;?>assets/form-validation.js"></script>

<script src="<?php echo $base_url;?>assets/scripts.js"></script>
<script>

    jQuery(document).ready(function() {
        FormValidation.init();
    });


    $(function() {
        $(".datepicker").datepicker();
        $(".uniform_on").uniform();
        $(".chzn-select").chosen();
        $('.textarea').wysihtml5();

        $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.bar').css({width:$percent+'%'});
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }});
        $('#rootwizard .finish').click(function() {
            alert('Finished!, Starting over!');
            $('#rootwizard').find("a[href*='tab1']").trigger('click');
        });
    });
</script>
</body>

</html>