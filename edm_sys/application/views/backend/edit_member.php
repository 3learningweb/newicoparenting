<div class="span9" id="content">
    <form action="?" method="post">
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">會員管理</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">


                    <form class="form-horizontal">
                        <fieldset>
                            <legend>編輯會員</legend>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">會員ID </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" value="<?php echo $user_data[0]["id"];?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">帳號 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="acc" value="<?php echo $user_data[0]["acc"];?>"  >
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">暱稱 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="name" value="<?php echo $user_data[0]["name"];?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">性別</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="sex">
                                        <?php  if(!empty($user_data)){
                                            if($user_data[0]["sex"] == 1)
                                            {
                                                echo ' <option value="1" selected>男</option>';
                                            }else
                                            {
                                                echo ' <option value="1" >男</option>';
                                            }
                                            if($user_data[0]["sex"] == 2)
                                            {
                                                echo ' <option value="2" selected>女</option>';
                                            }else
                                            {
                                                echo ' <option value="2" >女</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">年齡</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="age">
                                        <?php  if(!empty($user_data)){
                                            if($user_data[0]["age"] == 1)
                                            {
                                                echo ' <option value="1" selected>19 歲及以下 </option>';
                                            }else
                                            {
                                                echo ' <option value="1" >19 歲及以下 </option>';
                                            }
                                            if($user_data[0]["age"] == 2)
                                            {
                                                echo ' <option value="2" selected>20 歲-24 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="2" >20 歲-24 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 3)
                                            {
                                                echo ' <option value="3" selected>25 歲-29 歲</option>';
                                            }else
                                            {
                                                echo ' <option value="3" >25 歲-29 歲</option>';
                                            }
                                            if($user_data[0]["age"] == 4)
                                            {
                                                echo ' <option value="4" selected>30 歲-34 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="4" >30 歲-34 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 5)
                                            {
                                                echo ' <option value="5" selected>35 歲-39 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="5" >35 歲-39 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 6)
                                            {
                                                echo ' <option value="6" selected>40 歲-44 歲 <</option>';
                                            }else
                                            {
                                                echo ' <option value="6" >40 歲-44 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 7)
                                            {
                                                echo ' <option value="7" selected>45 歲-49 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="7" >45 歲-49 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 8)
                                            {
                                                echo ' <option value="8" selected>50 歲-54 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="8" >50 歲-54 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 9)
                                            {
                                                echo ' <option value="9" selected>55 歲-59 歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="9" >55 歲-59 歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 10)
                                            {
                                                echo ' <option value="10" selected>60 歲-64歲 </option>';
                                            }else
                                            {
                                                echo ' <option value="10" >60 歲-64歲 </option>';
                                            }
                                            if($user_data[0]["age"] == 11)
                                            {
                                                echo ' <option value="11" selected>65 歲及以上</option>';
                                            }else
                                            {
                                                echo ' <option value="11" >65 歲及以上</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">居住地 </label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="location">
                                        <?php 
                                            if(!empty($user_data[0]["location"]))
                                            {
                                                foreach($location as $item)
                                                {
                                                    if($user_data[0]["location"] == $item["id"])
                                                    {
                                                        echo '<option value='.$item["id"].' selected >'.$item["name"].'</option>';
                                                    }else
                                                    {
                                                        echo '<option value='.$item["id"].'>'.$item["name"].'</option>';
                                                    }

                                                }
                                            }else
                                            {
                                                foreach($location as $item)
                                                {
                                                    echo '<option value='.$item["id"].'>'.$item["name"].'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">教育程度</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="edu">
                                        <?php  if(!empty($user_data)){
                                            if($user_data[0]["edu"] == 1)
                                            {
                                                echo ' <option value="1" selected>無或自修 </option>';
                                            }else
                                            {
                                                echo ' <option value="1" >無或自修 </option>';
                                            }
                                            if($user_data[0]["edu"] == 2)
                                            {
                                                echo ' <option value="2" selected>自修或小學</option>';
                                            }else
                                            {
                                                echo ' <option value="2" >自修或小學</option>';
                                            }
                                            if($user_data[0]["edu"] == 3)
                                            {
                                                echo ' <option value="3" selected>國中/初中/初職</option>';
                                            }else
                                            {
                                                echo ' <option value="3" >國中/初中/初職</option>';
                                            }
                                            if($user_data[0]["edu"] == 4)
                                            {
                                                echo ' <option value="4" selected>高中/高職/士官學校</option>';
                                            }else
                                            {
                                                echo ' <option value="4" >高中/高職/士官學校</option>';
                                            }
                                            if($user_data[0]["edu"] == 5)
                                            {
                                                echo ' <option value="5" selected>五專/二專/三專 /軍警專修班/軍警官學校</option>';
                                            }else
                                            {
                                                echo ' <option value="5" >五專/二專/三專 /軍警專修班/軍警官學校</option>';
                                            }
                                            if($user_data[0]["edu"] == 6)
                                            {
                                                echo ' <option value="6" selected>大學（含技術學院與科技大學）</option>';
                                            }else
                                            {
                                                echo ' <option value="6" >大學（含技術學院與科技大學）</option>';
                                            }
                                            if($user_data[0]["edu"] == 7)
                                            {
                                                echo ' <option value="7" selected>碩士</option>';
                                            }else
                                            {
                                                echo ' <option value="7" >碩士</option>';
                                            }
                                            if($user_data[0]["edu"] == 8)
                                            {
                                                echo ' <option value="8" selected>博士</option>';
                                            }else
                                            {
                                                echo ' <option value="8" >博士</option>';
                                            }
                                            if($user_data[0]["edu"] == 9)
                                            {
                                                echo ' <option value="9" selected>其他</option>';
                                            }else
                                            {
                                                echo ' <option value="9" >其他</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">會員狀態</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="mail_check">
                                        <?php  if(!empty($user_data)){
                                            if($user_data[0]["mail_check"] == 0)
                                            {
                                                echo ' <option value="0" selected>非正式會員</option>';
                                            }else
                                            {
                                                echo ' <option value="0" >非正式會員</option>';
                                            }
                                            if($user_data[0]["mail_check"] == 1)
                                            {
                                                echo ' <option value="1" selected>正式會員</option>';
                                            }else
                                            {
                                                echo ' <option value="1" >正式會員</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">電子報訂閱狀態</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select" name="edm">
                                        <?php  if(!empty($user_data)){
                                            if($user_data[0]["edm"] == 0)
                                            {
                                                echo ' <option value="1" selected>無</option>';
                                            }else
                                            {
                                                echo ' <option value="1" >無</option>';
                                            }
                                            if($user_data[0]["edm"] == 1)
                                            {
                                                echo ' <option value="2" selected>已訂閱</option>';
                                            }else
                                            {
                                                echo ' <option value="2" >已訂閱</option>';
                                            }
                                            if($user_data[0]["edm"] == 2)
                                            {
                                                echo ' <option value="2" selected>未訂閱</option>';
                                            }else
                                            {
                                                echo ' <option value="2" >未訂閱</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">小孩生日</label>
                                <?php 
                                    if(!empty($child))
                                    {
                                        for($i=0; $i<count($child); $i++)
                                        {?>
                                            <div class="controls">
                                                    第<?php echo ($i+1);?>個小孩<input type="text" class="input-xlarge datepicker" id="date<?php echo $i;?> " name="date<?php echo $i;?>" value="<?php echo $child[$i]["birthday"];?>">
                                            </div>
                                        <?php }
                                    }
                                ?>
                                <input type="hidden" name="birthday_num" value="<?php  if(!empty($child)){echo count($child);}else{echo 0;};?>">

                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">訂閱日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="02/16/12">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">修改日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="16/09/01" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-primary" name="save" value="<?php echo $user_data[0]["id"];?>">儲存</button>
                                <button class="btn">取消</button>
                            </div>
                        </fieldset>
                    </form>
                    </table>
                </div>
            </div>
            <link href="<?php echo $base_url;?>vendors/datepicker.css" rel="stylesheet" media="screen">
            <link href="<?php echo $base_url;?>vendors/uniform.default.css" rel="stylesheet" media="screen">
            <link href="<?php echo $base_url;?>vendors/chosen.min.css" rel="stylesheet" media="screen">

            <link href="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

            <script src="<?php echo $base_url;?>vendors/jquery-1.9.1.js"></script>
            <script src="<?php echo $base_url;?>bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/jquery.uniform.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/chosen.jquery.min.js"></script>
            <script src="<?php echo $base_url;?>vendors/bootstrap-datepicker.js"></script>

            <script src="<?php echo $base_url;?>vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
            <script src="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

            <script src="<?php echo $base_url;?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

            <script type="text/javascript" src="<?php echo $base_url;?>vendors/jquery-validation/dist/jquery.validate.min.js"></script>
            <script src="<?php echo $base_url;?>assets/form-validation.js"></script>

            <script src="<?php echo $base_url;?>assets/scripts.js"></script>
            <script>

                jQuery(document).ready(function() {
                    FormValidation.init();
                });


                $(function() {
                    $(".datepicker").datepicker();
                    $(".uniform_on").uniform();
                    $(".chzn-select").chosen();
                    $('.textarea').wysihtml5();

                    $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index+1;
                        var $percent = ($current/$total) * 100;
                        $('#rootwizard').find('.bar').css({width:$percent+'%'});
                        // If it's the last tab then hide the last button and show the finish instead
                        if($current >= $total) {
                            $('#rootwizard').find('.pager .next').hide();
                            $('#rootwizard').find('.pager .finish').show();
                            $('#rootwizard').find('.pager .finish').removeClass('disabled');
                        } else {
                            $('#rootwizard').find('.pager .next').show();
                            $('#rootwizard').find('.pager .finish').hide();
                        }
                    }});
                    $('#rootwizard .finish').click(function() {
                        alert('Finished!, Starting over!');
                        $('#rootwizard').find("a[href*='tab1']").trigger('click');
                    });
                });
            </script>
        </div>
        <!-- /block -->
    </div>
        </form>
</div>
</div>