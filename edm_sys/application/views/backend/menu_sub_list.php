        <!--/span-->
        <div class="span9" id="content">

            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <form action="?" method="post">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">選單管理</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    <a href="new_newsletter.php"><button class="btn btn-success" name="new" value="1">新增選單 <i class="icon-plus icon-white"></i></button></a>
                                    <a href="new_newsletter.php"><button class="btn btn-success" name="back" value="1">返回 <i class="icon-plus icon-white"></i></button></a>
                                    <input type="hidden" name="parent" value="<?php echo $parent ?>">
                                </div>

                            </div>

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                                <thead>
                                <tr>
                                    <th>選單ID</th>
                                    <th>選單名稱</th>
                                    <th>排序</th>
                                    <th>功能</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($menu))
                                        {
                                            foreach($menu as $item)
                                            { ?>
                                                <tr class="odd gradeX">
                                                <td><?php echo $item["id"];?></td>
                                                <td><?php echo $item["name"];?></td>
                                                    <td><?php echo $item["m_order"];?></td>
                                                <td class="center"><button name="edit" value="<?php echo $item["id"];?>">編輯</button>
                                                    <button name="sub" value="<?php echo $item["id"];?>" >子選單</button></td>
                                                </tr>
                                        <?php     }
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                        </form>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
