<?php
define('_JEXEC', 1);

define('JPATH_BASE', '../../');

define('DS', DIRECTORY_SEPARATOR);

require_once JPATH_BASE . DS . 'includes' . DS . 'defines.php';
require_once JPATH_BASE . DS . 'includes' . DS . 'framework.php';

//JDEBUG ? $_PROFILER->mark('afterLoad') : null;
$mainframe = &JFactory::getApplication('site');
$mainframe->initialise();

// header("Content-Type:text/html;charset=utf-8");
// header("Cache-Control: no-cache, must-revalidate");

//jimport('joomla.factory');

// require_once "html2doc.php";

$app = JFactory::getApplication();

$post      = $app->input->getArray($_POST);




// Load the files we need:
require_once 'phpword/PHPWord.php';



// New Word Document
$PHPWord = new PHPWord();

// New portrait section
$section = $PHPWord->createSection();

/* 標題 */
$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'italic'=>false, 'size'=>36));
$PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
$section->addText('親子野餐趣', 'rStyle', 'pStyle');


/* 日期地點 */
$section->addText('野餐日期_______________', array('name'=>'Verdana', 'color'=>'006699'), array('align'=>'left', 'spaceAfter'=>100));
$section->addText('野餐地點_______________', array('name'=>'Verdana', 'color'=>'006699'), array('align'=>'left', 'spaceAfter'=>100));
$section->addTextBreak(1);




/* 加入表格  共同設定部分*/
// Define table style arrays
$styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>80);
$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'849F17', 'bgColor'=>'94BF17');
// Define cell style arrays
$styleCell = array('valign'=>'center');
// Define font style for first row
$fontStyle = array('bold'=>true, 'align'=>'center');
// Add table style
$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);




if ($post['invited_data']) {
	$items = explode(",", $post['invited_data']);

	// 加入第一個表格
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('邀請對象', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}





if ($post['place_data']) {
	$items = explode(",", $post['place_data']);
	$section->addTextBreak(1); //跳行！
	//第二個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('場地選擇', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}




if ($post['food_data']) {
	$items = explode(",", $post['food_data']);
	$section->addTextBreak(1); //跳行！
	//第3個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('食品準備', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}


if ($post['game_data']) {
	$items = explode(",", $post['game_data']);

	$section->addTextBreak(1); //跳行！
	//第4個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('野餐小活動', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}


if ($post['recommend_data']) {
	$items = explode(",", $post['recommend_data']);

	$section->addTextBreak(1); //跳行！
	//第5個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('共讀推薦', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}


if ($post['adult_data']) {
	$items = explode(",", $post['adult_data']);
	$section->addTextBreak(1); //跳行！
	//第6個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('大人攜帶物品', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}


if ($post['children_data']) {
	$items = explode(",", $post['children_data']);
	$section->addTextBreak(1); //跳行！
	//第7個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('小孩攜帶物品', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}

}


if ($post['other_data']) {
	$items = explode(",", $post['other_data']);

	$section->addTextBreak(1); //跳行！
	//第8個表格

	$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'00FF00', 'bgColor'=>'00FF00');
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// Add table
	$table = $section->addTable('myOwnTableStyle');

	// Add row
	$table->addRow(300);

	// Add cells
	$table->addCell(2000, $styleCell)->addText('其他', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('清點打Ｖ', $fontStyle);

	foreach ($items as $key => $item) {
		$table->addRow();
		$table->addCell(10000)->addText($key + 1 . "." . $item);
		$table->addCell(1000)->addText('');
	}
}



// Save File
$h2d_file_uri = tempnam('', 'htd');
$objWriter    = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
$objWriter->save($h2d_file_uri);

// Download the file:
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=親子野餐趣.docx');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($h2d_file_uri));
//ob_clean();
//flush();
$status = readfile($h2d_file_uri);
unlink($h2d_file_uri);
exit;

?>
