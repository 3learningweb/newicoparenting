<?php
define( '_JEXEC', 1 );

define('JPATH_BASE', '../../');

define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;
$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

header ("Content-Type:text/html;charset=utf-8");
header ("Cache-Control: no-cache, must-revalidate");

jimport( 'joomla.factory');

require_once ( "html2doc.php" );

$app = JFactory::getApplication();

$post = $app->input->getArray($_POST);
$todo_data = $post['todo_data'];

ob_start();

?>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=big5">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-1593833729 1073750107 16 0 415 0;}
@font-face
	{font-family:微軟正黑體;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:136;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:135 680476672 22 0 1048585 0;}
@font-face
	{font-family:"\@微軟正黑體";
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-charset:136;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"Calibri Light";
	panose-1:2 15 3 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-1610611985 1073750139 0 0 415 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:Verdana;
	mso-bidi-font-family:新細明體;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"註解文字 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"頁首 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"頁尾 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"註解主旨 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"註解方塊文字 字元";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"Calibri Light","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:24.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
p.small, li.small, div.small
	{mso-style-name:small;
	mso-style-unhide:no;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	mso-bidi-font-size:1.0pt;
	font-family:"Verdana","sans-serif";
	mso-fareast-font-family:Verdana;
	mso-bidi-font-family:新細明體;}
p.tt, li.tt, div.tt
	{mso-style-name:tt;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	line-height:17.0pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
span.a
	{mso-style-name:"註解文字 字元";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:註解文字;}
span.a0
	{mso-style-name:"頁首 字元";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁首;}
span.a1
	{mso-style-name:"頁尾 字元";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:頁尾;}
span.a2
	{mso-style-name:"註解主旨 字元";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:註解主旨;
	font-weight:bold;}
span.a3
	{mso-style-name:"註解方塊文字 字元";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:註解方塊文字;
	font-family:"Calibri Light","sans-serif";
	mso-ascii-font-family:"Calibri Light";
	mso-hansi-font-family:"Calibri Light";}
p.sol, li.sol, div.sol
	{mso-style-name:sol;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"新細明體","serif";
	mso-bidi-font-family:新細明體;}
p.msochpdefault, li.msochpdefault, div.msochpdefault
	{mso-style-name:msochpdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:新細明體;
	mso-bidi-font-family:新細明體;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;
	mso-font-kerning:0pt;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:36.0pt 36.0pt 36.0pt 36.0pt;
	mso-header-margin:4.55pt;
	mso-footer-margin:4.55pt;
	mso-paper-source:0;
	layout-grid:18.0pt;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=ZH-TW style='text-justify-trim:punctuation'>

<div class=WordSection1 style='layout-grid:18.0pt'>

<p class=MsoNormal align=center style='text-align:center;mso-line-height-alt:
17.0pt'><b><span style='font-size:18.0pt;font-family:"微軟正黑體","sans-serif"'>協力<span
class=GramE>育兒減家務</span>：家務分工清單</span></b></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=928
 style='width:557.0pt;margin-left:86.75pt;border-collapse:collapse;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:9.8pt'>
  <td width=314 colspan=2 valign=top style='width:188.45pt;border:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:9.8pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>每天都要做</span></b></p>
  </td>
  <td width=307 colspan=2 valign=top style='width:184.3pt;border:solid black 1.0pt;
  border-left:none;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:9.8pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  lang=EN-US style='font-family:"微軟正黑體","sans-serif"'>1</span></b><span
  class=GramE><b><span style='font-family:"微軟正黑體","sans-serif"'>週</span></b></span><b><span
  lang=EN-US style='font-family:"微軟正黑體","sans-serif"'>1-2</span></b><b><span
  style='font-family:"微軟正黑體","sans-serif"'>次</span></b></p>
  </td>
  <td width=307 colspan=2 valign=top style='width:184.25pt;border:solid black 1.0pt;
  border-left:none;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:9.8pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  lang=EN-US style='font-family:"微軟正黑體","sans-serif"'>1</span></b><b><span
  style='font-family:"微軟正黑體","sans-serif"'>個月<span lang=EN-US>1-2</span>次</span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:8.05pt'>
  <td width=135 valign=top style='width:81.25pt;border:solid black 1.0pt;
  border-top:none;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>工作項目</span></b></p>
  </td>
  <td width=179 valign=top style='width:107.2pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>主責人</span></b></p>
  </td>
  <td width=128 valign=top style='width:76.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>工作項目</span></b></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>主責人</span></b></p>
  </td>
  <td width=128 valign=top style='width:76.6pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>工作項目</span></b></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:8.05pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:15.0pt'><b><span
  style='font-family:"微軟正黑體","sans-serif"'>主責人</span></b></p>
  </td>
 </tr>
 
<?php 

	$jinput = JFactory::getApplication()->input;
	$dailyjobs =  $jinput->getString("dailyjobs");
	$weeklyjobs = $jinput->getString("weeklyjobs");
	$monthlyjobs = $jinput->getString("monthlyjobs");
	
	$length = 0;
	if(!empty($dailyjobs))
	{
		$dailyjobs_arr = json_decode($dailyjobs);	
		$length = sizeof($dailyjobs_arr);		
	}	
	if(!empty($weeklyjobs))
	{
		$weeklyjobs_arr = json_decode($weeklyjobs);	
		if(sizeof($weeklyjobs_arr) > $length)
			$length = sizeof($weeklyjobs_arr);		
	}
	if(!empty($monthlyjobs))
	{
		$monthlyjobs_arr = json_decode($monthlyjobs);	
		if(sizeof($monthlyjobs_arr) > $length)
			$length = sizeof($monthlyjobs_arr);		
	}
	
	for($i = 0 ; $i < $length ; $i++)
	{
		print <<<EOT

 <tr style='mso-yfti-irow:2;height:26.15pt'>
  <td width=135 valign=top style='width:81.25pt;border:solid black 1.0pt;
  border-top:none;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:14.0pt;
  mso-line-height-rule:exactly'><span style='font-family:"微軟正黑體","sans-serif";
  color:red'>$dailyjobs_arr[$i]</span></p>
  </td>
  <td width=179 valign=top style='width:107.2pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻<span
  lang=EN-US> <br>
  </span>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
  
  <td width=128 valign=top style='width:76.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:14.0pt;
  mso-line-height-rule:exactly'><span style='font-family:"微軟正黑體","sans-serif";
  color:red'>$weeklyjobs_arr[$i]</span></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻<span
  lang=EN-US> <br>
  </span>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
  
  <td width=128 valign=top style='width:76.6pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:14.0pt;
  mso-line-height-rule:exactly'><span style='font-family:"微軟正黑體","sans-serif";
  color:red'>$monthlyjobs_arr[$i]</span></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:26.15pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻 </span></p>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
  
 </tr>
EOT;
	}
?>
 
 <tr style='mso-yfti-irow:14;mso-yfti-lastrow:yes;height:24.35pt'>
  <td width=135 valign=top style='width:81.25pt;border:solid black 1.0pt;
  border-top:none;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>其他： <span lang=EN-US>________</span></span></p>
  </td>
  <td width=179 valign=top style='width:107.2pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻<span
  lang=EN-US> <br>
  </span>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
  <td width=128 valign=top style='width:76.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>其他： <span lang=EN-US>________</span></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻<span
  lang=EN-US> <br>
  </span>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
  <td width=128 valign=top style='width:76.6pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>其他： <span lang=EN-US>________</span></span></p>
  </td>
  <td width=179 valign=top style='width:107.65pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:3.6pt 7.2pt 3.6pt 7.2pt;height:24.35pt'>
  <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly'><span
  style='font-family:"微軟正黑體","sans-serif"'>□夫<span lang=EN-US>&nbsp; </span>□妻<span
  lang=EN-US> <br>
  </span>□夫妻一起<span lang=EN-US>&nbsp; <br>
  </span>□其他： <span lang=EN-US>________</span></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
style='font-family:"微軟正黑體","sans-serif"'>&nbsp;</span></p>

</div>

</body>

</html>

<?php
	$content = ob_get_contents();

	ob_end_clean();

	$filename = "家務分工清單";

	$htmltodoc= new HTML_TO_DOC();
	$htmltodoc->createDoc($content, $filename, true);
?>
