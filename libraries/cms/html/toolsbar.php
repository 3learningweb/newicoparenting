<?php
/**
 * @version		$Id: toolsbar.php 20196 2011-07-05 08:35:25Z ian $
 * @package		Efatek.Framework
 * @subpackage	HTML
 * @copyright	Copyright (C) 2005 - 2011 EFATEK, Inc. All rights reserved.
 * @license		EFATEK
 */

/**
 * Utility class for Tabs elements.
 *
 * @static
 * @package		Efatek.Framework
 * @subpackage	HTML
 * @version		1.6
 */
abstract class JHtmlToolsbar
{
	private static $title;
	private static $url;

	private static function getTitle()
	{
		if(!self::$title){
			$pathway = JFactory::getApplication()->getPathway()->getPathway();
			if( JRequest::getVar('option') == 'com_content'){
				$id = JRequest::getInt('id');
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select("a.title");
				$query->from('#__content AS a');
				$query->where('a.id = ' . (int) $id);
				$db->setQuery($query);

				self::$title = trim( $db->loadResult() );
			} else if(count($pathway)){
				self::$title = trim( $pathway[ count($pathway) -1 ]->name );
			}
		}

		return self::$title;
	}

	private static function getUrl()
	{
		if(!self::$url){
			$uri = JURI::getInstance();

			if(substr_count($uri, "?option") >= 2){
				self::$url = trim( JString::substr($uri, '0', strrpos($uri, "?option") ) );
			} else {
				self::$url = trim( $uri );
			}
		}

		return self::$url;
	}

	/**
	* Generates facebook btton
	*
	* @param string The value of the HTML title attribute
	* @param string The value of the HTML class attribute
	* @param string Buttom images path
	* @return string HTML for facebook btton
	*/
	public static function facebook( $title = 'JHTML_TOOLBAR_FACEBOOK' , $class = 'webicon' , $img = 'media/system/images/webicon_fb.gif')
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		//$href = "javascript:void(open('http://www.facebook.com/share.php?u='+encodeURIComponent(location.href)+'&amp;t='+encodeURIComponent(document.title.split('@')[0].replace(/([\\s]*$)/g,''))));";
		$href = "http://www.facebook.com/share.php?u=" . rawurlencode(self::getUrl()) . "&amp;t=" . rawurlencode(self::getTitle());

		$output = '<a id="fb_share_toolbar" target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass.'>'.$imgHtml.'</a>';

		return $output;
	}

	/**
	* Generates plurk btton
	*
	* @param string The value of the HTML title attribute
	* @param string The value of the HTML class attribute
	* @param string Buttom images path
	* @return string HTML for plurk btton
	*/
	public static function plurk( $title = 'JHTML_TOOLBAR_PLURK' , $class = 'webicon' , $img = 'media/system/images/webicon_plurk.gif')
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		//$href = "javascript:void(open('http://www.plurk.com/?qualifier=shares&amp;status='+encodeURIComponent(location.href+'%20('+document.title.split('@')[0].replace(/([\\s]*$)/g,'')+')')));";
		$href = "http://www.plurk.com/?qualifier=shares&amp;status=" . rawurlencode(self::getUrl()) ."%20+(" . rawurlencode(self::getTitle()) .")";

		$output = '<a target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass.'>'.$imgHtml.'</a>';

		return $output;
	}

	/**
	* Generates twitter btton
	*
	* @param string The value of the HTML title attribute
	* @param string The value of the HTML class attribute
	* @param string Buttom images path
	* @return string HTML for twitter btton
	*/
	public static function twitter( $title = 'JHTML_TOOLBAR_TWITTER' , $class = 'webicon' , $img = 'media/system/images/webicon_twitter.gif')
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		//$href = "javascript:void(open('http://twitter.com/home/?status='.concat(encodeURIComponent(document.title))%20.concat('%20')%20.concat(encodeURIComponent(location.href))));";
		$href = "http://twitter.com/share?text=" . rawurlencode(self::getTitle()) . "&amp;url=" . rawurlencode(self::getUrl());

		$output = '<a target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass.'>'.$imgHtml.'</a>';

		return $output;
	}

	/**
	* Generates yahoo btton
	*
	* @param string The value of the HTML title attribute
	* @param string The value of the HTML class attribute
	* @param string Buttom images path
	* @return string HTML for yahoo btton
	*/
	public static function yahoo( $title = 'JHTML_TOOLBAR_YAHOO' , $class = 'webicon yahooicon' , $img = 'media/system/images/webicon_yahoo.jpg')
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		//$href = "javascript:void(open('http://tw.myweb2.search.yahoo.com/myresults/bookmarklet?u='+encodeURIComponent(location.href)+'&amp;ei=UTF-8&amp;t='+encodeURIComponent(document.title.split('@')[0].replace(/([\\s]*$)/g,''))));";
		$href = "http://tw.myweb2.search.yahoo.com/myresults/bookmarklet?u=" . rawurlencode(self::getUrl()) . "&amp;ei=UTF-8&amp;t=" . rawurlencode(self::getTitle());

		$output = '<a target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass.'>'.$imgHtml.'</a>';

		return $output;
	}

	/**
	* Generates google btton
	*
	* @param string The value of the HTML title attribute
	* @param string The value of the HTML class attribute
	* @param string Buttom images path
	* @return string HTML for google btton
	*/
	public static function google( $title = 'JHTML_TOOLBAR_GOOGLE' , $class = 'webicon googleicon' , $img = 'media/system/images/webicon_google.gif')
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		//$href = "javascript:void(open('http://www.google.com/bookmarks/mark?op=add&amp;bkmk='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title.split('@')[0].replace(/([\\s]*$)/g,''))));";
		$href = "http://www.google.com/bookmarks/mark?op=add&amp;bkmk=" . rawurlencode(self::getUrl()) . "&amp;title=" . rawurlencode(self::getTitle());

		return '<a target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass .'>'.$imgHtml.'</a>';
	}

	public static function fbgood()
	{
		$lang = JText::_('JHTML_TOOLBAR_FBLIKE_LANG');

		$code = '<div id="fb-root"></div>
				<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/'.$lang.'/all.js#xfbml=1";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, "script", "facebook-jssdk"));</script>';

		$code .= '<div class="fb-like" data-send="false" data-layout="button_count" data-width="30" data-show-faces="false"></div>';

		return $code;
	}

	public static function googleplusgood()
	{
		$code = "<script type='text/javascript'>
				window.___gcfg = {lang: 'zh-TW'};

				(function() {
					var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
					po.src = 'https://apis.google.com/js/platform.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>";

		$code .= "<div class='g-plusone' data-size='small' data-annotation='none'></div>";

		return $code;
	}


	public static function line( $title = 'JHTML_TOOLBAR_LINE' , $class = 'webicon' , $img = 'media/system/images/webicon_line.png')
	{
		$output = '';
		
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/>';
		}

		if($class == ''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

//		$href = "http://line.naver.jp/R/msg/text/" . rawurlencode(self::getTitle()) . "%0D%0A" . rawurlencode(self::getUrl());
		$href = "http://line.naver.jp/R/msg/text/?" . rawurlencode(self::getTitle()) . "%20%0a" . rawurlencode(self::getUrl());

		$output = '<a target="_blank" title="'.$title.'" href="'. $href .'" '.$cssClass.'>'.$imgHtml.'</a>';

//		$output .= '<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>			<script type="text/javascript">new media_line_me.LineButton({"pc":false,"lang":"zh-hant","type":"b"});			</script>';

		return $output;
	}


	public static function pageprint( $title = 'JHTML_TOOLBAR_PRINT' , $class = 'icon_print' , $img = 'media/system/images/icon_printpage.jpg' )
	{
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/> '. JText::_('JHTML_TOOLBAR_PRINT');
		}

		if($class==''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		$js = "window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;";
		$href = JURI::getInstance();

		if (strpos($href, "?")) {
			$href .= '&amp;tmpl=component&amp;print=1';
		} else {
			$href .= '?tmpl=component&amp;print=1';
		}
		
		$layout = JRequest::getVar('layout','default');
		$output = '<a '.$cssClass.' rel="nofollow" target="_blank" onclick="'.$js.'" onkeypress="'.$js.'" title="'.$title.'" href="'.$href.'" >'.$imgHtml.'</a>';

		return $output;
	}


	public static function btn_print()
	{
		$html = '';

		// ?�印??
		$html .= '<a href="#" onClick="window.print();return false;" OnKeypress="window.print();return false;"><span class="icon-print"></span>&nbsp;'. JText::_('JGLOBAL_PRINT'). '&nbsp;</a>';

		$html .= '&nbsp;&nbsp;';

		// ?��???
		$html .= '<a href="#" onClick="window.close();return false;" OnKeypress="window.close();return false;"><span class="icon-print"></span>&nbsp;'. JText::_('JHTML_TOOLBAR_CLOSE'). '&nbsp;</a>';

		$html .= '<br><noscript>'. JText::_('JHTML_TOOLBAR_PRINT_NOSCRIPT'). '</noscript>';

		return $html;
	}

	public static function email( $title = 'JHTML_TOOLBAR_EMAIL' , $class = 'icon_sendmail' , $img = 'media/system/images/icon_sendmail.jpg' )
	{
		$title 	= JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';
		$forward = JText::_('JHTML_TOOLBAR_FORWARD') .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		$uri	= JURI::getInstance();
		$base	= $uri->toString(array('scheme', 'host', 'port'));

		$pathway = JFactory::getApplication()->getPathway()->getPathway();
		$template = JFactory::getApplication()->getTemplate();

		if( JRequest::getVar('option') == 'com_content'){
			$id = JRequest::getInt('id');
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select("a.title");
			$query->from('#__content AS a');
			$query->where('a.id = ' . (int) $id);
			$db->setQuery($query);

			$mailtitle = $db->loadResult();
		} else if(count($pathway)){
			$mailtitle = $pathway[ count($pathway) -1 ]->name;
		}
		$mailtitle = rawurlencode($mailtitle);

		//$link	= $base.JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid) , false);
		$url	= JRoute::_('index.php?option=com_mailto&tmpl=component&Itemid=101&template='.$template.'&link='.base64_encode($uri).'&title='.$mailtitle);
		//$url	= JRoute::_($url);

		$status = 'width=650,height=570,menubar=yes,resizable=yes';

		$text = JHTML::_('image', $img , $forward  );
		$text .= '&#160;'. JText::_('JHTML_TOOLBAR_FORWARD');


		$attribs['title']	= $title;
		$attribs['onclick'] = "window.open(this.href,'win2','".$status."'); return false;";
		$attribs['onkeypress'] = "window.open(this.href,'win2','".$status."'); return false;";
		$attribs['target'] = "_blank";

		if($class != ''){
			$attribs['class'] = $class;
		}

		$output = JHTML::_('link', $url, $text , $attribs);

		return $output;
	}


	// 下�? PDF
	public static function pdf( $title = 'JHTML_TOOLBAR_PDF' , $class = 'icon_pdf' , $img = 'media/system/images/icon_pdf.jpg' )
	{
	
		$title = JText::_($title) .' (' .JText::_('JBROWSERTARGET_NEW'). ')';

		if($img == ''){
			$imgHtml = '';
		} else {
			$imgHtml = '<img alt="'.$title.'" title="'.$title.'" src="'.$img.'"/> '. JText::_('JHTML_TOOLBAR_PDF');
		}

		if($class==''){
			$cssClass = '';
		} else {
			$cssClass = 'class="'.$class.'"';
		}

		$js = "window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;";
		$href = JURI::getInstance();

		if (strpos($href, "?")) {
			$href .= '&amp;tmpl=component&amp;format=pdf';
		} else {
			$href .= '?tmpl=component&amp;format=pdf';
		}

		$output = '<a '.$cssClass.' rel="nofollow" target="_blank" onclick="'.$js.'" onkeypress="'.$js.'" title="'.$title.'" href="'.$href.'&amp;tmpl=component&amp;format=pdf" >'.$imgHtml.'</a>';

		return $output;
	}

	public static function fontsize(){
		JHtml::_('jquery.framework');
		$doc = JFactory::getDocument();
		$doc->addScript('media/system/js/fontsize.js');
		$code = "jQuery(document).ready(function() {
			jQuery('#btn_fontsize').css('display','inline');
		});";
		$doc->addScriptDeclaration($code);

		$font = 0;
		$font = (JRequest::getCmd('small_x', false))? 1 : 0;
		$font = (JRequest::getCmd('middle_x', false))? 1 : 0;
		$font = (JRequest::getCmd('sbig_x', false))? 1 : 0;

		$output = '';
		$output ='<div class="fontsize_label"  style="display: inline-block;">'. JText::_('JHTML_TOOLBAR_FONTSIZE').'</div>';

		$output .='<span class="tool_fontsize">
				<noscript>
				<form method="post" style="display: inline;" action="">
					<input id="small" type="image" value="small" name="small" src="media/system/images/font_small.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_RESET') .'"/>
					<input id="middle" type="image" value="middle" name="middle" src="media/system/images/font_normal.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_RESET') .'"/>
					<input id="big" type="image" value="big" name="big" src="media/system/images/font_big.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_BIG') .'"/>
					'. JText::_('JHTML_TOOLBAR_FONTSIZE_NOSCRIPT').'
				</form>
				</noscript>
				<span id="btn_fontsize" style="display: none;">
            		<a id="fzde" title="'. JText::_('JHTML_TOOLBAR_FONTSIZE_SMALL') .'" href="javascript:return false;" class="size">
                		<img src="media/system/images/font_small.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_SMALL') .'"/>
            		</a>
            		<a id="fzre" title="'. JText::_('JHTML_TOOLBAR_FONTSIZE_RESET') .'" href="javascript:return false;" class="size">
                		<img src="media/system/images/font_normal.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_RESET') .'"/>
            		</a>
            		<a id="fzin" title="'. JText::_('JHTML_TOOLBAR_FONTSIZE_BIG') .'" href="javascript:return false;" class="size">
                		<img src="media/system/images/font_big.jpg" alt="'. JText::_('JHTML_TOOLBAR_FONTSIZE_BIG') .'"/>
            		</a>
        		</span></span>';

		return $output;
	}

	/**
	* Generates Social button
	*
	* @param array The value of the Social buttons
	* @return string HTML for Social btton
	*/
	public static function button( $button = null )
	{
		$html = '';

		if( is_array($button) )
		{
			foreach($button as $s)
			{
				$btn = 'toolsbar.'.$s;
				$html .= JHtml::_($btn);
			}
		}

		return $html;
	}

	public static function _default(){
		$doc = JFactory::getDocument();

		$code = "jQuery(document).ready(function() {
                	//jQuery('#social_tools').css('display','inline-block');
                	jQuery('.btn_fb').css('display','inline-block');
                	jQuery('#btn_fontsize').css('display','inline');
            	});";
		$doc->addScriptDeclaration($code);

		$output ='';
		$output .= '<noscript>'. JText::_('JHTML_TOOLBAR_NOSCRIPT') .'<br></noscript>';

		// ?�種社群?�享
		$output .= '<span style="display: inline-block; margin-right: 2px;" id="social_tools">';
		$output .= JHTML::_('toolsbar.facebook');
		$output .= JHTML::_('toolsbar.twitter');
		$output .= JHTML::_('toolsbar.plurk');
		$output .= JHTML::_('toolsbar.line');

		$output .= '</span>';

		// FB讚、Google+
		$output .= '<span style="display: none;;" class="btn_fb">';
		$output .= JHTML::_('toolsbar.fbgood');
		$output .= JHTML::_('toolsbar.googleplusgood');
		$output .= '</span>';

		// ?�印?��?�?
		$output .= JHTML::_('toolsbar.pageprint');
		$output .= JHTML::_('toolsbar.email');

		// PDF
//		$output .= JHTML::_('toolsbar.pdf');
		
		// 字�?大�?
		$output .= JHTML::_('toolsbar.fontsize');


		return $output;
	}

	public static function _content(){
		$doc = JFactory::getDocument();

		$code = "jQuery(document).ready(function() {
                	//jQuery('#social_tools').css('display','inline-block');
                	jQuery('.btn_fb').css('display','inline-block');
                	jQuery('#btn_fontsize').css('display','inline');
            	});";
		$doc->addScriptDeclaration($code);

		$output ='';
		//$output .= '<noscript>'. JText::_('JHTML_TOOLBAR_NOSCRIPT') .'<br></noscript>';
//		$output .= JHTML::_('toolsbar.facebook');
//		$output .= "&nbsp;&nbsp;&nbsp;";
		$output .= JHTML::_('toolsbar.pageprint');
		$output .= "&nbsp;&nbsp;&nbsp;";
		$output .= JHTML::_('toolsbar.email');
		$output .= "&nbsp;&nbsp;&nbsp;";
		$output .= JHTML::_('toolsbar.fontsize');

		return $output;
	}

	public static function _jnews(){
		$doc = JFactory::getDocument();

		$code = "jQuery(document).ready(function() {
                	//jQuery('#social_tools').css('display','inline-block');
                	jQuery('.btn_fb').css('display','inline-block');
                	jQuery('#btn_fontsize').css('display','inline');
            	});";
		$doc->addScriptDeclaration($code);

		$output ='';
		$output .= '<noscript>'. JText::_('JHTML_TOOLBAR_NOSCRIPT') .'<br></noscript>';

		// ?�種社群?�享
		$output .= '<span style="display: inline-block; margin-right: 2px;" id="social_tools">';
		$output .= JHTML::_('toolsbar.google');
		$output .= JHTML::_('toolsbar.facebook');
		$output .= JHTML::_('toolsbar.twitter');
		$output .= JHTML::_('toolsbar.plurk');
		//$output .= JHTML::_('toolsbar.yahoo');
		$output .= '</span>';

		// FB讚、Google+
		$output .= '<span style="display: none;;" class="btn_fb">';
		$output .= JHTML::_('toolsbar.fbgood');
		//$output .= JHTML::_('toolsbar.googleplusgood');
		$output .= '</span>';

		// ?�印?��?�?
		$output .= JHTML::_('toolsbar.pageprint');
		$output .= JHTML::_('toolsbar.email');

		return $output;
	}

	public static function _components() {
		$doc = JFactory::getDocument();

		$code = "jQuery(document).ready(function() {
                	//jQuery('#social_tools').css('display','inline-block');
                	jQuery('.btn_fb').css('display','inline-block');
                	jQuery('#btn_fontsize').css('display','inline');
            	});";
		$doc->addScriptDeclaration($code);

		$output ='';
		$output .= '<noscript>'. JText::_('JHTML_TOOLBAR_NOSCRIPT') .'<br></noscript>';

		// ?�種社群?�享
		$output .= '<span style="display: inline-block; margin-right: 2px;" id="social_tools">';
		$output .= JHTML::_('toolsbar.facebook');
		$output .= JHTML::_('toolsbar.twitter');
		$output .= JHTML::_('toolsbar.plurk');
		$output .= JHTML::_('toolsbar.line');

		$output .= '</span>';

		// FB讚、Google+
		$output .= '<span style="display: none;;" class="btn_fb">';
		$output .= JHTML::_('toolsbar.fbgood');
		$output .= JHTML::_('toolsbar.googleplusgood');
		$output .= '</span>';

		return $output;		
	}
	
		public static function _timeline() {
		$doc = JFactory::getDocument();

		$code = "jQuery(document).ready(function() {
                	//jQuery('#social_tools').css('display','inline-block');
                	jQuery('.btn_fb').css('display','inline-block');
                	jQuery('#btn_fontsize').css('display','inline');
            	});";
		$doc->addScriptDeclaration($code);

		$output ='';
		$output .= '<noscript>'. JText::_('JHTML_TOOLBAR_NOSCRIPT') .'<br></noscript>';

		// ?�種社群?�享
		$output .= '<span style="display: inline-block; margin-right: 2px;" id="social_tools">';
		$output .= JHTML::_('toolsbar.facebook');
		$output .= JHTML::_('toolsbar.twitter');
		$output .= JHTML::_('toolsbar.plurk');
		$output .= JHTML::_('toolsbar.line');

		$output .= '</span>';
		
		return $output;		
	}
}
