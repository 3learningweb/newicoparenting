<?php

/**
 * @version		: timeline.php 2015-10-22 05:06:09$
 * @author		efatek 
 * @package		com_timeline
 * @copyright	Copyright (C) 2015- efatek. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modellist');

class TimelineModelTimeline extends JModelList {

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$id	= $app->input->getInt('id');
		$this->setState('item.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		parent::populateState();
	}


	public function getListQuery() {
		$app = JFactory::getApplication();
		$user_id = $app->input->getInt("id");		

		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select("*");
		$query->from($db->quoteName('#__timeline'));
		$query->where("user_id = '{$user_id}'");
		$query->order("date ASC");

		return $query;
	}

}
