<?php
/**
 * @version     1.0.0
 * @package     com_timeline
 * @copyright   Efatek Inc. Copyright (C) 2015. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <rene_chen@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class TimelineControllerItems extends TimelineController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'List', $prefix = 'Timeline')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
	public function save() {
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$post = $app->input->getArray($_POST);
		$db = JFactory::getDBO();
		
		
		$query = $db->getQuery(true);
		if($post['id']) {
			$query->update($db->quoteName('#__timeline'));
			$query->set("title = '{$post['title']}', date = '{$post['date']}', description = '{$post['description']}'");
			$query->where("id = '{$post['id']}'");
		}else {
			$query->insert($db->quoteName('#__timeline'));
			$query->columns('user_id, title, date, description, created');
			$query->values("'{$post['user_id']}', '{$post['title']}', '{$post['date']}', '{$post['description']}', now()");
		}
		
		$db->setQuery($query);
		
		if(!$db->execute()) {
			$msg = '資料儲存發生不明錯誤(02632884)，請聯繫網站管理人員！';
			$link = JRoute::_("index.php?option=com_timeline&view=items&Itemid={$itemid}");
			$this->setRedirect($link, $msg);
			return;
		}
		
		$item_id = ($post['id']) ? $post['id'] : $db->insertid();
		
		$image = $app->input->files->get("photo");
		if($image) {
			$filepath = "filesys/images/com_timeline/{$post['user_id']}/";
			$image_path = $this->_uploadFiles($image, $item_id, $filepath);
			
			if($image_path) {
				$query_img = $db->getQuery(true);
				$query_img->update($db->quoteName('#__timeline'));
				$query_img->set("photo = '{$image_path}'");
				$query_img->where("id = {$item_id}");
				
				$db->setQuery($query_img);
				
				if(!$db->execute()) {
					$msg = "上傳檔案時發生不明錯誤！請聯繫網站管理人員！";
					$link = JRoute::_("index.php?option=com_timeline&view=items&Itemid={$itemid}");
					$this->setRedirect($link, $msg);
					return;
				}
			}	
		}
		
		$msg = JText::_("寶寶成長日記儲存成功");
		$link = JRoute::_("index.php?option=com_timeline&view=items&Itemid={$itemid}");
		$this->setRedirect($link, $msg);
	}

	public function delete() {
		$app = JFactory::getApplication();
		$id = $app->input->getInt("id");
		$itemid = $app->input->getInt("Itemid");
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->delete($db->quoteName('#__timeline'));
		$query->where("id = {$id}");
		
		$db->setQuery($query);
		
		if(!$db->execute()) {
			$msg = "刪除日記發生不明錯誤！請聯繫網站管理人員！";
			$link = JRoute::_("index.php?option=com_timeline&view=items&Itemid={$itemid}");
			$this->setRedirect($link, $msg);
			return;
		}

		$path		= JPATH_SITE . '/filesys/images/com_timeline/' . $user->get('id');
		foreach(scandir($path) as $file):
			if($file != "." && $file != "..") {
				$file_arr = explode(".", $file);
				$name = $file_arr[0];
				
				if($name == "photo_{$id}") {
					@unlink($path . "/" . $name . "." . $file_arr[1]);
					break;
				}
			}
		endforeach;
		
		$msg = "已成功刪除日記";
		$link = JRoute::_("index.php?option=com_timeline&view=items&Itemid={$itemid}");
		$this->setRedirect($link, $msg);
	}

	public function _uploadFiles($files, $id, $filepath) {
		
		/** 判斷是否有傳入 $files **/
		if($files != '') {
			$dbpath = $filepath;
			$path = JPATH_SITE . "/" . $dbpath;
			
			/** 搬移檔案 **/
			$filename = "photo_{$id}." . pathinfo($files['name'],PATHINFO_EXTENSION);
			$filetmpname = $files['tmp_name'];
			
			if(file_exists($filetmpname)) {
				if(!is_dir($path)) {
					mkdir($path, 0755);
				}
				move_uploaded_file($filetmpname, $path . $filename);
				chmod($path . filename, 0755);
				
				return $dbpath.$filename;
			}
		}	
	}
}