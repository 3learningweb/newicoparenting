<?php
/**
 * @version     1.0.0
 * @package     com_timeline
 * @copyright   Efatek Inc. Copyright (C) 2015. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <rene_chen@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class TimelineControllerTimeline extends TimelineController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'List', $prefix = 'Timeline')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}