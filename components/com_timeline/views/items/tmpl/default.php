<?php
/**
 * @version		: default.php 2015-10-21 21:06:39$
 * @author		EFATEK 
 * @package		com_timeline
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$user = JFactory::getUser();
$user_group = JUserHelper::getUserGroups($user->get('id'));

$menu = $app->getMenu() ;
$menuItem = $menu ->getItems( 'link', 'index.php?option=com_timeline&view=timeline', true );
$timeline_itemid = $menuItem->id;

?>
<div class="com_timeline" style="min-height: 100px;">
	<?php if(!empty($user_group)) { ?>
		<div class="timeline_menu">
			<div class="submit_block">
				<a href="<?php echo JRoute::_("index.php?option=com_timeline&view=items&layout=form&Itemid={$itemid}"); ?>" title="新增項目">
					<input type="button" id="submit_btn" value="新增日記" />
				</a>
			</div>
			<?php if($this->items) { ?>
				<div class="submit_block">
					<a href="<?php echo JRoute::_("index.php?option=com_timeline&view=timeline&id={$user->get('id')}&Itemid={$timeline_itemid}"); ?>" title="時間軸" target="_blank">
						<input type="button" id="submit_btn" value="時間軸呈現" />
					</a>
				</div>
			<?php } ?>
		</div>
		<div class="timeline_items" style="min-height: 300px;"><br/>
		<?php if($this->items) { ?>
			<table class="datatable">
				<thead>
				<tr>
					<td>#</td>
					<td colspan="2"></td>
					<td>日期</td>
					<td>標題</td>
				</tr>
				</thead>
				
				<tbody>
				<?php foreach($this->items as $key => $item) { ?>
				<tr>
					<td><?php echo $key+1; ?></td>
					<td>
						<a href="<?php echo JRoute::_("index.php?option=com_timeline&view=items&layout=form&id={$item->id}&Itemid={$itemid}"); ?>" title="修改日記">
							修改
						</a>
					</td>
					<td>
						<a href="<?php echo JRoute::_("index.php?option=com_timeline&task=items.delete&id={$item->id}&Itemid={$itemid}"); ?>"
						   onclick="javascript:return confirm('是否確定刪除日記')" title="刪除日記">
							刪除
						</a>
					</td>
					<td><?php echo $item->date; ?></td>
					<td><?php echo $item->title; ?></td>
				</tr>
				<?php } ?>
				</tbody>
			</table>
			<div class="pagination">
				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
		<?php }else{ ?>
			<table class="datatable">
				<tr>
					<td>尚無寶寶成長日記</td>
				</tr>
			</table>
		<?php } ?>
		</div>
	<?php } ?>
</div>