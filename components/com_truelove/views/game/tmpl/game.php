<?php
/**
 * @version		: game.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		truelove
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<script language="JavaScript">	
(function($) {
	$(document).ready(function() {
		$(document).on("click", "#submit_btn", function() {
			var url = "<?php echo JRoute::_("index.php?option=com_truelove&view=game&Itemid={$itemid}", false); ?>";
			document.location.href = url;
		});
	});	
})(jQuery);
</script>

<link href="components/com_truelove/assets/css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="components/com_truelove/assets/js/controller.js"></script>

<div class="com_truelove" id="com_truelove">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div id="navContent">
		<div class="game_result">
			<div class="score">Score：<span>0</span></div>
		</div>
		<div class="game_block">
			<div id="game1"></div>
			<div id="game2"></div>
		</div>
		<div class="game_img"><img src="filesys/images/com_truelove/game_img.png" alt="" /></div>
		<div class="result_img"></div>
	</div>
	<div class="next"></div>
	<div id="truelove_bg"></div>
</div>