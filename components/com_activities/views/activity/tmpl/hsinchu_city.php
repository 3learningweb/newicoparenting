<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['新竹市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76659642-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >新竹市</span></h5>
	<div id="system-message-container">
		<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">新竹市家庭教育中心</p></h4>
		<div id="alla">
			<div id="lay01">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
			       		<td align="left" style="line-height:30px;" class="c_p">
                  	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/5_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          	</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
			     	<tr>
			       		<td height="10" align="left" class="c_p" ></td>
		          	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/5_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          	</tr>
				</table>
			</div>
			<div id="lay02">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  		<tr>
			       		<td align="left" style="line-height:25px;">
			       			<p style="color:#009;">地址：新竹市東大路二段15巷1號2樓</p>
			         		<p>電話：03~5325885轉105</p><p>網址：<a href="http://dep-family.hccg.gov.tw/" target="_blank">http://dep-family.hccg.gov.tw/</a><br></p>
                   			<p>Facebook粉絲頁：<a href="https://www.facebook.com/hsinchufamily/?fref=ts" target="_blank">新竹市家庭教育中心</a></p></td>
		          	</tr>
			     	<tr>
			       		<td height="25" align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
			       		  <table width="100%" border="0" cellpadding="00">
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105/8/1~105/8/30  9：30~13：00 </span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">巡迴本市12所社區辦理「祖孫夏令營」及8/28慶祝祖父母節辦理「祖孫嘉年華會」預計在新竹市遠東巨城購物中心廣場， 歡迎本市市民洽中心踴躍報名參加。</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105年5月3日至7月31日止每星期二上午9：00~12：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">辦理「學校家長親職效能學習團隊推動」，上課地點文化局二樓習齋，歡迎有興趣及熱心學校家長報名參加！</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105年度5-9月 </span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">每月擇週六「親職教育系列-家庭展能教育支持活動」預計於新住民家庭服務中心各辦理一次家庭教育活動歡迎本市有興趣民眾洽中心詢問報名參加。</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1"> 105年5月7日9：30~12：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「~學習相愛 幸福相守~婚姻成長研習~」歡迎已婚民眾踴躍報名參與!</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105年5月~6月</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「~當我們同在一起~親子心指南系列~親職講座及家長讀書會」,辦理地點本市各學校場域，詳洽本中心網站，歡迎有興趣家長報名參與!</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105年6月~7月</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">辦理學習「預見愛~未婚溝通研習~」 歡迎有興趣未婚青年踴躍參與! 詳洽本中心網站。</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
		       		      </table>
			       		  <p>&nbsp;</p>
			       		  <p style="color:#009;">&nbsp;</p>
			        	</td>
		          	</tr>
		        </table>
			</div>
		</div>
	</div>
</div>