<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['高雄市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76674441-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >高雄市</span></h5>
	<div id="system-message-container">
    	<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">高雄市政府教育局家庭教育中心</p></h4>
			<div id="alla">
				<div id="lay01">
			   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p">
                  		</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/15_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          		</tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     		<tr>
			       			<td height="10" align="left" class="c_p" ></td>
		          		</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/15_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          		</tr>
		        	</table>
			 	</div>
			 	<div id="lay02">
			   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
			       			<td align="left" style="line-height:25px;">
			       				<p style="color:#009;">地址：80147高雄市前金區中正四路209號4樓</p>
			         			<p>電話：(07)2153918</p><p>網址：<a href="http://ks.familyedu.moe.gov.tw/SubSites/Home.aspx?site=2963aef4-cd1f-4ed2-a6d8-bc525f4bde19" target="_blank">http://ks.familyedu.moe.gov.tw/SubSites/Home.aspx?site=2963aef4-cd1f-4ed2-a6d8-bc525f4bde19</a><br></p
                				><p>Facebook粉絲頁：<a href="https://www.facebook.com/高雄市政府教育局家庭教育中心-375792702555616/?fref=ts" target="_blank">高雄市政府教育局家庭教育中心</a></p>
                			</td>
		          		</tr>
			     		<tr>
			       			<td height="25" align="center" valign="middle">&nbsp;</td>
		          		</tr>
			     		<tr>
			       			<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          		</tr>
			     		<tr>
			       			<td align="center" valign="middle">&nbsp;</td>
		          		</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p">
			       				<p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
  								<table width="100%" border="0" cellpadding="0" cellspacing="0">
	   								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/4/12、4/19、4/26、5/3、5/9、5/10、5/17、5/20、6/23、/6/25、7/7、7/14、7/21「關係加溫，航向樂齡社區婚姻教育」，歡迎對本活動有興趣年滿50歲之已婚夫妻或其中一方踴躍參加。</td>
       								</tr>
   								</table>
     							<table width="100%" border="0" cellpadding="0" cellspacing="0">
    								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/5、5/12「高雄市政府相關局處(或學校)志願服務人員家庭教育知能研習計畫」，歡迎其他局處(或學校)志願服務人員踴躍參與。</td>
       								</tr>
   								</table>
     							<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/14「慶祝2016國際家庭日幸福愛學習」活動，歡迎本市市民踴躍參加。</td>
       								</tr>
   								</table>
 								<table width="100%" border="0" cellpadding="0" cellspacing="0">
    								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/14「幸福加油站」電影討論會，歡迎對本活動有興趣之新移民、原住民婦女踴躍參與。</td>
       								</tr>
   								</table>
   								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/14、6/18、7/31、8/27、9/10「大手拉小手，我們一起走」親職教育講座，歡迎3歲-18歲子女之家長踴躍參與。</td>
					       			</tr>
					   			</table>
					  			<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/10至105/6/24「新手父母愛的教室」，歡迎新手媽媽踴躍報名參加。</td>
					       			</tr>
					   			</table>
					 			<table width="100%" border="0" cellpadding="0" cellspacing="0">
					   				<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/28、7/24「美好新生活~親職教育講座」，歡迎0歲-6歲之家庭、社區民眾踴躍參加。</td>
					       			</tr>
					   			</table>
					   			<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"> 105/5/29「新婚講習~牽手一世情」歡迎將婚夫妻共同參與。</td>
					       			</tr>
					   			</table>
					   			<table width="100%" border="0" cellpadding="0" cellspacing="0">
					 				<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/6/4「影〝想〞婚姻-婚前教育影片賞析」，歡迎對本活動有興趣大學生參加。</td>
					       			</tr>
					   			</table>
					   			<table width="100%" border="0" cellpadding="0" cellspacing="0">
					  				<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/6/25「婚前教育~遇見幸福成長團體」，對本活動有興趣之未婚適婚青年男女踴躍參加。</td>
					       			</tr>
					   			</table>
								<p style="color:#009;">&nbsp;</p>
			        		</td>
		          		</tr>
		        	</table>
				</div>
			</div>
		</div>
	</div>