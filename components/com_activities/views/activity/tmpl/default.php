<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>

<div class="com_activities">
	<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<td>
				<div style="text-align: center;">
					<span style="color: rgb(178, 34, 34);"><strong><span style="font-size: 20px;">999個親子心動時光相片徵文活動辦法 </span> </strong> </span><br />
					<span style="font-family: arial,helvetica,sans-serif;"> <strong> <span style="font-size: 16px;">「The Moment of Our Love」 </span> </strong> </span>
				</div>
				<br />
				<div style="padding-bottom: 25px;">
					<div>
						<span><strong><font color="#1158b3">壹、活動主旨：</font></strong> </span>
					</div>
					<div style="padding: 10px 0px; margin-left: 18px;">
						<p><span>幸福從現在開始！當婚姻和家庭充滿愉快、開心的記憶，自然家人的凝聚力愈強，家庭關係也會更加穩固。想一想親子之間的美好互動，快快拿起相機，秀出最溫馨、幸福的親子心動時光。</span></p>
					</div>
				</div>
				<div style="padding-bottom: 25px;">
					<span><strong><font color="#1158b3">貳、主辦機關：</font></strong> </span><br />
					<div style="padding: 10px 0px; margin-left: 18px;">
						<p><span><strong><font color="#a02626">一、主辦單位：</font></strong>教育部</span></p>
						<p><span><strong><font color="#a02626">二、承/協辦機關：</font></strong></span></p>
						<div style="padding: 10px 0px; margin-left: 25px;">
							<p>
								<span>
									<p style="text-decoration:underline; font-weight:bold;">一、承辦機關─相片徵文活動</p>
									桃園市政府教育局<br >
									桃園市政府家庭教育中心<br >
									
								</span>
							</p>
							<br/>
							<p>
								<span>
									<p style="text-decoration:underline; font-weight:bold;">二、協辦機關─個別縣市相片徵文活動</p>
									各直轄市政府教育局及縣（市）政府<br >
									各直轄市及縣（市）家庭教育中心<br >
                                    國立臺灣師範大學人類發展與家庭學系家庭教育數位加值計畫團隊
								</span>
							</p>
						</div>
					</div>
				</div>
				<div style="padding-bottom: 25px;">
					<span><strong><font color="#1158b3">參、活動時間：</font></strong> </span><br />
					<div style="padding: 10px 0px; margin-left: 18px;">
						<p>
							<span><font color="#ff0000">105年5月11日（星期三）10時起至105年7月11日（星期一）中午12時止。</font></span><br />
						</p>
					</div>
				</div>
				<div style="padding-bottom: 25px;">
					<span><strong><font color="#1158b3">肆、參加方式：</font></strong> </span><br />
					<div style="padding: 10px 0px; margin-left: 18px;">
						<p><span><strong><font color="#a02626">步驟一：</font></strong></span></p>
						<div style="margin-left: 25px;">
							<p><span>拍下家庭中親子互動時最溫馨、幸福的畫面。</span></p>
						</div>
						<br>
						<p><span><strong><font color="#a02626">步驟二：</font></strong></span></p>
						<div style="margin-left: 25px;">
							<p><span>請至活動網頁<a href="http://icoparenting.moe.edu.tw/event">http://icoparenting.moe.edu.tw/event </a>進入參賽活動頁面，選擇一個縣市進行投稿（每人限投稿一次，作品送出後不得修改，並且不得重複投稿至其他縣市，若重覆投稿者，一律取消參賽資格）。</span></p>
						</div>
						<br>
						<p><span><strong><font color="#a02626">作品格式及規定：</font></strong></span></p>
						<div style="margin-left: 25px;">
							<p><span>（一）	本活動採網路報名方式，所有參賽作品一律上傳數位照片，作品檔案須為小於2MB之JPG檔。</span></p>
							<br />
							<p><span>（二）	除投稿照片外，另附上30字以內（含標點符號）之作品說明。 </span></p>
							<br />
							<p><span>（三）	投稿作品不可加上任何商業logo、貼圖，並且禁止經過格放、加框、加色、修圖、合成、彩繪、疊片等後製加工行為。</span></p>
							<br />
							<p><span>（四）	照片與文字不得複製、下載或抄襲、使用他人作品，亦不得使用過去曾參與其他比賽之作品。</span></p>
                            <br />
							<p><span>（五）	未符合以上格式或規定者，一律不列入評選。</span></p>
						</div>
						<br>
						<p>
					</div>
                    	<div style="padding-bottom: 15px;">
							<span><strong><font color="#1158b3">伍、評選方式：</font></strong> </span><br />
						</div>
						<div style="margin-left: 25px;">
							<div style="margin-left: 25px;">
								<p><span><p style="text-decoration:underline; font-weight:bold;">一、縣市初賽</p></span></p>
								<div style="margin-left: 10px;">
									<ul style="line-height:25px;"><li>（一） 由各直轄市、縣（市）家庭教育中心依本辦法進行評選，淘汰重複參賽或不符本活動精神之作品（如：雜亂、色情、暴力具誹謗、攻擊等…）。</li><li>（二）由各縣市家庭教育中心依本活動辦法，邀請專家學者依據評選標準評選進行評選，並選出前12名初賽獲獎者進入全國決賽。</li>
                                		<li>（三）參與各縣市初賽作品未達評選水準者，各該獎項得從缺或不足額入選。</li>
                                		<li>（四）各縣市自行針對縣市12位得獎之優勝作品，進行縣市頒獎活動。</li>
                                	</ul>
								</div>
							  	<p><span><p style="text-decoration:underline; font-weight:bold;">二、全國決賽</p></span></p>
								<div style="margin-left: 10px;">
									<ul style="line-height:25px;">
										<li>（一）由承辦單位邀請家庭教育相關專家學者、專業工作人員等擔任決審評審委員，並決選出各獎項得獎作品。</li>
									  <li>（二） 若參賽作品未達水準，由評審委員決定從缺或不足額入選。</li>
                                		<li>（三） 全國決賽特優獎1名及優等獎5名之得獎者，將於105年9月25日至高雄市領獎，得獎者前往高雄領獎之交通費用，由承辦單位提供。</li>
                               		</ul>
								</div>
						  	</div>
					  	</div>
					  	<br />
					  	<div style="padding-bottom: 15px;">
							<span><strong><font color="#1158b3">陸、評選標準及規則：</font></strong> </span>
						</div>
						<table width="85%" border="0" style="margin-left: 70px; line-height:20px;">
  							<tr>
    							<td colspan="2">評核參賽作品以100分計算，評分項目與比重如下：</td>
    						</tr>
  							<tr>
    							<td width="4%">(1)	</td>
    							<td width="96%">主題符合：50％</td>
  							</tr>
  							<tr>
    							<td>&nbsp;</td>
    							<td>文字、照片呈現親子互動(父母與子女、成年子女與老年父母等)情境，可以感受到照片中呈現親子之間的情感流露。</td>
  							</tr>
  							<tr>
								<td>(2) </td>
    							<td>照片構圖：30％</td>
  							</tr>
  							<tr>
    							<td>&nbsp;</td>
    							<td>
    								人物的比例、位置及互動呈現的編排方式。<br>
      								照片主題鮮明、畫面簡潔、視覺不雜亂。
      							</td>
  							</tr>
  							<tr>
    							<td>(3) </td>
    							<td>整體創意：20％</td>
  							</tr>
  							<tr>
    							<td>&nbsp;</td>
    							<td><p>照片及其所附文字敘述呈現具創意。 </p></td>
  							</tr>
            			</table>
					</p>
						
					<br>
					<div style="padding-bottom: 25px;">
						<span><strong><font color="#1158b3">柒、獎勵項目：</font></strong> </span><br />
						<div style="margin-left: 25px;">
							<p style="font-weight:bold;">縣市初賽</p><br>
						  	<table width="70%" border="1" cellpadding="0" cellspacing="0" style="line-height:30px;">
  								<tr>
    								<td align="center" valign="middle">特優獎	1名</td>
    								<td align="center" valign="middle">（5,000元等值商品或禮券）</td>
  								</tr>
  								<tr>
  								    <td align="center" valign="middle">優等獎	3名</td>
      								<td align="center" valign="middle">（3,000元等值商品或禮券）</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">優選獎	3名</td>
      								<td align="center" valign="middle">（2,000元等值商品或禮券）</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">佳作	5名</td>
      								<td align="center" valign="middle">（1,000元等值商品或禮券）</td>
    							</tr>
  								<tr>
      								<td align="center" valign="middle">入選	10名</td>
      								<td align="center" valign="middle">（500元等值商品）</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">參加獎	數名</td>
      								<td align="center" valign="middle">請參閱各縣市家庭教育中心網站</td>
    							</tr>
  							</table>
  							各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。<br/><br>
  							
  							<p style="font-weight:bold;">全國決賽</p>
  							<br>
  							<table width="70%" border="1" cellpadding="0" cellspacing="0" style="line-height:30px;">
    							<tr>
      								<td align="center" valign="middle">特優獎	1名</td>
      								<td align="center" valign="middle">iPad Air2 64GB (Wi-Fi + Cellular)</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">優等獎	5名</td>
      								<td align="center" valign="middle">iPad Mini4 16GB (Wi-Fi)</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">優選獎	10名</td>
      								<td align="center" valign="middle">IRLAND 20吋 24速後避震折疊車</td>
    							</tr>
    							<tr>
      								<td align="center" valign="middle">佳作	20名</td>
      								<td align="center" valign="middle">德國MILEI 全自動智慧型麵包機</td>
    							</tr>
  							</table>
						</div>
						<br />
						<p>
						<span><strong><font color="#1158b3">捌、得獎公告：</font></strong> </span><br />
						<div style="margin-left: 25px; line-height:35px;">
							105年8月1日公布縣市初賽結果<br>
							105年8月31日公布全國決賽結果<br>
							105年9月25日頒獎（特優獎1名、優等獎5名，地點：高雄）
						</div>
					</div>
					<div style="padding-bottom: 25px;">
						<span><strong><font color="#1158b3">玖、注意事項：</font></strong> </span><br />
						<div style="padding: 10px 0px; margin-left: 18px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="line-height:25px;">
    						<tr>
     						   <td width="6%" align="left" valign="top">一、</td>
        						<td width="94%" align="left" valign="top">參賽作品須符合攝影主題、作品規格，如上傳影像照片未能明顯辨識與主題內容相關，或有違反善良風俗之不雅照片，主（承）辦單位有權取消其參加或獲獎資格。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">二、</td>
        						<td align="left" valign="top">每人限投稿一件，參賽作品應為參賽者本人拍攝，且為未經參加其它比賽得獎或未公開發表之作品，請勿一稿多投，如經檢舉有上開任一情形者，並經查證屬實，不列入評選；如於決賽評選完成後經查屬實者，該作品喪失獲獎資格，並追回獎勵品或按市價賠償。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">三、</td>
        						<td align="left" valign="top">所有參賽作品參賽者同意無償授予主（承）辦單位用於教育性質之利用推廣，主（承）辦單位擁有不同形式發行之權力，亦得以用任何形式推廣、保存、轉載、發行，不再另付酬勞、版稅及任何費用。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">四、</td>
        						<td align="left" valign="top">主（承）辦單位為文字流暢度等必要考量，有酌以潤飾參賽簡訊內容之權利。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">五、</td>
        						<td align="left" valign="top">主（承）辦單位保有修改、變更或暫停本活動之權利，本實施辦法如有未盡事宜，得經主辦單位決議修改公布之。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">六、</td>
        						<td align="left" valign="top">初賽獲獎作品於105年8月1日前、決賽得獎作品於105年8月31日前公布於本活動網站上，並同時個別聯繫得獎者領獎事宜，優勝獎項得以公開儀式頒發獎項。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">七、</td>
        						<td align="left" valign="top">獲獎獎金超過新臺幣1,000元(含)以上者，主（承）辦單位依法申報，獲獎獎金面額在新臺幣2,000元以上者，依所得稅法相關規定由承辦單位扣繳10%所得稅。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">八、</td>
        						<td align="left" valign="top">獲獎者將以電話通知，並依本辦法及承辦單位相關規定完成領獎手續，主辦單位若逾規定期限仍無法與獲獎者取得聯繫，或經通知獲獎者知悉而逾105年10月31日前未依本辦法及承辦單位相關規定完成獎項領取者，則視同放棄獲獎資格，主辦單位不負責變更中獎者聯絡地址或中獎者姓名之責任，以避免產生以人頭戶冒領獎項之情事。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">九、</td>
        						<td align="left" valign="top">任何因電腦、網路、電話、技術或不可歸責於主辦單位之事由，而使寄送得獎人之獎品有延遲、遺失、錯誤、無法辨識或毀損之情況，主辦單位概不負責，參加者視同放棄中獎資格亦不得因此異議。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">十、</td>
        						<td align="left" valign="top">獎品以實物為準，不得要求更換，不得折換現金。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">十一、</td>
        						<td align="left" valign="top">如遇產品缺貨或不可抗拒之事由導致獎品內容變更，主辦單位有權變更獎品品項，改由等值商品取代之，得獎者不得要求折現或轉換其他商品。如有任何變更內容或詳細注意事項將公布於本活動網站，恕不另行通知。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">十二、</td>
        						<td align="left" valign="top">本活動主辦單位及相關協辦、合作單位員工無參加本活動之權利。</td>
      						</tr>
      						<tr>
        						<td align="left" valign="top">十三、</td>
        						<td align="left" valign="top">保留隨時修改、變更、對本活動之所有事宜做出最終解釋及裁決權、以及終止本活動的權利；如有任何變更，將公布於活動網站，恕不另行通知。</td>
      						</tr>
    					</table>
						<br />
						  &nbsp;
						<p>
							<span style="font-size: 16px;">※</span><strong><span style="font-size: 14px;"><span style="font-size: 15px;"><span style="font-size: 16px;">蒐集個人資料告知及同意書</span>&nbsp;</span></span></strong><br />
							<br />
							<strong>依據個人資料保護法等相關規定，以下告知及同意事項，請參與本活動參賽者於參與本活動前務必詳閱，此外，若參賽者透過本活動網站參與本次活動，即視為已充分了解並同意以下之相關事項： </strong>：<br />
						  	&nbsp;
						</p>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="line-height:25px;">
							<tr>
						    	<td width="6%" align="left" valign="top">一、</td>
						    	<td width="94%">
						    		告知事項：<br>
						      		依據個人資料保護法等相關規定，明列以下告知事項： <br>
						      		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="line-height:30px;">
						        		<tr>
						          			<td width="3%" valign="top">1.</td>
						          			<td width="97%" valign="top">蒐集個人資料單位：教育部及各縣市家庭教育中心。</td>
						          		</tr>
						        		<tr>
						          			<td valign="top">2.</td>
						          			<td valign="top">蒐集之目的：999個親子心動時光相片徵文活動。</td>
						          		</tr>

						        		<tr>
						          			<td valign="top">3.</td>
						          			<td valign="top">
						          				個人資料之類別：包括個人資料中之識別類(辨識個人者中之姓名、居住住址、聯絡電話、手機號碼、電子信箱等)。<br>
						            			◎本活動參與者將本活動參與資料輸入本活動網站並透過網路遞送時，均視為本活動參與者已同意提供其輸入至活動網站之個人資料類別予教育部及各縣市家庭教育中心於本活動之必要範圍內處理及利用。 </td>
						          		</tr>
						        		<tr>
						          			<td valign="top">4.</td>
						          			<td valign="top">
						          				個人資料利用之期間：<br>
						            			(1)參加者所提供之個人資料於活動起始日起至本活動結束後3個月內利用。 <br>
						            			(2)獲獎人部份：獲獎人所提供之個人基本資料，僅作為領取獎項及申報個人所得使用；依據稅法規定本資料最長保存7年，屆時銷毀，不移作他用。 <br></td>
						          		</tr>
						        		<tr>
						          			<td valign="top">5.</td>
						          			<td valign="top">個人資料利用之地區：本次活動僅限於設籍於台灣、金門、澎湖、馬祖地區者參加，且所蒐集之個人資料將不會被移轉至台灣、金門、澎湖、馬祖以外地區。</td>
						          		</tr>
						        		<tr>
						          			<td valign="top">6.</td>
						          			<td valign="top">個人資料利用之對象及方式：由教育部及各縣市家庭教育中心於執行本活動時之必要相關人員利用之，利用人員需依執行本活動作業所必要方式利用此個人資料。 </td>
						          		</tr>
					          		</table>
					          	</td>
						    </tr>
						  	<tr>
						    	<td align="left" valign="top">二、</td>
						    	<td>參與者填寫本活動參與所須個人資料後，以任何方式遞送至教育部或各縣市家庭教育中心收執時，均視為已同意提供予教育部或各縣市家庭教育中心辦理本活動之特定目的必要範圍內處理及利用；此外，參與者可決定是否填寫相關之個人資料欄位，若參加者不填寫相關欄位時，教育部或各縣市家庭教育中心則於決定是否符合參與本活動之資格時，擁有自行判斷之權利。 </td>
						    </tr>
						  	<tr>
						    	<td align="left" valign="top">三、</td>
						    	<td>活動參與者同意將本活動辦法內所列事項之個人資料，無償並無條件提供給教育部或各縣市家庭教育中心於本活動參與者業務範圍內含理處理及利用。 </td>
						    </tr>
						  	<tr>
						    	<td align="left" valign="top">四、</td>
						    	<td>保護本活動參與者資料之安全措施：教育部及各縣市家庭教育中心將依據相關法令之規定及業己建構之完善措施，保障本活動參與者個人資料之安全。 </td>
						  	</tr>
						  </table>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
</div>	
		
