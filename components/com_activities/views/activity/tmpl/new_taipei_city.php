<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['新北市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76674052-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
			<h5><span style="font-size:36px; font-weight:bold;" >新北市</span></h5>
			<div id="system-message-container">
            <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">新北市政府家庭教育中心</p></h4>
			<div id="alla"><div id="lay01">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p">
                   
                   
			       
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/3_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          </tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     <tr>
			       <td height="10" align="left" class="c_p" ></td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/3_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          </tr>
		        </table>
			 </div><div id="lay02">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			     <tr>
			       <td align="left" style="line-height:25px;"> <p style="color:#009;">地址：新北市板橋區僑中一街1之1號4樓</p>
			         <p>電話：02-22724881</p><p>網址：<a href="http://family.ntpc.edu.tw/" target="_blank">http://family.ntpc.edu.tw</a><br>
			         </p>
                   <p>Facebook粉絲頁：<a href="https://www.facebook.com/FamilyEducationCenter" target="_blank">新北市政府家庭教育中心</a></p></td>
		          </tr>
			     <tr>
			       <td height="25" align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年度「幸福協奏曲婚姻(無交往對象、將婚、婚後)系列課程」   
   將於5/15、6/5、7/9、9/10(星期日)舉辦，名額有限，歡迎踴躍  
  報名參加。
</td>
       </tr>
   </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年5月份(5/1~5/30)五「心」級幸福家庭系列課程，辦理親子 
  創客營隊、親子電影導讀、親子創客市集等活動，名額有限，歡迎
  踴躍報名參加。
</td>
       </tr>
   </table>
  

			         <p style="color:#009;">&nbsp;</p>
			        </td>
		          </tr>
		        </table>
			 </div>
			</div></div>
</div>