<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['嘉義市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76666353-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >嘉義市</span></h5>
	<div id="system-message-container">
		<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">嘉義市家庭教育中心</p></h4>
		<div id="alla">
			<div id="lay01">
		   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		    		<tr>
		       			<td align="left" style="line-height:30px;" class="c_p">
                  		</tr>
		     		<tr>
		       			<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/13_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		         		</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
		     		<tr>
		       			<td height="10" align="left" class="c_p" ></td>
		         		</tr>
		     		<tr>
		       			<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/13_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		         		</tr>
		       	</table>
			</div>
			<div id="lay02">
		   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		       		<tr>
		       			<td align="left" style="line-height:25px;">
							<p style="color:#009;">地址：嘉義市山子頂269-1號</p>
		         			<p>電話：05-2754334</p><p>網址：<a href="http://family.cy.edu.tw/" target="_blank">http://family.cy.edu.tw/</a><br></p>
                  				<!-- <p>Facebook粉絲頁：<a href="https://www.facebook.com/groups/186160488218192/?ref=bookmarks" target="_blank">基隆市家庭教育中心</a></p>-->
                  			</td>
		         		</tr>
		     		<tr>
		       			<td height="25" align="center" valign="middle">&nbsp;</td>
		         		</tr>
		     		<tr>
			       			<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		        		</tr>
		     		<tr>
		       			<td align="center" valign="middle">&nbsp;</td>
		         		</tr>
		     		<tr>
		       			<td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
   							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
          							<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「愛家515-國際家庭日」慶祝活動暨教育志工博覽會訂於5月15日下午1時30分至5時30分，假東區體育館舉行，歡迎親子、市民踴躍參加。</td>
       							</tr>
								<tr>
								  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「快樂育兒列車－新手父母共學成長課程」訂於8/27-10/30辦理6場次，詳細資料，請至本市家庭教育中心網站（<a href="http://family.cy.edu.tw/" target="_blank">http://family.cy.edu.tw/）</a>查詢。</td>
							  </tr>
								<tr>
								  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「愛寶貝系列-優質父母親職講座（學齡期）」訂於9/7、9/21及10/26日辦理，詳細資料請至本市家庭教育中心網站（<a href="p://family.cy.edu.tw/" target="_blank">http://family.cy.edu.tw</a>/）查詢。</td>
							  </tr>
								<tr>
								  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「愛寶貝系列-優質父母親職講座（青少年期）」訂於7/2、7/9辦理，詳細資料請至本市家庭教育中心網站（<a href="p://family.cy.edu.tw/" target="_blank">http://family.cy.edu.tw</a>/）查詢。</td>
							  </tr>
								<tr>
								  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">如欲了解更多活動訊息，歡迎至本市家庭教育中心網站（<a href="p://family.cy.edu.tw/" target="_blank">http://family.cy.edu.tw</a>/）查詢。</td>
							  </tr>
   							</table>
			         			<p style="color:#009;">&nbsp;</p>
		       			</td>
		         	</tr>
		       	</table>
		 	</div>
		</div>
	</div>
</div>