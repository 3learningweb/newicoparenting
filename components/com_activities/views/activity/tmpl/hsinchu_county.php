<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['新竹縣']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76662158-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >新竹縣</span></h5>
	<div id="system-message-container">
    	<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">新竹縣家庭教育中心</p></h4>
		<div id="alla">
			<div id="lay01">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p">
                  	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/6_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          	</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
			     	<tr>
			       		<td height="10" align="left" class="c_p" ></td>
		          	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/6_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          	</tr>
				</table>
			</div>
			<div id="lay02">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  		<tr>
			       		<td align="left" style="line-height:25px;">
							<p style="color:#009;">地址：新竹縣竹北市縣政二路620號</p>
			         		<p>電話：(03)6571045*22</p><p>網址：<a href="http://hcc.familyedu.moe.gov.tw" target="_blank">http://hcc.familyedu.moe.gov.tw</a><br></p>
                   			<p>Facebook粉絲頁：<a href="https://www.facebook.com/新竹縣家庭教育中心-152841441441758/" target="_blank">新竹縣家庭教育中心</a></p>
                   		</td>
		          	</tr>
			     	<tr>
			       		<td height="25" align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
			    <table width="100%" border="0" cellpadding="00">
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105/05/21(六)14：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「幸福家庭樂書香讀書會-愛之語(兒童)」</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105/05/27(五)18：30</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「幸福家庭樂書香讀書會-餐桌上的奇蹟」</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105/07/16(六)09：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「親職教育系列講座-情緒發展、管理與情感教養」</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1"> 105/08/25(四)09：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「親密之旅讀書會-愛的探索」</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
			       		        <tr>
			       		          <td colspan="3"><span class="date1">105/09/24(六)09：00</span></td>
		       		            </tr>
			       		        <tr>
			       		          <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">「親職教育系列講座-家庭對個人價值觀的塑造~創造家庭尊重、仁愛及包容的氣氛」</td>
		       		            </tr>
		       		          </table></td>
		       		        </tr>
			       		    <tr>
			       		      <td>以上活動訊息請參閱本中心網站<a href="http://hcc.familyedu.moe.gov.tw/" target="_blank">http://hcc.familyedu.moe.gov.tw/ </a></td>
		       		        </tr>
		       		      </table>
			       		  <p>&nbsp;</p>
			       		  <p style="color:#009;">&nbsp;</p>
			        	</td>
		          	</tr>
		        </table>
			       		  <p style="color:#009;">&nbsp;</p>
			        	</td>
		        	</tr>
		    	</table>
			</div>
		</div>
	</div>
</div>