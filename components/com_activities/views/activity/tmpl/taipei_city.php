<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['臺北市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76660550-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
			<h5><span style="font-size:36px; font-weight:bold;" >臺北市</span></h5>
			<div id="system-message-container">
            <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">臺北市家庭教育中心</p></h4>
			<div id="alla"><div id="lay01">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p">
                   
                   
			       
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/2_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          </tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     <tr>
			       <td height="10" align="left" class="c_p" ></td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/2_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          </tr>
		        </table>
			 </div><div id="lay02">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			     <tr>
			       <td align="left" style="line-height:25px;"> <p style="color:#009;">地址：臺北市中山區吉林路110號5樓</p>
			         <p>電話：(02)2541-9690總機20</p><p>網址：<a href="http://www.family.gov.taipei/" target="_blank">http://www.family.gov.taipei/</a><br>
			         </p>
                    <!--<p>Facebook粉絲頁：<a href="https://www.facebook.com/groups/186160488218192/?ref=bookmarks" target="_blank">基隆市家庭教育中心</a></p>--></td>
		          </tr>
			     <tr>
			       <td height="25" align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">臺北市家庭教育中心<a href="http://www.family.gov.taipei/ct.asp?xItem=157059554&amp;ctNode=59033&amp;mp=104071" target="_blank">樂活家庭講座</a>105年度課程訂於每月第4個週六上午10至12時辦理。</td>
       </tr>
   </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">親密關係工作坊-將婚伴侶課程於5月28日(週六)舉辦，歡迎參加！</td>
       </tr>
   </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
       
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">親密關係工作坊-新婚夫妻課程於7月23日(週六)舉辦，歡迎參加！</td>
       </tr>
   </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年「祖孫同樂園 爸媽我愛您」第7屆祖父母節於8月28日舉辦，歡迎參加！</td>
       </tr>
   </table>
   
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">60期「親密互動我和你」課程於9-12月舉辦，歡迎參加！</td>
       </tr>
   </table>

			         <p style="color:#009;">&nbsp;</p>
			        </td>
		          </tr>
		        </table>
			 </div>
			</div></div>
</div>