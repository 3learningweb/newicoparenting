<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['臺南市']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76667458-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
			<h5><span style="font-size:36px; font-weight:bold;" >臺南市</span></h5>
			<div id="system-message-container">
            <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">臺南市家庭教育中心</p></h4>
			<div id="alla"><div id="lay01">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p">
                   
                   
			       
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/14_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          </tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     <tr>
			       <td height="10" align="left" class="c_p" ></td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/14_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          </tr>
		        </table>
			 </div><div id="lay02">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			     <tr>
			       <td align="left" style="line-height:25px;"> <p style="color:#009;">溪北服務處：臺南市新營區秦漢街118號</p> <p style="color:#009;">溪南服務處：臺南市中西區公園路127號</p> <p style="color:#009;">諮詢服務處：臺南市永康區華興街2號</p>
			         <p>溪北服務處：06-6591068</p><p>溪南服務處：06-2210510</p><p>諮詢服務處：06-3129926</p><p>網址：<a href="http://www.family.tn.edu.tw/index.php" target="_blank">http://www.family.tn.edu.tw/index.php</a><br>
			         </p>
                   <p>Facebook粉絲頁：<a href="https://www.facebook.com/tainanfamily" target="_blank">臺南市家庭教育中心</a></p></td>
		          </tr>
			     <tr>
			       <td height="25" align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
 
     <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">5月份辦理「心約定-牽手新旅程」活動，相關訊息請上臺南市家庭教育中心網站查詢。</td>
       </tr>
   </table>
   
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">5月份辦理「型男主廚俏媽咪」，相關訊息請上臺南市家庭教育中心網站查詢。</td>
       </tr>
   </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">5-9月辦理「家庭展能教育支持計畫」，相關訊息請上臺南市家庭教育中心網站查詢。</td>
       </tr>
   </table>        <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">6-9月歡慶祖父母節活動，相關訊息請上臺南市家庭教育中心網站查詢。</td>
       </tr>
   </table>        <table width="100%" border="0" cellpadding="0" cellspacing="0">
      
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">9月份辦理「幸福同行」活動，相關訊息請上臺南市家庭教育中心網站查詢。</td>
       </tr>
   </table>     
  

			         <p style="color:#009;">&nbsp;</p>
			        </td>
		          </tr>
		        </table>
			 </div>
			</div></div>
</div>