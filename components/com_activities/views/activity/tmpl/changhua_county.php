<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$city_id = $this->city['彰化縣']->id;
?>

<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76661952-1', 'auto');
  ga('send', 'pageview');

</script>


<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >彰化縣</span></h5>
	<div id="system-message-container">
	    <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">彰化縣家庭教育中心</p></h4>
		<div id="alla">
			<div id="lay01">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p">
					</tr>
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/10_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
					</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
				    <tr>
				    	<td height="10" align="left" class="c_p" ></td>
					</tr>
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/10_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
					</tr>
				</table>
			</div>
			
			<div id="lay02">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">  
					<tr>
				    	<td align="left" style="line-height:25px;"> <p style="color:#009;">地址：彰化市中山路2段678號</p>
				        	<p>電話：04-7531899</p><p>網址：<a href="http://chc.familyedu.moe.gov.tw/" target="_blank">http://chc.familyedu.moe.gov.tw/</a><br></p>
	                   		<!-- <p>Facebook粉絲頁：<a href="https://www.facebook.com/臺中市家庭教育中心-459348224077723/?ref=aymt_homepage_panel" target="_blank">臺中市家庭教育中心</a></p> -->
	                   </td>
					</tr>
				    <tr>
				    	<td height="25" align="center" valign="middle">&nbsp;</td>
					</tr>
				    <tr>
				    	<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
					</tr>
				    <tr>
				    	<td align="center" valign="middle">&nbsp;</td>
					</tr>
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
	   						<table width="100%" border="0" cellpadding="0" cellspacing="0">
	       						<tr>
	           						<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/2、3、5 (四)8:00「感恩五月-情意綿綿」，假彰化縣埔心鄉太平國小辦理。</td>
	       						</tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/4 (四)8:30「感恩奉茶活動」，假彰化縣彰化市大竹國小辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/9、13 (五)8:0 0「愛要大聲說，幫你媽媽『馬』一下」，假彰化縣二林鎮萬興國小辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/12、13 (五)8:30「用點心 表孝心」，假彰化縣永靖鄉福興國小辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/19 (四)9:00-10:00「愛家515-幸福家庭親子筆記學習單電台宣導」，假國立教育電台彰化分臺辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/6/16 (四)9:00-10:00「親子天天連線電台宣導」，假國立教育電台彰化分臺辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/7/14 (四)9:00-10:00「愛情一陣風-談情說愛話愛情：分手的藝術電台宣導」，假國立教育電台彰化分臺辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/8/18 (四)9:00-10:00「祖父母節系列活動電台宣導」，假國立教育電台彰化分臺辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/9/15 (四)9:00-10:00「學習讓我們更相愛-談婚姻教育電台宣導」，假國立教育電台彰化分臺辦理。</td>
       						  </tr>
	       						<tr>
	       						  <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">
       						      以上相關活動訊息，將公佈於本縣家庭教育網站最新公告。</td>
       						  </tr>
	       						<tr>
	       						 
       						  </tr>
	   						</table>
				         	<p style="color:#009;">&nbsp;</p>
				        </td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>