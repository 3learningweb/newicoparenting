<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['宜蘭縣']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76666165-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
			<h5><span style="font-size:36px; font-weight:bold;" >宜蘭縣</span></h5>
			<div id="system-message-container">
            <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">宜蘭縣家庭教育中心</p></h4>
			<div id="alla"><div id="lay01">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p">
                   
                   
			       
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/17_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          </tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     <tr>
			       <td height="10" align="left" class="c_p" ></td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/17_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          </tr>
		        </table>
			 </div><div id="lay02">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			     <tr>
			       <td align="left" style="line-height:25px;"> <p style="color:#009;">地址：宜蘭市民權路1段36號</p>
			         <p>電話：(03)9333837</p><p>網址：<a href="http://blog.ilc.edu.tw/blog/blog/26150" target="_blank">http://blog.ilc.edu.tw/blog/blog/26150</a><br>
			         </p>
                <!--<p>Facebook粉絲頁：<a href="https://www.facebook.com/高雄市政府教育局家庭教育中心-375792702555616/?fref=ts" target="_blank">高雄市家庭教育中心</a></p>--></td>
		          </tr>
			     <tr>
			       <td height="25" align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
   
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/4、105/5/18、105/5/25、105/6/1、105/6/8、105/9/6、105/9/14「我和我的孩子～家長親職教育多元數位教材研習」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>
     <table width="100%" border="0" cellpadding="0" cellspacing="0">
    
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/4、105/5/21、105/6/18、105/8/27、105/9/3「與父母有約～幸福爸媽親職講堂」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>
     <table width="100%" border="0" cellpadding="0" cellspacing="0">
    
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/7/9至105/7/30每週六上午「戲說幸福~婚姻與家庭關係電影討論團體」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>
   
   
     <table width="100%" border="0" cellpadding="0" cellspacing="0">
    
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/7、105/5/14、105/5/28「家庭展能～親職教育活動」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>  <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/6/5、105/6/18、105/6/26、105/7/3及105/9/4、105/9/11、105/9/18、105/9/25「幸福家庭樂書香～家庭教育親子共讀活動」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>
   
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/7/15、105/7/22、105/7/29、105/8/5「愛與陪伴～性別平等教育親子讀書」，歡迎民踴躍報名參加。</td>
       </tr>
   </table>
   
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">相關活動訊息請上<a href="http://blog.ilc.edu.tw/blog/blog/26150" target="_blank">宜蘭縣家庭教育中心網站</a>查詢</td>
       </tr>
   </table>
  

     
  

			         <p style="color:#009;">&nbsp;</p>
			        </td>
		          </tr>
		        </table>
			 </div>
			</div></div>
</div>