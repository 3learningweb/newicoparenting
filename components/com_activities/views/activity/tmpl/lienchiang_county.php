<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['連江縣']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76680954-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
			<h5><span style="font-size:36px; font-weight:bold;" >連江縣</span></h5>
			<div id="system-message-container">
            <h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">連江縣家庭教育中心</p></h4>
			<div id="alla"><div id="lay01">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p">
                   
                   
			       
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/22_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          </tr>
		          <tr>
		          	<td align="left" style="width: 290px;">
		          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
		          	</td>
		          </tr>
			     <tr>
			       <td height="10" align="left" class="c_p" ></td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/22_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          </tr>
		        </table>
			 </div><div id="lay02">
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			     <tr>
			       <td align="left" style="line-height:25px;"> <p style="color:#009;">地址：連江縣南竿鄉介壽村76號</p>
			         <p>電話：0836-25171#56、57</p><p>網址：<a href="http://lcc.familyedu.moe.gov.tw/SubSites/Home.aspx?site=983c40d1-53d5-4bfa-b80c-df219c5bc894 " target="_blank">連江縣家庭教育中心網站</a><br>
			         </p>
               <!--<p>Facebook粉絲頁：<a href="https://www.facebook.com/%E8%87%BA%E6%9D%B1%E7%B8%A3%E5%AE%B6%E5%BA%AD%E6%95%99%E8%82%B2%E4%B8%AD%E5%BF%83-412966682097443/" target="_blank">澎湖縣家庭教育中心</a></p>--></td>
		          </tr>
			     <tr>
			       <td height="25" align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          </tr>
			     <tr>
			       <td align="center" valign="middle">&nbsp;</td>
		          </tr>
			     <tr>
			       <td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年5月16-17日辦理「親職教育-其實，我們並沒有不一樣」活動。</td>
       </tr>
   </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年5月21日辦理「婚姻教育-情定愛芹海」活動。</td>
       </tr>
   </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年7月2日辦理「性別教育及社區婦女教育-支持多元型態家庭女性、強化女性生命歷程與角色講座」活動。</td>
       </tr>
   </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年7月2日辦理「性別教育及社區婦女教育-性別平等與成長講座」活動。</td>
       </tr>
   </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105年7月16-17日辦理「婚姻教育-中老年夫妻-牽手‧心‧旅程」活動</td>
       </tr>
   </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
  
       <tr>
           <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">5月到9月相關活動訊息請上連江縣家庭教育中心網站查詢 (<a href="http://lcc.familyedu.moe.gov.tw/SubSites/Home.aspx?site=983c40d1-53d5-4bfa-b80c-df219c5bc894">http://lcc.familyedu.moe.gov.tw/SubSites/Home.aspx?site=983c40d1-53d5-4bfa-b80c-df219c5bc894</a> )</td>
       </tr>
   </table>
     
   

     
  

			         <p style="color:#009;">&nbsp;</p>
			        </td>
		          </tr>
		        </table>
			 </div>
			</div></div>
</div>