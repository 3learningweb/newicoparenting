<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$city_id = $app->input->getInt('city');
?>


<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#submit_btn").show();
		jQuery("#submit_btn2").hide();
	});
	
	function sendForm() {
		if(!jQuery("#form_name").val()) {
			alert("請輸入姓名");
			jQuery("#form_name").focus();
			return false;
		}else if(!jQuery("#form_userid").val()) {
			alert("請輸入身分證字號");
			jQuery("#form_userid").focus();
			return false;
		}else if(!jQuery("#form_tel").val()) {
			alert("請輸入聯絡電話");
			jQuery("#form_tel").focus();
			return false;
		}else if(!jQuery("#form_mobile").val()) {
			alert("請輸入行動電話");
			jQuery("#form_mobile").focus();
			return false;
		}else if(!jQuery("#form_email").val()) {
			alert("請輸入Email");
			jQuery("#form_email").focus();
			return false;
		}else if(!jQuery("#form_intro").val()) {
			alert("請輸入文字介紹");
			jQuery("#form_intro").focus();
			return false;
		}else if(!jQuery("#form_photo").val()) {
			alert("請選擇上傳的圖片");
			jQuery("#form_photo").focus();
			return false;
		}else if(!jQuery("#accept").attr("checked")) {
			alert("請詳閱並同意接受本活動辦法及相關注意事項之規範");
			jQuery("#accept").focus();
			return false;
		}else{
			var email = jQuery("#form_email").val();
			reEmail=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			if (!reEmail.test(jQuery.trim(email))) {
				alert("Email格式錯誤");
				jQuery("#form_email").focus();
				return false;
			}
			if(!checkID(jQuery("#form_userid").val().toUpperCase())){
				alert("身分證字號格式錯誤");
				jQuery("#form_userid").focus();
				return false;	
			}
		}
		
		if(confirm("是否確定送出？")) {
			jQuery("#activity_form").submit();
		}	
	}

	function checkID(id) {
  		tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"                     
  		A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
  		A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
  		Mx = new Array (9,8,7,6,5,4,3,2,1,1);

  		if ( id.length != 10 ) return false;
  		i = tab.indexOf( id.charAt(0) );
  		if ( i == -1 ) return false;
  		sum = A1[i] + A2[i]*9;

  		for ( i=1; i<10; i++ ) {
    		v = parseInt( id.charAt(i) );
    		if ( isNaN(v) ) return false;
    		sum = sum + v * Mx[i];
  		}
  		if ( sum % 10 != 0 ) return false;
  		
  		return true;
	}

</script>

<div class="com_activities">
	<div class="game_page-header">
		<div class="title">
			<?php 
				foreach($this->city as $city) {
					if($city->id == $city_id) {
						echo $city->title;
					}
				} 
			?>
		</div>
	</div>
	
	<span style="color: red; font-weight: bolder; font-size: 1.3em;">*</span>以下投稿欄位都為必填欄位，請確實填寫，否則無法送出。
	<form id="activity_form" name="activity_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_activities&view=activity&layout=form&Itemid='. (int) $itemid, false); ?>">
		<!-- 投稿資料 -->
		<table width="80%" align="center" class="form_table datatable">
			<!-- 姓名 -->
			<tr>
				<th class="form_title">姓名：</th>
				<td class="form_text"><input type="text" class="form_data" id="form_name" name="name" value="<?php echo $app->getUserState('form.activity.name', ''); ?>" /></td>
			</tr>

			<!-- 身分證字號 -->
			<tr>
				<th class="form_title">身分證字號：</th>
				<td class="form_text">
					<input type="text" class="form_data" id="form_userid" name="userid" value="<?php echo $app->getUserState('form.activity.userid', ''); ?>" /><br/>
					<span style="color: red;">「每人限投一件，以身分證字號為憑，獲獎時需憑身分證明文件方得領獎」</span>
				</td>
			</tr>
			
			<!-- 聯絡電話 -->
			<tr>
				<th class="form_title">聯絡電話：</th>
				<td class="form_text">
					<input type="text" class="form_data" id="form_tel" name="tel" maxlength="12" value="<?php echo $app->getUserState('form.activity.tel', ''); ?>" /><br/>
					範例：(02-223-1234)
				</td>
			</tr>			
			
			<tr>
				<th class="form_title">行動電話：</th>
				<td class="form_text">
					<input type="text" class="form_data" id="form_mobile" name="mobile" maxlength="11" value="<?php echo $app->getUserState('form.activity.mobile', ''); ?>" /><br/>
					範例：(0923-123456)
				</td>
			</tr>
			
			<!-- Email -->
			<tr>
				<th class="form_title">Email：</th>
				<td class="form_text"><input type="text" class="form_data" id="form_email" name="email" value="<?php echo $app->getUserState('form.activity.email', ''); ?>" /></td>
			</tr>	
			
			<!-- 文字介紹 -->
			<tr>
				<th class="form_title">文字介紹：<br/>投稿文字(包含標點符號)，30字以內</th>
				<td class="form_text"><textarea class="form_data" id="form_intro" name="intro" rows="5" maxlength="30"><?php echo $app->getUserState('form.activity.intro', ''); ?></textarea></td>
			</tr>

			<!-- 照片 -->
			<tr>
				<th class="form_title">照片：<br/>(檔名請勿使用中文或特殊符號)</th>
				<td class="form_text">
					<input type="file" id="form_photo" name="photo" /><br/>
					<span class="note">(上傳的檔案類型為jpg、gif、png 三種圖片格式，圖檔以不超過2MB為限)</span>
				</td>
			</tr>
			
			<!-- 備註 -->
			<tr>
				<td colspan="2">
					聯絡電話與Email請填寫正確，若屆時無法聯繫上則等同放棄相關領獎權利！
				</td>
			</tr>	
			
			<tr>
				<td colspan="2">
					<input type="checkbox" name="accept" id="accept" value="1" />已詳閱並同意接受本活動辦法及相關注意事項之規範
				</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">
					<!-- submit -->
					<input id="submit_btn" type="button" onclick="sendForm()" onkeypress="sendForm()" value="送出" style="display: none;" />
					<input id="submit_btn2" type="submit" value="送出" />
					<input type="reset" value="清除">
					<input type="hidden" name="city" value="<?php echo $city_id; ?>" />
					<input type="hidden" name="catid" value="<?php echo $this->catid->id; ?>" />
					<input type="hidden" name="task" value="activity.check" />	
				</td>
			</tr>
		</table>
	</form>
</div>