<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['嘉義縣']->id;
?>

<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76675750-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >嘉義縣</span></h5>
	<div id="system-message-container">
    	<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">嘉義縣家庭教育中心</p></h4>
		<div id="alla">
			<div id="lay01">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" style="line-height:30px;" class="c_p">
					</tr>
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/12_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
					</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
				    <tr>
				    	<td height="10" align="left" class="c_p" ></td>
					</tr>
				    <tr>
				    	<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/12_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
					</tr>
				</table>
			</div>
			<div id="lay02">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" style="line-height:25px;">
							<p style="color:#009;">地址：嘉義縣太保市祥和二路東段8號</p>
			         		<p>電話：05-3620747</p><p>網址：<a href="http://cic.familyedu.moe.gov.tw" target="_blank">http://cic.familyedu.moe.gov.tw</a><br></p>
                  			<!--<p>Facebook粉絲頁：<a href="https://www.facebook.com/臺中市家庭教育中心-459348224077723/?ref=aymt_homepage_panel" target="_blank">臺中市家庭教育中心</a></p>-->
                  		</td>
		          	</tr>
			     	<tr>
			       		<td height="25" align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          	</tr>
			     	<tr>
			       		<td align="center" valign="middle">&nbsp;</td>
		          	</tr>
			     	<tr>
			       		<td align="left" style="line-height:30px;" class="c_p"><p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息</p>
  							<table width="100%" border="0" cellpadding="0" cellspacing="0">
       							<tr>
           							<td colspan="3"><span class="date1">105/5/7</span></td>
       							</tr>
       							<tr>
           							<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">上午「原住民夫妻成長活動-逐鹿『鄒』愛一生」活動，假北回國小辦理，歡迎本縣原住民家庭優先踴躍報名參加。</td>
       							</tr>
   							</table>
     						<table width="100%" border="0" cellpadding="0" cellspacing="0">
       							<tr>
           							<td colspan="3"><span class="date1">105/5/9</span></td>
       							</tr>
       							<tr>
           							<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">上午「我和我的孩子父母成長學習班」活動，假大林鎮社團國小辦理，歡迎本縣縣民踴躍報名參加。</td>
       							</tr>
   							</table>
     						<table width="100%" border="0" cellpadding="0" cellspacing="0">
       							<tr>
           							<td colspan="3"><span class="date1">105/5/26</span></td>
       							</tr>
       							<tr>
           							<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">上午「表情達意作父母」活動，假祥和國小辦理，歡迎本縣縣民踴躍報名參加。</td>
       							</tr>
   							</table></td>
		          	</tr>
		     	  <tr>
			     	  <td align="left" style="line-height:30px;" class="c_p"><table width="100%" border="0" cellpadding="0" cellspacing="0">
			     	    <tr>
			     	      <td colspan="3"><span class="date1">105/5/28</span></td>
		     	        </tr>
			     	    <tr>
			     	      <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">上午「吾愛吾家系列活動-舞動身影傳真情」活動，假番路鄉民和國小辦理，歡迎本縣縣民踴躍報名參加。</td>
		     	        </tr>
		     	    </table></td>
		     	  </tr>
			     	<tr>
			     	  <td align="left" style="line-height:30px;" class="c_p"><table width="100%" border="0" cellpadding="0" cellspacing="0">
			     	    <tr>
			     	      <td colspan="3"><span class="date1">105/6/1</span></td>
		     	        </tr>
			     	    <tr>
			     	      <td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">下午「預約幸福的秘訣」婚前教育，假民雄農工辦理，歡迎本縣縣民踴躍報名參加。</td>
		     	        </tr>
		     	      </table></td>
		     	  </tr>
		        </table>
			</div>
		</div>
	</div>
</div>