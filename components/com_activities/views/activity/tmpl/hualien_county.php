<?php
/**
 * @version		: default.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$city_id = $this->city['花蓮縣']->id;
?>


<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76671851-1', 'auto');
  ga('send', 'pageview');

</script>

<div class="com_activities">
	<h5><span style="font-size:36px; font-weight:bold;" >花蓮縣</span></h5>
		<div id="system-message-container">
        	<h4><p style="font-size:20px; font-weight:bold; line-height:50px;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">花蓮縣家庭教育中心</p></h4>
			<div id="alla">
				<div id="lay01">
			   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p">
						</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p"><img src="templates/activity/images/system/18_01.jpg" alt="家庭教育中心照片" title="家庭教育中心照片"  height="200"></td>
		          		</tr>
			          <tr>
			          	<td align="left" style="width: 290px;">
			          		各縣市領獎相關事宜，請見各縣市家庭教育中心網站公告。
			          	</td>
			          </tr>
			     		<tr>
			       			<td height="10" align="left" class="c_p" ></td>
		          		</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p"><span class="c_p" style="line-height:30px;"><img src="templates/activity/images/system/18_02.jpg"  height="286" alt="獎項及獎額與獎勵品名稱" title="獎項及獎額與獎勵品名稱"></span></td>
		          		</tr>
		        	</table>
			 	</div>
			 	<div id="lay02">
			   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  			<tr>
			       			<td align="left" style="line-height:25px;">
			       				<p style="color:#009;">地址：花蓮市達固湖灣大路一號</p>
			         			<p>電話：03-8569692</p><p>網址：<a href="http://hlc.familyedu.moe.gov.tw" target="_blank">hlc.familyedu.moe.gov.tw</a><br></p>
                				<!--<p>Facebook粉絲頁：<a href="https://www.facebook.com/高雄市政府教育局家庭教育中心-375792702555616/?fref=ts" target="_blank">高雄市家庭教育中心</a></p>-->
                			</td>
		          		</tr>
			     		<tr>
			       			<td height="25" align="center" valign="middle">&nbsp;</td>
		          		</tr>
			     		<tr>
			       			<td align="center" valign="middle"><div class="bb001"><a href="javascript:alert('活動已結束，謝謝您的參與。');<?php //echo JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$city_id}&Itemid={$itemid}"); ?>" title="我要投稿">我要投稿</a></div></td>
		          		</tr>
			     		<tr>
			       			<td align="center" valign="middle">&nbsp;</td>
		          		</tr>
			     		<tr>
			       			<td align="left" style="line-height:30px;" class="c_p">
			       				<p style="font-size:20px; font-weight:bold;"><img src="templates/activity/images/system/ctRightIcon01.png" width="19" height="26">105年5-9月家庭教育活動訊息                                </p>
			       				<table width="100%" border="0" cellpadding="0" cellspacing="0">
	   								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;"><p>105/6/25-9/4「親子互動快樂時光-共讀烤箱研習營！」，花蓮縣市社區家長及學齡兒童約計 50人，單親家庭、原住民族家庭、新住民家庭、隔代教養家庭優先接受報名。</p></td>
   								  </tr>
							  </table>
     							<table width="100%" border="0" cellpadding="0" cellspacing="0">
    								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/6/23-8/17「親子共讀角落讀書會- 玩出悅讀趣」，活動對象：歡迎花蓮縣市社區家長及學齡兒童約計 50人，單親家庭、原住民族家庭、新住民家庭、隔代教養、社區家長及有興趣民眾親子一起參加。</td>
       								</tr>
   								</table>
     							<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/4-5月「『慈孝在我家』溫馨慈孝宣導表揚暨親子活動：『慈孝臉譜』徵圖、文活動」，歡迎社區民眾以及鄰近學校師生參加。</td>
       								</tr>
   								</table>
 								<table width="100%" border="0" cellpadding="0" cellspacing="0">
    								<tr>
           								<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/15上午09:30-12:30「『慈孝在我家』溫馨慈孝宣導表揚暨親子活動：『愛家五一五、國際家庭日愛家五一五親子愛的連線、愛家五到的親子律動與親子五到走秀」，歡迎全縣親子家庭參加。</td>
       								</tr>
   								</table>
							  <table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
					           			<td colspan="2" style="  border-bottom-style:dotted; border-width:1px; line-height:25px;">105/5/15上午09:30-12:30「『慈孝在我家』溫馨慈孝宣導表揚暨親子活動：『慈孝在我家』溫馨慈孝卡片頒獎暨親子活動」，歡迎全縣親子家庭參加。</td>
					       			</tr>
					   			</table>
   								<p style="color:#009;">&nbsp;</p>
			        		</td>
	          		  </tr>
		        	</table>
			        		</td>
						</tr>
		        	</table>
				</div>
			</div>
		</div>
	</div>
