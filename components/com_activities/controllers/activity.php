<?php
/**
 * @version     1.0.0
 * @package     com_activities
 * @copyright   Efatek Inc. Copyright (C) 2015. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <rene_chen@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class ActivitiesControllerActivity extends ActivitiesController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'List', $prefix = 'Activities')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
	public function check() {
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt('Itemid');
		$post = $app->input->getArray($_POST);	
		$photo = $app->input->files->get("photo");	

		$allowedExts = array("gif", "jpeg", "jpg", "png");	// 允許的類型array
		$temp = explode(".", $photo["name"]);
		$extension = end($temp);  // 副檔名
		$type_check = 0;
		if ((($photo["type"] == "image/gif") 
				|| ($photo["type"] == "image/jpeg") 
				|| ($photo["type"] == "image/jpg")
				|| ($photo["type"] == "image/pjpeg")
				|| ($photo["type"] == "image/x-png")
				|| ($photo["type"] == "image/png"))
				&& in_array(strtolower($extension), $allowedExts)) {
			$type_check = 1;
		}else{
			$type_check = 0;
		}
		
		$app->setUserState('form.activity.name', $post["name"]);
		$app->setUserState('form.activity.userid', $post["userid"]);
		$app->setUserState('form.activity.tel', $post["tel"]);
		$app->setUserState('form.activity.mobile', $post["mobile"]);
		$app->setUserState('form.activity.email', $post["email"]);
		$app->setUserState('form.activity.intro', $post["intro"]);

		$msg = "";
		if(!$post["name"]) { $msg .= "請輸入姓名<br/>"; }
		if(!$post["userid"]) { $msg .= "請輸入身分證字號<br/>"; }
		if(!$post["tel"]) { $msg .= "請輸入聯絡電話<br/>"; }
		if(!$post["mobile"]) {$msg .= "請輸入行動電話<br/>"; }
		if(!$post["email"]) { $msg .= "請輸入Email<br/>"; }
		if(!preg_match('/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/', $post["email"])) { $msg .= "Email格式錯誤";	}
		if(!$post["intro"]) { $msg .= "請輸入文字介紹<br/>"; }
		if(utf8_strlen($post["intro"]) > 30) {$msg .= "投稿文字(包含標點符號)，請在30個字以內<br/>"; }
		
		if($photo["error"] == 1) {
			 $msg .= "投稿照片檔案大小請勿超過2MB<br/>"; 
		}else{
			if(!$photo['size']) { $msg .= "請選擇上傳的照片<br/>"; }
			if($photo['size'] > (2000 * 1024)){ $msg .= "投稿照片檔案大小請勿超過2MB<br/>"; }
			if($type_check == 0) {
				$msg .="上傳的檔案類型請為jpg、gif、png 三種圖片格式<br/>";
			}				
		}

		if(!$post['city'] or $post['city'] < 11 or $post['city'] > 32 ) { $msg .= "請從首頁選擇投稿縣市<br/>"; }
		if(!$post['accept']) { $msg .= "請詳閱並同意接受本活動辦法及相關注意事項之規範<br/>"; }
		
		if(!self::checkUserID($post["userid"])) {
			$msg .= "此身分證字號已投稿過<br/>";
		}

		if($msg != "") {
			$link = JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$post['city']}&Itemid={$itemid}", false);
			$this->setRedirect($link, $msg);
			return;
		}else{
			$this->submit();
		}

	}

	public function submit() {		
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$post = $app->input->getArray($_POST);

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		date_default_timezone_set("Asia/Taipei");
		$datetime = date("Y-m-d H:i:s");
		// $date = JFactory::getDate();
		// $datetime = $db->Quote($date->toSql());
		
		$userid = strtoupper($post["userid"]);
		$intro = JFilterOutput::cleanText($post['intro']);
		
		$query->insert($db->quoteName('#__contribute_list'));
		$query->columns("catid, city, Name, UserID, Tel, Mobile, Email, Introduction, CreateTime, Flg, state");
		$query->values("'{$post['catid']}', '{$post['city']}', '{$post['name']}', '{$userid}', '{$post['tel']}', 
						'{$post['mobile']}', '{$post['email']}', '{$intro}', '{$datetime}', '0', '0'");

		$db->setQuery($query);
		
		if(!$db->execute()) {
			$msg = '資料存入時發生不明錯誤！請連繫網站管理人員。';
			$link = JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$post['city']}&Itemid={$itemid}", false);
			$this->setRedirect($link, $msg);
			return;
		}

		$id = $db->insertid();
		
		// 圖片
		$image = $app->input->files->get("photo");
		$filepath = "filesys/images/activity/";
		$image_path = $this->_uploadFiles($image, $id, $filepath);
		
		if(file_exists($image_path)) {
			$query_img = $db->getQuery(true);
			$query_img->update($db->quoteName('#__contribute_list'));
			$query_img->set("PicPath = '{$image_path}'");
			$query_img->where("id = '{$id}'");
			
			$db->setQuery($query_img);
	
			if(!$db->execute()) {
				$msg = '照片資料存入時發生不明錯誤！請連繫網站管理人員。';
				$link = JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$post['city']}&Itemid={$itemid}", false);
				$this->setRedirect($link, $msg);		
				return;
			}else {
				$app->setUserState('form.activity', null);
				$msg = "資料已送出，謝謝您的投稿<br/>";
				$link = JRoute::_("index.php?option=com_activities&view=activity&layout=complete&Itemid={$itemid}", false);
				$this->setRedirect($link, $msg);
				return;
			}
		}else{
			$query_del = $db->getQuery(true);
			$query_del->delete($db->quoteName('#__contribute_list'));
			$query_del->where("id = '{$id}'");
			
			$db->setQuery($query_del);
			$db->execute();
			
			$msg = "照片上傳失敗，請重新投稿。";
			$link = JRoute::_("index.php?option=com_activities&view=activity&layout=form&city={$post['city']}&Itemid={$itemid}", false);
			$this->setRedirect($link, $msg);
		}		
	}
	
	public function checkUserID($user_id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select("UserID");
		$query->from($db->quoteName('#__contribute_list'));
		$query->where("UserID = '{$user_id}'");
		
		$db->setQuery($query);
		$item = $db->loadObject();
		
		if($item) {
			return false;
		}else{
			return true;
		}
	}
	
	public function _uploadFiles($files, $id, $filepath) {
		/** 判斷是否有傳入 $files **/
		if( $files != '' )
		{
			$dbpath = $filepath.$id."/";
			$path = JPATH_SITE."/".$dbpath;
			// $indexfile = JPATH_SITE . "/" . $filepath . "index.html";
			
			/** 搬移檔案 **/
			$filename = $files['name'];
			$filetmpname = $files['tmp_name'];
			if( file_exists( $filetmpname ) )
			{
				if(!is_dir($path)) {
					mkdir ($path, 0755);
				}
				move_uploaded_file($filetmpname, $path . $filename);
				chmod($path . $filename, 0755);

				return $dbpath.$filename;
			}
		}		
	}

}