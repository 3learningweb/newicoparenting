<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<script>
	(function($) {		
		// submit
		$(document).on("click", "#submit_btn", function() {
			$("#profile_form").submit();
		});	
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>
	
		<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				活潑好動的寶寶，不免出現一些小狀況，爬上爬下不小心跌倒了！半夜寶寶忽然發燒了？運用家庭急救包的物品進行初步的處理，也減少爸爸媽媽的手足無措喔!!想想看，家庭急救包應該放些什麼？讓我們一起來列出專屬的家庭急救包清單，也不要忘記定期的盤點、更新喔！！
			</p>
		</div>
	</div>
	
	<div class="checklist_block" style="text-align: right;">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item2&layout=checklist&Itemid={$itemid}", false); ?>">
			<div class="submit_block"><input type="button" id="submit_btn" value="開始準備" style="width: 155px;" /></div>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_checklist/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
