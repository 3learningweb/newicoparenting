<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<script>
	(function($) {		
		// submit
		$(document).on("click", "#submit_btn", function() {
			$("#profile_form").submit();
		});	
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo "親子野餐趣"; ?>
		</div>
	</div>
	
		<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				想要讓心靈放鬆一下，享受與家人同樂的時光嗎？來場野餐吧！讓我們一起列出野餐清單，周休二日就能輕輕鬆鬆，帶著全家大小共同來趟專屬的微旅行！
			</p>
		</div>
	</div>
	
	<div class="checklist_block" style="text-align: right;">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item2&layout=checklist_picnic&Itemid={$itemid}", false); ?>">
			<div class="submit_block"><input type="button" id="submit_btn" value="開始準備" style="width: 155px;" /></div>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_checklist/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
