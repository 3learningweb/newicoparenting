<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<!-- include jquery ui -->
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/jquery-1.9.1.js"></script>
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style>
	.ui-corner-all {
		padding: 0px;
	}
	
	.ui-widget-header {
		border: 0px;
		height: 37px;
		text-align: center;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
		background: url("<?php echo JURI::base();  ?>components/com_checklist/assets/images/title_bg.jpg");
	}
	
	.ui-widget-header .ui-dialog-title {
		font-family: '微軟正黑體',"PMingLiU","Arial";
    	color: #FFFFFF;
    	font-size: 22px;
	}
</style>


<div class="com_checklist">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>
	
	<div class="checklist_block">
		<img src="components/com_checklist/assets/images/bg2.png" alt="家庭急救包" usemap="#checklist" />
		<map name="checklist">
			<area shape="rect" coords="170,115,255,195" href="javascript:void();" id="equipment" alt="器材" title="器材" />
			<area shape="rect" coords="285,95,370,175" href="javascript:void();" id="medicine" alt="內服藥" title="內服藥" />
			<area shape="rect" coords="400,80,485,160" href="javascript:void();" id="traumatic" alt="外傷" title="外傷" />
			<area shape="rect" coords="250,215,330,295" href="javascript:void();" id="medical" alt="醫療用品" title="醫療用品" />
			<area shape="rect" coords="360,195,445,275" href="javascript:void();" id="other" alt="其他" title="其他" />
		</map>
		
		<form id="output" name="output" method="post" action="<?php echo JURI::base(); ?>rd/export/print_checklist2.php">
			<input type="hidden" id="equipment_data" name="equipment_data" value="" />
			<input type="hidden" id="medicine_data" name="medicine_data" value="" />
			<input type="hidden" id="traumatic_data" name="traumatic_data" value="" />
			<input type="hidden" id="medical_data" name="medical_data" value="" />
			<input type="hidden" id="other_data" name="other_data" value="" />

			<input type="hidden" id="check_list" name="check_list" value="0" />
			<div class="submit_block" style="visibility: hidden;"><input type="button" id="return_btn" value="上一步" onclick="javascript:history.go(-1)" /></div>
			<div class="submit_block"><input type="button" id="submit_btn" value="下載清單" /></div>
		</form>			
			
	</div>
	
	<!-- 器材 -->
	<div style='display:none' id='equipment_list'>
		<div class="add_item" title="新增醫療器材"><span class="icon-save-new"></span> (點擊新增醫療器材)</div>
		<div class="list_block"><input type="checkbox" id="elist_1" name="equipment[]" value="溫度計" /> 溫度計</div>
		<div class="list_block"><input type="checkbox" id="elist_2" name="equipment[]" value="吸鼻器" /> 吸鼻器</div>
		<div class="list_block"><input type="checkbox" id="elist_3" name="equipment[]" value="拍痰器" /> 拍痰器</div>
		<div class="list_block"><input type="checkbox" id="elist_4" name="equipment[]" value="手電筒" /> 手電筒</div>
		<div class="list_block"><input type="checkbox" id="elist_5" name="equipment[]" value="耳溫槍" /> 耳溫槍</div>
	</div>
	
	<!-- 內服藥 -->
	<div style="display:none" id="medicine_list">
		<div class="add_item" title="新增內服藥"><span class="icon-save-new"></span> (點擊新增內服藥)</div>
		<div class="list_block"><input type="checkbox" id="mlist_1" name="medicine[]" value="退燒藥" /> 退燒藥</div>
		<div class="list_block"><input type="checkbox" id="mlist_2" name="medicine[]" value="止咳藥" /> 止咳藥</div>
		<div class="list_block"><input type="checkbox" id="mlist_3" name="medicine[]" value="止痛藥" /> 止痛藥</div>
	</div>
	
	<!-- 外傷 -->
	<div style="display:none" id="traumatic_list">
		<div class="add_item" title="新增外傷藥"><span class="icon-save-new"></span> (點擊新增外傷藥)</div>
		<div class="list_block"><input type="checkbox" id="tlist_1" name="traumatic[]" value="蚊蟲咬傷藥膏" /> 蚊蟲咬傷藥膏</div>
		<div class="list_block"><input type="checkbox" id="tlist_2" name="traumatic[]" value="凡士林" /> 凡士林</div>
		<div class="list_block"><input type="checkbox" id="tlist_3" name="traumatic[]" value="生理食鹽水" /> 生理食鹽水</div>
		<div class="list_block"><input type="checkbox" id="tlist_4" name="traumatic[]" value="碘酒" /> 碘酒</div>
	</div>

	<!-- 醫療用品 -->
	<div style="display:none" id="medical_list">
		<div class="add_item" title="新增醫療用品"><span class="icon-save-new"></span> (點擊新增醫療用品)</div>
		<div class="list_block"><input type="checkbox" id="mdlist_1" name="medical[]" value="滅菌紗布" /> 滅菌紗布</div>
		<div class="list_block"><input type="checkbox" id="mdlist_2" name="medical[]" value="滅菌棉棒" /> 滅菌棉棒</div>
		<div class="list_block"><input type="checkbox" id="mdlist_3" name="medical[]" value="透氣膠帶" /> 透氣膠帶</div>
		<div class="list_block"><input type="checkbox" id="mdlist_4" name="medical[]" value="OK蹦" /> OK蹦</div>
		<div class="list_block"><input type="checkbox" id="mdlist_5" name="medical[]" value="剪刀" /> 剪刀</div>
		<div class="list_block"><input type="checkbox" id="mdlist_6" name="medical[]" value="冷熱敷袋" /> 冷熱敷袋</div>
	</div>	
	
	<!-- 其他 -->
	<div style="display:none" id="other_list">
		<div class="add_item" title="新增其他用品"><span class="icon-save-new"></span> (點擊新增其他用品)</div>
	</div>
</div>

<script language="JavaScript">
(function($) {
	$(document).ready(function() {
		// 器材
        $("#equipment").bind('click',function(){
			$("#equipment_list").dialog({
		        title: '器材',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var equipment = new Array();
						$("input[name='equipment[]']:checked").each(function() {
							equipment.push(jQuery(this).val());
						});
						$("#equipment_data").val(equipment);			
		                $(this).dialog('close');
		            }
		        }
			});	
        });
        
        // 內服藥
        $("#medicine").bind('click', function() {
			$("#medicine_list").dialog({
		        title: '內服藥',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var medicine = new Array();
						$("input[name='medicine[]']:checked").each(function() {
							medicine.push(jQuery(this).val());
						});
						$("#medicine_data").val(medicine);			
		                $(this).dialog('close');
		            }
		        }
			});
        });
        
        // 外傷
        $("#traumatic").bind('click', function() {
			$("#traumatic_list").dialog({
		        title: '外傷',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var traumatic = new Array();
						$("input[name='traumatic[]']:checked").each(function() {
							traumatic.push(jQuery(this).val());
						});
						$("#traumatic_data").val(traumatic);			
		                $(this).dialog('close');
		            }
		        }
			});
        });   

        // 醫療用品
        $("#medical").bind('click', function() {
			$("#medical_list").dialog({
		        title: '醫療用品',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var medical = new Array();
						$("input[name='medical[]']:checked").each(function() {
							medical.push(jQuery(this).val());
						});
						$("#medical_data").val(medical);			
		                $(this).dialog('close');
		            }
		        }
			});
        });   

         // 其他
        $("#other").bind('click', function() {
			$("#other_list").dialog({
		        title: '其他',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var other = new Array();
						$("input[name='other[]']:checked").each(function() {
							other.push(jQuery(this).val());
						});
						$("#other_data").val(other);			
		                $(this).dialog('close');
		            }
		        }
			});
        }); 
        
        
		// 新增物品         
		$(".add_item span").bind('click',function(){
        	var item = prompt("請輸入要新增的物品");
         	var list = $(this).parent().parent();
         	var num = $("#" +list.prop("id") + " .list_block").length + 1;
         	
         	if(item != null) {
	         	if(list.prop("id") == "equipment_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='elist_"+num+"' name='equipment[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "medicine_list"){
	         		list.append("<div class='list_block'><input type='checkbox' id='mlist_"+num+"' name='medicine[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "traumatic_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='tflist_"+num+"' name='traumatic[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "medical_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='mdlist_"+num+"' name='medical[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "other_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='olist_"+num+"' name='other[]' value='"+item+"' /> "+item+"</div>");
	         	}         		
         	}         	
		});
		
		
		// submit
		$(document).on("click", "#submit_btn", function() {
			var check_list = 0;
			
			if($("#equipment_data").val()) {
				check_list = 1;
			}else if($("#medicine_data").val()) {
				check_list = 1;
			}else if($("#traumatic_data").val()) {
				check_list = 1;
			}else if($("#medical_data").val()) {
				check_list = 1;
			}else if($("#other_data").val()) {
				check_list = 1;
			}
			$("#check_list").val(check_list);

			if(check_list == 1) {
				$("#output").submit();
			}else{
				alert("請至少勾選一項目");
				return false;
			}
			
			
		});
	});
})(jQuery);
</script>