<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<!-- include jquery ui -->
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/jquery-1.9.1.js"></script>
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style>
	.ui-corner-all {
		padding: 0px;
	}
	
	.ui-widget-header {
		border: 0px;
		height: 37px;
		text-align: center;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
		background: url("<?php echo JURI::base();  ?>components/com_checklist/assets/images/title_bg.jpg");
	}
	
	.ui-widget-header .ui-dialog-title {
		font-family: '微軟正黑體',"PMingLiU","Arial";
    	color: #FFFFFF;
    	font-size: 22px;
	}


	#checklist > div {
		border:1px solid #cfdd90;
		margin: 10px;
		clear:both;
		height:113px;
		cursor:pointer
	}

	#checklist  div .pull-left{
		padding:55px;
		background: url("<?php echo JURI::base();  ?>components/com_checklist/assets/images/img_06.png") no-repeat;
		background-size:contain;
		position: relative;
		width:30%;
		float:left;
		height:100%;
	}

	#checklist  div:hover .pull-left {
		background: url("<?php echo JURI::base();  ?>components/com_checklist/assets/images/img_03.png") no-repeat;
	}

	#checklist  div .pull-right{
		padding-top: 5px;
		width: 70%;
	}



	#checklist  div img{
		
		position: absolute;
		left: 30px;
		top: 2px;

	}

	.com_checklist #output .submit_block {
		position: relative;
		top: 50px;
		left: 145px;


	}

	.caption {
	    display: block;
	    position: absolute;
	    bottom: 5px;
	    left:30px;
	    color: #fff;
	    font-size: 20px;
	}

	#other {
		background:initial;
	}

	.checklist_block * {
	  -webkit-box-sizing: border-box;
	     -moz-box-sizing: border-box;
	          box-sizing: border-box;
	}

	#checklist .text {
		font-size:15px;
		text-align: left;
	}

	#father, #granpa {
	    
	    color: #fff;
	    padding: 1px 10px 1px 10px;
	    border-radius: 10px;
	    display: block;
	    float: left;
	}

	#father {background-color: #f5a340;}
	#granpa {
		background-color: #c75681;
		margin-left:10px;

	}

	#checklist a:link, #checklist a:visited {
		color:#44D7EF;
	}



	#other {
	    background: #C7DF26; /* For browsers that do not support gradients */
	    background: -webkit-linear-gradient(#C7DF26, #63A80D); /* For Safari 5.1 to 6.0 */
	    background: -o-linear-gradient(#C7DF26, #63A80D); /* For Opera 11.1 to 12.0 */
	    background: -moz-linear-gradient(#C7DF26, #63A80D); /* For Firefox 3.6 to 15 */
	    background: linear-gradient(#C7DF26, #63A80D); /* Standard syntax */
	    font-size: 16px;
	    color:#fff;
	    padding:16px;
	    height:initial !important;
	}

	/* div 裡頭文字垂直置中 */ 
	.pull-right.text {
	    display: table;
	    height: 100%;
	    overflow: hidden;
	}

	.pull-right.text > div {
		display: table-cell;
		vertical-align: middle;
	}






</style>


<div class="com_checklist">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="game_page-header">
		<div class="title">
			<?php echo "親子野餐趣"; ?>
		</div>
	</div>
	
	<div class="checklist_block">
	    <div id="checklist">
	        <div id="invited" alt="邀請對象" title="邀請對象">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_03.png" style="height: 70px;" />
	                <p class="caption">邀請對象</p>
	            </div>
	            <div class="pull-right text">
			<div>野餐，透過策畫、準備到實際聚餐的過程中，都是情感聯繫、愛的展現，邀請您的親朋好友，一起來野餐吧！</div>
	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        
	        <div id="place" alt="場地選擇" title="場地選擇">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_11.png" style="height: 70px;" />
	                <p class="caption">場地選擇</p>
	            </div>
	            <div class="pull-right text">
			<div>建議以明亮、遮風雨，且附近有乾淨水源的地方為考量。</div>
	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        
	        <div id="food" alt="食品準備" title="食品準備">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_16.png" style="height: 70px;" />
	                <p class="caption">食品準備</p>
	            </div>
	            <div class="pull-right text">
			<div>建議以方便攜帶、保鮮期較長之食品為考量。</div>
	            </div>
	            <div style="clear:both;"></div>
	        </div>

	        <div id="game" alt="野餐小活動" title="野餐小活動">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_20.png" style="height: 70px;" />
	                <p class="caption">野餐小活動</p>
	            </div>
	            <div class="pull-right text">
	            	<div>
			<p>野餐時，除了享受美味的食物，還可以做些什麼呢？參考看看吧！
			<br />※<a href="https://imyfamily.moe.edu.tw/家庭活動/家庭聚會卡" target="_blank">家庭聚會卡</a>：透過家庭聚會卡來炒熱氣氛吧。
			<br />※<a href="https://icoparenting.moe.edu.tw/寶寶的健康成長/親子活動" target="_blank">體驗活動</a>：來看看和孩子還可以玩什麼體驗活動。
			<br />※<a href="https://imyfamily.moe.edu.tw/家庭紀錄簿/回憶珠寶盒" target="_blank">回憶珠寶盒</a>：拍照上傳野餐快樂時光的照片吧。</p>
			</div>

	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        <div id="recommend" alt="共讀推薦" title="共讀推薦">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_23.png" style="height: 70px;" />
	                <p class="caption">共讀推薦</p>
	            </div>
	            <div class="pull-right text">
	            	<div>
			<p>除了動態活動外，全家大小也可以挑選自己所愛的書，互相分享、或是一同享受共讀恬靜的親密時光。
			<br>※<a href="https://icoparenting.moe.edu.tw/教養孩子有方法/共讀方法-技巧" target="_blank">親子共讀趣</a>：點選文字連結，瞭解家中的寶貝適合的書籍。</p>

			<p style="margin-top:10px;"><a href="https://imyfamily.moe.edu.tw/家庭圖書館/給成為好兒女的你/32-和爸爸一起讀書" target="_blank"><span id="father">和爸爸一起讀書</span></a><a href="https://imyfamily.moe.edu.tw/家庭圖書館/給邁向黃金念代的你/47-米爺爺學認字" target="_blank"><span id="granpa">米爺爺學認字</span></a></p>
			</div>

	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        <div id="adult" alt="大人攜帶物品" title="大人攜帶物品">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_27.png" style="height: 70px;" />
	                <p class="caption">大人攜帶物品</p>
	            </div>
	            <div class="pull-right text">
			<div><p>建議以方便攜帶、活動必備品為考量。</p></div>

	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        <div id="children" alt="小孩攜帶物品" title="小孩攜帶物品">
	            <div class="pull-left">
	                <img src="<?php echo JURI::base();  ?>components/com_checklist/assets/images/icon_31.png" style="height: 70px;" />
	                <p class="caption">小孩攜帶物品</p>
	            </div>
	            <div class="pull-right text">
			<div><p>讓小孩攜帶自己的物品，會更有參與感唷！但要記得依據體力、實際使用作為攜帶考量。</p></div>
	            </div>
	            <div style="clear:both;"></div>
	        </div>
	        <div id="other" alt="其他" title="其他">其他</div>
	    </div>
	    <form id="output" name="output" method="post" action="<?php echo JURI::base(); ?>rd/export/print_checklist3.php">
	        <input type="hidden" id="invited_data" name="invited_data" value="" />
	        <input type="hidden" id="place_data" name="place_data" value="" />
	        <input type="hidden" id="food_data" name="food_data" value="" />
	        <input type="hidden" id="game_data" name="game_data" value="" />
	        <input type="hidden" id="recommend_data" name="recommend_data" value="" />
	        <input type="hidden" id="adult_data" name="adult_data" value="" />
	        <input type="hidden" id="children_data" name="children_data" value="" />
	        <input type="hidden" id="other_data" name="other_data" value="" />
	        <input type="hidden" id="check_list" name="check_list" value="0" />
	        <div class="submit_block" style="visibility: hidden;">
	            <input type="button" id="return_btn" value="上一步" onclick="javascript:history.go(-1)" />
	        </div>
	        <div class="submit_block">
	            <input type="button" id="submit_btn" value="下載清單" />
	        </div>
	        <div style="clear:both;height:60px;"></div>
	    </form>
	</div>

	
	<!-- 邀請對象 -->
	<div style='display:none' id='invited_list'>
		<div class="add_item" title="新增邀請對象"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="inlist_1" name="invited[]" value="父母__________" /> 父母__________</div>
		<div class="list_block"><input type="checkbox" id="inlist_2" name="invited[]" value="手足__________" /> 手足__________</div>
		<div class="list_block"><input type="checkbox" id="inlist_3" name="invited[]" value="小孩__________" /> 小孩__________</div>
		<div class="list_block"><input type="checkbox" id="inlist_4" name="invited[]" value="孫子__________" /> 孫子__________</div>
		<div class="list_block"><input type="checkbox" id="inlist_5" name="invited[]" value="鄰居__________" /> 鄰居__________</div>
		<div class="list_block"><input type="checkbox" id="inlist_6" name="invited[]" value="朋友__________" /> 朋友__________</div>
	</div>
	
	<!-- 場地選擇 -->
	<div style="display:none" id="place_list">
		<div class="add_item" title="新增場地"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="pllist_1" name="place[]" value="交通方式____________" /> 交通方式____________</div>
		<div class="list_block"><input type="checkbox" id="pllist_2" name="place[]" value="附近需注意之蚊蟲________" /> 附近需注意之蚊蟲________</div>
		<div class="list_block"><input type="checkbox" id="pllist_3" name="place[]" value="遮陽、遮雨備案__________" /> 遮陽、遮雨備案__________</div>
		<div class="list_block"><input type="checkbox" id="pllist_4" name="place[]" value="太陽直射時間____________" /> 太陽直射時間____________</div>
		<div class="list_block"><input type="checkbox" id="pllist_5" name="place[]" value="鄰近之廁所______________" /> 鄰近之廁所______________</div>
		<div class="list_block"><input type="checkbox" id="pllist_6" name="place[]" value="鄰近乾淨之水源__________" /> 鄰近乾淨之水源__________</div>
	</div>
	
	<!-- 食品準備 -->
	<div style="display:none" id="food_list">
		<div class="add_item" title="新增食品"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="folist_1" name="food[]" value="三明治" /> 三明治</div>
		<div class="list_block"><input type="checkbox" id="folist_2" name="food[]" value="水果" /> 水果</div>
		<div class="list_block"><input type="checkbox" id="folist_3" name="food[]" value="沙拉" /> 沙拉</div>
		<div class="list_block"><input type="checkbox" id="folist_4" name="food[]" value="飯糰" /> 飯糰</div>
		<div class="list_block"><input type="checkbox" id="folist_5" name="food[]" value="小點心" /> 小點心</div>
		<div class="list_block"><input type="checkbox" id="folist_6" name="food[]" value="水、飲品" /> 水、飲品</div>
	</div>

	<!-- 野餐小活動 -->
	<div style="display:none" id="game_list">
		<div class="add_item" title="新增其他項目"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="galist_1" name="game[]" value="運動____________ (ex：羽球、足球等）" /> 運動____________ (ex：羽球、足球等）</div>
		<div class="list_block"><input type="checkbox" id="galist_2" name="game[]" value="吹泡泡" /> 吹泡泡</div>
		<div class="list_block"><input type="checkbox" id="galist_3" name="game[]" value="放風箏" /> 放風箏</div>
		<div class="list_block"><input type="checkbox" id="galist_4" name="game[]" value="身體律動遊戲" /> 身體律動遊戲</div>
		<div class="list_block"><input type="checkbox" id="galist_5" name="game[]" value="繪本即興演出，繪本：____________" /> 繪本即興演出，繪本：____________</div>
	</div>	

	<!-- 共讀推薦 -->
	<div style="display:none" id="recommend_list">
		<div class="add_item" title="新增其他項目"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="relist_1" name="recommend[]" value="和爸爸一起讀書" /> 和爸爸一起讀書</div>
		<div class="list_block"><input type="checkbox" id="relist_2" name="recommend[]" value="米爺爺學認字" /> 米爺爺學認字</div>
	</div>	
	
	<!-- 大人攜帶物品 -->
	<div style="display:none" id="adult_list">
		<div class="add_item" title="新增其他項目"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="adlist_1" name="adult[]" value="野餐墊" /> 野餐墊</div>
		<div class="list_block"><input type="checkbox" id="adlist_2" name="adult[]" value="便當袋/野餐籃" /> 便當袋/野餐籃</div>
		<div class="list_block"><input type="checkbox" id="adlist_3" name="adult[]" value="餐盒" /> 餐盒</div>
		<div class="list_block"><input type="checkbox" id="adlist_4" name="adult[]" value="餐具" /> 餐具</div>
		<div class="list_block"><input type="checkbox" id="adlist_5" name="adult[]" value="防蚊用品" /> 防蚊用品</div>
		<div class="list_block"><input type="checkbox" id="adlist_6" name="adult[]" value="防曬用品" /> 防曬用品</div>
		<div class="list_block"><input type="checkbox" id="adlist_7" name="adult[]" value="清潔用品(紙巾、手帕等)" /> 清潔用品(紙巾、手帕等)</div>
		<div class="list_block"><input type="checkbox" id="adlist_8" name="adult[]" value="收納袋" /> 收納袋</div>
		<div class="list_block"><input type="checkbox" id="adlist_9" name="adult[]" value="防曬/保暖衣物" /> 防曬/保暖衣物</div>
		<div class="list_block"><input type="checkbox" id="adlist_10" name="adult[]" value="音樂" /> 音樂</div>
		<div class="list_block"><input type="checkbox" id="adlist_11" name="adult[]" value="簡易醫藥箱" /> 簡易醫藥箱</div>
		
	</div>	

	<!-- 小孩攜帶物品 -->
	<div style="display:none" id="children_list">
		<div class="add_item" title="新增其他項目"><span class="icon-save-new"></span> (點擊新增其他項目)</div>
		<div class="list_block"><input type="checkbox" id="chlist_1" name="children[]" value="便當袋" /> 便當袋</div>
		<div class="list_block"><input type="checkbox" id="chlist_2" name="children[]" value="小餐盒" /> 小餐盒</div>
		<div class="list_block"><input type="checkbox" id="chlist_3" name="children[]" value="小水壺" /> 小水壺</div>
		<div class="list_block"><input type="checkbox" id="chlist_4" name="children[]" value="玩具____________(ex：泡泡槍、足球、飛盤等)" /> 玩具____________(ex：泡泡槍、足球、飛盤等)</div>
	</div>	

	<!-- 其他 -->
	<div style="display:none" id="other_list">
		<div class="add_item" title="新增其他用品"><span class="icon-save-new"></span> (點擊新增其他用品)</div>
	</div>
</div>

<script language="JavaScript">
(function($) {
    $(document).ready(function() {
        
        /* 在checklist裡頭的anchor 不要讓他的event bubble上去 */
        
        $("#checklist a").click(function(e) { 
           //do something
           e.stopPropagation();
        })

        // 邀請對象
        $("#invited").bind('click', function() {
            $("#invited_list").dialog({
                title: '邀請對象',
                bgiframe: true,
                width: 360,
                height: 450,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var invited = new Array();
                        $("input[name='invited[]']:checked").each(function() {
                            invited.push(jQuery(this).val());
                        });
                        $("#invited_data").val(invited);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 場地選擇
        $("#place").bind('click', function() {
            $("#place_list").dialog({
                title: '場地選擇',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var place = new Array();
                        $("input[name='place[]']:checked").each(function() {
                            place.push(jQuery(this).val());
                        });
                        $("#place_data").val(place);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 食品準備
        $("#food").bind('click', function() {
            $("#food_list").dialog({
                title: '食品準備',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var food = new Array();
                        $("input[name='food[]']:checked").each(function() {
                            food.push(jQuery(this).val());
                        });
                        $("#food_data").val(food);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 野餐小活動
        $("#game").bind('click', function() {
            $("#game_list").dialog({
                title: '野餐小活動',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var game = new Array();
                        $("input[name='game[]']:checked").each(function() {
                            game.push(jQuery(this).val());
                        });
                        $("#game_data").val(game);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 共讀推薦
        $("#recommend").bind('click', function() {
            $("#recommend_list").dialog({
                title: '共讀推薦',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var recommend = new Array();
                        $("input[name='recommend[]']:checked").each(function() {
                            recommend.push(jQuery(this).val());
                        });
                        $("#recommend_data").val(recommend);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 大人攜帶物品
        $("#adult").bind('click', function() {
            $("#adult_list").dialog({
                title: '大人攜帶物品',
                bgiframe: true,
                width: 360,
                height: 550,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var adult = new Array();
                        $("input[name='adult[]']:checked").each(function() {
                            adult.push(jQuery(this).val());
                        });
                        $("#adult_data").val(adult);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 小孩攜帶物品
        $("#children").bind('click', function() {
            $("#children_list").dialog({
                title: '小孩攜帶物品',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var children = new Array();
                        $("input[name='children[]']:checked").each(function() {
                            children.push(jQuery(this).val());
                        });
                        $("#children_data").val(children);
                        $(this).dialog('close');
                    }
                }
            });
        });

        // 其他
        $("#other").bind('click', function() {
            $("#other_list").dialog({
                title: '其他',
                bgiframe: true,
                width: 360,
                height: 400,
                modal: true,
                draggable: true,
                resizable: true,
                buttons: {
                    '確定': function() {
                        var other = new Array();
                        $("input[name='other[]']:checked").each(function() {
                            other.push(jQuery(this).val());
                        });
                        $("#other_data").val(other);
                        $(this).dialog('close');
                    }
                }
            });
        });


        // 新增物品         
        $(".add_item span").bind('click', function() {
            var item = prompt("請輸入：");
            var list = $(this).parent().parent();
            var num = $("#" + list.prop("id") + " .list_block").length + 1;

            if (item != null) {
                if (list.prop("id") == "invited_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='inlist_" + num + "' name='invited[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "place_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='pllist_" + num + "' name='place[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "food_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='folist_" + num + "' name='food[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "game_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='galist_" + num + "' name='game[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "recommend_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='relist_" + num + "' name='recommend[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "adult_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='adlist_" + num + "' name='adult[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "children_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='chlist_" + num + "' name='children[]' value='" + item + "' /> " + item + "</div>");
                } else if (list.prop("id") == "other_list") {
                    list.append("<div class='list_block'><input type='checkbox' id='olist_" + num + "' name='other[]' value='" + item + "' /> " + item + "</div>");
                }
            }
        });


        // submit
        $(document).on("click", "#submit_btn", function() {
            var check_list = 0;

            if ($("#invited_data").val()) {
                check_list = 1;
            } else if ($("#place_data").val()) {
                check_list = 1;
            } else if ($("#food_data").val()) {
                check_list = 1;
            } else if ($("#game_data").val()) {
                check_list = 1;
            } else if ($("#recommend_data").val()) {
                check_list = 1;
            } else if ($("#adult_data").val()) {
                check_list = 1;
            } else if ($("#children_data").val()) {
                check_list = 1;
            } else if ($("#other_data").val()) {
                check_list = 1;
            }


            $("#check_list").val(check_list);

            if (check_list == 1) {
                $("#output").submit();
            } else {
                alert("請至少勾選一項目");
                return false;
            }


        });
    });
})(jQuery);
</script>
