<?php
/**
 * @version		: checklist.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$post = $app->input->getArray($_POST);
$post['name'] = htmlspecialchars($post['name'],ENT_QUOTES);
$post['date'] = htmlspecialchars($post['date'],ENT_QUOTES);

if($post['name']) {
	$app->setUserState('form.checklist.mother', $post['name']);
}
if($post['date']) {
	$app->setUserState('form.checklist.date', $post['date']);
}

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<!-- include jquery ui -->
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/jquery-1.9.1.js"></script>
<script src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style>
	.ui-corner-all {
		padding: 0px;
	}
	
	.ui-widget-header {
		border: 0px;
		height: 37px;
		text-align: center;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
		background: url("<?php echo JURI::base();  ?>components/com_checklist/assets/images/title_bg.jpg");
	}
	
	.ui-widget-header .ui-dialog-title {
		font-family: '微軟正黑體',"PMingLiU","Arial";
    	color: #FFFFFF;
    	font-size: 22px;
	}

</style>

<div class="com_checklist">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_ONE"); ?>
		</div>
	</div>
	
	<div class="checklist_block">
		<div class="checklist_title" ><?php echo $app->getUserState("form.checklist.mother"); ?>的待產包</div>
		<img src="components/com_checklist/assets/images/bg1.png" alt="幸福待產包" usemap="#checklist" />
		<map name="checklist">
			<area shape="rect" coords="115,75,190,155" href="javascript:void();" id="mother" alt="媽媽用品" title="媽媽用品"  />
			<area shape="rect" coords="225,55,305,135"  href="javascript:void();" id="baby" alt="寶寶用品" title="寶寶用品" />
			<area shape="rect" coords="340,35,415,115" href="javascript:void();" id="breastfeed" alt="哺乳用品" title="哺乳用品" />
			<area shape="rect" coords="190,170,265,250" href="javascript:void();" id="card" alt="證件" title="證件" />
			<area shape="rect" coords="300,150,380,230" href="javascript:void();" id="other" alt="其他" title="其他" />
			<area shape="rect" coords="270,260,350,340" href="javascript:void();" id="todo" alt="代辦事項" title="代辦事項" />
			<area shape="rect" coords="385,240,465,320" href="javascript:void();" id="contact" alt="重要聯絡資訊" title="重要聯絡資訊" />
		</map>
		
		<form id="output" name="output" method="post" action="<?php echo JURI::base(); ?>rd/export/print_checklist1.php">
			<input type="hidden" id="mother_data" name="mother_data" value="" />
			<input type="hidden" id="baby_data" name="baby_data" value="" />
			<input type="hidden" id="breastfeed_data" name="breastfeed_data" value="" />
			<input type="hidden" id="card_data" name="card_data" value="" />
			<input type="hidden" id="other_data" name="other_data" value="" />
			<div id="todo_data"></div>
			<div id="contact_block">
				<input type="hidden" id="hospital_num" name="hospital_num" value="0" />
				<input type="hidden" id="postpartum_num" name="postpartum_num" value="0" />
				<input type="hidden" id="postpartum_diet_num" name="postpartum_diet_num" value="0" />
				<input type="hidden" id="cord_blood_num" name="cord_blood_num" value="0" />
				<input type="hidden" id="important_num" name="important_num" value="0" />
				<input type="hidden" id="other_contact_num" name="other_contact_num" value="0" />
				<div id="contact_data">
				</div>
			</div>
			<input type="hidden" id="mother_name" name="mother_name" value="<?php echo $app->getUserState("form.checklist.mother"); ?>" />
			<input type="hidden" id="date" name="date" value="<?php echo $app->getUserState("form.checklist.date"); ?>" />
			<input type="hidden" id="check_list" name="check_list" value="0" />
			<input type="hidden" id="check_contact" name="check_contact" value="0" />
			<div class="submit_block"><input type="button" id="return_btn" value="上一步" onclick="javascript:history.go(-1)" /></div>
			<div class="submit_block"><input type="button" id="submit_btn" value="下載清單" /></div>
		</form>			
	<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_58.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><span style="font-size: 12pt;"><a href="index.php?option=com_content&view=article&id=34&catid=2&Itemid=145" style="color: #333333;">寶寶誰照顧？ 給新手父母的重點筆記。</a></span>
				</td>
				<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" />
				</td>
			</tr>
		</tbody>
	</table>
	<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_52.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><span style="font-size: 12pt;"><a href="index.php?option=com_content&view=article&id=53&catid=2&Itemid=145" style="color: #333333;">善用家庭資源，擬定育兒計畫</a></span>
				</td>
				<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" />
				</td>
			</tr>
		</tbody>
	</table>
			
	</div>
	
	<!-- 媽媽用品 -->
	<div style='display:none' id='mother_list'>
		<div class="add_item" title="新增媽媽用品"><span class="icon-save-new"></span> (點擊新增媽媽用品)</div>
		<div class="list_block"><input type="checkbox" id="mlist_1" name="mother[]" value="看護墊" /> 看護墊</div>
		<div class="list_block"><input type="checkbox" id="mlist_2" name="mother[]" value="產褥墊" /> 產褥墊</div>
		<div class="list_block"><input type="checkbox" id="mlist_3" name="mother[]" value="生理沖洗瓶(自然產)" /> 生理沖洗瓶(自然產)</div>
		<div class="list_block"><input type="checkbox" id="mlist_4" name="mother[]" value="產後坐浴盆(自然產)" /> 產後坐浴盆(自然產)</div>
		<div class="list_block"><input type="checkbox" id="mlist_5" name="mother[]" value="束腹帶" /> 束腹帶</div>
		<div class="list_block"><input type="checkbox" id="mlist_6" name="mother[]" value="保養品" /> 保養品</div>
		<div class="list_block"><input type="checkbox" id="mlist_7" name="mother[]" value="羊脂糕" /> 羊脂糕</div>
		<div class="list_block"><input type="checkbox" id="mlist_8" name="mother[]" value="免洗內褲" /> 免洗內褲</div>
		<div class="list_block"><input type="checkbox" id="mlist_9" name="mother[]" value="有帽外套" /> 有帽外套</div>
		<div class="list_block"><input type="checkbox" id="mlist_10" name="mother[]" value="外出服" /> 外出服</div>
		<div class="list_block"><input type="checkbox" id="mlist_11" name="mother[]" value="襪子" /> 襪子</div>
	</div>
	
	<!-- 寶寶用品 -->
	<div style="display:none" id="baby_list">
		<div class="add_item" title="新增寶寶用品"><span class="icon-save-new"></span> (點擊新增待寶寶用品)</div>
		<div class="list_block"><input type="checkbox" id="blist_1" name="baby[]" value="包巾" /> 包巾</div>
		<div class="list_block"><input type="checkbox" id="blist_2" name="baby[]" value="連身紗布衣" /> 連身紗布衣</div>
		<div class="list_block"><input type="checkbox" id="blist_3" name="baby[]" value="帽子" /> 帽子</div>
		<div class="list_block"><input type="checkbox" id="blist_4" name="baby[]" value="尿布" /> 尿布</div>
		<div class="list_block"><input type="checkbox" id="blist_5" name="baby[]" value="手套" /> 手套</div>
		<div class="list_block"><input type="checkbox" id="blist_6" name="baby[]" value="襪子" /> 襪子</div>
		<div class="list_block"><input type="checkbox" id="blist_7" name="baby[]" value="濕紙巾" /> 濕紙巾</div>
	</div>
	
	<!-- 哺乳用品 -->
	<div style="display:none" id="breastfeed_list">
		<div class="add_item" title="新增哺乳用品"><span class="icon-save-new"></span> (點擊新增哺乳用品)</div>
		<div class="list_block"><input type="checkbox" id="bflist_1" name="breastfeed[]" value="防溢奶墊" /> 防溢奶墊</div>
		<div class="list_block"><input type="checkbox" id="bflist_2" name="breastfeed[]" value="吸奶器" /> 吸奶器</div>
		<div class="list_block"><input type="checkbox" id="bflist_3" name="breastfeed[]" value="奶粉" /> 奶粉</div>
		<div class="list_block"><input type="checkbox" id="bflist_4" name="breastfeed[]" value="奶瓶" /> 奶瓶</div>
		<div class="list_block"><input type="checkbox" id="bflist_5" name="breastfeed[]" value="母乳冷凍袋" /> 母乳冷凍袋</div>
		<div class="list_block"><input type="checkbox" id="bflist_6" name="breastfeed[]" value="哺孕睡衣" /> 哺孕睡衣</div>
		<div class="list_block"><input type="checkbox" id="bflist_7" name="breastfeed[]" value="哺孕內衣" /> 哺孕內衣</div>
	</div>

	<!-- 證件 -->
	<div style="display:none" id="card_list">
		<div class="add_item" title="新增證件項目"><span class="icon-save-new"></span> (點擊新增證件項目)</div>
		<div class="list_block"><input type="checkbox" id="clist_1" name="card[]" value="身分證(夫妻)" /> 身分證(夫妻)</div>
		<div class="list_block"><input type="checkbox" id="clist_2" name="card[]" value="健保卡(夫妻)" /> 健保卡(夫妻)</div>
		<div class="list_block"><input type="checkbox" id="clist_3" name="card[]" value="媽媽手冊" /> 媽媽手冊</div>
	</div>	
	
	<!-- 其他 -->
	<div style="display:none" id="other_list">
		<div class="add_item" title="新增其他用品"><span class="icon-save-new"></span> (點擊新增其他用品)</div>
		<div class="list_block"><input type="checkbox" id="olist_1" name="other[]" value="衛生紙" /> 衛生紙</div>
		<div class="list_block"><input type="checkbox" id="olist_2" name="other[]" value="數位相機、記憶卡" /> 數位相機、記憶卡</div>
		<div class="list_block"><input type="checkbox" id="olist_3" name="other[]" value="充電器(手機、相機)" /> 充電器(手機、相機)</div>
		<div class="list_block"><input type="checkbox" id="olist_4" name="other[]" value="保溫杯" /> 保溫杯</div>
		<div class="list_block"><input type="checkbox" id="olist_5" name="other[]" value="盥洗用具(刷牙、沐浴使用)" /> 盥洗用具(刷牙、沐浴使用)</div>
		<div class="list_block"><input type="checkbox" id="olist_6" name="other[]" value="毛巾" /> 毛巾</div>
		<div class="list_block"><input type="checkbox" id="olist_7" name="other[]" value="拖鞋" /> 拖鞋</div>
	</div>
	
	<!-- 待辦事項 -->
	<div style="display: none;" id="todo_list">
		<div class="add_todo_item" title="新增待辦事項"><span class="icon-save-new"></span> (點擊新增待辦事項)</div>
	</div>
	
	<!-- 重要聯絡資訊 -->
	<div style="display: none;" id="contact_list">
		<div class="list_block">
			<div class="contact_cat" id="hospital">• 待產醫院</div>
			<div class="add_contact_item" title="新增待產醫院相關聯絡資訊"><span class="icon-save-new"></span></div>
		</div>
		<div class="list_block">
			<div class="contact_cat" id="postpartum">• 月子中心</div>
			<div class="add_contact_item" title="新增月子中心相關聯絡資訊"><span class="icon-save-new"></span></div>
		</div>
		<div class="list_block">
			<div class="contact_cat" id="postpartum_diet">• 月子餐</div>
			<div class="add_contact_item" title="新增月子餐相關聯絡資訊"><span class="icon-save-new"></span></div>
		</div>
		<div class="list_block">
			<div class="contact_cat" id="cord_blood">• 臍帶血</div>
			<div class="add_contact_item" title="新增臍帶血相關聯絡資訊"><span class="icon-save-new"></span></div>
		</div>
		<div class="list_block">
			<div class="contact_cat" id="important">• 重要聯絡人</div>
			<div class="add_contact_item" title="新增重要聯絡人資訊"><span class="icon-save-new"></span></div>
		</div>
		<div class="list_block">
			<div class="contact_cat" id="other_contact">• 其他</div>
			<div class="add_contact_item" title="新增其他聯絡資訊"><span class="icon-save-new"></span></div>
		</div>
	</div>
	
	<div style="display: none" id="contact_profile">
		<table class="datatable">			
			<tbody>
			<tr>
				<td>姓名</td>
				<td><input type="text" id="name" name="name" value="" /></td>
			</tr>
			<tr>
				<td>電話：</td>
				<td><input type="text" id="tel" name="tel" value="" /></td>
			</tr>
			<tr>
				<td>E-mail：</td>
				<td><input type="text" id="email" name="email" value="" /></td>
			</tr>
			<tr>
				<td>其他資訊：</td>
				<td><textarea id="other_text" name="other_text"></textarea></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<script language="JavaScript">
(function($) {
	$(document).ready(function() {
		// 媽媽用品
        $("#mother").bind('click',function(){
			$("#mother_list").dialog({
		        title: '媽媽用品',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var mother = new Array();
						$("input[name='mother[]']:checked").each(function() {
							mother.push(jQuery(this).val());
						});
						$("#mother_data").val(mother);			
		                $(this).dialog('close');
		            }
		        }
			});	
        });
        
        // 寶寶用品
        $("#baby").bind('click', function() {
			$("#baby_list").dialog({
		        title: '寶寶用品',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var baby = new Array();
						$("input[name='baby[]']:checked").each(function() {
							baby.push(jQuery(this).val());
						});
						$("#baby_data").val(baby);			
		                $(this).dialog('close');
		            }
		        }
			});
        });
        
        // 哺乳用品
        $("#breastfeed").bind('click', function() {
			$("#breastfeed_list").dialog({
		        title: '哺乳用品',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var breastfeed = new Array();
						$("input[name='breastfeed[]']:checked").each(function() {
							breastfeed.push(jQuery(this).val());
						});
						$("#breastfeed_data").val(breastfeed);			
		                $(this).dialog('close');
		            }
		        }
			});
        });   

        // 證件
        $("#card").bind('click', function() {
			$("#card_list").dialog({
		        title: '證件',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var card = new Array();
						$("input[name='card[]']:checked").each(function() {
							card.push(jQuery(this).val());
						});
						$("#card_data").val(card);			
		                $(this).dialog('close');
		            }
		        }
			});
        });   

         // 其他
        $("#other").bind('click', function() {
			$("#other_list").dialog({
		        title: '其他',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var other = new Array();
						$("input[name='other[]']:checked").each(function() {
							other.push(jQuery(this).val());
						});
						$("#other_data").val(other);			
		                $(this).dialog('close');
		            }
		        }
			});
        });  

         // 待辦事項
        $("#todo").bind('click', function() {
			$("#todo_list").dialog({
		        title: '待辦事項',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var todo = $("#todo_data");
		            	todo.html("");
						$("input[name='todo[]']:checked").each(function() {
							var value = jQuery(this).val();
							todo.append("<input type='hidden' name='todo_data[]' value='"+value+"' />");
						});			
		                $(this).dialog('close');
		            }
		        }
			});
        });  

         // 重要聯絡資訊
        $("#contact").bind('click', function() {
			$("#contact_list").dialog({
		        title: '重要聯絡資訊',
		        bgiframe: true,
		        width: 300,
		        height: 350,
		        modal: true,
		        draggable: true,
		        resizable: true,		
		        buttons: {
		            '確定': function() {
		            	var contact = $("#contact_data");
		            	contact.html("");
						
						// 待產醫院
						var hospital_num = $("#hospital .items_block").length;
						if(hospital_num > 0) {
							for(var h=1 ; h<= hospital_num ; h++) {
								$("input[name='hospital_"+h+"[]']:checked").each(function() {
									$(".hospital_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='hospital_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#hospital_num").val(hospital_num);
						}

						// 月子中心
						var postpartum_num = $("#postpartum .items_block").length;
						if(postpartum_num > 0) {
							for(var h=1 ; h<= postpartum_num ; h++) {
								$("input[name='postpartum_"+h+"[]']:checked").each(function() {
									$(".postpartum_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='postpartum_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#postpartum_num").val(postpartum_num);
						}
						
						// 月子餐
						var postpartum_diet_num = $("#postpartum_diet .items_block").length;
						if(postpartum_diet_num > 0) {
							for(var h=1 ; h<= postpartum_diet_num ; h++) {
								$("input[name='postpartum_diet_"+h+"[]']:checked").each(function() {
									$(".postpartum_diet_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='postpartum_diet_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#postpartum_diet_num").val(postpartum_diet_num);
						}						
						
						// 臍帶血
						var cord_blood_num = $("#cord_blood .items_block").length;
						if(cord_blood_num > 0) {
							for(var h=1 ; h<= cord_blood_num ; h++) {
								$("input[name='cord_blood_"+h+"[]']:checked").each(function() {
									$(".cord_blood_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='cord_blood_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#cord_blood_num").val(cord_blood_num);
						}
							
						// 重要聯絡人
						var important_num = $("#important .items_block").length;
						if(important_num > 0) {
							for(var h=1 ; h<= important_num ; h++) {
								$("input[name='important_"+h+"[]']:checked").each(function() {
									$(".important_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='important_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#important_num").val(important_num);
						}
						
						// 其他
						var other_contact_num = $("#other_contact .items_block").length;
						if(other_contact_num > 0) {
							for(var h=1 ; h<= other_contact_num ; h++) {
								$("input[name='other_contact_"+h+"[]']:checked").each(function() {
									$(".other_contact_"+h).each(function() {
										var value = $(this).val();
										contact.append("<input type='hidden' name='other_contact_"+h+"[]' value='"+value+"' />");
									});
								});		
							}
							$("#other_contact_num").val(other_contact_num);
						}						

		                $(this).dialog('close');
		            }
		        }
			});
        }); 
        
		// 新增物品         
		$(".add_item span").bind('click',function(){
        	var item = prompt("請輸入要新增的物品");
         	var list = $(this).parent().parent();
         	var num = $("#" +list.prop("id") + " .list_block").length + 1;
         	
         	if(item != null) {
	         	if(list.prop("id") == "mother_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='mlist_"+num+"' name='mother[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "baby_list"){
	         		list.append("<div class='list_block'><input type='checkbox' id='blist_"+num+"' name='baby[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "breastfeed_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='bflist_"+num+"' name='breastfeed[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "card_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='clist_"+num+"' name='card[]' value='"+item+"' /> "+item+"</div>");
	         	}else if(list.prop("id") == "other_list") {
	         		list.append("<div class='list_block'><input type='checkbox' id='olist_"+num+"' name='other[]' value='"+item+"' /> "+item+"</div>");
	         	}         		
         	}         	
		});
		
		// 新增待辦事項
		$(".add_todo_item span").bind('click', function() {
			var item = prompt("請輸入要新增的待辦事項");
			var list = $(this).parent().parent();
			var num = $("#" +list.prop("id") + " .list_block").length + 1;
			
			if(list.prop("id") == "todo_list" && item != null) {
         		list.append("<div class='list_block'><input type='checkbox' id='tlist_"+num+"' name='todo[]' value='"+item+"' /> "+item+"</div>");
         	}
		});
		
		// 新增聯絡資訊
		$(".add_contact_item span").bind('click', function() {
			var list = $(this).parent().parent().parent();
			var block = $(this).parent().parent();
			var items = block.children(".contact_cat");			
			var num = $("#" +items.prop("id") + " .items_block").length + 1;

			if(list.prop("id") == "contact_list") {
				
				$("#contact_profile").dialog({
			        title: '聯絡資訊',
			        bgiframe: true,
			        width: 350,
			        height: 320,
			        modal: true,
			        draggable: true,
			        resizable: false,
			        buttons: {
			            '確定': function() {
			            	var name = $("#name");
			            	var tel = $("#tel");
			            	var email = $("#email");
			            	var other_text = $("#other_text");			
							
							if(!name.val()) {
								alert("請輸入聯絡姓名");
								return false;
							}else{
								var add_name = "<input type='checkbox' class='"+items.prop("id")+"_"+num+"' name='"+items.prop("id")+"_"+num+"[]' value='"+name.val()+"' /> "+name.val();
								var add_tel = "<input type='hidden' class='"+items.prop("id")+"_"+num+"' name='"+items.prop("id")+"_"+num+"[]' value='"+tel.val()+"' />";
								var add_email = "<input type='hidden' class='"+items.prop("id")+"_"+num+"' name='"+items.prop("id")+"_"+num+"[]' value='"+email.val()+"' />";
								var add_other_text = "<input type='hidden' class='"+items.prop("id")+"_"+num+"' name='"+items.prop("id")+"_"+num+"[]' value='"+other_text.val()+"' />";
								items.append("<div class='items_block'>" + add_name + add_tel + add_email + add_other_text + "</div>");
							}
							
							name.val("");
							tel.val("");
							email.val("");
							other_text.val("");
			                $(this).dialog('close');
			            },
			            '取消': function() {	
			                $(this).dialog('close');
			            }
			        }
				});
         	}
		});
		
		// submit
		$(document).on("click", "#submit_btn", function() {
			var check_list = 0;
			var check_contact = 0;
			
			if($("#mother_data").val()) {
				check_list = 1;
			}else if($("#baby_data").val()) {
				check_list = 1;
			}else if($("#breastfeed_data").val()) {
				check_list = 1;
			}else if($("#card_data").val()) {
				check_list = 1;
			}else if($("#other_data").val()) {
				check_list = 1;
			}else if($("#todo_data input").length > 0) {
				check_list = 1;
			}
			$("#check_list").val(check_list);
			
			if($("#hospital_num").val() != 0) {
				check_contact = 1;
			}else if($("#postpartum_num").val() != 0) {
				check_contact = 1;
			}else if($("#postpartum_diet_num").val() != 0) {
				check_contact = 1;
			}else if($("#cord_blood_num").val() != 0) {
				check_contact = 1;
			}else if($("#important_num").val() != 0) {
				check_contact = 1;
			}else if($("#other_contact_num").val() != 0) {
				check_contact = 1;
			}
			$("#check_contact").val(check_contact);
			
			if(check_list == 1 || check_contact == 1) {
				$("#output").submit();
			}else{
				alert("請至少勾選一項目");
				return false;
			}
			
			
		});
	});
})(jQuery);
</script>
