<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	(function($) {
		$(document).ready(function(){
			$("#date").datepicker({
				dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				dayNamesMin:["日","一","二","三","四","五","六"],
				monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				prevText:"上月",
				nextText:"次月",
				weekHeader:"週",
				changeYear: true,
				showMonthAfterYear:true,
				dateFormat:"yy-mm-dd",
				
				onSelect: function(dateText, inst) {
					var date = $(this).val();
			        var url = "<?php echo JRoute::_(JURI::base()."components/com_checklist/assets/js/getDate.php ", false); ?>" ;
    			    jQuery.post(url, { date: date }, function(data) {
            			$(".weeks").html("(懷孕週數：" + data + "週)");
        			});
				}
			});
		});
		
		// submit
		$(document).on("click", "#submit_btn", function() {
			var name = $("#name").val();
			var baby = $("#date").val();
			
			if(!name) {
				alert("請填寫媽媽姓名");
				return false;	
			}else if(!baby) {
				alert("請填寫寶寶的預產期");
				return false;
			}else{
				$("#profile_form").submit();
			}			
		});	
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_ONE"); ?>
		</div>
	</div>
	
		<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				懷孕足月(37週)之後，隨時都有可能出現產兆(落紅、破水、陣痛)，需要立即前往醫院待產，因此懷孕7~8個月左右，就可以開始陸續準備待產包了，準爸媽可以一起準備待產及坐月子時所需要的用品，方便待產時快速攜帶，減少手忙腳亂的慌張喔!!
			</p>
		</div>
	</div>
	
	<div class="checklist_block">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item&layout=checklist&Itemid={$itemid}", false); ?>">
			<table align="center" class="datatable">
				<thead>
				<tr>
					<th colspan="2">填寫您的基本資料</th>
				</tr>
				</thead>
				
				<tbody>
				<tr>
					<td>媽媽姓名：</td>
					<td><input type="text" id="name" name="name" value="" /></td>
				</tr>	
				<tr>
					<td>寶寶的預產期：</td>
					<td>
						<input type="type" id="date" name="date" value="" />
						<div class="weeks"></div>
					</td>
				</tr>
				</tbody>
				
				<tfoot>
				<tr>
					<td colspan="2" style="text-align: right;">
						<div class="submit_block"><input type="button" id="submit_btn" value="下一步" style="width: 155px;" /></div>
					</td>
				</tr>
				</tfoot>
			</table>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_checklist/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
