<?php
/**
 * @version     1.0.0
 * @package     com_checklist
 * @copyright   Efatek Inc. Copyright (C) 2015. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <rene_chen@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class ChecklistControllerItem2 extends ChecklistController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'List', $prefix = 'Checklist')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}