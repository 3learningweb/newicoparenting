<?php
	define( '_JEXEC', 1 );
	define('JPATH_BASE', '../../../../');
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	//require_once ( JPATH_BASE .DS.'libraries'.DS.'phputf8'.DS.'utf8.php' );
	
	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();
	
	$post = JRequest::get( 'post' );
	$now = strtotime(date("Y-m-d"));
	$end = strtotime($post['date']);
	$start = strtotime("-40 week", $end);
	$weeks = ceil(($now - $start) / 604800);
	
	echo $weeks;

?>