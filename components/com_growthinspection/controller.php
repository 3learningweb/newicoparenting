<?php

/**
 * @version		: controller.php 2015-09-21 10:06:39$
 * @author		EFATEK 
 * @package		Growthinspection
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * GROWTHINSPECTION Component Controller
 */
class GrowthInspectionController extends JControllerLegacy {

	
}