<?php

/**
 * @version		: game.php 2015-07-07 05:06:09$
 * @author		efatek 
 * @package		com_growthinspection
 * @copyright	Copyright (C) 2015- efatek. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modellist');

class GrowthinspectionModelGame extends JModelList {

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$id	= $app->input->getInt('id');
		$this->setState('item.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$limit	= $app->input->getInt('limit',  $app->getCfg('list_limit', 0));
		$this->setState('list.limit', $limit);

		$limitstart	= $app->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);


	}
	
	function getQuiz(){
		$app = JFactory::getApplication();
		$start_date = $app->input->getString('love_date');
		$start_date = ($start_date) ? $start_date : date("Y-m-d");
		$diff_days = ceil((time() - strtotime($start_date)) / 86400);
		if($diff_days<=30){						//1個月
			$baby_icon = "baby01";
			$age = "<span>0</span>個月~<span>1</span>個月";
			$quiz1=array('俯臥時頭稍可抬起','會反射性抓住放入手中之物');
			$quiz2=array('聽到聲音會轉頭');
			$quiz3=array('注意別人的臉');
		}
		if($diff_days>30 & $diff_days<=60){		//2個月
			$baby_icon = "baby01";
			$age = "<span>1</span>個月~<span>3</span>個月";
			$quiz1=array('俯臥時頭抬起45度','眼睛隨物可轉動90度以上');
			$quiz2=array('發出各種無意義聲音');
			$quiz3=array('逗他會微笑');
		}
		if($diff_days>60 & $diff_days<=90){		//3個月
			$baby_icon = "baby01";
			$age = "<span>1</span>個月~<span>3</span>個月";
			$quiz1=array('俯臥時頭抬起90度','雙手可移至胸前接觸');
			$quiz2=array('發出等ㄚ、ㄨ等牙牙學語聲','笑出聲音');
			$quiz3=array('會自動對人笑');
		}
		if($diff_days>90 & $diff_days<=120){	//4個月
			$baby_icon = "baby02";
			$age = "<span>4</span>個月~<span>6</span>個月";
			$quiz1=array('協助坐起時頭可以固定','側躺','可將手抓住的物品送入嘴巴');
			$quiz2=array('偶爾模仿大人的聲調');
			$quiz3=array('會注意其他孩子的存在');
		}
		if($diff_days>120 & $diff_days<=150){	//5個月
			$baby_icon = "baby02";
			$age = "<span>4</span>個月~<span>6</span>個月";
			$quiz1=array('拉小孩坐起，他會稍用力配合頭不會後仰','兩手各可抓緊小物品');
			$quiz2=array('會因為高興而尖叫');
			$quiz3=array();
		}
		if($diff_days>150 & $diff_days<=180){	//6個月
			$baby_icon = "baby02";
			$age = "<span>4</span>個月~<span>6</span>個月";
			$quiz1=array('完全會翻身','坐著用雙手可支撐30秒','手會去玩弄繫在玩具上的繩','會敲打玩具');
			$quiz2=array('開始出現母音ㄚ、ㄧ、ㄨ');
			$quiz3=array('自己會拿餅乾吃');
		}
		if($diff_days>180 & $diff_days<=210){	//7個月
			$baby_icon = "baby03";
			$age = "<span>6</span>個月~<span>9</span>個月";
			$quiz1=array('肚子觸地式爬行','抱起會在大人腿上亂跳','坐起時手會各拿一塊積木','將積木從一手移到另一手');
			$quiz2=array('正確轉向聲源');
			$quiz3=array('會設法取較遠處的玩具');
		}
		if($diff_days>210 & $diff_days<=240){	//8個月
			$baby_icon = "baby03";
			$age = "<span>6</span>個月~<span>9</span>個月";
			$quiz1=array('坐得很好','雙膝爬行','手像耙子一樣抓東西');
			$quiz2=array('發出ㄅㄚ、ㄇㄚ、ㄌㄚ聲','注意聽熟悉聲');
			$quiz3=array('會玩躲貓貓');
		}
		if($diff_days>240 & $diff_days<=270){	//9個月
			$baby_icon = "baby03";
			$age = "<span>6</span>個月~<span>9</span>個月";
			$quiz1=array('扶持東西可維持站的姿勢','可前進後退爬行','以拇指合併四指鉗物','以食指觸碰或推東西');
			$quiz2=array('會隨著大人的手或眼神注視某樣東西');
			$quiz3=array('看到陌生人會哭');
		}
		if($diff_days>270 & $diff_days<=300){	//10個月
			$baby_icon = "baby04";
			$age = "<span>10</span>個月~<span>12</span>個月";
			$quiz1=array('扶東西邊緣會移步','站著時會想辦法坐下','會把小東西放下杯子或容器中');
			$quiz2=array('會揮手表示拜拜','知道別人的名字');
			$quiz3=array('以手指出要去的地方或東西');
		}
		if($diff_days>300 & $diff_days<=330){	//11個月
			$baby_icon = "baby04";
			$age = "<span>10</span>個月~<span>12</span>個月";
			$quiz1=array('獨立站10秒','拉著一手可以走','拍手','雙手各拿一塊積木互相敲打');
			$quiz2=array('模仿大人說話聲','對叫自己名字有反應');
			$quiz3=array('會抓住湯匙','可拉下頭上的帽子');
		}
		if($diff_days>300 & $diff_days<=365){	//12個月
			$baby_icon = "baby04";
			$age = "<span>10</span>個月~<span>12</span>個月";
			$quiz1=array('單獨走幾步','蹲著可以站起來','以姆指和食指間拿東西');
			$quiz2=array('有意義地叫爸爸、媽媽','知道別人的名字');
			$quiz3=array('以手指出要去的地方或東西');
		}
		if($diff_days>365 & $diff_days<=420){	//12~14個月
			$baby_icon = "baby05";
			$age = "<span>1</span>歲~<span>1</span>歲<span>6</span>個月";
			$quiz1=array('可維持跪姿','會側行數步','走得很穩，會轉身','一隻手同時撿起兩個小東西','可重疊兩塊積木','可將瓶中物倒出');
			$quiz2=array('模仿未聽過的音','會用一些單字','知道大部份物品名稱','熟悉且位置固定的東西不見了會拉');
			$quiz3=array('堅持要自己吃東西','模仿成人簡單動作如打人、抱哄洋娃娃','會脫襪子','嘗試自己穿鞋，但不一定能穿好');
		}
		if($diff_days>420 & $diff_days<=480){	//14~16個月
			$baby_icon = "baby05";
			$age = "<span>1</span>歲~<span>1</span>歲<span>6</span>個月";
			$quiz1=array('可獨自趴著而手扶地站起','隨音樂而做簡單跳舞動作','扶欄杆可上下三層樓梯','會打開盒蓋','自動拿筆亂塗','已固定較喜歡用那邊');
			$quiz2=array('會說10個單字','會說一些兩個字的名詞','在要求下，會指出熟悉的東西','會遵從簡單的指示');
			$quiz3=array('睡覺時要抱心愛的玩具或衣物','出去散步時，會注意路上各種東西','自己拿杯子喝水','自己用湯匙進食(會灑出)');
		}
		if($diff_days>480 & $diff_days<=630){	//16~21個月
			$baby_icon = "baby05";
			$age = "<span>1</span>歲~<span>1<span>歲</span>6</span>個月";
			$quiz1=array('自己坐上嬰兒椅','扶著可單腳站立','一腳站立，另一腳踢大球','可疊三塊積木','模仿畫直線','可認出圓形，並放入模型板上');
			$quiz2=array('會哼哼唱唱','至少會用10個單字','瞭解一般動作如「親親」「抱抱」');
			$quiz3=array('被欺侮時會設法抵抗或還手','有能力主動拒絕別人的命令','會表示尿片濕了或大便了','午睡不尿床');
		}
		if($diff_days>630 & $diff_days<=730){	//21~24個月
			$baby_icon = "baby06";
			$age = "<span>1</span>歲<span>6</span>個月~<span>2</span>歲";
			$quiz1=array('自己單獨上、下椅子','原地雙腳離地跳躍','腳著地方式帶動小三輪車','球丟給他，他會去捕捉','可一頁一頁翻厚書','疊高6至7個積木');
			$quiz2=array('會重複字句的最後一兩個字','會講50個字彙','知道玩伴的名字','認得出電視上常見之物');
			$quiz3=array('幫忙做一些簡單家事','會咒罵玩伴、玩具','脫下未扣扣子的外套','會用語言或姿勢表示要尿尿或大便');
		}
		if($diff_days>730 & $diff_days<=810){	//24~27個月
			$baby_icon = "baby07";
			$age = "<span>2</span>歲~<span>2</span>歲<span>6</span>個月";
			$quiz1=array('用整個腳掌跑步並可避開障礙物','可倒退走10公尺','不扶物、可單腳站1秒以上','模仿畫橫線','可依樣用三塊積木排直線','可一頁一頁翻簿書');
			$quiz2=array('懂得簡單數量(多、少)所有權(誰的)地點(裡面、上面)的觀念','稍微有一點「過去」的概念','瞭解「上、下、裡面、旁邊...」位置概念','知道在什麼場合通常做什麼事');
			$quiz3=array('會去幫助別人','會和其他孩子合作，做一件事或造一件東西','在幫忙下，會用肥皂洗手，並擦乾');
		}
		if($diff_days>810 & $diff_days<=930){	//27~31個月
			$baby_icon = "baby07";
			$age = "<span>2</span>歲~<span>2</span>歲<span>6</span>個月";
			$quiz1=array('雙腳較遠距離跳躍，向前翻斛斗','單腳可跳躍兩次以上','疊高八塊積木','會用打蛋器','玩黏土時，會給自己成品命名');
			$quiz2=array('會問「誰」「哪裡」「做什麼」...句子','會用「這個」「那個」...冠詞','知道「明天」意味著不是「現在」','會回答「誰在做什麼」的問句');
			$quiz3=array('對幼小的孩子會保護','會告狀，白天可控制大、小便','會拉下褲子，準備大、小便');
		}
		if($diff_days>930 & $diff_days<=1095){	//31~36個月
			$baby_icon = "baby08";
			$age = "<span>2</span>歲<span>7</span>個月~<span>3</span>歲";
			$quiz1=array('一腳一階上下樓梯','單腳可平衡站立','會騎小三輪車','會過肩投球','模仿畫圓形','用小剪刀，不一定剪得好');
			$quiz2=array('會正確使用「我們」「你們」「他們」','會用「什麼」「怎麼會」「如果」「因為」「但是」','會回答有關位置，所有權及數量的問話','會接熟悉的語句或故事');
			$quiz3=array('會找藉口以逃避責罰','自己能去鄰居小朋友家玩，自行大、小便','能解開一個或一個以上之鈕扣');
		}
		if($diff_days>1095 & $diff_days<=1278){			//三歲至三歲六個月
			$baby_icon = "baby09";
			$age = "<span>3</span>歲~<span>3</span>歲<span>6</span>個月";
			$quiz1=array('走路時兩手交互擺動','可繞障礙物跑過去','丟球可丟10呎遠','想辦法用手臂接球','單腳站立5秒','會蓋、開小罐子','可完成◇菱形圖的連連看','模依畫十字');
			$quiz2=array('會用否定命令句如不要做','會用「這是...」來表達','會用「什麼時候...」的問句','了解「大」「小」「上」「下」「前」「後」「裡」「外」','能回答「這是誰的?」「為什麼」...等問題');
			$quiz3=array('會道歉，做錯事會說「對不起」','已有一個要好的同伴','會給小朋友一些暗示','從小壺倒水喝，不會潑得到處都是','自己脫衣服','晚上不會尿床');
		}
		if($diff_days>1278 & $diff_days<=1460){			//三歲六個月至四歲
			$baby_icon = "baby10";
			$age = "<span>3</span>歲<span>6</span>個月~<span>4</span>歲";
			$quiz1=array('可接住反彈球','以腳趾接腳跟向前走直線','原地單腳跳','自己畫十字形','模仿畫╳形');
			$quiz2=array('可解釋簡單圖畫','圖畫字彙至少可說出14種或以上','能回答「有多少」「多久」的問題','瞭解昨天、今天');
			$quiz3=array('會與其他小孩在遊戲中比賽','能自己過斑馬線或過街','會穿長統鞋子','自己洗臉、刷牙(但洗得還不好)');
		}
		if($diff_days>1460 & $diff_days<=1643){			//四歲至四歲六個月
			$baby_icon = "baby11";
			$age = "<span>4</span>歲~<span>4</span>歲<span>6</span>個月";
			$quiz1=array('以單腳向前跳','向上攀、爬垂直的階梯','過肩丟球12呎','單腳站立超過10秒','照樣寫自己名字','25秒中可將10個珠子放入瓶中','用剪刀剪直線','跟著摺紙');
			$quiz2=array('正確使用「為什麼」','為引起別人注意會用誇張的語調及簡單語句','至少能唱完一首完整的歌','會用「XX和XX」「靠近XX在XX旁邊」','能回答「有多少」「多久」的問題','瞭解昨天、今天');
			$quiz3=array('會與其他小孩在遊戲中比賽','能自己過斑馬線或過街','會穿長統鞋子','自己洗臉、刷牙(但洗得還不好)');
		}
		if($diff_days>1643 & $diff_days<=1825){			//四歲六個月至五歲
			$baby_icon = "baby12";
			$age = "<span>4</span>歲<span>6</span>個月~<span>5</span>歲";
			$quiz1=array('單腳連續向前跳2至碼','騎三輪車繞過障礙物','雙腳跳在5秒內可跳7至8次','會寫自己名字','會畫□但還畫不好','用剪刀剪曲線','會用繩索打結、繫鞋帶','會扣及解扣子','能畫出身體三個部份');
			$quiz2=array('會用「一個XX」','會說出相反詞三種中對兩種','會由1數到10或以上','會懂得「多加一點」或「減少一點」','會在要求中指出一系列東西中第幾個是哪一個');
			$quiz3=array('會同情、安慰同伴(用語言)','和同伴計畫將來玩什麼','會穿襪子','扣襯子、褲子或外套扣子','晚上睡醒會自己上廁所');
		}
		if($diff_days>1825 & $diff_days<=2008){			//五歲至五歲六個月
			$baby_icon = "baby13";
			$age = "<span>5</span>歲~<span>5</span>歲<span>6</span>個月";
			$quiz1=array('腳尖平衡站立10秒','用雙手接住反彈的乒乓球','主動且有技巧地攀爬、滑、溜、搖擺','自己會寫一些字','20秒中可將10個珠子放入瓶中','會寫1至5的數字','會畫△');
			$quiz2=array('可說出物體的用途，如「帽子是戴在頭上」','會說出六個單子的意思','會區分「最接近」「最遠」「整個」「一半」','依要求能正確找出1至10所要的數字');
			$quiz3=array('在有些遊戲中有些性別區分了','會選擇要好的朋友','遊戲中會遵守公平及規則','自己換上睡衣或脫下衣服','能將食物組合在一起如三明治');
		}
		if($diff_days>2008 & $diff_days<=2190){		//五歲六個月至六歲
			$baby_icon = "baby14";
			$age = "<span>5</span>歲<span>6</span>個月~<span>6</span>歲";
			$quiz1=array('有韻律地雙腳交換跳躍如跳繩','跑得很好','可接住丟來的球(5吋大)(用手接)','用腳趾接腳跟倒退走直線','以拇指有順序觸碰其他四指','將鞋子、鞋帶穿好','能畫身體六個部份');
			$quiz2=array('能很流利地表達','可經由點數而區分兩堆東西是不是一樣多','瞭解「以前」「以後」','區分左、右','能認得一些注音符號及國字');
			$quiz3=array('會玩簡單桌上遊戲和撲克牌','和同伴分享秘密(不告訴大人)','會用刀子切東西','自己會梳或刷頭髮','自己繫鞋帶');
		}
		
		if($diff_days > 2190) {
			$quiz = 0;
		}else{
			$quiz=array($quiz1,$quiz2,$quiz3, $baby_icon, $age);
		}

		return $quiz;
	}
}
