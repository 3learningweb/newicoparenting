<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		richgame
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<!-- fancybox -->
<script type="text/javascript" src="components/com_richgame/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_richgame/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_richgame/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".playgame").attr("disabled", true);
		
		jQuery(".level").on("click", function() {
			var choose = jQuery('input[name=level]:checked').val();
			if(choose) {
				jQuery(".playgame").attr("disabled", false);
			}
		});

		jQuery(".playgame").fancybox({
			
		});
	});
</script>

<div class="com_richgame">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>

	<div class="hint_msg" style="display: none;">此頁面請切換為電腦版，即可正常使用功能</div>

	<div class="intro_text">
		<?php echo JText::_("COM_RICHGAME_INTRO_TEXT"); ?> <br/>
		<div class="richgame_level">
			<input type="radio" name="level" value="1" id="level_1" class="level" /> 幼稚園（簡單）<br/>
			<input type="radio" name="level" value="2" id="level_2" class="level" /> 國中（普通）<br/>
			<input type="radio" name="level" value="3" id="level_3" class="level" /> 高中（困難）<br/>
		</div>
	</div>
	
	<!-- <div class="intro_image">
		<img id="rich_intro_image" src="components/com_richgame/assets/images/intro_image.png" alt="<?php echo $menu_title; ?>" />
	</div>	 -->
	
	<div class="start">
		<div class="submit_block">
			<input type="button" id="submit_btn" class="playgame" value="開始遊戲" href="<?php echo JURI::base()."components/com_richgame/game.html?level=0"; ?>" />
		</div>
	</div>	
</div>	
		
