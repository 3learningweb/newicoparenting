/**
*1.加选择
*2.加用户不点击时的倒计时，时间
*3.
*/



var game = {
	go: function (userId, step) {
		ui['user' + userId].locked = true;
		engine.go(userId, step);
	},
	confirm: function (userId) {
		console.log('game.confirm', userId);
		engine.confirm(userId);
	},
	cancel: function (userId) {
		console.log('game.cancel', userId);
		engine.cancel(userId);
	},
	back: function (userId) { // rene
		console.log('game.back', userId);
		engine.back(userId);
	},
	block: function (userId) {
		console.log('game.block', userId);
		engine.block(userId);
	},
	chance: function () {  // rene
		console.log('game.chance', userId);
	},
	certificate: function () {  // rene
		console.log('game.certificate', userId);
	},
	gameover: function () {  // rene
		console.log('game.gameover', userId);
	}
};

var ui = {
	user1: {
		balance: $("#player1 .money"),
		notice: $("#player1 .normal-notice"),
		confirm: $("#player1 .confirm-notice"),
		chance: $("#player1 .chance-notice"), // rene
		certificate: $("#player1 .certificate-notice"), // rene
		gameover: $("#player1 .gameover-notice"), // rene
		blockNumber: $("#block-number-1"),
		locked: false,
		num: 0,
		level: 0,
		pre_rand: 0
	},
	user2: {
		balance: $("#player2 .money"),
		notice: $("#player2 .normal-notice"),
		confirm: $("#player2 .confirm-notice"),
		blockNumber: $("#block-number-2"),
		locked: false
	}
};

function ExtraView(){
}

ExtraView.prototype.addPerson  = function(userId){
	if(!ui['user' + userId]) {
		alert('目前只支持添加两个用户！');
	}
	
	// 路障  rene
	// $("#action-panel-" + userId).delegate('.blocker', 'click', function(){
		// game.block(userId);
		// ui['user' + userId].locked = true;
	// })
	
	$("#action-panel-" + userId).delegate('.saizi', 'click', function(){
		var life = jQuery(".blocker").length;
		if(life != 0) {
			if (ui['user' + userId].locked === true) {
				return;
			}
			var rand_num = Math.floor(Math.random() * 6) + 1  // 亂數擲骰
			var pre_num = ui['user' + userId].pre_rand;
			while(rand_num == pre_num) {  // 若和上一次的數字相同，骰子不會有動畫效果
				rand_num = Math.floor(Math.random() * 6) + 1
			}
			ui['user' + userId].pre_rand = rand_num;
			jQuery(".saizi").css("background-image", "url(components/com_richgame/assets/images/saizi.png)");
			jQuery(".saizi").css("background-image", "url(components/com_richgame/assets/images/saizi_"+rand_num+".gif)");
			setTimeout(function () { 
        		game.go(userId, rand_num);
		    }, 1000);		
			
			ui['user' + userId].locked = true;
		}else{  // rene gameover 不能骰
			ui['user' + userId].gameover.end().show();			
		}
	})
	
	ui['user' + userId].confirm.delegate('.confirm-btn' ,'click',function(){
		game.confirm(userId);
		ui['user' + userId].confirm.hide();
	})
	
	ui['user' + userId].confirm.delegate('.cancel-btn' ,'click',function(){
		game.cancel(userId);
		ui['user' + userId].confirm.hide();
	})
	
	ui['user' + userId].confirm.delegate('.radio_btn' ,'click',function(){
		game.cancel(userId);
		ui['user' + userId].confirm.hide();
		
		var option = jQuery(this).attr("value");
		var value = ui['user' + userId].confirm.find('.tip').attr("value")
		var valueStr = '';
		if(option == value) {
			valueStr = "答對了";
			ui['user' + userId].notice.html(valueStr).show().removeClass('notice-hidden');
		}else{
			valueStr = "答錯了"; 
			ui['user' + userId].notice.html(valueStr).show().removeClass('notice-hidden');
			jQuery(".blocker").eq(0).remove();
			
			var life = jQuery(".blocker").length;
			if(life == 0) {
				ui['user' + userId].notice.hide();
				ui['user' + userId].gameover.find('.tip').end().show();
			}else{
				game.back(userId);
			}
		}
	})

	// 點擊確定
	ui['user' + userId].chance.delegate('#chance_btn' , 'click', function() {
		ui['user' + userId].chance.hide();
		game.go(userId, ui['user' + userId].num);
		ui['user' + userId].locked = true;
	});
	
	// 重新挑戰
	ui['user' + userId].certificate.delegate('#again', 'click', function() {
		location.reload();
	});
	
	ui['user' + userId].gameover.delegate('#again', 'click', function() {
		location.reload();
	});
};

ExtraView.prototype.setBalance  = function(userId, balance){
	ui['user' + userId].balance.html(balance);
};

ExtraView.prototype.confirmSpend  = function(userId, mapIndex, type, price){
	ui['user' + userId].locked = true;
	var level = jQuery('input[name=level]:checked').val();
	var confirmStr = '';
	var url = "components/com_richgame/assets/ajax/getQuestion.php";
	if(type === "buy") {
		// rene 取得題目
		jQuery.post(url, { level: level, mapIndex: mapIndex }, function(data) {
			var item = JSON.parse(data);
			confirmStr = item.question;
			
			jQuery(".option_btn").each(function() {
				jQuery(this).hide();
			});
			
			var i = 0;
			for(var key in item.options) {
				i++;
				ui['user' + userId].confirm.find('#btn'+i+" span").html(item.options[key]);
				ui['user' + userId].confirm.find('#btn'+i+" input").attr('checked', false);
				ui['user' + userId].confirm.find('#btn'+i+" input").attr("value", key);
				ui['user' + userId].confirm.find('#btn'+i).show();
			}
			
			if(i > 2) {
				jQuery(".qtype").html("選擇題");
			}else{
				jQuery(".qtype").html("是非題");
			}
			
			ui['user' + userId].confirm.find('.tip').html(confirmStr).end().show();
			ui['user' + userId].confirm.find('.tip').attr("value", item.answer);
		});
	}
	
	// if(type === "upgrade") {
		// confirmStr = "只需要" + price +  "大洋就可以升级您的土地，要升级么？";	
		// ui['user' + userId].confirm.find('.tip').html(confirmStr).end().show();
	// }
};

// rene
ExtraView.prototype.chance = function(userId, num, count) {
	var msg = '';
	ui['user' + userId].num = num;
	// 前進
	msg = "恭喜您，直接前進"+num+"步";
	
	setTimeout(function(){
		ui['user' + userId].chance.find(".tip").html(msg).end().show();
	}, 250 * count);
}

//rene
ExtraView.prototype.endmsg = function(userId) {
	var level = jQuery('input[name=level]:checked').val();
	var src = "";
	
	if(level == 1) {
		src = "diploma_1.png";
	}else if(level == 2) {
		src = "diploma_2.png";
	}else if(level == 3) {
		src = "diploma_3.png";
	}
	
	jQuery(".saizi").hide();
	ui['user' + userId].notice.hide();
	jQuery(".certificate-notice img").attr("src", "components/com_richgame/assets/images/"+src);
	jQuery(".certificate-notice img").attr("href", "components/com_richgame/assets/images/" +src);
	jQuery(".certificate-notice a").attr("href", "components/com_richgame/assets/images/" +src);
	ui['user' + userId].certificate.find(".tip").end().show();
	
}


ExtraView.prototype.setBlockNumber  = function(userId, blockNumber){
	$("#action-panel-" + userId + " .blocker").each(function(index){
		if((index+blockNumber) < 3){
			$(this).addClass('block-item-hidden');
		}
	});
};

ExtraView.prototype.confirm  = function(){
};

ExtraView.prototype.lock  = function(userId){
	ui['user' + userId].locked = true;
};

ExtraView.prototype.unlock  = function(userId){
	console.log('unlock', userId);
	ui['user' + userId].locked = false;
};

ExtraView.prototype.Notice  = function(userId, notice, count){
	console.log('Notice', notice);
	ui['user' + userId].notice.html(notice).show().removeClass('notice-hidden');
	
	if(ui['user' + userId].noticeTimer) {
		clearTimeout(ui['user' + userId].noticeTimer);
		ui['user' + userId].noticeTimer = null;
	}
	
	ui['user' + userId].noticeTimer = setTimeout(function(){
		ui['user' + userId].notice.html(notice).addClass('notice-hidden');
		
		setTimeout(function(){
			ui['user' + userId].notice.hide();
		},300)
		
	}, 250 * count)
};

var extraView = {
	addPerson: function (user) {
		// console.log('extra.addperson', user);
	},
	setBalance: function (user, balance) {
		// console.log('extra.setbalance', user, balance);
		ui['user' + user].balance.text(balance);
	},
	setBlockNumber: function (user, blockNumber) {
		// console.log('extra.setblocknumber', user, blockNumber);
		ui['user' + user].blockNumber.text(blockNumber);
	},
	confirm: function (user, type, price) {
		// console.log('extra.confirm', user, type, price);
		var tips = '你确定花' + price + '元' + (type == 'buy' ? '购买这片土地' : '在这里加盖房屋') + '吗？';//加选择
		ui['user' + user].confirm.text(tips);
	},
	unlock: function (user) {
		// console.log('extra.unlock', user);
		ui['user' + user].locked.text(false);
	}
};


$(window).keydown(function (event) {
	// A|up: go
	// S|down: block
	// D|left: confirm
	// F|right: cancel
	
	event.preventDefault();
	
	var keyCode = event.keyCode;
	var userId = 1;
	if(keyCode == 65 || keyCode == 83 || keyCode == 68 || keyCode == 70) {
		userId = 1;
	}
	
	if(keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40) {
		userId = 2;
	}
	
	if (ui['user' + userId].locked === true) {
		return;
	}
	// 65 83 68 70
	// 38 40 37 39
	switch (event.keyCode) {
		case 65:
		game.go(userId, Math.floor(Math.random() * 6) + 1);
		break;
		case 83:
		game.block(userId);
		break;
		case 68:
		game.confirm(userId);
		break;
		case 70:
		game.cancel(userId);
		break;

		case 38:
		game.go(userId, Math.floor(Math.random() * 6) + 1);
		break;
		case 40:
		game.block(userId);
		break;
		case 37:
		game.confirm(userId);
		break;
		case 39:
		game.cancel(userId);
		break;

		default:
		return;
	}
	ui['user' + userId].locked == true;
});

function userTimer (user) {
  var maxtime = 15 //一个小时，按秒计算，自己调整!  
function CountDown(){  
	if(maxtime>=0){  
		minutes = Math.floor(maxtime/60);  
		seconds = Math.floor(maxtime%60);  
		msg = "还有"+seconds+"秒";  
		document.all["timer"].innerHTML=msg;  
		--maxtime;  
	}else{  
		clearInterval(timer);  
		alert("时间到，结束!"); 
		ui['user' + user].balance.text(balance); 
	}  
}  
timer = setInterval("CountDown()",1000);
}