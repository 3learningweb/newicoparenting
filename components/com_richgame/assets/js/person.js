if(!window.CONFIG) {
	CONFIG = {};
}

CONFIG.gridSize = 100*SCALE;
CONFIG.roleSize = 100;

//计算头像的偏移值
var roleOffsetX = (CONFIG.roleSize - CONFIG.gridSize)/2;
var roleOffsetY = CONFIG.roleSize - CONFIG.gridSize;

var roleIndex = 1;

function RoleBase(){
	//基本属性
	this.name = "";
	this.age = 10;
	
	//人物特殊特性
	this.talent = 10;
	this.lucky = 20;
	
	
	//人物装备
	this.props = [];
		
	this._init()
};

/**
 ** 初始化用户头像节点，并插入DOM
 **/
RoleBase.prototype._init = function(position){
	
	this.roleImage = $('<div class="role-image" style="background:url(components/com_richgame/assets/images/role0' + roleIndex + '.gif) no-repeat 0 0;"></div>');
	
	this.roleImage.appendTo('#richgame_block');
	
	roleIndex++;
};

/**
 ** 设置用户位置索引，以便计算
 **/
RoleBase.prototype.setPosIndex = function(mapIndex){	
	this.posIndex = mapIndex;
};

/**
 ** 设置用户头像位置
 **/
RoleBase.prototype.setPosition = function(position){

	this.roleImage.css({
		left: (position.x - roleOffsetX) + "px",
		top : (position.y - roleOffsetY) + "px"
	});
};

function PersonView(mapView){
	
	//错位位移
	this.offsetx = 0;
	
	//用户角色实例列表
	this.roleList = {};
	
	this.mapView = mapView;
	
	// rene 紀錄上一步的index
	this.perIndex = 0;
	// rene 紀錄這一步的index
	this.lastIndex = 0;
}

/**
 ** 初始化一个用户角色，并设置坐标位置
 **/
PersonView.prototype.init = function(userId, mapIndex) {
	
	var role, 
		position;
	
	if(!this.roleList[userId]) {
		console.log('create role');
		this.roleList[userId] = new RoleBase();
	}
	role = this.roleList[userId];
	position = this.mapView.getMapByIndex(mapIndex);

	if(this._checkPositionDouble(userId, mapIndex) === true) {
		position.x = position.x - this.offsetx;
	}
	role.setPosIndex(mapIndex);
	role.setPosition(position);
};

/**
 ** 检查某坐标点是否有
 **/
PersonView.prototype._checkPositionDouble = function(userId, mapIndex) {
	
	var role, 
		position;
	
	for(var roleId in this.roleList) {
		if(roleId !== userId) {
			role = this.roleList[roleId];
			console.log(parseInt(role.posIndex, 10) === parseInt(mapIndex, 10))
			if(parseInt(role.posIndex, 10) === parseInt(mapIndex, 10)) {
				return true;
			}
		}
	}
	
	return false;
};


/**
 ** 移动角色到某个位置
 **/
PersonView.prototype.move = function(userId, mapIndex){
	// posIndex: 起點 ; mapIndex: 終點 ; mapPositionArr.length: 移動步數

	var mapPositionArr = [],
		role = this.roleList[userId],
		Map = this.mapView,
		posIndex,
		position;
	
	if(!role) {
		console.log('不存在该用户角色');
	}
	
	posIndex = role.posIndex;
	
	
	// rene 紀錄還未開始走的index, 這裡存在preIndex中
	preIndex = role.posIndex;
	
	if( (mapIndex < posIndex) && (posIndex - mapIndex) < 4 ) {
		console.log('位置信息不正确');
	}
	
	role.setPosIndex(mapIndex);
	
	//计算用户头像移动路径坐标数组
	if(mapIndex > posIndex) {
		var i = posIndex+1;
		for(; i <= mapIndex; i++) {
			mapPositionArr.push(Map.getMapByIndex(i));
		}
	} else {
		for(var j = (posIndex+1); j <= 19; j++) { // rene 格字增加數字也要增加
			mapPositionArr.push(Map.getMapByIndex(j));
		}
		// for(var k = 0; k <= mapIndex; k++) {
			// mapPositionArr.push(Map.getMapByIndex(k));
		// }
	}

	if(this._checkPositionDouble(userId, mapIndex) === true) {
		mapPositionArr[mapPositionArr.length-1].x = mapPositionArr[mapPositionArr.length-1].x - this.offsetx;
	}
	
	//让用户头像按路径位置移动
	for(var p = 0; p < mapPositionArr.length;p++) {		
		//定时设置用户头像位置，保证动画进行
		(function(arrIndex, position){
			setTimeout(function(){
				role.setPosition(position);
			}, arrIndex*250);
		})(p, mapPositionArr[p]);
		
	}

	// rene 紀錄目前走到的位置
	lastIndex = role.posIndex;
};


// rene 直接回到初始位置
// preIndex 上一次的位置
// lastIndex 目前的位置
PersonView.prototype.goback = function(userId) {
	
	var mapPositionArr = [];
	var role = this.roleList[userId];
	var position;
	var Map = this.mapView;
	
	role.setPosIndex(preIndex);
	position = this.mapView.getMapByIndex(preIndex);
	
	// 計算用戶路徑移動座標
	var i = lastIndex;
	for(i ; i>=preIndex ; i--) {
		mapPositionArr.push(Map.getMapByIndex(i));
	}
	
	if(this._checkPositionDouble(userId, preIndex) === true) {
		mapPositionArr[mapPositionArr.length-1].x = mapPositionArr[mapPositionArr.length-1].x - this.offsetx;
	}
	
	// 讓用戶按照位置移動
	for(var p=0 ; p<mapPositionArr.length ; p++) {
		//定时设置用户头像位置，保证动画进行
		(function(arrIndex, position){
			setTimeout(function(){
				role.setPosition(position);
			}, arrIndex*250);
		})(p, mapPositionArr[p]);
	}
	
	role.setPosition(position);
	
}

// rene 直接回到初始位置
PersonView.prototype.getIndex = function(userId) {
	return preIndex;
}

