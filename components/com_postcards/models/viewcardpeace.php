<?php

/**
 * @version		: list.php 2012-12-17 05:06:09$
 * @author		efatek 
 * @package		com_cinema
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class PostcardsModelViewcardpeace extends JModelList {

	public function getImage() {
		$app = JFactory::getApplication();

		$image_id = $app->getUserState('form.postcards.image_id', '');
		
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__postcards') . ' AS a');
		$query->where('id = '. (int) $image_id);
        		$db->setQuery($query);

		return $db->loadObject();
	}



	
}
