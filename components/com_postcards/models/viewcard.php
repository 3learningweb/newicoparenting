<?php

/**
 * @version		: list.php 2012-12-17 05:06:09$
 * @author		efatek 
 * @package		com_cinema
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modellist');

class PostcardsModelViewcard extends JModelList {

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$id	= $app->input->getInt('id');
		$this->setState('item.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$limit	= $app->input->getInt('limit',  $app->getCfg('list_limit', 0));
		$this->setState('list.limit', $limit);

		$limitstart	= $app->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);


	}

	public function getListQuery() {
		
	}


	public function getImage() {
		$app = JFactory::getApplication();

		$image_id = $app->getUserState('form.postcards.image_id', '');
		
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__postcards') . ' AS a');
		$query->where('id = '. (int) $image_id);
        		$db->setQuery($query);

		return $db->loadObject();

	}



	
}
