<?php
/**
 * @version     1.0.0
 * @package     com_cinema
 * @copyright   Efatek Inc. Copyright (C) 2012. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <sam@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class PostcardsControllerViewcardpeace extends PostcardsController
{
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    
    public function getModel($name = 'viewcardpeace', $prefix = '', $config = array('ignore_request' => true)) {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }

    /* 將中文句子固定某個位置跳行 */
    function utf8_wordwrap($str, $width, $break, $cut = false) { 
        if (!$cut) {
            $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.',}\b#U';
        } else {
            $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.'}#';
        }
        if (function_exists('mb_strlen')) {
            $str_len = mb_strlen($str,'UTF-8');
        } else {
            $str_len = preg_match_all('/[\x00-\x7F\xC0-\xFD]/', $str, $var_empty);
        }
        $while_what = ceil($str_len / $width);
        $i = 1;
        $return = '';
        while ($i < $while_what) {
            preg_match($regexp, $str,$matches);
            $string = $matches[0];
            $return .= $string.$break;
            $str = substr($str, strlen($string));
            $i++;
        }
        return $return.$str;
    }


    public function check() {
        $app = JFactory::getApplication();
        $jinput = $app->input;
        $Itemid = $jinput->getInt('Itemid');
        $post = $jinput->getArray($_POST);


        // $post["content"] = JFilterOutput::cleanText($post["content"]);
        // $app->setUserState('form.postcards.content', $post["content"]);

        $msg = '';
        // if ($post["content"] == "") {
        //     $msg = JText::_('COM_POSTCARDS_FORM_REUIRED');
        // } else {
        //     if (utf8_strlen($post["content"]) > 50) {
        //         $msg .= JText::_('COM_POSTCARDS_FORM_CONTENT_LONG');
        //     }
        // }

        if($msg != "") { //代表有錯！
            $this->setRedirect("index.php?option=com_postcards&view=viewcardpeace&Itemid={$Itemid}", $msg);
            return;
        } else {
            $menu = $app->getMenu();
            $par = $menu->getParams( $Itemid );

            $model = $this->getModel();
            $image = $model->getImage();

            if ($par->get('card_type') == "sticker") {

                $pic1 = $jinput->files->get('uploadphoto1');
                $pic2 = $jinput->files->get('uploadphoto2');
                $new_img = time(). ".jpg"; // 新的照片
                $bg_img = "tmp/". $new_img;  //處理後檔案要放的位置！


                $receiver = $app->getUserStateFromRequest("form.postcards.receiver", 'receiver', '', 'string');
                $inputtext1 = $app->getUserStateFromRequest("form.postcards.inputtext1", 'inputtext1', '', 'string');
                $inputtext2 = $app->getUserStateFromRequest("form.postcards.inputtext2", 'inputtext2', '', 'string');
                $inputtext3 = $app->getUserStateFromRequest("form.postcards.inputtext3", 'inputtext3', '', 'string');
                $inputtext4 = $app->getUserStateFromRequest("form.postcards.inputtext4", 'inputtext4', '', 'string');
                $sender = $app->getUserStateFromRequest("form.postcards.sender", 'sender', '', 'string');

                
                //判斷第一張圖 第一張圖一定要有！！
                if(!empty($pic1["tmp_name"])) { 
                    // 判斷是否為jpg，若否則轉換
                    $img_type = exif_imagetype($pic1["tmp_name"]);
                    if ($img_type == 1) { //gif
                        $im = @imagecreatefromgif ($pic1["tmp_name"]);
                        imagejpeg($im, $pic1["tmp_name"], 100);
                    }

                    if ($img_type == 3) { //png 
                        $im = @imagecreatefrompng ($pic1["tmp_name"]);
                        imagejpeg($im, $pic1["tmp_name"], 100);
                    }

                    $new_img = "local_". uniqid(). ".jpg"; 
                    $new_img = "tmp/". $new_img;

                    //記錄下來 選了哪張照片
                    $app->setUserState("form.postcards.uploadphoto1", $new_img);
                    move_uploaded_file($pic1["tmp_name"], $new_img);

                    

                }

                //判斷第二張圖 （可有可無）
                if(!empty($pic2["tmp_name"])) {

                    // 判斷是否為jpg，若否則轉換
                    $img_type = exif_imagetype($pic2["tmp_name"]);
                    if ($img_type == 1) { //gif
                        $im = @imagecreatefromgif ($pic2["tmp_name"]);
                        imagejpeg($im, $pic2["tmp_name"], 100);
                    }

                    if ($img_type == 3) { //png 
                        $im = @imagecreatefrompng ($pic2["tmp_name"]);
                        imagejpeg($im, $pic2["tmp_name"], 100);
                    }

                    $new_img = "local_". uniqid(). ".jpg"; 
                    $new_img = "tmp/". $new_img;

                    //記錄下來 選了哪張照片
                    $app->setUserState("form.postcards.uploadphoto2", $new_img);
                    move_uploaded_file($pic2["tmp_name"], $new_img);
                }



                /* 處理文字部分 */
                $inputtext4 = str_replace("\r\n",'', $inputtext4); //把換行符號取代成\\n
                if($image->msg_pos_x > 150)  { //判斷橫式還是直式
                    $inputtext4 = $this->utf8_wordwrap($inputtext4, 20, "\\n", true);  //在寬度20處跳行
                    $all_text = "親愛的{$receiver}：\\n\\n關於「{$inputtext1}」這件事，\\n其實我只是{$inputtext2}，\\n希望你能{$inputtext3}我。\\n{$inputtext4}\\n\\n\\n                           愛你的{$sender}";

                    $app->setUserState("form.postcards.horizontal", false); //直式
                } else  {
                    //整理文字
                    $inputtext4 = $this->utf8_wordwrap($inputtext4, 34, "\\n", true);
                    $all_text = "親愛的{$receiver}：\\n關於「{$inputtext1}」這件事，\\n其實我只是{$inputtext2}，\\n希望你能{$inputtext3}我。\\n{$inputtext4}\\n\\n                                                                               愛你的{$sender}";
                    $app->setUserState("form.postcards.horizontal", true); //橫式
                }

                $app->setUserState("form.postcards.content", $all_text); //將整理好的文字 放在session
                

            } else {
                $bg_img = $image->image;
            }

            $app->setUserState('form.postcards.bg_img', $bg_img);  //最後處理完畢的照片放在這邊！ 透過session傳遞！

            //$link = "index.php?option=com_postcards&view=viewcardpeace&Itemid={$Itemid}";
            //$this->setRedirect($link);
            $view = $this->getView( 'viewcardpeace', 'html' );
            $model = $this->getModel();

            // Push the model into the view (as default).
            $view->setModel($model, true);  //重要！  若沒有這樣做 ， 去到 view.html.php 那邊 會沒有model 可以抓取

            // Push document object into the view.
            //$view->document = $document;

            $view->display();

        }

        

    }



    // 合併
    // ( 底圖  欲合併的圖檔  欲合併的文字  小圖左上角座標 寬度 高度 儲存路徑 儲存檔名  文字坐標)
    function mergeImg($background_img, $pic_img, $content, $position_x, $position_y, $pic_width, $pic_height, $save_path, $new_img, $msg_pos_x, $msg_pos_y) {

        $font  = JPATH_SITE. "/components/com_postcards/assets/wqy-zenhei.ttc"; // 字型
        $alpha = 100;               //浮水印透明度
        $markerImg_width = $pic_width;      // 小圖-寬
        $markerImg_height = $pic_height;    // 小圖-高



        //得到底圖info
        $dst_im = imagecreatefromjpeg($background_img);


        // 取得上傳的圖像
        $src_img_filename = pathinfo($pic_img, PATHINFO_BASENAME);

        
        // 進行縮圖
        $this->thumbnailImg($pic_img, $save_path, $src_img_filename, $markerImg_width, $markerImg_height);

        $thumbnail_src = $save_path. "/". $src_img_filename;
        $src_im = imagecreatefromjpeg($thumbnail_src);
        $size = getimagesize($thumbnail_src);   // 取得縮圖後的大小

        if ($size[0] > $markerImg_width) {
            $size_width = $markerImg_width;
        } else {
            $size_width = $size[0];
        }

        if ($size[1] > $markerImg_height) {
            $size_height = $markerImg_height;
        } else {
            $size_height = $size[1];
        }

        //合並景點圖片
        imagecopymerge($dst_im, $src_im, $position_x, $position_y, 0, 0, $size_width, $size_height, $alpha);

        //合併文字
        $text_color = imagecolorallocate($dst_im, 0, 0, 0);  //設定文字顏色為黑色
        imagettftext($dst_im, 12, 0, $msg_pos_x, $msg_pos_y, $text_color, $font, $content);


        $filename = $save_path. "/". $new_img;  // 合併後的圖片檔名

        //輸出合並後水印圖片
        if (imagejpeg($dst_im, $filename)) {
            imagedestroy($dst_im);
            imagedestroy($src_im);

            unlink($thumbnail_src);

            return true;
        } else {
            return false;
        }


    }

    /*** 縮圖 ***/
    function thumbnailImg($filename, $dest_path, $dest_filename, $small_w = 300, $small_h = 300) {
        // 取得上傳圖片
        $src = imagecreatefromjpeg($filename);

        // 取得來源圖片長寬
        $src_w = imagesx($src);
        $src_h = imagesy($src);

        // 儲存縮圖到指定 thumb 目錄
        if (!is_dir ($dest_path)) {
            @mkdir($dest_path, 0755);
        }

        // 查看原始圖檔是否達到指定大小，未達到，則以原檔為新圖
        if ( ($src_w < $small_w) && ($src_h < $small_h)) {
            copy($filename, $dest_path. "/". $dest_filename);
            return;
        }

        // 假設要長寬不超過指定大小
        if ($src_w > $src_h) {
            $thumb_w = $small_w;
            $thumb_h = intval($src_h / $src_w * $small_w);
        } else {
            $thumb_h = $small_h;
            $thumb_w = intval($src_w / $src_h * $small_h);
        }

        // 建立縮圖
        $thumb = imagecreatetruecolor($thumb_w, $thumb_h);

        // 開始縮圖
        imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);


        imagejpeg($thumb, $dest_path. "/". $dest_filename);

    }


    public function send() {
        $app = JFactory::getApplication();
        $jinput = $app->input;
        $post = $jinput->getArray($_POST);

        $Itemid = $jinput->getInt('Itemid');

        $post["subject"] = JFilterOutput::cleanText($post["subject"]);
        $app->setUserState('form.postcards.friend_email', $post["friend_email"]);
        $app->setUserState('form.postcards.name', $post["name"]);
        $app->setUserState('form.postcards.email', $post["email"]);
        $app->setUserState('form.postcards.subject', $post["subject"]);

        $msg = '';
        if ( $post["friend_email"] == "" || $post["name"] == "" || $post["email"] == "" || $post["subject"] == "" ) {
            $msg = JText::_('COM_POSTCARDS_FORM_REUIRED');
        } else {
            $reg = "/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/";
            if(!preg_match($reg, $post["friend_email"]) || !preg_match($reg, $post["email"])) {
                $msg = JText::_('COM_POSTCARDS_FORM_EMAIL');
            }


        }

        if($msg != "") {
            $this->setRedirect("index.php?option=com_postcards&view=viewcardpeace&Itemid={$Itemid}", $msg);

            return;
        } else {
            $menu = $app->getMenu();
            $par = $menu->getParams( $Itemid );

            $sitedomain = substr_replace(JURI::root(), '', -1, 1);

            $html = array ();
            $html [] = '<div style="width: 95%; margin: 0 auto; background: #d3ece4; border: 1px solid #78b5a2; padding: 10px;">';
            $html [] = "<p>". sprintf(JText::_('COM_POSTCARDS_SEND_CONTENT'), $post["name"]). "</p><br>";
            if ($par->get('card_type') == "sticker") {
                $attachment = JPATH_SITE. "/tmp/". $post['final_photo'];
            } else {

                $html [] = '<div><img src="'. JURI::root() ."/tmp/" . $post['final_photo'] . '"></div>';
                $html [] = "<p>". $app->getUserState('form.postcards.content', ''). "</p>";
                $attachment = "";
            }


            $html [] = '<div style="text-align: right;"><a style="color: #265c4b; text-decoration: none;" href="' . JURI::root() . '">' . JText::_('COM_POSTCARDS_SEND_FROM_SITE') . '</a></div>';
            $html [] = '</div>';
            $body = implode ( '<br/>', $html );

            $mail = JFactory::getMailer();
             
            $mail->addRecipient($post['friend_email']);
            $mail->setSender(array($post['email'], $post["name"]));
            $mail->setSubject($post["subject"]);
            $mail->setBody($body);
            $mail->IsHTML(true);
            $mail->addAttachment($attachment);

            if ($mail->Send()) {
                $msg = JText::_('COM_POSTCARDS_SEND_OK');
                $app->setUserState('form.postcatds', null);
                
                if ($par->get('card_type') == "sticker") {
                    unlink($attachment);
                }

                $this->setRedirect(JRoute::_("index.php?option=com_postcards&view=intro&Itemid=". $Itemid, false), $msg);

                return;
            } else {
                $msg = JText::_('COM_POSTCARDS_SEND_ERROR');

                $this->setRedirect(JRoute::_("index.php?option=com_postcards&view=viewcardpeace&Itemid=".$Itemid, false), $msg);

                return;
            }

        

            
        }



    }


}
