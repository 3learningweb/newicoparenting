<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewImage extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		
		$config = JFactory::getConfig();
		$config->set('list_limit', 100);

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');
		$this->items		= $this->get('Items');


		// 清空所有的session
		$app->setUserState('form.postcards.image_id', null);
		$app->setUserState('form.postcards.content', null);
		$app->setUserState('form.postcards.friend_email', null);
		$app->setUserState('form.postcards.name', null);
		$app->setUserState('form.postcards.email', null);
		$app->setUserState('form.postcards.subject', null);


		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
