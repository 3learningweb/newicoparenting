<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>

<p style="font-size: 22px; font-weight: bolder; margin-bottom:40px;">話解衝突第二式</p>
<div class="com_postcards">
	<div class="message_form">
		<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=step3&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

			
			<p style="font-size: 16px; line-height: 28px;">親愛的 <?php echo $this->receiver; ?>：</p>
			<p style="font-size: 16px; line-height: 28px;">關於「<?php echo $this->inputtext1; ?>」這件事，</p>
			<p style="font-size: 16px; line-height: 28px;">其實，我只是感到<input type="text" id="inputtext2" name="inputtext2" value="<?php echo $this->inputtext2; ?>"></p>

			<br><br>
			<div class="explainText" style="border-left: 3px solid #009400; font-size: 16px; font-weight: bolder; padding-left:10px;">
			<p style="color:#009400; font-size: 20px;">分享內心的情緒感受</p>
<p style="color:#777;">憤怒經常只是一種「外顯行為」，大多源自我們內心的各種真實情緒，練習分析自己生氣時的真實情緒後，與對方分享，就能往前一步，不再生氣。
當我生氣，真實的情緒可能是……</p>

			</div>

			<p style="border: 3px solid #BCDB78; margin-top: 15px; padding: 20px; font-size: 16px; color: #777;">失望、委屈、憂慮、無奈、無助、孤單、自卑、丟臉、擔心、焦慮、掙扎、被控制、難過、沮喪、不知所措、害怕、不高興、被忽略、著急、被輕視、被遺棄、緊張、被勉強、被誤會………還有嗎?

			</p>

			
			<div class="submit submit_block" style="margin-top:60px; left:0; background: #4797B0; ">
				<a href="<?php echo JRoute::_('index.php?option=com_postcards&view=step1&Itemid='. (int) $itemid); ?>"><input id="submit_btn" type="button" value="上一步"/></a>
			</div>
			<div class="submit submit_block" style="margin-top:60px; position: absolute; right:0;">
				<input id="submit_btn" type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>"  />
			</div>
				
			</div>
		</form>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
		
	});

	
</script>