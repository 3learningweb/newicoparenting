<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewStep2 extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');

		
		//$this->receiver = $jinput->getString('receiver');

		$this->receiver = $app->getUserStateFromRequest("form.postcards.receiver", 'receiver', '', 'string');
		$this->inputtext1 = $app->getUserStateFromRequest("form.postcards.inputtext1", 'inputtext1', '', 'string');
		$this->inputtext2 = $app->getUserStateFromRequest("form.postcards.inputtext2", 'inputtext2', '', 'string');

                        //設定預設值
		if(!$this->inputtext2) $this->inputtext2 = "擔心又害怕";

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
