<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>

<style>
	
	div.com_postcards .img_block {
	    width: 210px;
	    border: 2px solid #C2DF86;
	    padding: 10px !important;
	    margin: 10px !important;
	}

	div.com_postcards .img_blocks .image img {
	    width: 210px;
	}

	form {
	    text-align: center;
	}

	.img_title {
	    text-align: left;
	}

	form .title {
	    right:10%;
	    position: relative;
	    font-size:16px;
	}

	.submit.submit_block {
	    margin-top:30px;
	    position: relative;
	    left: 21%;
	}


</style>
<div class="com_postcards">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	<div style="clear:both"></div>

<?php
if ($this->items) {
	?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=step1&Itemid='. (int) $itemid); ?>">
			<div class="title">
				<?php echo "首先，選一張足以「傳達心意」的圖片："; ?>
			</div>
			<div class="img_blocks">
	<?php
	$check = $app->getUserState('form.postcards.image_id', ''); //看以前有沒有選擇過卡片，若有的話 就會記錄使用者的選擇！
	foreach ($this->items as $i => $item) {
		if ($i == 0 && $check == 0) {
			$check = $item->id; //如果都沒有，就預設第一張？
		}
		?>
					<div class="img_block">
						<div class="image">
							<img alt="<?php echo $item->title; ?>" src="<?php echo JURI::root() . $item->image; ?>" />
						</div>
						<div class="img_title">
							<input type="radio" id="image_id_<?php echo $i; ?>" name="image_id" value="<?php echo $item->id; ?>" <?php echo ($check == $item->id) ? "checked" : ""; ?>>
							<label for="image_id_<?php echo $i; ?>"><?php echo $item->title; ?></label>
							
							
						</div>
						
					</div>
						<?php
					}
					?>
			</div>
			<div class="submit submit_block">
				<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
			</div>
		</form>
	<?php
} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

		<?php
	}
	?>


</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		

	});
</script>
