<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewStep1 extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');

		
		//$image_id = $jinput->getInt('image_id');  //必須把 intro 那邊設定的image_id傳遞下去（用session的方式）
		$this->image_id = $app->getUserStateFromRequest("form.postcards.image_id", 'image_id', '', 'int');
		$this->receiver = $app->getUserStateFromRequest("form.postcards.receiver", 'receiver', '', 'string');
		$this->inputtext1 = $app->getUserStateFromRequest("form.postcards.inputtext1", 'inputtext1', '', 'string');

                        //設定預設值
		if(!$this->receiver) $this->receiver = "大華";
		if(!$this->inputtext1) $this->inputtext1 = "孩子半夜哭鬧";

		//$this->image = $this->get('Image'); //在PostcardsModelForm 那邊執行$app->getUserState('form.postcards.image_id');  以這個值當作查詢的值 將卡片背景圖的檔名撈出來！

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
