<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>

<p style="font-size: 22px; font-weight: bolder; margin-bottom:40px;">話解衝突第一式</p>
<div class="com_postcards">
	<div class="message_form">
		<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=step2&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" style="position: relative;">

			
			<p style="font-size: 16px; line-height: 28px;">親愛的 <input type="text" id="receiver" name="receiver" value="<?php echo $this->receiver; ?>"  style="width:60px;">：</p>
			<p style="font-size: 16px; line-height: 28px;">關於「<input type="text" id="inputtext1" name="inputtext1" value="<?php echo $this->inputtext1; ?>">」這件事，</p>
				
			<br><br>

			<div class="explainText" style="border-left: 3px solid #009400; font-size: 16px; font-weight: bolder; padding-left:10px;"><p style="color:#009400; font-size: 20px;">用和緩的語氣重述情境</p><p style="color:#777;">練習用和緩的語氣，避免不相關或負向的語詞，描述在衝突原因發生時，自己看見的情境。</p>
			</div>

			
			<div class="submit submit_block" style="margin-top:60px; left:0; background: #4797B0; ">
				<!--input id="submit_btn" type="button" value="上一步"  onclick="window.history.go(-1); return false;"/-->
				<a href="<?php echo JRoute::_('index.php?option=com_postcards&view=cardselect&Itemid='. (int) $itemid); ?>"><input id="submit_btn" type="button" value="上一步"/></a>
			</div>
			<div class="submit submit_block" style="margin-top:60px; position: absolute; right:0;">
				<input id="submit_btn" type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>"  />
			</div>
		</form>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
		
	});

	
</script>