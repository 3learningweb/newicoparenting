<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewUploadphoto extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');



		$this->receiver = $app->getUserStateFromRequest("form.postcards.receiver", 'receiver', '', 'string');
		$this->inputtext1 = $app->getUserStateFromRequest("form.postcards.inputtext1", 'inputtext1', '', 'string');
		$this->inputtext2 = $app->getUserStateFromRequest("form.postcards.inputtext2", 'inputtext2', '', 'string');
		$this->inputtext3 = $app->getUserStateFromRequest("form.postcards.inputtext3", 'inputtext3', '', 'string');
		$this->inputtext4 = $app->getUserStateFromRequest("form.postcards.inputtext4", 'inputtext4', '', 'string');
		$this->sender = $app->getUserStateFromRequest("form.postcards.sender", 'sender', '', 'string');
		$this->image_id = $app->getUserStateFromRequest("form.postcards.image_id", 'image_id', '', 'int');

		$this->image = $this->get('Image'); //在PostcardsModelForm 那邊執行$app->getUserState('form.postcards.image_id');  以這個值當作查詢的值 將卡片背景圖的檔名撈出來！

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
