<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>

<p style="font-size: 22px; font-weight: bolder; margin-bottom:40px;">加入兩人的照片</p>
<div class="com_postcards">
	<div class="message_form">
		<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=viewcardpeace&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

			<div style="border: 3px solid #BCDB78; margin-top: 15px; padding: 20px; font-size: 16px; color: #777;">
			<p style="font-size: 16px; line-height: 28px;">親愛的 <span><?php echo $this->receiver; ?></span>：</p>
			<p style="font-size: 16px; line-height: 28px;">關於「<?php echo $this->inputtext1; ?>」這件事，</p>
			<p style="font-size: 16px; line-height: 28px;">其實，我只是感到<?php echo $this->inputtext2; ?></p>
			<p style="font-size: 16px; line-height: 28px;">希望你能<?php echo $this->inputtext3; ?>我。</p>
			<br>

			<p style="font-size: 16px; line-height: 28px;"><?php echo $this->inputtext4; ?></p>
			

			<br>
			<p style="font-size: 16px; line-height: 28px; text-align: right;">愛你的<?php echo $this->sender ?></p>
			</div>

			<br><br>

			<p style="font-size: 16px; line-height: 28px;">放1~2張兩人合照：</p>
			<input type="file"  id="uploadphoto1" name="uploadphoto1" accept="image/jpeg" style="width:30%"> <input type="file" id="uploadphoto2" name="uploadphoto2" accept="image/jpeg" style="width:50%">
			
			<p>(建議上傳最佳尺寸為<?php echo $this->image->pic_width; ?>x<?php echo $this->image->pic_height; ?> ，避免上傳圖片被裁切，影響圖片最佳顯示尺寸。)</p>

			<div class="submit submit_block" style="margin-top:60px; left:0; background: #4797B0; ">
				<a href="<?php echo JRoute::_('index.php?option=com_postcards&view=step3&Itemid='. (int) $itemid); ?>"><input id="submit_btn" type="button" value="上一步"/></a>
			</div>
			<div class="submit submit_block" style="margin-top:60px; position: absolute; right:0; margin-right:15px;">
				<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>" onClick="sendForm()" />
			</div>

				<input type="hidden" name="task" value="viewcardpeace.check" />
			</div>
		</form>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
		
	});

	function sendForm() {
		
		<?php if ($this->card_type == "sticker") { ?>
		if (jQuery("#pic").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_PIC'); ?>");
			return false;
		}
		<?php } ?>

		if (jQuery("#uploadphoto1").val() == "") {
			alert("<?php echo "請上傳照片唷！"; ?>");
			return false;
		}


		if(confirm("<?php echo JText::_('COM_POSTCARDS_FORM_CHECK'); ?>")) {
			document.getElementById("submitForm").submit();
		}
	}

	
</script>