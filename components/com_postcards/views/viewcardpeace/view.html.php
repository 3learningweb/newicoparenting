<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewViewcardpeace extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');
		

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');  //這邊很奇怪，從 controller那邊呼叫 $view->display() 到這邊 $this-state  會是null ，執行這行就會crash !!   答案是因為沒有push model into view的關係！

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');
		

		$this->bg_img = $app->getUserState('form.postcards.bg_img', '');  //這是處理完的結果！！

                        //底圖
                        $this->image_id = $app->getUserStateFromRequest("form.postcards.image_id", 'image_id', '', 'int');
                        $model = $this->getModel();
                        //$this->image = $model->getImage();

                        $this->image = $this->get('Image'); //背景圖

                        $this->pic1 = $app->getUserState("form.postcards.uploadphoto1", '');
                        $this->pic2 = $app->getUserState("form.postcards.uploadphoto2", '');

                        $this->content = $app->getUserState("form.postcards.content", '');


		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}


                        $document = JFactory::getDocument();
                        $document->addScript(JURI::root(true) . '/media/jui/js/jquery.min.js');
                        $document->addScript(JURI::root(true) . '/components/com_postcards/assets/fabric.touch.js');
                        $document->addScript(JURI::root(true) . '/components/com_postcards/assets/jscolor.min.js');
                        $document->addScript(JURI::root(true) . '/components/com_postcards/assets/FileSaver.min.js');
                        $document->addScript(JURI::root(true) . '/components/com_postcards/assets/canvas-to-blob.min.js');
		
		
		// Display the view
		parent::display($tpl);


	}


}
