<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid	= $app->input->getInt('Itemid');

?>


<style>
	.pull-right {
		padding-top: 15px;
		float:right;
		width:50%;
		position: relative;
	}

	.email_send {
		display: none;
		margin-top: 30px;
	}

	.form_table input {
		width:400px;
		
	}

	.pull-left {

		float:left;
		width:50%;
	}

	.finish {
		width:100%;
		border: 5px solid #C1DF86;
	}

	.submit_block {
	    background-color: #51BA68;
	    filter: progid: DXImageTransform.Microsoft.gradient(GradientType=0, startColorstr=#51BA68, endColorstr=#25933D);
	    background: linear-gradient(to bottom, #51BA68 0%, #25933D 100%);
	    right: 0;
	    position: absolute;
	}




</style>



<p style="font-size: 16px; padding:10px;">匯出圖片，email或傳訊息分享給對方，以圖傳情，話解衝突，溝通更無礙!</p>
<div class="com_postcards">
<?php
	if ($this->image) {
?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=email&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">

                                    <canvas id="card_canvas" width="640" height="540"></canvas>
                                    <span class="highlight">*</span><span style="font-weight: bold;">您可拖曳或縮放照片和文字，調整至您喜歡的風格。</span><br>

                                    <span class="highlight">*</span><span style="font-weight: bold;">您可修改：<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}" style="background-image: none; background-color: rgb(255, 232, 67); color: rgb(0, 0, 0);"> 文字顏色 </button>，文字字型：<select name="FontStyleNumber" id="FontStyleNumber">
                                      <option value="Times New Roman">Times New Roman</option>
                                      <option value="Arial">Arial</option>
                                      <option value="Georgia">Georgia</option>
                                      <option value="細明體">細明體</option>
                                      <option value="新細明體">新細明體</option>
                                      <option value="標楷體">標楷體</option>
                                      <option value="微軟正黑體">微軟正黑體</option>
                                      <option value="微軟雅黑體">微軟雅黑體</option>
                                    </select></span><br><br>


			<div class="pull-left">

			<div id="quiz_explanation" style="">
			    <div class="noteTableExplain" style="font-size: large; padding-top:15px; line-height: 30.3999996185303px;">
			        <table border="0" cellspacing="0" cellpadding="0">
			            <tbody>
			                <tr>
			                    <td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_58.png" alt="Speaker 52" width="49" height="44"></td>
			                    <td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><a href="#" target="_blank"><span style="font-size: 12pt;">認識情緒冰山</span></a></td>
			                    <td>
			                        <img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44">
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			    <div class="noteTableExplain" style="font-size: large;  line - height: 30.3999996185303 px;">
			        <table border="0" cellspacing="0" cellpadding="0">
			            <tbody>
			                <tr>
			                    <td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_76.png" alt="Speaker 52" width="49" height="44"></td>
			                    <td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><a href="#" target="_blank"><span style="font-size: 12pt;">衝突解決五步驟</span></a></td>
			                    <td>
			                        <img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44">
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			    <div class="noteTableExplain" style="font-size: large;  line - height: 30.3999996185303 px;">
			        <table border="0" cellspacing="0" cellpadding="0">
			            <tbody>
			                <tr>
			                    <td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_46.png" alt="Speaker 52" width="49" height="44"></td>
			                    <td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><a href="https://icoparenting.moe.edu.tw/%E7%88%B8%E5%AA%BD%E5%8A%A0%E6%B2%B9%E7%AB%99/%E5%8D%94%E5%8A%9B%E5%85%B1%E8%A6%AA%E8%81%B7" target="_blank"><span style="font-size: 12pt;">協力共親職</span></a></td>
			                    <td>
			                        <img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44">
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			</div>
	


			</div>

			<div class="pull-right">

				<div class="submit_block">
					<a href="<?php echo $this->bg_img; ?>"   download="card.jpg"><input type="button" id="download" value="下載圖片"></a>
				</div>
				<br><br>
				<div class="submit_block" style="background: #4797B0; margin-top: 30px;">
					<input type="button" id="email_btn" value="寄email">
				</div>

				

			</div>
			<div style="clear:both;"></div>

			<div class="email_send"> 
				<table class="form_table" width="100%">
					<tr>
						<td colspan="2">
							
						</td>
					</tr>
					<?php if ($this->card_type == "postcard") { ?>
					<tr>
						<td colspan="2">
							<?php echo $app->getUserState('form.postcards.content', ''); ?>
						</td>
					</tr>
					<?php } ?>

				
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_EMAIL'); ?></th>
						<td class="form_text"><input type="text" id="friend_email" name="friend_email" class="required" value="<?php echo $app->getUserState('form.postcards.friend_email', ''); ?>"></td>
					</tr>
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_NAME'); ?></th>
						<td class="form_text"><input type="text" name="name" class="required" value="<?php echo $app->getUserState('form.postcards.name', ''); ?>"></td>
					</tr>
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_EMAIL'); ?></th>
						<td class="form_text"><input type="text" id="email" name="email" class="required" value="<?php echo $app->getUserState('form.postcards.email', ''); ?>"></td>
					</tr>
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_SUBJECT'); ?></th>
						<td class="form_text"><input type="text" name="subject" class="required" value="<?php echo $app->getUserState('form.postcards.subject', ''); ?>"></td>
					</tr>


				</table>

				<div class="submit">
					<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_SEND'); ?>" onClick="sendForm()" />
					<input type="hidden" name="task" value="viewcardpeace.send" />
				</div>
			</div>

			<div class="submit submit_block" style="margin-top:60px; left:0; background: #4797B0; position:relative;">
				<a href="<?php echo JRoute::_('index.php?option=com_postcards&view=uploadphoto&Itemid='. (int) $itemid); ?>"><input id="submit_btn" type="button" value="上一步"/></a>
			</div>

			
		</div>
		
	</form>
	
	<?php
		} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
		}
	?>


</div>


<script>

                var canvas = new fabric.Canvas('card_canvas');
                /* 載入底圖 */
                fabric.Image.fromURL("<?php echo JURI::root() . explode(".",$this->image->image)[0] . "_r.jpg"; ?>", function(img) {
                    img.selectable = false;
                    img.scaleToWidth(640);
                    canvas.add(img);
                    canvas.moveTo(img, 0);
                });


                /* 將本機相簿載入 */
                <?php if (!empty($this->pic1)): ?>
                fabric.Image.fromURL("<?php echo JURI::base(). $this->pic1; ?>", function(img) {
                    img.scaleToWidth(200);
                    <?php if($app->getUserState('form.postcards.horizontal', '')):  //判斷直式還是橫式?>
                    img.setLeft(50);
                    img.setTop(250);
                    <?php else:  //直式?>
                    img.setLeft(30);
                    img.setTop(50);
                    <?php endif; ?>
                    canvas.add(img);
                    canvas.moveTo(img, 1);  // 調整 z-index
                });
                <?php endif?>

                /* 將本機相簿載入 */
                <?php if (!empty($this->pic2)): ?>
                fabric.Image.fromURL("<?php echo JURI::base(). $this->pic2; ?>", function(img) {
                    img.scaleToWidth(200);
                    <?php if($app->getUserState('form.postcards.horizontal', '')):  //判斷直式還是橫式?>
                    img.setLeft(400);
                    img.setTop(250);
                    <?php else: ?>
                    img.setLeft(30);
                    img.setTop(240);
                    <?php endif; ?>
                    canvas.add(img);
                    canvas.moveTo(img, 1);
                });
                <?php endif?>


                /* 加入文字 */
                var text = new fabric.Text('<?php echo $this->content; ?>', { <?php if($app->getUserState('form.postcards.horizontal', '')) echo "left: 50, top: 40"; else echo "left: 280, top: 50"; ?>, fill: '#f00', fontSize: 16, fontFamily: '楷體' });
                canvas.add(text);
                canvas.moveTo(text, 2);

                function setTextColor(picker) {  //改變文字顏色
                    text.setColor('#'+picker.toString());
                    canvas.renderAll();
                }


                var fontControl = jQuery('#FontStyleNumber');
                jQuery(document.body).on('change', '#FontStyleNumber', function () {
                    text.fontFamily = fontControl.val();
                    canvas.renderAll();
                });

                jQuery("#download").click(function() {
                	  canvas.deactivateAll().renderAll();
                    jQuery("canvas").get(0).toBlob(function(blob){  //可以取得blob格式的圖檔
                        saveAs(blob, "myCard.jpg");
                    });
                });

	(function($) {
		
		// $("#download").click(function(e) {
		// 	e.preventDefault();  //stop the browser from following
		// 	window.location.href = "<?php echo $this->bg_img; ?>";
		// });

		$("#email_btn").click(function(e) {
			$('.email_send').fadeIn( "slow" );
		});

	})(jQuery);


	function sendForm() {

		if (jQuery(".required").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_REUIRED'); ?>");

			return false;
		}

		reEcourse=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
		var friend_email = jQuery("#friend_email").val();
		if (!reEcourse.test(jQuery.trim(friend_email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}

		var email = jQuery("#email").val();
		if (!reEcourse.test(jQuery.trim(email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}


                        jQuery("canvas").get(0).toBlob(function(blob) {  //可以取得blob格式的圖檔
                            var reader = new FileReader();
                            reader.onload = function(event) { //確定blob檔案完成讀取之後，上傳檔案到server
                                var fd = {};

                                if (blob.type == "image/jpeg")
                                    fd["ext"] = "jpg";
                                else if (blob.type == "image/png")
                                    fd["ext"] = "png";
                                else if (blob.type == "image/gif")
                                    fd["ext"] = "gif";
                                else
                                    fd["ext"] = "jpg";

                                    fd["fbphoto"] = event.target.result;
                                    jQuery.ajax({
                                        type: 'POST',
                                        url: '<?php echo JURI::root(); ?>components/com_postcards/assets/upload.php',
                                        data: fd,
                                        dataType: 'text'
                                    }).done(function(data) {
                                        console.log(data); // 回傳唯一檔名！

                                        //上傳至server的照片檔名用input 方式送到server去
                                        jQuery('#submitForm').append('<input type="hidden" name="final_photo" value="' + data + '" />');

                                        /* 檔案上傳完畢 才開始submit form */
                                        if(confirm("<?php echo JText::_('COM_POSTCARDS_SEND_CHECK'); ?>")) {
                                            document.getElementById("submitForm").submit();
                                        }
                                    });
                            };
                            reader.readAsDataURL(blob);
                        });
	}
</script>