<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>

<p style="font-size: 22px; font-weight: bolder; margin-bottom:40px;">話解衝突第三式</p>
<div class="com_postcards">
	<div class="message_form">
		<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=uploadphoto&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

			
			<p style="font-size: 16px; line-height: 28px;">親愛的 <span><?php echo $this->receiver; ?></span>：</p>
			<p style="font-size: 16px; line-height: 28px;">關於「<?php echo $this->inputtext1; ?>」這件事，</p>
				
			<p style="font-size: 16px; line-height: 28px;">其實，我只是感到<?php echo $this->inputtext2; ?></p>

			<p style="font-size: 16px; line-height: 28px;">希望你能<input type="text" id="inputtext3" name="inputtext3" value="<?php echo $this->inputtext3; ?>">我。</p>

			<p style="font-size: 16px; line-height: 38px;">
			<textarea id="inputtext4" name="inputtext4" class="required" rows="3" style="width:100%"><?php echo $this->inputtext4; ?></textarea>
			</p>

			<br>
			<p style="font-size: 16px; line-height: 28px;">愛你的<input type="text" id="sender" name="sender" value="<?php echo $this->sender; ?>"></p>
			<br><br>


			<div class="explainText" style="border-left: 3px solid #009400; font-size: 16px; font-weight: bolder; padding-left:10px;">
			<p style="color:#009400; font-size: 20px;">向對方發出求救信號</p>
			<p style="color:#777;">負向情緒代表著，內心有尚未被滿足的需求，
			清楚表達自己的需要，能夠幫助對方更理解我生氣的原因，有助於解決問題。
			負向情緒背後的內在需求，可能是…….</p>

			
			</div>

			<p style="border: 3px solid #BCDB78; margin-top: 15px; padding: 20px; font-size: 16px; color: #777;">渴望被愛、被幫助、被支持、被記得、被體諒、被理解…..還有嗎?
			</p>

			
			<div class="submit submit_block" style="margin-top:60px; left:0; background: #4797B0; ">
				<a href="<?php echo JRoute::_('index.php?option=com_postcards&view=step2&Itemid='. (int) $itemid); ?>"><input id="submit_btn" type="button" value="上一步"/></a>
			</div>

			<div class="submit submit_block" style="margin-top:60px; position: absolute; right:0;">
				<input id="submit_btn" type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>"  />
			</div>
			</div>
		</form>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
		
	});

	
</script>