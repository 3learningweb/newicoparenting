<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewIntro extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		
		$config = JFactory::getConfig();
		$config->set('list_limit', 100);

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		// 清空所有的session
		$app->setUserState('form.postcards.image_id', null);
		
		$app->setUserState('form.postcards.receiver', null); //第一式
		$app->setUserState('form.postcards.inputtext1', null);
		
		$app->setUserState('form.postcards.inputtext2', null); //第二式
		
		$app->setUserState('form.postcards.inputtext3', null); //第三式
		$app->setUserState('form.postcards.inputtext4', null);
		$app->setUserState('form.postcards.sender', null);

		//輸入1~2張照片

		$app->setUserState('form.postcards.friend_email', null);
		$app->setUserState('form.postcards.name', null);
		$app->setUserState('form.postcards.email', null);
		$app->setUserState('form.postcards.subject', null);


		$app->setUserState('form.postcards.uploadphoto1', null);
		$app->setUserState('form.postcards.uploadphoto2', null);

		$app->setUserState('form.postcards.bg_img', null);
		$app->setUserState("form.postcards.horizontal", null);

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
