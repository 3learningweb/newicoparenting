<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>
<script>
	(function($) {		
		// submit
		$(document).on("click", "#submit_btn", function() {
			$("#profile_form").submit();
		});	
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo "話解衝突"; ?>
		</div>
	</div>
	
	<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				生活難免因忙碌而顯得急促，衝突的發生，是因為在意彼此，看重對方在家庭中的角色，試著，暫歇片刻，換一種方式，傳遞心中真正的想法。

				

			</p>
		</div>
	</div>
	<img src="filesys/files/images/4-1_12.png" alt="" style="float:right; width:25%;">
	<p style="font-size: 17px; line-height: 35px; margin: 10px;">話解衝突123，跟著一起做：<br>
				選一張「足以傳達心意」的圖片<br>
				1.用和緩的語氣重述情境<br>
				2.分享內心的情緒感受<br>
				3.向對方發出求救信號
			</p>


	

	
	<div class="checklist_block" style="text-align: center; margin-top:50px;">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_postcards&view=cardselect&Itemid={$itemid}", false); ?>">
			<div class="submit_block"><input type="button" id="submit_btn" value="話解衝突去" style="width: 155px;" /></div>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_checklist/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #e4ffb5;
		border: 8px solid #649800;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #e4ffb5;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #649800;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
