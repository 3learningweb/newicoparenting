<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );


jimport( 'joomla.application.component.view' );
class VquizViewQuizresult extends JViewLegacy
{ 
 
		function display($tpl = null)
 		{
			   
	          $mainframe =JFactory::getApplication();
			  $user=JFactory::getUser();
		      $context	= 'com_vquiz.result.list.';
			  $layout = JRequest::getCmd('layout', '');
			  $search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '',	'string' );
		      $search = JString::strtolower( $search );
			  $startdatesearch = $mainframe->getUserStateFromRequest( $context.'startdatesearch', 'startdatesearch', '',	'string' );
			  $enddatesearch = $mainframe->getUserStateFromRequest( $context.'enddatesearch', 'enddatesearch', '',	'string' );
			  
			  $filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );
       		  $filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
			  
			  $this->config = $this->get('Config');	
			  	     if($user->get('guest')){
					 jerror::raiseWarning('403', JText::_('UNAUTH_ACCESS_PLZ_LOGIN'));
					 return false;	
					 }
			  

			  
				if($layout == 'form')

				{

					$item =$this->get('Item');
					$this->assignRef( 'item', $item );
					$this->showresult=$this->get('Showresult');
 

			}

			else	{
 

				$items = $this->get('Items');
 				$this->assignRef( 'items', $items );
				$this->pagination = $this->get('Pagination');
				
				$lists['search']= $search;
				$lists['startdatesearch']= $startdatesearch;
				$lists['enddatesearch']= $enddatesearch;
				$this->assignRef( 'lists', $lists );
				
				// Table ordering.
				$this->lists['order_Dir'] = $filter_order_Dir;
				$this->lists['order']     = $filter_order;
 
			   }
 
			 parent::display($tpl);
 
		 }
 

}

 