<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );



	$document = JFactory::getDocument();
	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');
	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/icomoon.css');
	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/popup.css');
	$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
	$document->addScript(JURI::root().'components/com_vquiz/assets/js/popup.js');


JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
jimport( 'joomla.html.pagination' );
if(version_compare(JVERSION, '3.0', '>=')) 
JHtml::_('formbehavior.chosen', 'select');

	$user = JFactory::getUser();
	$listOrder = $this->lists['order']; 
	$listDirn = $this->lists['order_Dir'];
	$canOrder	= $user->authorise('core.edit.state', 'com_vquiz.quizquestion');
	$saveOrder	= $listOrder == 'ordering';

	$saveOrder 	= ($listOrder == 'ordering' && strtolower($listDirn) == 'asc');

?>
 
<script type="text/javascript">
 
Joomla.submitbutton = function(task) {
     var x=jQuery('#questioncsv').val();	
	 
				if (task == 'cancel') {
				Joomla.submitform(task, document.getElementById('adminForm'));
				} 
				else if(task=='import') {
					lightbox_import();
					var form = document.adminForm;
					if(x == "")  {
					alert("<?php echo JText::_('Please Chosse  csv file'); ?>");
					return false;
					}
				}
				  else if(task=='export') {
					var form = document.adminForm;
					if (document.adminForm.boxchecked.value==0)  {
					alert("<?php echo JText::_('Please Chose Questions'); ?>");
					return false;
					}
				}
					else if(task=='copy') {
					var form = document.adminForm;
					if (document.adminForm.boxchecked.value==0)  {
					alert("<?php echo JText::_('Please Chose Questions'); ?>");
					return false;
					}
					else{
					lightbox_copy();
					return false;
					}
				}
					else if(task=='move') {
					var form = document.adminForm;
					if (document.adminForm.boxchecked.value==0)  {
					alert("<?php echo JText::_('Please Chose Questions'); ?>");
					return false;
					}
					else{
					lightbox_move();
					return false;
					}
					
				}
				Joomla.submitform(task, document.getElementById('adminForm'));
			
}
</script>

 <style>
#toolbar div
{
	display:inline-block;
}
input[type="checkbox"]
{
	display:block;
}
</style>
<div class="manage_quizzes">



            <div id="toolbar" class="btn-toolbar">
            
            <div id="toolbar-back" class="btn-wrapper">
              <a class="btn btn-small" href="index.php?option=com_vquiz&view=usersquizzes"><span class="icon-previous"></span> <?php echo JText::_('Return to Quizzes')?></a>
            </div>
               
            <div id="toolbar-new" class="btn-wrapper">
                <button class="btn btn-small btn-success" onclick="Joomla.submitbutton('add')">
                <span class="icon-new icon-white"></span> <?php echo JText::_('NEW');?></button>
            </div>
            <div id="toolbar-delete" class="btn-wrapper">
                <button class="btn btn-small" onclick="if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{if (confirm('Do you want to delete the record(s)')){Joomla.submitbutton('remove');}}">
                <span class="icon-delete"></span> <?php echo JText::_('DELETE');?></button>
            </div>
            <div id="toolbar-move" class="btn-wrapper">
                <button class="btn btn-small" onclick="Joomla.submitbutton('move')">
                <span class="icon-move"></span> <?php echo JText::_('MOVE');?></button>
            </div>
            <div id="toolbar-copy" class="btn-wrapper">
                <button class="btn btn-small" onclick="Joomla.submitbutton('copy')">
                <span class="icon-copy"></span> <?php echo JText::_('COPY');?></button>
            </div>
            <div id="toolbar-publish" class="btn-wrapper">
                <button class="btn btn-small" onclick="Joomla.submitbutton('publish')">
                <span class="icon-publish"></span> <?php echo JText::_('PUBLISH');?></button>
            </div>
            <div id="toolbar-unpublish" class="btn-wrapper">
                <button class="btn btn-small" onclick="Joomla.submitbutton('unpublish')">
                <span class="icon-unpublish"></span> <?php echo JText::_('UNPUBLISH');?></button>
            </div>
            <div id="toolbar-edit" class="btn-wrapper">
                <button class="btn btn-small" onclick="if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{ Joomla.submitbutton('edit')}">
                <span class="icon-edit"></span> <?php echo JText::_('EDIT');?></button>
            </div>
          
            </div>
<form action="<?php echo JRoute::_( 'index.php?option=com_vquiz&view=quizquestion' ); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" onSubmit="return false">
     <div class="filter-select fltrt" style="float:right;">
     
                <input type="hidden" name="quizzesid" id="quizzesid" value="<?php echo $this->lists['quizzesid'];?>" class="text_area" onchange="document.adminForm.submit();" />
               <?php if(!empty($this->lists['quizzesid'])):?>
               <button  class="btn btn-small" <?php /*?>onclick="document.getElementById('quizzesid').value='';this.form.submit();"<?php */?> id="quizzesid_button">
                <?php echo "Quizzes ID";  echo $this->lists['quizzesid'];?><!--<span class="icon-unpublish"></span>--></button>
                <?php endif?>
     
                <select name="publish_item" id="publish_item" class="inputbox" onchange="this.form.submit()">
                <option value=""><?php echo JText::_('All State');?></option>
                <option value="p" <?php  if( 'p'== $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Published');?></option>
                <option value="u" <?php  if('u'== $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished');?></option>
                </select>
    
    </div>

        <div class="search_buttons">
        <div class="btn-wrapper input-append">
                <input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
                <button class="btn" onclick="this.form.submit();"><i class="icon-search"></i></button>
                <button class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></div></div>
 
 

 

    <div id="editcell">
        <table class="adminlist table table-striped table-hover">
                <thead>
                <tr>
                <th width="5">
                 <?php echo JText::_( 'Num' ); ?>
                </th>
                <th width="20">
                <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
                </th>			
                <th>
 
                  <?php echo JHTML::_('grid.sort', 'QUESTION', 'i.qtitle', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
                </th>
                
                <th>
				<?php /*?><?php echo JText::_( 'Quizzes' ); ?><?php */?>
                <?php echo JHTML::_('grid.sort', 'Quizzes', 'u.quizzes_title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			   </th>
                 <th>
                  <?php echo JHTML::_('grid.sort', 'QUESTIONTYPE', 'i.optiontype', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
                </th>
                <th>
				<?php echo JText::_( 'Flag' ); ?>
			   </th>
               <th>
				<?php echo JText::_( 'Published' ); ?>
			   </th>
               <th width="10%">
                <?php echo JHTML::_('grid.sort', 'JGRID_HEADING_ORDERING', 'ordering', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
                <?php if ($canOrder && $saveOrder) :?>
                <?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'saveorder'); ?>
                <?php endif; ?>
                </th>
                
                <th width="5">
                
                <?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
                </th>
 
                </tr>
                </thead>
                
                </thead>
                
                <tfoot>
                <tr>
                <td colspan="10"><?php echo $this->pagination->getListFooter(); ?></td>
                </tr>
                </tfoot>
                

                <?php
				$k = 0;
				for ($i=0, $n=count( $this->items ); $i < $n; $i++)	{
				$row = &$this->items[$i];
				$checked 	= JHTML::_('grid.id',   $i, $row->id );
				$published    = JHTML::_( 'jgrid.published', $row->published, $i );
				$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizquestion&task=edit&cid[]='. $row->id );
				
				$ordering	= ($listOrder == 'ordering');
				$canCreate	= $user->authorise('core.create',		'com_vquiz.quizquestion.'.$row->id);
				$canEdit	= $user->authorise('core.edit',			'com_vquiz.quizquestion.'.$row->id);
				$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $row->checked_out == $user->get('id') || $row->checked_out == 0;
				$canEditOwn	= true; //$user->authorise('core.edit.own',		'com_djimageslider.category.'.$item->catid) && $item->created_by == $userId;
				$canChange	= $user->authorise('core.edit.state',	'com_vquiz.quizquestion.'.$row->id) && $canCheckin;

				?> 
                
                <tr class="<?php echo "row$k"; ?>">
                
               <td><?php echo $this->pagination->getRowOffset($i); ?></td>
            
                <td>
                <?php echo $checked; ?>
                </td>
                <td>
                <a href="<?php echo $link; ?>"><?php echo substr(strip_tags($row->qtitle),0,50); ?></a>
                </td> 
                 <td>
                <?php echo $row->categoryname; ?> 
                </td> 
                <td>
                 <?php
				 if($row->optiontype==1)
				 	echo "Single Choice Answer";
				 else if($row->optiontype==2)
				  	echo "Multi Choice Answer";
				 ?> 
                </td> 
                
                <td align="center"><?php echo $row->flagcount; ?></td>
                
                <td align="center"><?php /*?><?php echo $published; ?><?php */?>
            <?php if($row->published==1) {?>
                <a class="btn btn-micro active hasTooltip" title="Publish Item" onclick="return listItemTask('cb<?php echo $i; ?>','unpublish')" href="javascript:void(0);" data-original-title="Publish Item">
                <i class="icon-publish" style="color:#f89406;"></i>
                </a>
                <?php } else  {?>
                <a class="btn btn-micro active hasTooltip" title="Unpublish Item" onclick="return listItemTask('cb<?php echo $i; ?>','publish')" href="javascript:void(0);" data-original-title="Unpublish Item">
                <i class="icon-unpublish"></i>
                </a>
                <?php }?></td>
                
                <td class="order" nowrap="nowrap">
					<?php if ($canChange) : ?>

						<?php if ($saveOrder) :?>

							<?php if ($listDirn == 'asc') : ?>

								<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid == @$this->items[$i-1]->catid),'orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>

								<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid  == @$this->items[$i+1]->catid), 'orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>

							<?php elseif ($listDirn == 'desc') : ?>

								<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid  == @$this->items[$i-1]->catid),'quizquestion.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>

								<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid == @$this->items[$i+1]->catid), 'quizquestion.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>

							<?php endif; ?>

						<?php endif; ?>

						<?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>

						<input  style="width:20px;" type="text" name="order[]" size="5" value="<?php echo $row->ordering;?>" <?php echo $disabled ?> class="text-area-order input-mini" />

					<?php else : ?>

						<?php echo $row->ordering; ?>

					<?php endif; ?>

				</td>
                <td>
                <?php echo $row->id; ?>
                </td>
 
                </tr>
                <?php
                
                $k = 1 - $k;
                }
                
                ?>
        </table>
    </div>
 
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="view" value="quizquestion" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
 


<?php /*?><a href="#" onclick="lightbox_open();" id="lightbox_open" ></a><?php */?>
<div id="light_import">
<a href="javascript:void(0);"  onclick="lightbox_close();" class="closepop">X</a>
	<div style="text-align:center;">
    <p><input type="file" name="questioncsv" id="questioncsv"></p>
    <p> <input type="button" value="Import" name="submitcsv" id="submitcsv" onclick="Joomla.submitform('importquestioncsv');"></p>
    </div>
</div>


<div id="light_move">
<a href="javascript:void(0);"  onclick="lightbox_close();" class="closepop">X</a>
<div style="text-align:center;">
<p><?php echo JText::_('Select Quiz'); ?></p>
  <p><select name="quizzesmoveid">
            <option value=""><?php echo JText::_('---Select---'); ?></option>
            <?php    for ($i=0; $i <count($this->quizes); $i++)	
            {
            ?>
            <option value="<?php echo $this->quizes[$i]->id;?>"  <?php  if($this->quizes[$i]->id ==!empty($this->item->quizzesid)?$this->item->quizzesid:0) echo 'selected="selected"'; ?> ><?php echo $this->quizes[$i]->quizzes_title;?></option>		
            <?php
            }
            ?>
            </select>
            </p>
            <p> <input type="button" value="Move" name="move"  onclick="Joomla.submitform('movequestion');"></p>
  </div>          
</div>

<div id="light_copy">
<a href="javascript:void(0);"  onclick="lightbox_close();" class="closepop">X</a>
<div style="text-align:center;">
<p><?php echo JText::_('Select Quiz'); ?></p>
  <p><select name="quizzescopyid">
            <option value=""><?php echo JText::_('---Select---'); ?></option>
            <?php    for ($i=0; $i <count($this->quizes); $i++)	
            {
            ?>
            <option value="<?php echo $this->quizes[$i]->id;?>"  <?php  if($this->quizes[$i]->id ==!empty($this->item->quizzesid)?$this->item->quizzesid:0) echo 'selected="selected"'; ?> ><?php echo $this->quizes[$i]->quizzes_title;?></option>		
            <?php
            }
            ?>
            </select>
            </p>
            <p> <input type="button" value="Copy" name="copy"  onclick="Joomla.submitform('copyquestion');"></p>
  </div>          
</div>


<div id="fade" onClick="lightbox_close();"></div> 

</form>
</div>


 


