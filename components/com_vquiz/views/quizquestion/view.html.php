<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
 	jimport( 'joomla.application.component.view' );
  	class VquizViewQuizquestion extends JViewLegacy
  	{ 
 	function display($tpl = null)
 		{
			$mainframe =JFactory::getApplication();
			$context	= 'com_vquiz.question.list.';
			$layout = JRequest::getCmd('layout', '');
			$search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '',	'string' );
			$search = JString::strtolower( $search );
			$quizzesid= $mainframe->getUserStateFromRequest( $context.'quizzesid', 'quizzesid',	'',	'string' );
			$publish_item= $mainframe->getUserStateFromRequest( $context.'publish_item', 'publish_item',	'',	'string' );
			
			$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );
			$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
			  
			  $this->config = $this->get('Config');	
  				if($layout == 'form')

 				{
 
					$item =$this->get('Item');
 
					$isNew		= ($item->id < 1);
 
					$this->assignRef( 'item', $item );
					$this->optinscoretype = $this->get('Optinscoretype');
 					$this->options = $this->get('Options');
 
 
			}
 
			else	{
				
 				$items = $this->get('Items');
			 
 
				$this->assignRef( 'items', $items );
				$this->quizes   = $this->get('Quizes');
				
				$this->pagination = $this->get('Pagination');
				
				$lists['search']= $search;
				$lists['publish_item']= $publish_item;
				$lists['quizzesid']= $quizzesid;
				$this->assignRef( 'lists', $lists );
				
 
				
				
				// Table ordering.
				$this->lists['order_Dir'] = $filter_order_Dir;
				$this->lists['order']     = $filter_order;
 
			   }

 		 parent::display($tpl);
 

		 }
 

}



 