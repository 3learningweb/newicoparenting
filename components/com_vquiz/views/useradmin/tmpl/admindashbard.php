<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
$document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
$user = JFactory::getUser();
?>
<script type="text/javascript" 
src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart'],'language':'ru'}]}">
</script>
<script type="text/javascript">
jQuery(function()	{
    if(typeof google !== "undefined")
	    google.setOnLoadCallback(drawLineChart);
	jQuery('.linecharttype').on('click', 'span', function()	{
		if(jQuery(this).hasClass('active'))
			return;
		else	{
			jQuery('.linecharttype>span').removeClass('active');
			jQuery(this).addClass('active');
			drawLineChart();
		}
	});


	jQuery('.linecharttype').on('change', 'select[name="category"]', function()	{
		drawLineChart();
	});

 
    function drawLineChart() 
	{
		var type = jQuery('.linecharttype>span.active').data('type');
		var category = jQuery('select[name="category"]').val();
		jQuery.ajax(
		{
			url: "index.php",
			type: "POST",
			dataType:"json",
			//data: {'option':'com_vquiz', 'task':'drawLineChart', 'tmpl':'component', 'type':type, 'category':category},
			data: {'option':'com_vquiz','view':'useradmin','task':'drawLineChart','tmpl':'component','type':type,'category':type,"<?php echo JSession::getFormToken(); ?>":1},
			beforeSend: function()	{
				jQuery(".loadingblock").show();
			},
			complete: function()	{
				jQuery(".loadingblock").hide();
			},

			success: function(data)	
			{ 
				if(data.result == "success")
				{
					var playedquiz=data.playedquiz;
					var data = new Array();

							data[0] = ['<?php echo JText::_('QUIZNAME'); ?>', '<?php echo JText::_('NUMBER_OF_USERS'); ?>'];
 							for(var j=0;j<playedquiz.length;j++)	{
							data[j+(1)] = new Array();
							data[j+(1)][0] = String(playedquiz[j][1]);
							data[j+(1)][1] = Number(playedquiz[j][2]);

							}

							var data = google.visualization.arrayToDataTable(data);
							//view code for animation
							var view = new google.visualization.DataView(data);
							view.setColumns([0, {
								type: 'number',
								label: data.getColumnLabel(1),
								calc: function () {return 0;}
							}]);

							var options =  
							{
							title: '<?php echo JText::_('PLAYED_QUIZZES'); ?>',
							hAxis: {title: '<?php echo JText::_('QUIZNAME'); ?>', titleTextStyle: {color: 'red'}},
						    backgroundColor:'#ffffff',
							height:250,
							chartArea:{left:5,top:0,width:'100%',height:'100%'},
							animation: { duration: 1500, easing:'out'}
							};
							var chart = new google.visualization.LineChart(
							document.getElementById('line_chart_div'));
							var runOnce = google.visualization.events.addListener(chart, 'ready', function () {
								google.visualization.events.removeListener(runOnce);
								chart.draw(data, options);
							});
							chart.draw(view, options);
						}

				}

		});

    }
});

</script>



<script type="text/javascript">
jQuery(function()	{
    if(typeof google !== "undefined")
	    google.setOnLoadCallback(drawpieChart);
	jQuery('.pietype').on('change', 'select[name="piecategory"]', function()	{
		drawpieChart();
	});

 

    function drawpieChart() 
	{
 
		var category = jQuery('select[name="piecategory"]').val();
		jQuery.ajax(
		{
			url: "index.php",
			type: "POST",
			dataType:"json",
 
			data: {'option':'com_vquiz','view':'useradmin', 'task':'drawpieChart','tmpl':'component','category':category,"<?php echo JSession::getFormToken(); ?>":1},
			beforeSend: function()	{
				jQuery(".poploadingbox").show();
			},
			complete: function()	{
				jQuery(".poploadingbox").hide();
			},
			success: function(data)	
			{ 
				if(data.result == "success")
				{
					var playedquiz=data.playedquiz;
					var data = new Array();
							data[0] = ['<?php echo JText::_('QUIZNAME'); ?>','Link','<?php echo JText::_('NUMBER_OF_USERS'); ?>'];
							for(var j=0;j<playedquiz.length;j++)	{
							var linkj ='<?php echo JURI::root().( 'administrator/index.php?option=com_vquiz&view=quizmanager&quid=')?>'+playedquiz[j][1];
							data[j+(1)] = new Array();
					    	data[j+(1)][0] = String(playedquiz[j][0]);
							data[j+(1)][1] = String(linkj);
							data[j+(1)][2] = Number(playedquiz[j][2]);
							}
							var data = google.visualization.arrayToDataTable(data);
							//view code for animation
							var view = new google.visualization.DataView(data);
							view.setColumns([0, 2]);
							var options =  
							{
							title: '<?php echo JText::_('TOP_PLAYED_QUIZZES'); ?>',
							hAxis: {title: '<?php echo JText::_('QUIZNAME'); ?>', titleTextStyle: {color: 'red'}},
						    backgroundColor:'#ffffff',
							height:200,
							chartArea:{left:5,top:0,width:'100%',height:'100%'},
							animation: { duration: 1500, easing:'out'}
							};

 							var chart = new google.visualization.PieChart( 
								document.getElementById('pie_chart_div'));
								chart.draw(view, options);
								var selectHandler = function(e) {
								window.location = data.getValue(chart.getSelection()[0]['row'], 1 );
								}
								// Add our selection handler.
								google.visualization.events.addListener(chart, 'select', selectHandler);

						}

				}

		});

    }

});

</script>
 
<script type="text/javascript">
jQuery(function()	{
    if(typeof google !== "undefined")
	    google.setOnLoadCallback(drawflagChart);
    function drawflagChart() 
	{
		jQuery.ajax(
		{
			url: "index.php",
			type: "POST",
			dataType:"json",
			//data: {'option':'com_vquiz', 'task':'drawflagChart', 'tmpl':'component'},
			data: {'option':'com_vquiz','view':'useradmin', 'task':'drawflagChart','tmpl':'component',"<?php echo JSession::getFormToken(); ?>":1},
			beforeSend: function()	{
				jQuery(".poploadingbox").show();
			},
			complete: function()	{
				jQuery(".poploadingbox").hide();
			},
			success: function(data)	
			{ 
				if(data.result == "success")
				{
					var flagquestion=data.flagquestion;
					var data = new Array();
 					data[0] = ['<?php echo JText::_('QUESTION'); ?>','Link','<?php echo JText::_('NUMBER_OF_FLAG'); ?>'];
					for(var j=0;j<flagquestion.length;j++)	{
					var linkj ='<?php echo JURI::root().( 'administrator/index.php?option=com_vquiz&view=quizquestion&task=edit&cid[]=')?>'+flagquestion[j][1];
 			
					data[j+(1)] = new Array();
					data[j+(1)][0] = String(flagquestion[j][0]);
					data[j+(1)][1] = String(linkj);
					data[j+(1)][2] = Number(flagquestion[j][2]);
					}							

							var data = google.visualization.arrayToDataTable(data);
							//view code for animation
							var view = new google.visualization.DataView(data);
							view.setColumns([0, 2]);
							var options =  
							{
							title: '<?php echo JText::_('TOP_FLAG_QUESTION'); ?>',
							hAxis: {title: '<?php echo JText::_('QUESTION'); ?>', titleTextStyle: {color: 'red'}},
						    backgroundColor:'#ffffff',
							height:200,
							chartArea:{left:5,top:0,width:'100%',height:'100%'},
							animation: { duration: 1500, easing:'out'}
							};

						
					  		var chart = new google.visualization.PieChart( 
								document.getElementById('flag_chart_div'));
								chart.draw(view, options);
								var selectHandler = function(e) {
								window.location = data.getValue(chart.getSelection()[0]['row'], 1 );
								}
								google.visualization.events.addListener(chart, 'select', selectHandler);
						}
				}
		});
    }
});

</script>
 
<script type="text/javascript">
jQuery(function()	{
    if(typeof google !== "undefined")
	    google.setOnLoadCallback(drawgeoChart);
	jQuery('.geotype').on('change', 'select[name="agegroup"]', function()	{
		drawgeoChart();
	});
    function drawgeoChart() 
	{
		var agegroup = jQuery('select[name="agegroup"]').val();	
		jQuery.ajax(
		{
			url: "index.php",
			type: "POST",
			dataType:"json",
  			data: {'option':'com_vquiz','view':'useradmin', 'task':'drawgeoChart','tmpl':'component','agegroup':agegroup,"<?php echo JSession::getFormToken(); ?>":1},
			beforeSend: function()	{
				jQuery(".poploadingbox").show();
			},
			complete: function()	{
				jQuery(".poploadingbox").hide();
			},

			success: function(data)	
			{ 
				if(data.result == "success")
				{
					var userinfo=data.userinfo;
 				      var data = new Array();

							data[0] = ['<?php echo JText::_('COUNTRY'); ?>', '<?php echo JText::_('NAME'); ?>'];
							for(var j=0;j<userinfo.length;j++)	{
							data[j+(1)] = new Array();
							data[j+(1)][0] = String(userinfo[j][0]);
							data[j+(1)][1] = String(userinfo[j][1]);
						}
							var datas = google.visualization.arrayToDataTable(data);
							 var options = {
								backgroundColor:'#ffffff', 
								height:250,
								chartArea:{left:5,top:0,width:'100%',height:'100%'}
								animation: { duration: 1500, easing:'out'}
							  };
							var chart = new google.visualization.GeoChart(document.getElementById('geo_chart_div'));
							chart.draw(datas, options);
						}
				}
		});
    }
});

</script>
<div class="manage_quizzes">
<div class="poploadingbox">
<?php echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/loading.gif"   />' ?>
</div>
 
	<div class="cpanel-left">
		<div class="cpanel">
                <div class="icon">
                    <a href="index.php?option=com_vquiz&view=usersquizzes"><img src="components/com_vquiz/assets/images/icon-48-quizzes.png" alt="<?php echo JText::_('Quizzes'); ?>" /><span class="texticon"><?php echo JText::_('Quizzes'); ?></span></a> 
                </div>
                    <div class="icon">
                    <a href="index.php?option=com_vquiz&view=quizresult"><img src="components/com_vquiz/assets/images/icon-48-quizresults.png" alt="<?php echo JText::_('Quiz Result'); ?>" /><span class="texticon"><?php echo JText::_('Quiz Result'); ?></span></a> 
                   </div>
 
                </div></div>
 	 <div class="clr"></div>
 
 <div class="cp_blocks">
   <div class="cp_block top_played_quizzes">
 	  <div class="chart_box">
 	   <div class="chart_x_tool ">
                       <h2><?php echo JText::_('TOP_PLAYED_QUIZZES'); ?></h2>                
                            <div class="select_type pietype">
                            <select name="piecategory" id="piecategory">
                             <option value="0"><?php echo JText::_('SELECT_CATEGORY'); ?></option>
                            <?php	for($i=0;$i<count($this->category);$i++)	{	?>
                            <option value="<?php echo $this->category[$i]->id; ?>">
                            <?php echo JText::_($this->category[$i]->quiztitle); ?>
                            </option>
                            <?php	}	?>
                            </select>
                            </div>
               </div>         
				 <div class="cp_block_chart_value"><div id="pie_chart_div"></div></div>
                         </div>
             </div>
 		 <div class="cp_block top_flagged_questions">
 		 <div class="chart_box">
          <div class="chart_x_tool">
               <h2><?php echo JText::_('TOP_FLAG_QUESTION'); ?></h2>
               </div>  
               <div class="cp_block_chart_value"><div id="flag_chart_div"></div></div>
               </div>
             </div>

            <div class="cp_block played_quizzes">
            <div class="chart_box">
            <div class="chart_x_tool linecharttype">
                   <h2><?php echo JText::_('PLAYED_QUIZZEZ'); ?></h2>
                    <span class="btn" data-type="day"><?php echo JText::_('DAILY'); ?></span> 
                    <span class="btn" data-type="week"><?php echo JText::_('WEEKLY'); ?></span> 
                    <span class="active btn" data-type="month"><?php echo JText::_('MONTHLY'); ?></span>
                        <div class="select_type">
                        <select name="category" id="category">
                         <option value="0"><?php echo JText::_('SELECT_CATEGORY'); ?></option>
                        <?php	for($i=0;$i<count($this->category);$i++)	{	?>
                        <option value="<?php echo $this->category[$i]->id; ?>">
                        <?php echo JText::_($this->category[$i]->quiztitle); ?>
                        </option>
                        <?php	}	?>
                        </select>
                        </div>
                        </div>
                     <div class="cp_block_chart_value"><div id="line_chart_div"></div></div>
                     </div> 
         </div>
            <div class="cp_block users_geo_charts">
          <div class="chart_box">
          <div class="chart_x_tool">
            <h2><?php echo JText::_('USERS_GEO_CHART'); ?></h2>
            <div class="select_type geotype">
            <select name="agegroup" id="agegroup">
            <option value=""><?php echo JText::_('CHOOSE_AGE_GROUP'); ?></option>
 			<option value="10"><?php echo JText::_('10-20'); ?></option>
            <option value="20"><?php echo JText::_('20-30'); ?></option>
            <option value="30"><?php echo JText::_('30-40'); ?></option>
            <option value="40"><?php echo JText::_('40-50'); ?></option>
            <option value="50"><?php echo JText::_('50-60'); ?></option>
            <option value="60"><?php echo JText::_('60-All'); ?></option>
            </select>
            </div>
        </div>    
            <div class="cp_block_chart_value"><div id="geo_chart_div"></div></div>
           </div>
   </div>
   </div>
    </div>

</div>





 