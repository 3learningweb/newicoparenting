<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access


defined('_JEXEC') or die('Restricted access'); 

 



	$document = JFactory::getDocument();



	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');

	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');

	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');

	$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/popup.css');

	$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');

	$document->addScript(JURI::root().'components/com_vquiz/assets/js/popup.js');





jimport( 'joomla.html.pagination');	

JHtml::_('behavior.framework');

JHtml::_('behavior.tooltip');

JHTML::_('behavior.modal');



	if(version_compare(JVERSION, '3.0', '>=')) 

	JHtml::_('formbehavior.chosen', 'select');

	$user = JFactory::getUser();

	$listOrder = $this->lists['order']; 

	$listDirn = $this->lists['order_Dir'];

	$canOrder	= $user->authorise('core.edit.state', 'com_vquiz.quizcategory');

	$saveOrder	= $listOrder == 'ordering';

	$saveOrder 	= ($listOrder == 'ordering' && strtolower($listDirn) == 'asc');



	$quizid_from_chart=JRequest::getInt('quid');



?>



<script type="text/javascript">

$( document ).ready(function() {

	if(<?php echo $quizid_from_chart?>){

		jQuery('input[name="search"]').val(<?php echo $quizid_from_chart;?>);

		document.adminForm.submit();

	}

});

Joomla.submitbutton = function(task) {

				if (task == 'cancel') {

				Joomla.submitform(task, document.getElementById('adminForm'));

				} 

					else if(task=='copy') {



					var form = document.adminForm;



					if (document.adminForm.boxchecked.value==0)  {



					alert("<?php echo JText::_('PLZCHOOSEQUESTION'); ?>");



					return false;



					}

					else{



					lightbox_copy();

					return false;

					}

 				}



					else if(task=='move') {

					var form = document.adminForm;

					if (document.adminForm.boxchecked.value==0)  {

					alert("<?php echo JText::_('PLZCHOOSEQUESTION'); ?>");

					return false;



					}

					else{

					lightbox_move();



					return false;

					}

 

				}



				Joomla.submitform(task, document.getElementById('adminForm'));



}



</script>

<style>

#toolbar div

{

	display:inline-block;

}

input[type="checkbox"]

{

	display:block;

}

</style>



<div class="manage_quizzes">

<div id="toolbar" class="btn-toolbar">

<div id="toolbar-new" class="btn-wrapper">

	<button class="btn btn-small btn-success" onclick="Joomla.submitbutton('add')">

	<span class="icon-plus2"></span>

	New</button>

</div>

<div id="toolbar-delete" class="btn-wrapper">

	<button class="btn btn-small" onclick="if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{if (confirm('Do you want to delete the record(s)')){Joomla.submitbutton('remove');}}">

	<span class="icon-cross3"></span>

	Delete</button>

</div>

<div id="toolbar-move" class="btn-wrapper">

	<button class="btn btn-small" onclick="Joomla.submitbutton('move')">

	<span class="icon-palette"></span>

	Move</button>

</div>

<div id="toolbar-copy" class="btn-wrapper">

	<button class="btn btn-small" onclick="Joomla.submitbutton('copy')">

	<span class="icon-docs"></span>

	Copy</button>

</div>

<div id="toolbar-publish" class="btn-wrapper">

	<button class="btn btn-small" onclick="Joomla.submitbutton('publish')">

	<span class="icon-checkmark" style="color:#51a351;"></span>

	Publish</button>

</div>







<div id="toolbar-unpublish" class="btn-wrapper">

	<button class="btn btn-small" onclick="Joomla.submitbutton('unpublish')">

	<span class="icon-cross3" style="color:#bd362f;"></span>

	Unpublish</button>

</div>

<div id="toolbar-edit" class="btn-wrapper">

	<button class="btn btn-small" onclick="if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{ Joomla.submitbutton('edit')}">

	<span class="icon-pencil"></span>

	Edit</button>

</div>

</div>



<form action="<?php echo JRoute::_( 'index.php?option=com_vquiz&view=quizzes' ); ?>" method="post" name="adminForm" id="adminForm" onSubmit="return false">

 <div class="filter-select fltrt" style="float:right;">

<select name="categorysearch" id="categorysearch" class="inputbox" onchange="this.form.submit()">

                <option value=""><?php echo JText::_('All Category'); ?></option>



                <?php    for ($i=0; $i <count($this->category); $i++)	

                {



                ?>

                <option value="<?php echo $this->category[$i]->id;?>"  <?php  if($this->category[$i]->id == $this->lists['categorysearch']) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle;?></option>		

                <?php



                }

                ?>

</select>

             <input type="hidden" name="qcategoryid" id="qcategoryid" value="<?php echo $this->lists['qcategoryid'];?>" class="text_area" onchange="document.adminForm.submit();" />

  <select name="publish_item" id="publish_item"class="inputbox" onchange="this.form.submit()">

  <option value=""><?php echo JText::_('All State');?></option>

  <option value="p" <?php  if( 'p'== $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Published');?></option>

  <option value="u" <?php  if('u'== $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished');?></option>

</select>

</div>



 <div class="search_buttons">

<div class="btn-wrapper input-append">

 

			<input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />

			<button class="btn" onclick="this.form.submit();"><i class="icon-search"></i></button></div>

			<button class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'RESET' ); ?></button>



 </div>



<div id="editcell">

	<table class="adminlist table table-striped table-hover">



	<thead>

		<tr>

			<th width="5">



				<?php echo JText::_( 'Num' ); ?>

			</th>



			<th width="20">

 

	          <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);"/>

 

			</th>			

 

			<th >

 

                <?php echo JHTML::_('grid.sort', 'TITLE', 'i.quizzes_title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

 

			</th>

 

            <th>

 

                 <?php echo JHTML::_('grid.sort', 'CATEGORY', 'x.quiztitle', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>



 

			</th>

 

             <th width="10%">

 

            <?php echo JHTML::_('grid.sort', 'JGRID_HEADING_ORDERING', 'ordering', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>



            <?php if ($canOrder && $saveOrder) :?>



            <?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'saveorder'); ?>





            <?php endif; ?>



            </th>

			<th>

                <?php echo JHTML::_('grid.sort', 'STARTPUBLISHDATE', 'i.startpublish_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

 

            <th>

				<?php echo JText::_( 'QUESTIONS' ); ?>



			</th>



		    	<th>



				<?php echo JText::_( 'PUBLISHED' ); ?>



			</th>

              <th>

                   <?php echo JText::_( 'FEATURED' ); ?>

			</th>



                 <th>

                 <?php echo JText::_( 'STATISTICS' ); ?>

                </th>

                

               <th width="5">

            <?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

            </th>



		</tr>

	</thead>



                <tfoot>

                <tr>

                <td colspan="0"><?php echo $this->pagination->getListFooter(); ?></td>

                </tr>

                </tfoot>



	<?php

 

	$k = 0;

 

	for ($i=0, $n=count( $this->items ); $i < $n; $i++)	{

 

		$row = &$this->items[$i];

 		$checked 	= JHTML::_('grid.id',   $i, $row->id );

		$published    = JHTML::_( 'jgrid.published', $row->published, $i );

		$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizzes&task=edit&cid[]='. $row->id );



			$ordering	= ($listOrder == 'ordering');

			$canCreate	= $user->authorise('core.create',		'com_vquiz.quizcategory.'.$row->id);

			$canEdit	= $user->authorise('core.edit',			'com_vquiz.quizcategory.'.$row->id);

			$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $row->checked_out == $user->get('id') || $row->checked_out == 0;

			$canEditOwn	= true; //$user->authorise('core.edit.own',		'com_djimageslider.category.'.$item->catid) && $item->created_by == $userId;

			$canChange	= $user->authorise('core.edit.state',	'com_vquiz.quizcategory.'.$row->id) && $canCheckin;



		?>

 

		<tr class="<?php echo "row$k"; ?>">

 

         <td><?php echo $this->pagination->getRowOffset($i); ?></td>

			<td>



				<?php echo $checked; ?>

			</td>



			<td>



				<a href="<?php echo $link; ?>"><?php echo substr($row->quizzes_title,0,50); ?></a>



			</td> 



            <td>

				 <?php echo $row->categoryname; ?> 

			</td> 





            <td class="order" nowrap="nowrap">

					<?php if ($canChange) : ?>

						<?php if ($saveOrder) :?>

							<?php if ($listDirn == 'asc') : ?>

								<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid == @$this->items[$i-1]->catid),'orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>



								<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid == @$this->items[$i+1]->catid), 'orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>

							<?php elseif ($listDirn == 'desc') : ?>

								<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid == @$this->items[$i-1]->catid),'quizcategory.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>

								<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid == @$this->items[$i+1]->catid), 'quizcategory.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>



							<?php endif; ?>

						<?php endif; ?>

						<?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>

						<input  style="width:20px;" type="text" name="order[]" size="5" value="<?php echo $row->ordering;?>" <?php echo $disabled ?> class="text-area-order input-mini" />



					<?php else : ?>



						<?php echo $row->ordering; ?>



					<?php endif; ?>

 

				</td>



           	<td>

			  <?php echo $row->startpublish_date; ?>

			</td> 

 

            <td>



				<a href="<?php echo JRoute::_('index.php?option=com_vquiz&view=quizquestion&quizzesid='.$row->id);?>">

                <input class="btn btn-small" type="button" value="<?php echo JText::_('View')?>(<?php echo $row->totalquestion; ?>)" />

				 </a>

			</td> 



			<td align="center"><?php echo $published; ?></td>

            <td> 

                  <?php if($row->featured==1) {?>

                <a class="btn btn-micro active hasTooltip" title="Unfeatured Item" onclick="return listItemTask('cb<?php echo $i; ?>','unfeatured')" href="javascript:void(0);" data-original-title="featured Item">

                <i class="icon-star" style="color:#f89406;"></i>

                </a>



                <?php } else  {?>

                <a class="btn btn-micro active hasTooltip" title="Featured Item" onclick="return listItemTask('cb<?php echo $i; ?>','featured')" href="javascript:void(0);" data-original-title="unfeatured Item">

                <i class="icon-star2"></i>

                </a>

                <?php }?>





            </td>



            <td> 

            <a class="modal" id="modal" title="Select" href="<?php echo 'index.php?option=com_vquiz&view=quizzes&layout=chartview&tmpl=component&chart=1&quizzesid='.$row->id;?>" rel="{handler: 'iframe', size: {x: 800, y: 500}}"><input class="btn btn-small" type="button" value="<?php echo JText::_('VIEW')?>" /></a>

            </td>

        <td>

        <?php echo $row->id; ?>

        </td>



		</tr>

		<?php

		$k = 1 - $k;

	}

 

	?>

	</table>

 

</div>



<input type="hidden" name="option" value="com_vquiz" />

<input type="hidden" name="task" value="" />

<input type="hidden" name="boxchecked" value="0" />

<input type="hidden" name="view" value="quizzes" />

<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />

<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />

 

<div id="light_move">



<a href="javascript:void(0);"  onclick="lightbox_close2();" class="closepop">X</a>



    <div style="text-align:center;">



    <p><?php echo JText::_('SELECTCATEGORY'); ?></p>



      <p><select name="quizcategoryidmove" style="width:200px;" >



                <option value=""><?php echo JText::_('SELECT'); ?></option>



                <?php    for ($i=0; $i <count($this->category); $i++)	



                {



                ?>



                <option value="<?php echo $this->category[$i]->id;?>"  <?php  if($this->category[$i]->id == @$this->item->quiz_categoryid) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle;?></option>		



                <?php



                }



                ?>



                </select>



                </p>



                <p> <input type="button" value="Move" name="move"  onclick="Joomla.submitform('movequestion');"></p>



      </div>          

</div>



<div id="light_copy">

<a href="javascript:void(0);"  onclick="lightbox_close2();" class="closepop">X</a>

<div style="text-align:center;">

<p><?php echo JText::_('SELECTCATEGORY'); ?></p>

  <p><select name="quizcategoryidcopy"  style="width:200px;">

                <option value=""><?php echo JText::_('SELECT'); ?></option>

                <?php    for ($i=0; $i <count($this->category); $i++)	

                {

                ?>

                <option value="<?php echo $this->category[$i]->id;?>"  <?php  if($this->category[$i]->id == @$this->item->quiz_categoryid) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle;?></option>		

                <?php

                }

                ?>

                </select>

            </p>

            <p> <input type="button" value="Copy" name="copy"  onclick="Joomla.submitform('copyquestion');"></p>



  </div>          

</div>



<div id="fade" onClick="lightbox_close2();"></div> 



</form>

</div>

