<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access



defined('_JEXEC') or die('Restricted access');



  $document = JFactory::getDocument();



  $document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');



  $document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');



  $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');



  //$document->addScript('components/com_vquiz/assets/js/googlemap.js');



  $quizzesid = JRequest::getInt('quizzesid');



  $chart = JRequest::getInt('chart');



 ?>



<script type="text/javascript" 



src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart'],'language':'ru'}]}">



</script>



 <script type="text/javascript">



jQuery(function()	{



    if(typeof google !== "undefined")



	{   



	    google.setOnLoadCallback(drawChart);		



	}



		function mouseEventHandler(event)  {



		document.getElementById('chart').innerHTML += "You clicked " + event.region + "<br/>";



		}





    function drawChart() 



	{



		var quizzesid = jQuery('input[name="quizzesid"]').val();



		var chart = jQuery('input[name="chart"]').val();



		



		jQuery.ajax(



		{



			url: "index.php",



			type: "POST",



			dataType:"json",



			data: {'option':'com_vquiz','view':'quizzes', 'task':'drawChart','quizzesid':quizzesid,'charttype':chart,'tmpl':'component',"<?php echo JSession::getFormToken(); ?>":1},



			beforeSend: function()	{



				jQuery(".loadingblock").show();



			},



			complete: function()	{



				jQuery(".loadingblock").hide();



			},



			success: function(data)	



			{ 



				if(data.result == "success")



				{



					 



					var responce1=data.responce1;

					var responce2=data.responce2;

					var charttype=data.charttype;



					var data = new Array();



							data[0] = ['<?php echo JText::_('SCORE'); ?>', '<?php echo JText::_('NUMBER_OF_USERS'); ?>', {role: 'style'}];

							for(var j=0;j<responce1.length;j++)	{

							data[j+(1)] = new Array();

							data[j+(1)][0] = String(responce1[j][0]);

							data[j+(1)][1] = Number(responce1[j][1]);

							data[j+(1)][2] = 'color: #0000ff';

							}



							 

							var data= google.visualization.arrayToDataTable(data);



							//view code for animation



							var view = new google.visualization.DataView(data);



							view.setColumns([0, {



								type: 'number',



								label: data.getColumnLabel(1),



								calc: function () {return 0;}



							}]);







							var options =  



							{



							title: '<?php echo JText::_('PLAYED_QUIZZES'); ?>',



							hAxis: {title: '<?php echo JText::_('SCORES'); ?>', titleTextStyle: {color: 'red'}},



						    backgroundColor:'#fef4d4',



							height:300,



							chartArea:{left:100,top:80,width:"100%"},



							animation: { duration: 1500, easing:'out'}



							};



 



								var chart = new google.visualization.ColumnChart(document.getElementById('chart1'));



 



								



								function errorHandler(errorMessage) {



								console.log(errorMessage);



								google.visualization.errors.removeError(errorMessage.id);



								}



							    google.visualization.events.addListener(chart, 'error', errorHandler);



							  



							   chart.draw(data, options);



								



				  			    if(responce1==''){

								var ht='<?php echo JText::_('No Data');?>';

								jQuery("#chart1").html(ht);

								}



					  



								 				 



								var data = google.visualization.arrayToDataTable(responce2);



							



								var options = {



								height: 300,



								backgroundColor:'#fef4d4',



								chartArea:{left:100,top:80,width:"100%"},



								title: '<?php echo JText::_('CHOOSED_OPTION_BY_USER'); ?>',



								hAxis: {title: '<?php echo JText::_('USERS'); ?>', titleTextStyle: {color: 'red'},gridlines:{color: '#FFFFF', count: 0}},



								vAxis: {title: '<?php echo JText::_('QUESTIONS'); ?>',titleTextStyle: {color: 'red'}},



								



								legend: { position: 'top', maxLines: 3 },



								bar: { groupWidth: '75%' },



								isStacked: true



								};



								



								var chart = new google.visualization.BarChart(document.getElementById("chart2"));



								



								function errorHandler(errorMessage) {



								console.log(errorMessage);



								google.visualization.errors.removeError(errorMessage.id);



								}



							    google.visualization.events.addListener(chart, 'error', errorHandler);



								



								chart.draw(data, options);



 



				           }



					



							 



							



					//	}



					 		



				}



		});



    }

 

});



</script>

 

<div class="poploadingbox">



<?php echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/loading.gif"   />' ?>



</div>







	<div class="cpanel-left">

        <div class="chart2">

        <h3><?php echo JText::_('Most Options Choosed Chart')?></h3>

        <div id="chart2"></div>

        </div>

        <div class="chart1">

        <h3><?php echo JText::_('USER_PLAYED_CHART')?></h3>

        <div id="chart1"></div>

        </div>

   </div>



   



<input type="hidden" name="option" value="com_vquiz" />



<input type="hidden" name="task" value="" />



<input type="hidden" name="view" value="quizzes" />



<input type="hidden" name="quizzesid" value="<?php echo $quizzesid; ?>" />



<input type="hidden" name="chart" value="<?php echo $chart; ?>" />



