<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class VquizViewQuizzes extends JViewLegacy

{ 

	function display($tpl = null)

		{



			  $mainframe =JFactory::getApplication();

			  $user=JFactory::getUser();

		      $context	= 'com_vquiz.quizzes.list.';

			  $layout = JRequest::getCmd('layout', '');

			  $search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '',	'string' );

		      $search = JString::strtolower( $search );

			  $qcategoryid= $mainframe->getUserStateFromRequest( $context.'qcategoryid', 'qcategoryid',	'',	'string' );

			  $categorysearch= $mainframe->getUserStateFromRequest( $context.'categorysearch', 'categorysearch',	'',	'string' );

			  $publish_item= $mainframe->getUserStateFromRequest( $context.'publish_item', 'publish_item',	'',	'string' );

			  

			  $filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );

       		  $filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );

			  

			  $this->config = $this->get('Config');	

			  	    if($user->get('guest')){

					 jerror::raiseWarning('403', JText::_('UNAUTH_ACCESS_PLZ_LOGIN'));

					 return false;	

					 }

			  

				if($layout == 'form')

				{

					

					$this->access = $this->get('Access');

					$item =$this->get('Item');

					$isNew		= ($item->id < 1);

					$this->assignRef( 'item', $item );

					$this->category  = $this->get('Category');

					$this->lists['access'] = @JHtml::_('access.assetgrouplist', 'access', $this->item->access,'size="5"',false);

 



					if($isNew)	{

					//JToolBarHelper::title( JText::_( 'New Quizzes' ), 'generic.png' );					

					}



					else	{

						//JToolBarHelper::title( JText::_( 'Edit Quizzes' ), 'generic.png' );

					}	 



			}



	

			else	{



 



				$items = $this->get('Items');

 



				$this->assignRef( 'items', $items );

				$this->category = $this->get('Category');

								

				$this->pagination = $this->get('Pagination');

				

					$lists['search']= $search;

					$lists['publish_item']= $publish_item;

					if(!empty($qcategoryid))

					{

					$lists['categorysearch']=$qcategoryid;

					}else

					$lists['categorysearch']= $categorysearch;

		

					$this->assignRef( 'lists', $lists );

				

				

				

				// Table ordering.

				$this->lists['order_Dir'] = $filter_order_Dir;

				$this->lists['order']     = $filter_order;



			   }

	 

			 parent::display($tpl);



	

		 }





}



 