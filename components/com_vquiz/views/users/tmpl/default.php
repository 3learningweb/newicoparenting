<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
JHtml::_('behavior.tooltip');
 $document = JFactory::getDocument();
 $document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
 $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
 $document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
 $document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
 $user = JFactory::getUser();
 $uid = $user->id;
 ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 <script type="text/javascript">
 var $tm = jQuery.noConflict();
                    $tm(function(){
                      $tm('*[name=dob]').datepicker({
						dateFormat: "yy-mm-dd",
						changeMonth: true,
						changeYear: true,
						yearRange: '1950:2015',
						minDate: "-80Y" , 
						maxDate: "+0" ,
						//showButtonPanel: true,
                      });
 
                        });
 
$tm(function()	{
 $tm(document).on('change', 'input[name="cond"]', function(event)	{
 if(this.checked === true)	{
 $tm('.login_block').show();
 $tm('.register_block').hide();
 }
 else	{
 $tm('.login_block').hide();
 $tm('.register_block').show();
 }
 });

 $tm(document).on('click', '.log_block button[type="submit"]', function(event){
			event.preventDefault();
			var task='login';
		
			if($tm('form[name="login_form"] input[name="username"]').val() == "")	{
			alert("<?php echo JText::_('INFNNOLL'); ?>");
			return false;
			}
			
			if($tm('form[name="login_form"] input[name="password"]').val() == "")	{
			alert("<?php echo JText::_('INFNNOLL'); ?>");
			return false;
			}
 		Joomla.submitform(task, document.getElementById('login_form'));
  });

 $tm(document).on('click', '.nav_block button[type="submit"]', function(event){
event.preventDefault();
var task='save';
var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;

				if($tm('form[name="adminForm"] input[name="name"]').val() == "")	{
				alert("<?php echo JText::_('INFNNOLL'); ?>");
				document.adminForm.name.focus();
				return false;
				}
				
				if($tm('form[name="adminForm"] input[name="username"]').val() == "")	{
				alert("<?php echo JText::_('INFNNOLL'); ?>");
				document.adminForm.username.focus();
				return false;
				}
				
				if($tm('form[name="adminForm"] input[name="email"]').val() == "")	{
				alert("<?php echo JText::_('INFNNOLL'); ?>");
				document.adminForm.email.focus();
				return false;
				}
 
				if ($tm('form[name="adminForm"] input[name="email"]').val() != "" && !emailFilter.test($tm('form[name="adminForm"] input[name="email"]').val()))	{
				alert("<?php echo JText::_('VALIDINVALIDEMAIL'); ?>");
				document.adminForm.email.focus();
				return false;
				}

	 
			if($tm('form[name="adminForm"] input[name="password"]').val().length < 6 && $tm('form[name="adminForm"] input[name="password"]').val() !=""){
				alert("<?php echo JText::_('VALIDSHORTPASSWORD'); ?>");
				document.adminForm.password.focus();
					return false;
			}

			if($tm('form[name="adminForm"] input[name="password2"]').val() != $tm('form[name="adminForm"] input[name="password"]').val()){
					alert("<?php echo JText::_('PASSWORDNTMATCH'); ?>");
					document.adminForm.password2.focus();
				return false;
			}
			
	 		if($tm('form[name="adminForm"] input[name="dob"]').val() == "")	{
 			alert("<?php echo JText::_('INFNNOLL'); ?>");
			document.adminForm.dob.focus();
 			return false;
 			}
			
			if($tm('form[name="adminForm"] input[name="country"]').val() == "")	{
 			alert("<?php echo JText::_('INFNNOLL'); ?>");
			document.adminForm.country.focus();
 			return false;
 			}
 		Joomla.submitform(task, document.getElementById('adminForm'));

  });
 
	});	

</script>	
 
<div id="dabpanel" class="login dabpanel ">
<div class="page-header">
  </div>
<?php

$user=JFactory::getUser();
$checked = JRequest::getInt('login', 0);
?>
      <?php if($user->get('guest')) : ?>        
        <div class="cond_block"><h5><span class="check_box"><input type="checkbox" name="cond" value="1" <?php if($checked) echo 'checked="checked"'; ?> style="display:inline-block;" /></span> <?php echo JText::_('Already a user'); ?></h5></div>
   <div class="login_block" <?php if(!$checked) echo 'style="display:none;"'; ?>>
      <div class="page-header"><h2><?php echo JText::_('Login');?> </h2></div>
 	<form action="<?php echo JRoute::_( 'index.php?option=com_vquiz&view=users' ); ?>" method="post" id="login_form" name="login_form" class="form-validate form-horizontal well" >
      <div class="control-group">
         <div class="control-label">
             <label><?php echo  JText::_( 'USERNAME' ); ?><span class="star"> *</span></label>
 			     </div>
 			       <div class="controls">
 			          <input type="text" id="username" name="username" size="30" value="<?php echo $user->username;?>" maxlength="100" />
 
                    </div>
         <div class="clr"></div>
		    </div>
 
                <div class="control-group">
 		           <div class="control-label">
 		              <label><?php echo  JText::_( 'PASSWORD' ); ?><span class="star"> *</span></label>
			       </div>
 				     <div class="controls">

                        <input  type="password" id="password" name="password" size="30" value="" />
 				    </div>
 				 <div class="clr"></div>
 
                </div>
 			 <div class="control-group resi_sfs log_block">
 			<button class="button validate " type="submit"><?php echo JText::_('LOGIN'); ?></button>
 				</div>
 
                <input type="hidden" name="return" value="<?php echo base64_encode(JRoute::_('index.php?option=com_vquiz&view=users&tmpl=component')); ?>" />
 
				<input type="hidden" name="option" value="com_vquiz" />
         <input type="hidden" name="view" value="users" />
 		<input type="hidden" name="task" value="login" />
        <?php echo JHtml::_('form.token'); ?>
    </form>
  </div>
    <?php endif; ?>
 
         <div class="register_block" <?php if($checked) echo 'style="display:none;"'; ?>>
 
<div class="page-header"><h2><?php if($uid) { echo JText::_('YOUR_PROFILE');}else  {echo JText::_('REGISTRATION');}?> </h2></div>
 
<form action="<?php echo JRoute::_('index.php?option=com_vreview&view=users'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-validate form-horizontal well">
 
<div class="col100">
     <div class="control-group">
       <div class="control-label">
       <label class="required"><?php echo JText::_('NAME'); ?><span class="star"> *</span></label>
       </div>
       <div class="controls">
 	    <input type="text" name="name" id="name" class="inputbox required" value="<?php echo $this->item->name; ?>" size="50" />
 		   </div>
 
                    <div class="clr"></div>
 
                </div>
 
                <div class="control-group">
 			         <div class="control-label">
 			           <label class="required"><?php echo JText::_('USERNAME'); ?><span class="star"> *</span></label>
 
                    </div>
 				      <div class="controls">
 		         <input type="text" name="username" id="username" class="inputbox required" value="<?php echo $this->item->username; ?>" size="50" />
 			   </div>
       <div class="clr"></div>
     </div>
    <div class="control-group">
           <div class="control-label">
 <label class="required"><?php echo JText::_('PASSWORD'); ?><span class="star"> *</span></label>
         </div>
           <div class="controls">
         <input type="password" class="inputbox" name="password" autocomplete="off" value="" cols="39" rows="5" />
        </div>
          <div class="clr"></div>
     </div>

      <div class="control-group">
             <div class="control-label">
                  <label class="required"><?php echo JText::_('VERIFY_PASSWORD'); ?><span class="star"> *</span></label>
        </div>

        <div class="controls">

          <input type="password" class="inputbox" name="password2" autocomplete="off" value="" cols="39" rows="5" />
       </div>

      <div class="clr"></div>
 
                </div>
     <div class="control-group">
        <div class="control-label">
           <label class="required"><?php echo JText::_('EMAIL'); ?><span class="star"> *</span></label>
 		    </div>
       <div class="controls">
        <input type="text" class="inputbox required" name="email" maxlength="100" size="50" value="<?php echo $this->item->email;?>" />
 
                    </div>
 		     <div class="clr"></div>
         </div>
 
                  <div class="control-group">
 					 <div class="control-label">
      <label class="required"><?php echo JText::_('DOB'); ?><span class="star"> *</span></label>
 
                    </div>
       <div class="controls">
                  <input type="text" name="dob" id="dob" value="<?php echo $this->item->dob;?>"  class="text_area" />
        </div>
         <div class="clr"></div>
        </div>
    <div class="control-group">
            <div class="control-label">
            <label class="required"><?php echo JText::_('COUNTRY'); ?><span class="star"> *</span></label>
     </div>
       <div class="controls">
          <input type="text" name="country" id="country" value="<?php echo $this->item->country;?>"  class="text_area" />
      </div>
     <div class="clr"></div>
  </div>
   <div class="control-group">
         <div class="control-label">
             <label class="required"><?php echo JText::_('GENDER'); ?><span class="star"> *</span></label>
      </div>
   <div class="controls controls_gender">
   <input type="radio" class="inputbox" name="gender"  value="0" <?php if($this->item->gender==0) echo 'checked="checked"';?> style="display:inline-block"/>
      <?php echo JText::_('Male'); ?>
        <input type="radio" class="inputbox" name="gender"  value="1" <?php if($this->item->gender==1) echo'checked="checked"';?>  style="display:inline-block"/>
  <?php echo JText::_('Female'); ?>
    </div>
     <div class="clr"></div>
     </div>    
                <div class="control-group">
                    <div class="control-label">
                         <label class="hasTip"><?php echo JText::_('PROFILE_PICS'); ?> :</label>
                    </div>
                    <div class="controls controls_upload">
                     <input type="file" name="profile_pic" id="profile_pic" class="inputbox required" size="50" value="" style="vertical-align:top;"/>
                     
			 <?php 
             if(!empty($this->item->profile_pic))
             echo '<img src="'.JURI::root().'administrator/components/com_vquiz/assets/uploads/users/'.$this->item->profile_pic.'" alt="Your image here"/>';
             ?>

 

                    </div>
                    <div class="clr"></div>
                </div>
    <div class="buttonBar-right nav_block ">
    <?php if($uid) { ?>
		<button   class="btn btn-small validate" type="submit"><?php echo JText::_('SAVE');?></button>
    <?php } else { ?>
    	<button   class="btn btn-small validate" type="submit"><?php echo JText::_('REGISTER');?></button>
    <?php } ?>
	</div>
</div>
<div class="clr"></div>
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="id" value="<?php echo $uid; ?>" />
<input type="hidden" name="task" value="save" />
<input type="hidden" name="view" value="users" />
</form>
</div>
    <div class="clr"></div>
</div>
 