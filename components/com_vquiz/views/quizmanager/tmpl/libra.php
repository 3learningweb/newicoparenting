<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHTML::_('form.token');

$session = JFactory::getSession();
if ($session->has('quizoptions')) {
	$session->clear('quizoptions');
	$session->clear('flag');
	$session->clear('fqid');
	$session->clear('random_quizoption');
	$session->clear('guest_result_id');
}
$document = JFactory::getDocument();
$app = JFactory::getApplication();
$title = $this->fitem->quizzes->quizzes_title;
$document->setTitle($title . ' - ' . $app->getCfg('sitename'));

$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/responsive_layout.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/popup.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/jquery.countdownTimer.css');
//$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/smoothness.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/jquery-ui.css');


//  $document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/html2canvas.js');
//  $document->addScript('components/com_vquiz/assets/js/jquery.countdownTimer.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/addthis.js');
//  $document->addScript(JURI::root().'components/com_vquiz/assets/js/googlemap.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery.plugin.html2canvas.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/popup.js');

//$document->addScript(JURI::root() . 'components/com_vquiz/assets/js/all_compress.js');

$user = JFactory::getUser();
$questions_array = array();
$optionns_array = array();


if ($this->resultcats) {
	unset($resultcats);
	foreach ($this->resultcats as $resultcat) {
		$resultcats[$resultcat->id] = $resultcat->quiztitle;
	}
}

?>
<!--<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>-->
<!--<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/StackBlur.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>-->
<!--<script type="text/javascript" src="http://gabelerner.github.io/canvg/canvg.js"></script> -->


<script type="text/javascript">
//	var vg=jQuery.noConflict();
	var vg=jQuery;
	var result_status=1;
	var t_oggle=false;
	var t_oggle1=false;
	var resultimage;
	var secs;
	var totalsec;
	var last = false;
	var first=true;
	var kk=false;
	var cki=false;
	var flag=false;
	var flag_count=0;
	var stortime=0;
	var lastelementcheck=false;
	var paging='<?php echo $this->fitem->quizzes->paging_limit > 1 ? $this->fitem->quizzes->paging_limit : 1; ?>';


 
	vg(document).ready(function() {
		// 初始化
		<?php if ($this->fitem->quizzes->desc_button == 1) { ?>
			vg(".photopath").hide();
			vg(".slides_part").hide();
			vg(".allbutton").hide();
			vg(".desc_part").show();
			vg(".score_part").hide();
		<?php } else { ?>
			vg(".photopath").show();
			vg(".slides_part").show();
			vg(".allbutton").show();
			vg(".desc_part").hide();
			vg(".score_part").show();
		<?php } ?>


		vg( ".startbutton" ).bind( "click", function() {
			vg(".desc_part").slideUp(500);
			vg(".photopath").show();
			vg(".score_part").fadeIn(1000);
			vg(".slides_part").fadeIn(1000);
			vg(".allbutton").show();
	   });

		
						
	});
			 

	function executesuccess(data)
	{
		
		flag=true;
		var checks;
		
		var optiontypescore=data.optiontypescore;
		var livescore=data.livescore;
		var maxscore=data.maxscore;
		if(optiontypescore==1)
			var live_html=livescore+'/'+maxscore;
		else
			var live_html=livescore;


		var nextid=data.item.id;
		var ck=data.checked;
		var optiontype=data.item.optiontype;
 
		var questions_array=new Array();
		var html = '<div class="quiz-ques">';
		for( var j =0;j<data.item.length;j++){
			
			html +='<div class="qti">'+data.item[j].qtitle+'</div>';
			
			questions_array.push(data.item[j].id);
			
			var optiontype=data.item[j].optiontype;
			
			html +='<div class="q-options"><ul>';
			for( var i =0;i<data.answer[j].length;i++){
				
				if (jQuery.inArray(data.answer[j][i].id, ck) != -1)
					checks ='checked="checked"';
				else
					checks = '';

				html += '<li><input type="radio" name="qoption'+j+'[]" id="r'+j+i+'" value="'+data.answer[j][i].id+'" '+checks+' /><label for="r'+j+i+'" style="display: inline;"></label><input type="hidden" id="ids_r'+j+i+'"  value="'+data.answer[j][i].resultcats_ids+'"  />'+data.answer[j][i].qoption+'</li>';
 
			}
			html +='</ul></div>';
		}
		
		html += '</div>';

		vg("#next_explanation").html(data.item[0].explanation);
		
		var json_question=JSON.stringify(questions_array);
		
		vg('input[name="questions_id"]').val(json_question);


		vg('.slides').toggle('slide', { direction: 'left' }, 700);
					

		vg('.slides').append('<div class="slide"></div>');
		vg('.slides>.slide:first').html(html);
	}


	
	
	vg(function() {
 
		var scores;
		var textscore;
		var nextvariable=0;
		var skipcount=0;
		var questiontime=vg('input[name="quetime"]').val();
		var totaltime= vg('input[name="totaltime"]').val();
					
		if(totaltime>0)
			tcountdown(totaltime);
					
		if(questiontime>0)
			qcountdown(questiontime);


				


		jQuery(".nextbutton").click(function(){
			var checkquestion=false;
			var questions_id=vg('input[name="questions_id"]').val();
			var question_value = jQuery.parseJSON(questions_id);
			for(var i=0; i<question_value.length;i++){
				var question_checked=jQuery('input[name="qoption'+i+'[]"]').is(':checked');
				if(question_checked==false){
					checkquestion=false;
					jQuery('input[name="qoption'+i+'[]"]').focus();
					break;
				}
				else
					checkquestion=true;
			}
  								
			if(checkquestion==false) {
				alert('<?php echo JText::_("CHOOSE_ANSWER_FIRST"); ?>');
				return false;
			} else {
				
				nextslide('next');
			}
			
			
						
		});



		function nextslide(type)
		{
			var expiredtime;
			var buttontype=type;
			var answers = new Array();
			var resultcats_ids = new Array();
			if(secs>0)
				expiredtime=0;
			else
				expiredtime=1;
					
			nextvariable=parseInt(nextvariable+parseInt(paging));
							
			t_oggle=false;
			t_oggle1=false;

			var questions_id=vg('input[name="questions_id"]').val();
			var question_value = jQuery.parseJSON(questions_id);
						
 
			for(var i=0; i<question_value.length;i++){
				var answers1 = new Array();
				var resultcats_ids1 = new Array();

				jQuery('input[name="qoption'+i+'[]"]:checked').each(function() {
					answers1.push(jQuery(this).val());

					_id = jQuery(this).attr('id');
					_catid = jQuery('#ids_' + _id).val();
					resultcats_ids1.push(_catid);

					// 計算總數
					_now_val = parseInt(jQuery('#score_' + _catid).val());
					jQuery('#score_' + _catid).val((_now_val + 1));
					jQuery('#score_text_' + _catid).text((_now_val + 1));

					if ((_now_val + 1) <= 9) {
						jQuery('#score_img_' + _catid).attr("src", "<?php echo JURI::root(); ?>components/com_vquiz/assets/images/libra/"+ jQuery('#imgcode_' + _catid).val() + "_" + (_now_val + 1) + ".png");
					}

				});
				answers.push(answers1);
				resultcats_ids.push(resultcats_ids1);
			}

			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'nextslide', 'tmpl':'component','id':jQuery('input[name="id"]').val() ,'qid':jQuery('input[name="questions_id"]').val(), 'aid[]':answers,'resultcats_ids[]':resultcats_ids,'limit':nextvariable,'buttontype':buttontype,'expiredtime':expiredtime,'qspenttime':secs, "<?php echo JSession::getFormToken(); ?>":1},
 
				beforeSend: function()	{
					vg('.slides').toggle('slide', { direction: 'right' }, 100);
					vg(".poploadingbox").css('display','inline-block');
				},
				complete: function()	{
					vg(".poploadingbox").hide();
				},

				success: function(data)
				{
					last=false;
					if(data.result=="success")	{
									
						if(data.skip_slide==1 && data.limit>=0)
							nextvariable=data.limit;
						else if(data.limit>0)
							nextvariable=data.limit;

						executesuccess(data);
									
						vg('#completedslider > span .qcount').html(nextvariable);
 
									
						if(data.question_limit==1)
							var qetime=data.item[0].question_timelimit;
						else
							var qetime=0;
									
						var q_timeformate=data.item[0].questiontime_parameter;
											
 
						if(qetime<=0){
							vg('.question_time_div').css('display','none');
						}
						else{
							vg('.question_time_div').css('display','inline-block');
						}

											
						switch (q_timeformate) {
							case 'seconds':
								var qsecond=1;
								break;
							case 'minutes':
								var qsecond=60;
								break;
							default:
								var qsecond=0;
						}
											
						var question_times=qsecond*qetime;
 
						if(question_times==0 || data.quespenttime==-1 || data.quespenttime=='empty'){
						}
						else if(data.idexist==1 && data.ansid==data.lastitem)
						{
							qcountdown(stortime);
							lastelementcheck=false;
						}
						else if(data.idexist==1 && data.ansid>=0)
						{
							qcountdown(data.quespenttime);
						}
						else{
							qcountdown(question_times);
						}

					}
											
											
					else if(data.result=="endquiz")	{
										
						result_status=data.result_status;
										
										
						if(data.skip_slide=1 && data.limit>=0){
							nextvariable=data.limit;
						}
										
						if(data.limit>0)
							nextvariable=data.limit;
										
						vg('#completedslider > span .qcount').html(nextvariable);
  
						vg('.text').remove();
						vg('.type').remove();
						vg('.resultpreview').show();
						vg('.sharebutton').show();
						vg('.slides>.slide').remove();
						if(data.score_message){
							vg('#message').append('<div class="score_message">'+data.score_message+'</div>');
						}
//						vg('.textdisplay').append('<div class="text">'+data.text+'</div>');
											
						
						vg("#quiz_explanation").html(data.quiz_explanation);
						vg("#quiz_explanation").show();
 
						var optiontypescore=data.optiontypescore;
						var livescore=data.livescore;
						var maxscore=data.maxscore;
						if(optiontypescore==1)
							var live_html=livescore+'/'+maxscore;
						else
							var live_html=livescore;
									
									


						vg('.nextbutton').hide();
						vg('.slides_part').hide();

 	
						last = true;
					} else {
						alert(data.error);
					}
				}
			});

		}



	});

</script> 

<style>


</style>

<div class="main_box">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="middle_box">
		<div id="dialogbox" class="dialogbox">
			
			<div class="poploadingbox">
				<?php echo '<img src="' . JURI::root() . '/components/com_vquiz/assets/images/loading.gif"   />' ?>
			</div>

			<div class="quest_head">
				<h1><?php echo $this->fitem->quizzes->quizzes_title; ?></h1>
			</div>


			<!--狀態bar 開始 -->
			<div class="photopath">

				<div  class="timediv">

					<div id="completedslider" class="show_page"><span class="headertime">
							<?php echo JText::_('COMPLETED'); ?></span>
						<span class="completed_span">
							<span class="qcount">0</span><?php echo JText::_('OF'); ?><?php echo $this->fitem->totalq; ?>
						</span>
					</div>
				</div>

			</div>
			<!--狀態bar 結束 -->

			<div class="desc_part" >
				<div class="desc">
					<?php echo $this->fitem->quizzes->description; ?>
				</div>
				<div id="startbutton">
					<button type="button"  class="startbutton btn"><?php echo JText::_("START_BUTTON") ?></button>
				</div>
			</div>

			<div class="score_part" >
				<div class="libra">
					<div clss="image_list">
						<img class="gif_img" src="<?php echo JURI::root(); ?>components/com_vquiz/assets/images/libra/left_gif.gif">
						<img id="score_img_<?php echo $this->fitem->option[0][0]->resultcats_ids; ?>" class="machine_left" src="<?php echo JURI::root(); ?>components/com_vquiz/assets/images/libra/left_0.png">
						<img id="score_img_<?php echo $this->fitem->option[0][1]->resultcats_ids; ?>" class="machine_right" src="<?php echo JURI::root(); ?>components/com_vquiz/assets/images/libra/right_0.png">
						<img class="gif_img" src="<?php echo JURI::root(); ?>components/com_vquiz/assets/images/libra/right_gif.gif">
					</div>
					<div style="clear:both;">&nbsp;</div>
					<div class="score">
						<div class="score_item">
							<?php echo $resultcats[$this->fitem->option[0][0]->resultcats_ids]; ?>：
							<span id="score_text_<?php echo $this->fitem->option[0][0]->resultcats_ids; ?>">0</span>
							<input type="hidden" id="score_<?php echo $this->fitem->option[0][0]->resultcats_ids; ?>" value="0">
							<input type="hidden" id="imgcode_<?php echo $this->fitem->option[0][0]->resultcats_ids; ?>" value="left">
						</div>

						<div class="score_item">
							<?php echo $resultcats[$this->fitem->option[0][1]->resultcats_ids]; ?>：
							<span id="score_text_<?php echo $this->fitem->option[0][1]->resultcats_ids; ?>">0</span>
							<input type="hidden" id="score_<?php echo $this->fitem->option[0][1]->resultcats_ids; ?>" value="0">
							<input type="hidden" id="imgcode_<?php echo $this->fitem->option[0][1]->resultcats_ids; ?>" value="right">
						</div>
					</div>
				</div>
			</div>


			<!-- 題目 開始 -->
			<div class="slides_part" >
				<div class="slides">
					<div class="slide">
						<div class="quiz-ques">
							<?php
							for ($j = 0; $j < count($this->fitem->ques); $j++) {
								echo $this->fitem->ques[$j]->qtitle;
								array_push($questions_array, $this->fitem->ques[$j]->id);
								?>
								<div class="q-options">
									<ul>
										<?php for ($i = 0; $i < count($this->fitem->option[$j]); $i++) { ?>
											<li>
												
												
												<input type="radio" name="qoption<?php echo $j ?>[]" id="<?php echo "r" . $j . $i; ?>"  value="<?php echo $this->fitem->option[$j][$i]->id; ?>"  />
												<label for="<?php echo "r" . $j . $i; ?>" style="display:inline;"></label>
												<input type="hidden"id="<?php echo "ids_r" . $j . $i; ?>" value="<?php echo $this->fitem->option[$j][$i]->resultcats_ids; ?>"  />
												
												<?php echo $this->fitem->option[$j][$i]->qoption; ?>
											</li>
										<?php } ?>
									</ul>
								</div>
								<?php
							}
							$questions_array_json = json_encode($questions_array);
							?>
						</div>


					</div>
				</div>

				<div class="slide"></div>

		


			</div>
			<!-- 題目 結束 -->


			<div id="message"></div>

			<!--按鈕列  開始 -->
			<div class="allbutton">


				<div id="nextbutton" class="nebutton">
                    <button type="button" class="btn nextbutton"><?php echo JText::_("SAVE_NEXT") ?></button>
				</div>

            </div>
			<!--按鈕列  結束 -->



			<!--測驗結果解說 -->
			<div id="quiz_explanation" style="display:none;">
			</div>

		</div>
	</div>
</div>

<input type="hidden" name="option" value="com_vquiz" /> 
<input type="hidden" name="id" value="<?php echo JRequest::getInt('id', 0); ?>" />
<input type="hidden" name="questions_id" id="questions_id" value='<?php echo $questions_array_json; ?>'/>
<input type="hidden" name="optinscoretype" id="optinscoretype" value='<?php echo $this->fitem->quizzes->optinscoretype; ?>'/>
<input type="hidden" name="view" value="quizmanager" />
<input type="hidden" name="task" value="" />  
