<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHTML::_('form.token');

$session = JFactory::getSession();
if ($session->has('quizoptions')) {
	$session->clear('quizoptions');
	$session->clear('flag');
	$session->clear('fqid');
	$session->clear('random_quizoption');
	$session->clear('guest_result_id');
}
$document = JFactory::getDocument();
$app = JFactory::getApplication();
$title = $this->fitem->quizzes->quizzes_title;
$document->setTitle($title . ' - ' . $app->getCfg('sitename'));

$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/responsive_layout.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/popup.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/jquery.countdownTimer.css');
//$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/smoothness.css');
$document->addStyleSheet(JURI::root() . 'components/com_vquiz/assets/css/jquery-ui.css');


//  $document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/html2canvas.js');
  $document->addScript('components/com_vquiz/assets/js/jquery.countdownTimer.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/addthis.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/googlemap.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery.plugin.html2canvas.js');
  $document->addScript(JURI::root().'components/com_vquiz/assets/js/popup.js');

//$document->addScript(JURI::root() . 'components/com_vquiz/assets/js/all_compress.js');

$user = JFactory::getUser();
$questions_array = array();
$optionns_array = array();
?>
<!--<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>-->
<!--<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/StackBlur.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>-->
<!--<script type="text/javascript" src="http://gabelerner.github.io/canvg/canvg.js"></script> -->

<script src="components/com_vquiz/assets/js/Chart.js"></script>

<script type="text/javascript">
//	var vg=jQuery.noConflict();
	var vg=jQuery;
	var result_status=1;
	var t_oggle=false;
	var t_oggle1=false;
	var resultimage;
	var secs;
	var totalsec;
	var last = false;
	var first=true;
	var kk=false;
	var cki=false;
	var flag=false;
	var flag_count=0;
	var stortime=0;
	var lastelementcheck=false;
	var paging='<?php echo $this->fitem->quizzes->paging_limit > 1 ? $this->fitem->quizzes->paging_limit : 1; ?>';

	function validate() {
		var email = document.getElementById('email');
		var remail = document.getElementById('remail');
		var certificate_name = document.getElementById('certificate_name');
		if (certificate_name.value == '') {
			alert('<?php echo JText::_("PLEASE_ENTER_YOUR_CERTIFICATE_NAME") ?>');
			certificate_name.focus();
			return false;
		}
		if (email.value == '') {
			alert('<?php echo JText::_("PLEASE_ENTER_YOUR_EMAIL_ID") ?>');
			email.focus();
			return false;
		}
		var reEmail = /^([\w\.\-]+)@([\w\-]+)((\.([a-zA-z]){2,3})+)$/;
		if (!reEmail.test(email.value)) {
			alert('<?php echo JText::_("INVALID_EMAIL_ID") ?>');
			email.focus();
			return false;
		}
 
	}
 
	vg(document).ready(function() {
		// 初始化
		<?php if ($this->fitem->quizzes->desc_button == 1) { ?>
			vg(".photopath").hide();
			vg(".slides_part").hide();
			vg(".allbutton").hide();
			vg(".desc_part").show();
		<?php } else { ?>
			vg(".photopath").show();
			vg(".slides_part").show();
			vg(".allbutton").show();
			vg(".desc_part").hide();
		<?php } ?>


		vg( ".startbutton" ).bind( "click", function() {
			vg(".desc_part").slideUp(500);
			vg(".photopath").show();
			vg(".slides_part").fadeIn(1000);
			vg(".allbutton").show();
	   });


	   // 新增進度bar
		vg( "#progressbar" ).progressbar({
			value: 0
		});


		
		vg("#downlad_certificate").click(function () {
			certificate_text();
			vg('#chardiv').css('display','none');
			vg('#certificate_send_div').css('display','block');
		});
		
		function certificate_text()
		{
			if(validate() == false)
				return false;
				
			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'certificate_text','optinscoretype':jQuery('input[name="optinscoretype"]').val(),'certificate_name':jQuery('input[name="certificate_name"]').val(), 'tmpl':'component',"<?php echo JSession::getFormToken(); ?>":1},

				beforeSend: function()	{
					vg(".poploadingbox").css('display','inline-block');
				},
				complete: function()	{
					vg(".poploadingbox").hide();
				},
						  
				success: function(data)
				{
					var target1 = data.result;
					vg('#certificate_send_div').html(target1);
					vg('#chardiv').css('display','none');
					var target = vg('#certificate_send_div');
						 

					html2canvas(target, {
						onrendered: function(canvas) {
							var data = canvas.toDataURL("image/png");
							var a_cerificate = vg("<a>").attr("href", data).attr("download","Certificate").appendTo("body");
							a_cerificate[0].click();
							a_cerificate.remove();
						},
						timeout:0,
						 
					});
						  
				}
 				

			});

		}
		
		
		vg(".close_pop").click(function () {
			vg('#chardiv').css('display','block');
			vg('#certificate_send_div').css('display','none');
		});
			
			
		vg("#takeshot_result").click(function () {
			var target = vg('#takesnapshot_div');
			svgToCanvas(target);
		});
			
		function svgToCanvas (targetElem) {
			var nodesToRecover = [];
			var nodesToRemove = [];
			
			var svgElem = targetElem.find('svg');
			
			svgElem.each(function(index, node) {
				var parentNode = node.parentNode;
				var svg = parentNode.innerHTML;
			
				var canvas = document.createElement('canvas');
			
				canvg(canvas, svg);
			
				nodesToRecover.push({
					parent: parentNode,
					child: node
				});
				parentNode.removeChild(node);
			
				nodesToRemove.push({
					parent: parentNode,
					child: canvas
				});
			
				parentNode.appendChild(canvas);
			});
			
			html2canvas(targetElem, {
				onrendered: function(canvas) {
					var ctx = canvas.getContext('2d');
					ctx.webkitImageSmoothingEnabled = false;
					ctx.mozImageSmoothingEnabled = false;
					ctx.imageSmoothingEnabled = false;
					var data = canvas.toDataURL("image/png");
					var a_takeshot = vg("<a>").attr("href", data).attr("download","Result_Snap_Shot").appendTo("body");
					a_takeshot[0].click();
					a_takeshot.remove();
				},
				timeout:0,
 
			});
		}
						
	});
			 

	function executesuccess(data)
	{

		flag=true;
		var checks;
		
		var optiontypescore=data.optiontypescore;
		var livescore=data.livescore;
		var maxscore=data.maxscore;
		if(optiontypescore==1)
			var live_html=livescore+'/'+maxscore;
		else
			var live_html=livescore;
		
		vg('#live_score >.live_score').html(live_html);
		var nextid=data.item.id;
		var ck=data.checked;
		var optiontype=data.item.optiontype;
 
		var questions_array=new Array();
		var html = '<div class="quiz-ques">';
		for( var j =0;j<data.item.length;j++){
			
			html +='<div class="qti">'+data.item[j].qtitle+'</div>';
			
			questions_array.push(data.item[j].id);
			
			var optiontype=data.item[j].optiontype;
			
			html +='<div class="q-options"><ul>';
			for( var i =0;i<data.answer[j].length;i++){
				
				if (jQuery.inArray(data.answer[j][i].id, ck) != -1)
					checks ='checked="checked"';
				else
					checks = '';

				html += '<li><input type="radio" name="qoption'+j+'[]" id="r'+j+i+'" value="'+data.answer[j][i].id+'" '+checks+' /><label for="r'+j+i+'" style="display: inline;"></label><input type="hidden" id="ids_r'+j+i+'"  value="'+data.answer[j][i].resultcats_ids+'"  />'+data.answer[j][i].qoption+'</li>';
				
						
 
			}
			html +='</ul></div>';
		}
		
		html += '</div>';

		vg("#next_explanation").html(data.item[0].explanation);
		
		var json_question=JSON.stringify(questions_array);
		
		vg('input[name="questions_id"]').val(json_question);

		<?php if ($this->fitem->quizzes->show_explanation == 0){ ?>											
		vg('.slides').toggle('slide', { direction: 'left' }, 700);
					
		if(data.first == 1)
			vg('#backbutton').hide();
		else
			vg('#backbutton').css('display','inline-block');

		<?php } ?>

		vg('.slides').append('<div class="slide"></div>');
		vg('.slides>.slide:first').html(html);
	}


	function jschart(data) {
		
		var ctx = document.getElementById("chart-area").getContext("2d");
		var myDoughnut = new Chart(ctx).<?php  echo $this->fitem->quizzes->graph_type; ?>(data.chartdata, {
			responsive : true,
			animationEasing: "easeOutQuart"
		});

	}

	
	
	vg(function() {
 
		var scores;
		var textscore;
		var nextvariable=0;
		var skipcount=0;
		var questiontime=vg('input[name="quetime"]').val();
		var totaltime= vg('input[name="totaltime"]').val();
					
		if(totaltime>0)
			tcountdown(totaltime);
					
		if(questiontime>0)
			qcountdown(questiontime);


		jQuery("#flag").click(function(){

			if(flag==true || flag_count==0){
				flag_count=parseFloat(flag_count+1);
				flag=false;
				flagcount(flag_count);
				alert('<?php echo JText::_('MARK_FALGE'); ?>');
			}
			else{
				flag_count=parseFloat(flag_count);
			}
		});

				

		function flagcount(flag_count)
		{
			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'flagcount', 'tmpl':'component','count':flag_count,'flagqid':jQuery('input[name="questions_id"]').val(),"<?php echo JSession::getFormToken(); ?>":1},
 				
				success: function(data)
				{
					if(data.result=="success")	{
						 
					}
				}
			});

		}
 
 
		jQuery("#skipbutton").click(function(){
			nextslide('skip');
			vg('#showcorrectans').hide();
			vg('#showexplation').hide();

		});

		jQuery(".q-options input[name^='qoption']").live("click", function(){ //點選選項直接進入下一步
		    var checkquestion=false;
			var questions_id=vg('input[name="questions_id"]').val();
			var question_value = jQuery.parseJSON(questions_id);
			for(var i=0; i<question_value.length;i++){
				var question_checked=jQuery('input[name="qoption'+i+'[]"]').is(':checked');
				if(question_checked==false){
					checkquestion=false;
					jQuery('input[name="qoption'+i+'[]"]').focus();
					break;
				}
				else
					checkquestion=true;
			}
  								
			if(checkquestion==false) {
				alert('<?php echo JText::_("CHOOSE_ANSWER_FIRST"); ?>');
				return false;
			} else {
				<?php if ($this->fitem->quizzes->show_explanation == 1){ ?>
//				vg(".slides_part").slideUp(400);
				vg(".slides_part").hide();
				vg(".allbutton").hide();
				vg('#showexplation').fadeIn(2000);
				vg(".box").hide();
			<?php } ?>
				
				nextslide('next');
			}
			
					
			vg('#showcorrectans').hide();
		});

		jQuery(".nextbutton").click(function(){
			var checkquestion=false;
			var questions_id=vg('input[name="questions_id"]').val();
			var question_value = jQuery.parseJSON(questions_id);
			for(var i=0; i<question_value.length;i++){
				var question_checked=jQuery('input[name="qoption'+i+'[]"]').is(':checked');
				if(question_checked==false){
					checkquestion=false;
					jQuery('input[name="qoption'+i+'[]"]').focus();
					break;
				}
				else
					checkquestion=true;
			}
  								
			if(checkquestion==false) {
				alert('<?php echo JText::_("CHOOSE_ANSWER_FIRST"); ?>');
				return false;
			} else {
				<?php if ($this->fitem->quizzes->show_explanation == 1){ ?>
//				vg(".slides_part").slideUp(400);
				vg(".slides_part").hide();
				vg(".allbutton").hide();
				vg('#showexplation').fadeIn(2000);
				vg(".box").hide();
			<?php } ?>
				
				nextslide('next');
			}
			
					
			vg('#showcorrectans').hide();
			
			
						
		});


		jQuery(".shownextbutton").click(function(){
			vg(".slides_part").slideDown(1000);
			vg(".allbutton").show();
			// vg('#showexplation').fadeOut(500);
			vg('#showexplation').hide();
			vg('#explanation').html(vg('#next_explanation').html());
			vg(".box").slideDown(1000);
		});

 
		function nextslide(type)
		{
			var expiredtime;
			var buttontype=type;
			var answers = new Array();
			var resultcats_ids = new Array();
			if(secs>0)
				expiredtime=0;
			else
				expiredtime=1;
					
			nextvariable=parseInt(nextvariable+parseInt(paging));
							
			t_oggle=false;
			t_oggle1=false;
			jQuery('#showcorrectans').find('a').html('<span><?php echo JText::_('SHOW_PREVIOUS_ANSWER') ?></span>');

			var questions_id=vg('input[name="questions_id"]').val();
			var question_value = jQuery.parseJSON(questions_id);
						
 
			for(var i=0; i<question_value.length;i++){
				var answers1 = new Array();
				var resultcats_ids1 = new Array();

				jQuery('input[name="qoption'+i+'[]"]:checked').each(function() {
					answers1.push(jQuery(this).val());

					_id = jQuery(this).attr('id');
					resultcats_ids1.push(jQuery('#ids_' + _id).val());
				});
				answers.push(answers1);
				resultcats_ids.push(resultcats_ids1);
			}

			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'nextslide', 'tmpl':'component','id':jQuery('input[name="id"]').val() ,'qid':jQuery('input[name="questions_id"]').val(), 'aid[]':answers,'resultcats_ids[]':resultcats_ids,'limit':nextvariable,'buttontype':buttontype,'expiredtime':expiredtime,'qspenttime':secs, "<?php echo JSession::getFormToken(); ?>":1},
 
				beforeSend: function()	{
				<?php if ($this->fitem->quizzes->show_explanation == 0){ ?>
					vg('.slides').toggle('slide', { direction: 'right' }, 100);
				<?php } ?>
					vg(".poploadingbox").css('display','inline-block');
				},
				complete: function()	{
					vg(".poploadingbox").hide();
				},

				success: function(data)
				{
					last=false;

					if(data.result=="success")	{
						<?php if ($this->fitem->quizzes->show_correctans == 1) { ?>
							vg('#showcorrectans').show();
						<?php } ?>
									
						if(data.skip_slide==1 && data.limit>=0)
							nextvariable=data.limit;
						else if(data.limit>0)
							nextvariable=data.limit;

						executesuccess(data);
									
						vg('#completedslider > span .qcount').html(nextvariable);


						// 新增進度bar
						_value = (data.limit / vg("#totalq").val()) * 100;
						vg( "#progressbar" ).progressbar({
							value: _value
						});

									
						if(data.question_limit==1)
							var qetime=data.item[0].question_timelimit;
						else
							var qetime=0;
									
						var q_timeformate=data.item[0].questiontime_parameter;
											
 
						if(qetime<=0){
							vg('.question_time_div').css('display','none');
						}
						else{
							vg('.question_time_div').css('display','inline-block');
						}

											
						switch (q_timeformate) {
							case 'seconds':
								var qsecond=1;
								break;
							case 'minutes':
								var qsecond=60;
								break;
							default:
								var qsecond=0;
						}
											
						var question_times=qsecond*qetime;
 
						if(question_times==0 || data.quespenttime==-1 || data.quespenttime=='empty'){
							clearTimeout(qtimer);
						}
						else if(data.idexist==1 && data.ansid==data.lastitem)
						{
							clearTimeout(qtimer);
							qcountdown(stortime);
							lastelementcheck=false;
						}
						else if(data.idexist==1 && data.ansid>=0)
						{
							clearTimeout(qtimer);
							qcountdown(data.quespenttime);
						}
						else{
							clearTimeout(qtimer);
							qcountdown(question_times);
						}

					}
											
											
					else if(data.result=="endquiz")	{

						result_status=data.result_status;
										
										
						vg('#backbutton').css('display','inline-block');
										
						if(data.skip_slide=1 && data.limit>=0){
							nextvariable=data.limit;
						}
										
						if(data.limit>0)
							nextvariable=data.limit;
										
						vg('#completedslider > span .qcount').html(nextvariable);

						_value = (data.limit / vg("#totalq").val()) * 100;
						vg( "#progressbar" ).progressbar({
							value: _value
						});
  
						vg('.text').remove();
						vg('.type').remove();
						vg('.resultpreview').show();
						vg('.sharebutton').show();
						vg('.slides>.slide').remove();
						if(data.score_message){
							vg('#message').append('<div class="score_message">'+data.score_message+'</div>');
						}
//						vg('.textdisplay').append('<div class="text">'+data.text+'</div>');
											
											
						
						<?php if ($this->fitem->quizzes->display_userscore == 1) {  ?>
						vg(".box").show();
						<?php if ($this->fitem->quizzes->graph_type == "Thermometer" || $this->fitem->quizzes->graph_type == "Star" || trim($this->fitem->quizzes->graph_type) == "m2f3") { ?>
							vg(".box").html(data.chartdata);
						<?php } else { ?>
							jschart(data);
						<?php } } ?>

						<?php if ($this->fitem->quizzes->show_explanation == 1){ ?>
						vg('#explanation_button').text("看結果");
						vg(".box").hide();
						<?php } ?>
						

						// 測驗的結果解說
						vg("#quiz_explanation").html(data.quiz_explanation);
						vg("#quiz_explanation").fadeIn(1000);
						vg(".allbutton").hide();

						if (data.chartdata_more) {
							jQuery("#quiz_explanation").html(data.chartdata_more + "<br>" + jQuery("#quiz_explanation").html());
						}
									
 
						var optiontypescore=data.optiontypescore;
						var livescore=data.livescore;
						var maxscore=data.maxscore;
						if(optiontypescore==1)
							var live_html=livescore+'/'+maxscore;
						else
							var live_html=livescore;
									
						vg('#live_score >.live_score').html(live_html);

						vg('.chartscore').show();
						vg('#submitscore').show();
						//									vg('.closeButton').show();	// 不顯示關閉按鈕
						vg('.takeshot').show();
						vg('.nextbutton').hide();
						vg('#skipbutton').hide();
						vg('#flag').hide();
									
									
						<?php if ($this->fitem->quizzes->show_correctans == 1): ?>
							vg('#showcorrectans').show();
						<?php endif ?>
 	
						last = true;
					}
					else	{

						alert(data.error);
					}
				}
			});

		}




		jQuery(".backbutton").click(function(){
			if(lastelementcheck==false){
				stortime=secs;
				lastelementcheck=true;
			}
					
			if(totalsec<=0)
			{
				alert('<?php echo JText::_('Total Time Limits End') ?>');
				return false;
			}
 

			if(last==true)	{
				jQuery('.chartscore').hide();
				jQuery('#skipbutton').show();
				jQuery('.nextbutton').show();
				jQuery('#submitscore').hide();
				jQuery('#addthis_container').hide();
				jQuery('.resultpreview').hide();
			}

			backslide();
						
		});

		function backslide()
		{
			nextvariable=parseInt(nextvariable-parseInt(paging));
			if(nextvariable<0)nextvariable=0;

			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'backslide', 'tmpl':'component','id':jQuery('input[name="id"]').val(),'limit':nextvariable,"<?php echo JSession::getFormToken(); ?>":1},

				beforeSend: function()	{
				<?php if ($this->fitem->quizzes->show_explanation == 0){ ?>
					vg('.slides').toggle('slide', { direction: 'right' }, 100);
				<?php } ?>
					vg(".poploadingbox").show();
				},

				complete: function()	{
					vg(".poploadingbox").hide();
				},

				success: function(data)
				{
					if(data.result=="success")	{
						kk=true;
						var quespenttime=data.quespenttime;

						if( quespenttime==-1 || quespenttime=='empty')
						{    	last=false;
							clearTimeout(qtimer);
							vg('.question_time_div').css('display','none');
						}
							
						else if(quespenttime>0)
						{    	last=false;
							vg('.question_time_div').css('display','inline-block');
							clearTimeout(qtimer);
							qcountdown(quespenttime);
								
						}
						else{
							alert('<?php echo JText::_('QUESTION_TIME_LIMIT_END') ?>');
							if(last==true)
								nextslide('skip');
							else
								nextvariable=parseInt(nextvariable+parseInt(paging));
									
							return false;
						}
							
						if(data.skip_slide=1 && data.limit>=0){
							nextvariable=data.limit;
						}
							
						executesuccess(data);
							 
						vg('#completedslider > span .qcount').html(nextvariable);

						_value = (data.limit / vg("#totalq").val()) * 100;
						vg( "#progressbar" ).progressbar({
							value: _value
						});
					}
					else	{
						alert(data.error);
					}
				}

			});

		}

 
		

		vg(".submitscore").click(function(){
			if(result_status==0){
				alert('<?php echo JText::_('SORRY_FOR_TAKE_CERTIFICATE') ?>');
				return false;
			}
			clearTimeout(qtimer);
			clearTimeout(timer);
			vg('#backbutton').hide();
			vg('#showcorrectans').hide();
			
			vg('#showexplation').hide();
//			vg('.explanation').hide();
			lightbox_submitscore();
		});

 
 
		/*Per Question Timer Countdown */

		var qh;
		var qm;
		var qs;
		var qtimer;
		var timer;

		function qcountdown(abc){
			secs=abc;
			qh=Math.floor(secs/3600);
			var hr=secs%3600;
			qm=Math.floor(hr/60);
			var mr=secs%60;
			qs=Math.floor(mr);

			var qht=qh>0?qh+'h&nbsp;:&nbsp;':'';
					
			var html='<span class="t_digit">'+qht+'</span><span class="t_digit">'+qm+'m&nbsp;:&nbsp;</span>';
			html+=secs<10?'<span class="t_digit" style="color:red">'+qs+'s</span>':'<span class="t_digit">'+qs+'s</span>';
				
			vg('.question_time').html(html);

			if(secs<1){
				if(last==true)
					return false;
				nextslide('skip');
				clearTimeout(qtimer);
				return false;
			}
			secs--;
			qtimer=setTimeout(function () {
				qcountdown(secs);
			}, 1000);
		}

		/*End....*/

 
		/*TOtal Question Timer Countdown */
			
		function tcountdown(secs){
			totalsec=secs;
			var h=Math.floor(secs/3600);
			var hr=secs%3600;
			var m=Math.floor(hr/60);
			var mr=secs%60;
			var s=Math.floor(mr);
							
			var ht=h>0?h+'h&nbsp;:&nbsp;':'';
				
			var html='<span class="t_digit">'+ht+'</span><span class="t_digit">'+m+'m&nbsp;:&nbsp;</span>';
			html+=secs<60?'<span class="t_digit" style="color:red">'+s+'s</span>':'<span class="t_digit">'+s+'s</span>';
			vg('.total_time').html(html);
			
			if(secs<1){
				if(last==true)
					return false;
				nextvariable='<?php echo $this->fitem->totalq > 0 ? $this->fitem->totalq : 1 ?>'-parseInt(paging);
				nextslide('skip');
				clearTimeout(timer);
				return false;
			}

			secs--;
			timer=setTimeout(function () {
				tcountdown(secs);
			}, 1000);
		}

		/*End*/
			
			
		var urlsnap;
		vg("#addthis_container> .addthis_container").mouseover(function () {
			var html='<canvas id="canvas"></canvas>';
			vg('#chart').html(html).css("display","none") ;
			var svg =vg('svg').parent().html();
			canvg('canvas', svg);
			canvas = document.getElementById("canvas");
			var resultimage = canvas.toDataURL("image/png");
			//window.open(resultimage, "_blank");
			share_snapshot(resultimage);
		});
				
		/*Share image Snapshot*/
		function share_snapshot(that)
		{

			jQuery.ajax(
			{
				url: "index.php",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'share_snapshot', 'tmpl':'component','img_data':that,"<?php echo JSession::getFormToken(); ?>":1},

				success: function(data)
				{
					if(data.result=="success")	{
						var x;
					}
					else	{
						alert(data.error);
					}
				}

			});

		}


	});

</script> 

<style>
	
	.main_box .zone{
		width:300px; height:300px;
	}
</style>

<div class="main_box">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="middle_box">
		<div id="dialogbox" class="dialogbox">
			<div class="closeButton" style="display:none;">
				<a href="<?php echo JURI::root(); ?>"><button class="btn" ><?php echo JText::_("X") ?></button></a>
			</div>
			<div class="poploadingbox">
				<?php echo '<img src="' . JURI::root() . '/components/com_vquiz/assets/images/loading.gif"   />' ?>
			</div>

			<div class="quest_head">
				<h1><?php echo $this->fitem->quizzes->quizzes_title; ?></h1>
			</div>


			<!--狀態bar 開始 -->
			<div class="photopath">

				<div  class="timediv">

					<div id="completedslider" class="show_page"><span class="headertime">
							<?php echo JText::_('COMPLETED'); ?></span>
						<span class="completed_span">
							<span class="qcount">0</span><?php echo JText::_('OF'); ?><?php echo $this->fitem->totalq; ?>
						</span>
					</div>

					<div id="progressbar"></div>
					<input type="hidden" id="totalq" value="<?php echo $this->fitem->totalq; ?>">
				</div>

			</div>
			<!--狀態bar 結束 -->

			<div class="desc_part" >
				<div class="desc">
					<?php echo $this->fitem->quizzes->description; ?>
				</div>
				<div id="startbutton">
					<button type="button" class="startbutton btn"><?php echo JText::_("START_BUTTON") ?></button>
				</div>
			</div>


			<!-- 題目 開始 -->
			<div class="slides_part" >
				<div class="slides">
					<div class="slide">
						<div class="quiz-ques">
							<?php
							for ($j = 0; $j < count($this->fitem->ques); $j++) {
								echo $this->fitem->ques[$j]->qtitle;
								array_push($questions_array, $this->fitem->ques[$j]->id);
								?>
								<div class="q-options">
									<ul>
										<?php for ($i = 0; $i < count($this->fitem->option[$j]); $i++) { ?>
											<li>
												
												
												<input type="radio" name="qoption<?php echo $j ?>[]" id="<?php echo "r" . $j . $i; ?>"  value="<?php echo $this->fitem->option[$j][$i]->id; ?>"  />
												<label for="<?php echo "r" . $j . $i; ?>" style="display:inline;"></label>
												<input type="hidden"id="<?php echo "ids_r" . $j . $i; ?>" value="<?php echo $this->fitem->option[$j][$i]->resultcats_ids; ?>"  />
												
												<?php echo $this->fitem->option[$j][$i]->qoption; ?>
											</li>
										<?php } ?>
									</ul>
								</div>
								<?php
							}
							$questions_array_json = json_encode($questions_array);
							?>
						</div>


					</div>
				</div>

				<div class="slide"></div>

				<div class="chartscore" id="takesnapshot_div">

					<?php if ($this->fitem->quizzes->show_correctans != 0) { ?>
						<div class="textdisplay"></div>
					<?php } ?>

					<div class="rdisplay" id="chardiv"></div>
					<div id="certificate_send_div"></div>

				</div>


			</div>
			<!-- 題目 結束 -->


			<div id="message"></div>

			<!--按鈕列  開始 -->
			<div class="allbutton">

				<?php if ($this->fitem->quizzes->prev_button == 1): ?>
					<div id="backbutton" class="babutton">
						<button type="button"  class=" btn backbutton"><?php echo JText::_("BACK") ?></button>
					</div>
				<?php endif; ?>


				<div id="nextbutton" class="nebutton">
                    <button type="button" class="btn nextbutton"><?php echo JText::_("SAVE_NEXT") ?></button>
				</div>


				<div id="submitscore" style="display:none;" class="submitscore">
					<?php if ($this->configuration->get_certificate == 1): ?>
						<button  type="button" class="btn"><?php echo JText::_("GET_CERTIFICATE") ?></button>
					<?php endif; ?>
				</div>


				<div class="resultpreview" style=" display:none;">
					<?php if ($this->configuration->results_preview == 1): ?>
						<a class="modal" id="modal" title="Select" href="<?php echo JURI::root() . 'index.php?option=com_vquiz&view=quizmanager&layout=quizresult&tmpl=component'; ?>'" rel="{handler: 'iframe', size: {x: 800, y: 500}}">
							<input class="btn btn-success" type="button" value="<?php echo JText::_('SHOW_RESULT_PREVIEW'); ?>" /></a>
					<?php endif; ?>
				</div>


            </div>
			<!--按鈕列  結束 -->


			<!--解說-->
			<?php if ($this->fitem->quizzes->show_explanation == 1): ?>
				<div id="showexplation"  style="display:none;">

					<div id="explanation" class="explanation first-explanation">
						<?php echo $this->fitem->ques[0]->explanation; ?>
					</div>

					<div id="next_explanation" style="display:none;">
						&nbsp;
					</div>

					<?php if ($this->fitem->quizzes->prev_button == 1): ?>
						<div id="backbutton" class="babutton">
							<button type="button"  class=" btn backbutton"><?php echo JText::_("BACK") ?></button>
						</div>
					<?php endif; ?>

					<div id="shownextbutton" class="nebutton">
						<button id="explanation_button" type="button" class="btn shownextbutton"><?php echo JText::_("SAVE_NEXT") ?></button>
					</div>


				</div>
			<?php endif; ?>

			<!--圖表-->
			<div class="box" style="display:none;">
				<canvas id="chart-area" class="zone"></canvas>
			</div>

			<!--測驗結果解說 -->
			<div id="quiz_explanation" style="display:none;">
			</div>

		</div>
	</div>
</div>

<input type="hidden" name="option" value="com_vquiz" /> 
<input type="hidden" name="id" value="<?php echo JRequest::getInt('id', 0); ?>" />
<input type="hidden" name="questions_id" id="questions_id" value='<?php echo $questions_array_json; ?>'/>
<input type="hidden" name="optinscoretype" id="optinscoretype" value='<?php echo $this->fitem->quizzes->optinscoretype; ?>'/>
<input type="hidden" name="view" value="quizmanager" />
<input type="hidden" name="task" value="" />  


<!--Set Time in second to the browswer -->
<?php
$totime = $this->fitem->quizzes->total_timelimit;
$totimeformate = $this->fitem->quizzes->totaltime_parameter;

switch ($totimeformate) {
	case 'seconds':
		$seconds = 1;
		break;
	case 'minutes':
		$seconds = 60;
		break;
	default:
		$seconds = 0;
}
$total_times = $seconds * $totime;


$qetime = $this->fitem->quizzes->paging_limit > 1 ? 0 : $this->fitem->ques[0]->question_timelimit;
$q_timeformate = $this->fitem->ques[0]->questiontime_parameter;

switch ($q_timeformate) {
	case 'seconds':
		$qsecond = 1;
		break;
	case 'minutes':
		$qsecond = 60;
		break;
	default:
		$qsecond = 0;
}
$question_times = $qsecond * $qetime;
?>
<input type="hidden" name="quetime" value="<?php echo $question_times; ?>" />
<input type="hidden" name="totaltime" value="<?php echo $total_times; ?>" />  

<div id="fade" class="close_pop" onClick="lightbox_close4();"></div> 
<div id="light_submitscore" >
	<div class="score_submit">
		<div class="submit_score_header">

			<h2><?php echo JText::_("GET_CERTIFICATE") ?></h2>
			<a href="javascript:void(0);"  onclick="lightbox_close4();" class="closepop close_pop">X</a>
		</div>
		<div class="submit_score_inner">
			<div class="control-group">
				<div class="control-label">
					<label><?php echo JText::_("CERTIFICATE_NAME") ?><span class="star">&nbsp;*</span></label>
                </div>
                <div class="controls">
					<input type="text" name="certificate_name"  id="certificate_name" value="<?php if (!$user->get('guest')) echo $user->username; ?>" />
                </div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<label><?php echo JText::_("EMAIL_ADDRESS") ?><span class="star">&nbsp;*</span></label>
                </div>
                <div class="controls">
					<input type="text" name="email"  id="email" value="<?php if (!$user->get('guest')) echo $user->email; ?>" />
                </div>
			</div>
			<div class="control-group">

                <div class="dwnld_certificate">
					<?php if ($this->configuration->download_certificate == 1): ?>
						<button type="button" id="downlad_certificate" class="btn"><?php echo JText::_("DOWNLOAD_CERTIFICATE") ?></button>
					<?php endif ?>
                </div>
			</div>

		</div>

	</div>
</div>
