<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
$document->addScript(JURI::root().'components/com_vquiz/assets/js/html2canvas.js');
$document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery.plugin.html2canvas.js');

?>

<script type="text/javascript">
$(document).ready(function() {
	
		$(".print_button").click(function () {		
			//var target=$('#myIframe').contents().find("body").addClass("snapback");
			var target = $('.all');
				html2canvas(target, {
				onrendered: function(canvas) {
				var data = canvas.toDataURL();					
				var a = $("<a>").attr("href", data).attr("download","VquizSnapshot").appendTo("body");
				a[0].click();
				a.remove();
				//window.open(data,"_self");
				//screenshot(data);
				},
				timeout:0,
				//background:'#09AC1C'
				});
			});
	
 
		});
</script>

<form action="index.php?option=com_vquiz" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

 <div class="print_button"><input type="button" class="btn" value="<?php echo JText::_('SNAPSHOT');?>"/></div>
 
    <div class="all">
    <?php for( $i=0;$i<count($this->showresult->question_array);$i++){
 		$given_answer=explode(',',$this->showresult->givenanswer[$i]);
  		?>
    
            <div class="qu_list">
                <h4><?php echo $this->showresult->question[$i]->qtitle;?></h4>
            </div>
            
            <table border="0" width="100%" cellpadding="5" cellspacing="0">
                <tr>
                <th><?php echo JText::_('COUNT');?></th><th><?php echo JText::_('OPTIONS');?></th>
                
                <th>
				<?php
                 if($this->showresult->optiontypescore==1)
					 echo JText::_('YOUR_ANSWER');
				 elseif($this->showresult->optiontypescore==2){
					 echo JText::_("(Point)");
				 	 echo JText::_('YOUR_ANSWER');
				 }

					for($k=0;$k<count($given_answer);$k++){
						if ($given_answer[$k]==0)
						echo '<label style="padding-left:10px;font-size:9px">('.JText::_('SKIP_QUESTION').')</label>';
					}
				 ?>
                
                </th>
                </tr>
                
                
                <?php for( $j=0;$j<count($this->showresult->options[$i]);$j++){?>
                  
                    <tr>
                        <td><p><?php echo $j+1;?></p></td>
                        
                        <td><p><?php echo $this->showresult->options[$i][$j]->qoption;?></p></td>
                        
                        <td>
                        <p style="text-align:center;">
                         
                        
                        <?php 
						
						if($this->showresult->optiontypescore==1)
						{
 							if($this->showresult->options[$i][$j]->correct_ans==1){
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
							for($k=0;$k<count($given_answer);$k++){
							if ($given_answer[$k]==$this->showresult->options[$i][$j]->id and ($this->showresult->options[$i][$j]->correct_ans!=1) )
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/delete-icon.png" />';
							}
				 
						}
						
						elseif($this->showresult->optiontypescore==2)
						{
							for($k=0;$k<count($given_answer);$k++){
							echo $this->showresult->options[$i][$j]->options_score;
							if($this->showresult->options[$i][$j]->id==$given_answer[$k])
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
						}
                        ?>
                        
                        </p>
                        </td>
                        
                    </tr> 
                <?php }?>
            </table>    
            <?php }?>
    </div>
            
            <div class="print_button"><input type="button" class="btn" value="<?php echo JText::_('SNAPSHOT');?>"/></div>
 
</form>



