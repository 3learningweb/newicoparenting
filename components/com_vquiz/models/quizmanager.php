<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz

  # ------------------------------------------------------------------------

  # author    Team WDMtech

  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.

  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

  # Websites: http://www.wdmtech..com

  # Technical Support:  Forum - http://www.wdmtech.com/support-forum

  ----------------------------------------------------------------------- */

// No direct access

defined('_JEXEC') or die('Restricted access');



jimport('joomla.application.component.modellist');

jimport('joomla.filesystem.file');

jimport('joomla.filesystem.folder');

class VquizModelQuizmanager extends JModelList {

	var $_total = null;
	var $_pagination = null;

	function __construct() {

		parent::__construct();

		$db = JFactory::getDBO();

		$mainframe = JFactory::getApplication();

		$array = JRequest::getVar('cid', 0, '', 'array');

		$this->setId((int) $array[0]);





		// Get pagination request variables

		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');

		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');

		// In case limit has been changed, adjust it

		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);



		$this->setState('limit', $limit);

		$this->setState('limitstart', $limitstart);
	}

	function getTotal() {

		// Load the content if it doesn't already exist

		if (empty($this->_total)) {

			$query = $this->_buildQuery();

			$query .= $this->_buildFilter();

			$query .= $this->_buildOrderBy();



			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;
	}

	function getPagination() {

		// Load the content if it doesn't already exist

		if (empty($this->_pagination)) {

			jimport('joomla.html.pagination');

			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_pagination;
	}

	protected function getListQuery() {

		$itemid = JRequest::getInt('id', 0);



		$db = JFactory::getDBO();

		$query = $db->getQuery(true);

		$query->select('*');

		if (!empty($itemid))
			$query->from('#__vquiz_quizzes where Where published = 1 AND id = ' . $this->_db->quote($itemid) . '');

		else
			$query->from('#__vquiz_quizzes Where published = 1');



		return $query;
	}

	function setId($id) {

		$this->_id = $id;

		$this->_data = null;
	}

	function getFitem() {

		$session = JFactory::getSession();

		/* 		if($session->has('quizoptions')){

		  $session->clear('quizoptions');

		  $session->clear('flag');

		  $session->clear('fqid');

		  $session->clear('start_datetime');

		  $session->clear('random_quizoption');

		  $session->clear('guest_result_id');

		  } */

		$app = JFactory::getApplication();

		$lang = JFactory::getLanguage();

		$user = JFactory::getUser();

		$date = JFactory::getDate();

		$itemid = JRequest::getInt('id', 0);



		$query = 'select access from #__vquiz_quizzes';

		$where = array();

		$where[] = ' id = ' . $this->_db->quote($itemid);

		$where[] = ' published = 1';

		$where[] = ' access in (' . implode(',', $user->getAuthorisedViewLevels()) . ')';

		$where[] = ' startpublish_date <= ' . $this->_db->quote($date->format('Y-m-d')) . ' and (endpublish_date >= ' . $date->format('Y-m-d') . ' or endpublish_date=' . ('0000-00-00') . ')';



		if ($app->getLanguageFilter()) {

			$where[] = ' language in (' . $this->_db->quote($lang->getTag()) . ', ' . $this->_db->Quote('*') . ')';
		}

		$query .= ' where ' . implode(' and ', $where);

		$query .=' order by id asc';

		$this->_db->setQuery($query);
		$accescheck = $this->_db->loadResult();
		$starttime = $date->toUnix();
		$session->set('starttime', $starttime);

		$query = ' SELECT * FROM #__vquiz_quizzes where id = ' . $this->_db->quote($itemid) . ' and  published = 1 order by id asc';

		$this->_db->setQuery($query);

		$all = $this->_db->loadObject();





		$query = ' SELECT count(*) FROM #__vquiz_question where quizzesid = ' . $this->_db->quote($itemid) . ' and  published = 1';

		$this->_db->setQuery($query);

		$totalq_count = $this->_db->loadResult();



		if ($all->question_limit > 0 and $all->question_limit < $totalq_count) {

			$totalq = $all->question_limit;
		} else {
			$totalq = $totalq_count;
		}


		$this->_data = new stdClass();



		if (!empty($accescheck)) {

			$query = ' SELECT * FROM #__vquiz_question where quizzesid = ' . $this->_db->quote($itemid) . ' and  published = 1';



			if ($all->paging_limit > 1) {

				$limit = $all->paging_limit > 0 ? $all->paging_limit : 1;

				if ($all->random_question == 1)
					$query .=' order by RAND() desc limit  ' . $limit;

				else
					$query .=' order by ordering asc limit ' . $limit;
			}

			else {

				if ($all->random_question == 1)
					$query .=' order by RAND() desc limit 1';

				else
					$query .=' order by ordering asc limit 1';
			}



			$this->_db->setQuery($query);

			$this->_data->ques = $this->_db->loadObjectList();
		}

		else
			$this->_data->ques = null;



		for ($j = 0; $j < count($this->_data->ques); $j++) {

			$qustionid = $this->_data->ques[$j]->id;

			if (!empty($accescheck) and !empty($qustionid)) {

				$query = 'select * from #__vquiz_option where qid =' . $this->_db->quote($qustionid) . ' order by id asc';

				$this->_db->setQuery($query);

				$this->_data->option[$j] = $this->_db->loadObjectList();
			}

			else
				$this->_data->option = null;
		}



		$this->_data->quizzes = $all;

		$this->_data->totalq = $totalq;





		if (!$this->_data) {

			$this->_data = new stdClass();

			$this->_data->id = 0;

			$this->_data->qtitle = null;

			$this->_data->optiontypescore = null;
		}



		return $this->_data;
	}

	function flagcount() {

		$obj = new stdClass();

		$obj->result = "error";

		$session = JFactory::getSession();

		$questions_id = JRequest::getVar('flagqid', 0);

		$flageqid = json_decode($questions_id);





		if ($session->has('fqid')) {

			$fqid = $session->get('fqid');

			$total_flage = $fqid['qid'];
		} else {

			$fqid = array('qid' => array());
		}

		if ($flageqid) {

			for ($j = 0; $j < count($flageqid); $j++) {

				array_push($fqid['qid'], $flageqid[$j]);
			}
		}

		$session->set('fqid', $fqid);

		$obj->result = "success";





		return $obj;
	}




	function nextslide() {

		$session = JFactory::getSession();
		$itemid = JRequest::getInt('id', 0);
		$buttontype = JRequest::getVar('buttontype');

		$obj = new stdClass();
		$obj->result = "error";



		$limit = JRequest::getInt('limit', 0);


		$questions_id = JRequest::getVar('qid', '');
		$qid = json_decode($questions_id);

		$aid = JRequest::getVar('aid', 0, '', 'array');
		$resultcats_ids = JRequest::getVar('resultcats_ids', 0, '', 'array');
		$scores_ids = JRequest::getVar('scores_ids', 0, '', 'array');



		$expiredtime = JRequest::getInt('expiredtime');
		$qspenttime = JRequest::getInt('qspenttime');



		$query = ' SELECT * FROM #__vquiz_quizzes where id = ' . $this->_db->quote($itemid) . ' order by id asc';

		$this->_db->setQuery($query);

		$quizzesresult = $this->_db->loadObject();

		$quiz_categoryid = $quizzesresult->quiz_categoryid;
		$quizzesid = $quizzesresult->id;
		$quizzestitle = $quizzesresult->quizzes_title;
		$passed_score = $quizzesresult->passed_score;
		$total_questionlimit = $quizzesresult->question_limit;
		$paging = $quizzesresult->paging;

		$question_limit = $quizzesresult->paging_limit > 1 ? $quizzesresult->paging_limit : 1;
		$obj->question_limit = $question_limit;

		// 其他測驗的欄位
		$display_userscore = $quizzesresult->display_userscore;
		$graph_type = $quizzesresult->graph_type;
		$resultcats_id = $quizzesresult->resultcats_id;
		$quiz_explanation = $quizzesresult->explanation;
		$quiz_opscore = $quizzesresult->display_opscore;




		$query = 'select question_timelimit,score,penality,expire_timescore from #__vquiz_question ';
		$query .=' where  id IN (' . implode(',', $qid) . ')';

		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();


		// 儲存答案
		if ($session->has('quizoptions')) {
			$quizoptions = $session->get('quizoptions');

			if (!$qid or !$aid) {
				$obj->error = JText::_('Chose Answer');

				return $obj;
			}
		} else {
			$quizoptions = array('qids' => array(), 'aids' => array(), 'score' => array(), 'penality' => array(), 'expire_timescore' => array(), 'qspenttime' => array(), 'resultcats_ids' => array(), 'scores_ids' => array());
		}


		if ($qid and $aid) {

			for ($j = 0; $j < count($qid); $j++) {
				$chkqtime[$j] = $result[$j]->question_timelimit;
				$qscore[$j] = $result[$j]->score;
				$qpenality[$j] = $result[$j]->penality;
				$expire_timescore[$j] = $result[$j]->expire_timescore;


				$ansid = array_search($qid[$j], $quizoptions['qids']);


				if ($ansid === false) {
					array_push($quizoptions['qids'], $qid[$j]);

					if ($buttontype == 'skip') {
						array_push($quizoptions['aids'], 0);
						array_push($quizoptions['penality'], 0);
						array_push($quizoptions['expire_timescore'], 0);
						array_push($quizoptions['resultcats_ids'], 0);
						array_push($quizoptions['scores_ids'], 0);
					} else {
						array_push($quizoptions['aids'], $aid[$j]);
						array_push($quizoptions['penality'], $qpenality[$j]);

						if ($expiredtime == 1 and $chkqtime[$j] > 0 and $question_limit == 1) {
							array_push($quizoptions['expire_timescore'], $expire_timescore[$j]);
						} else {
							array_push($quizoptions['expire_timescore'], 0);
						}

						array_push($quizoptions['resultcats_ids'], $resultcats_ids[$j]);
						array_push($quizoptions['scores_ids'], $scores_ids[$j]);

					}

					array_push($quizoptions['score'], $qscore[$j]);

					if ($chkqtime[$j] == 0 or $question_limit > 1) {
						array_push($quizoptions['qspenttime'], -1);
					} else {
						array_push($quizoptions['qspenttime'], $qspenttime);
					}

					$obj->idexist = 0;

				} else {

					if ($buttontype == 'skip') {
						$quizoptions['aids'][$ansid] = 0;
						$quizoptions['expire_timescore'][$ansid] = 0;

						$quizoptions['resultcats_ids'][$ansid] = 0;
						$quizoptions['scores_ids'][$ansid] = 0;
					} else {
						$quizoptions['aids'][$ansid] = $aid[$j];
						$quizoptions['resultcats_ids'][$ansid] = $resultcats_ids[$j];
						$quizoptions['scores_ids'][$ansid] = $scores_ids[$j];

						if ($expiredtime == 1 and $chkqtime[$j] > 0 and $question_limit == 1) {
							$quizoptions['expire_timescore'][$ansid] = $expire_timescore[$j];
						} else {
							$quizoptions['expire_timescore'][$ansid] = 0;
						}

					}



					if ($chkqtime[$j] == 0 or $question_limit > 1) {
						$quizoptions['qspenttime'][$ansid] = -1;
					} else {
						$quizoptions['qspenttime'][$ansid] = $qspenttime;
					}



					$obj->idexist = 1;
					$obj->ansid = $ansid;
					$obj->lastitem = key(array_slice($quizoptions['qids'], -1, 1, TRUE));
				}
				
			}

		}


		if (!$limit == 0) {
			$obj->first = 0;
		} else {
			$obj->first = 1;
		}


		$session->set('quizoptions', $quizoptions);

		$question_limit = $quizzesresult->paging_limit > 1 ? $quizzesresult->paging_limit : 1;
		$quizoptions = $session->get('quizoptions');

		if ($session->has('random_quizoption')) {

			$random_quizoption = $session->get('random_quizoption');

			for ($s = 0; $s < count($qid); $s++) {

				$id_check = in_array($qid[$s], $random_quizoption);

				if ($id_check == false) {
					array_push($random_quizoption, $qid[$s]);
				}

			}

			$session->set('random_quizoption', $random_quizoption);
		}



		$query = 'select * from #__vquiz_question where  quizzesid = ' . $this->_db->quote($itemid) . '  and published = 1';



		if ($quizzesresult->random_question == 1) {

			if ($session->has('random_quizoption')) {
				if ($total_questionlimit > 0 and count($quizoptions['qids']) >= $total_questionlimit) {
					$query .=' and id in (' . implode(',', $quizoptions['qids']) . ')';
				}

				$query .=' and id not in (' . implode(',', $random_quizoption) . ')';
				$query .=' order by RAND() desc limit  ' . $question_limit;

			} else {
				$query .=' and id not in (' . implode(',', $quizoptions['qids']) . ') order by RAND() desc limit  ' . $question_limit;
			}
		} else {
			$query .=' order by ordering asc limit ' . $limit . ',' . $question_limit;
		}


		$this->_db->setQuery($query);

		$obj->item = $this->_db->loadObjectList();

		$qide = $this->_db->loadColumn();

		// rene
		foreach ($obj->item as $key => $item) {
			$explanation = str_replace('filesys', JURI::base().'filesys', $item->explanation);
			$explanation = str_replace('/images/', '/temp/', $explanation);
			$explanation = str_replace('images', JURI::base().'images', $explanation);
			$explanation = str_replace('/temp/', '/images/', $explanation);
			$obj->item[$key]->explanation = $explanation; 
		}

		if ($question_limit == 1) {
			$qide = count($qide) > 0 ? $qide[0] : 0;

			$index = array_search($qide, $quizoptions['qids']);



			if ($index == true) {
				$obj->skip_slide = 1;


				for ($j = $index; $j < count($quizoptions['qspenttime']); $j++) {
					if ($quizoptions['qspenttime'][$j] > 0 or $quizoptions['qspenttime'][$j] == -1) {
						break;
					}

				}





				if ($j == count($quizoptions['qspenttime']) or $quizoptions['qspenttime'][$j] == -1) {

					$limit = $j;

					if ($j == count($quizoptions['qspenttime'])) {
						$obj->ansid = $limit - 1;
					} else {
						$obj->ansid = $limit;
					}



					$query = 'select * from #__vquiz_question where  quizzesid = ' . $this->_db->quote($itemid) . '  and published = 1';

					if ($quizzesresult->random_question == 1) {


						$qid = $quizoptions['qids'][$j];

						$query .=' and id =' . $this->_db->quote($qid);
					} else {
						$query .=' order by ordering asc limit ' . $limit . ', 1';

					}



					$this->_db->setQuery($query);

					$obj->item = $this->_db->loadObjectList();

					$qide = $this->_db->loadColumn();

					$quespenttime = $quizoptions['qspenttime'][$j];

					$obj->quespenttime = $quespenttime;

					//$obj->limit=$limit;
				} else {

					$qid = $quizoptions['qids'][$j];

					$quespenttime = $quizoptions['qspenttime'][$j];

					$obj->quespenttime = $quespenttime;

					$query = 'select * from #__vquiz_question where id =' . $this->_db->quote($qid) . ' and published = 1';

					$this->_db->setQuery($query);

					$obj->item = $this->_db->loadObjectList();

					$qide = $this->_db->loadColumn();

					//$obj->limit=$j;

					$limit = $j;
				}
			}
		}



		$obj->limit = $limit;



		if ($total_questionlimit > 0 and $limit == $total_questionlimit) {

			if ($quizzesresult->random_question == 1 and $session->has('random_quizoption')) {

				if ($obj->limit == count($random_quizoption))
					$obj->item = NULL;
			} else {
				$obj->item = NULL;
			}

		}



		$a = array();

		for ($j = 0; $j < count($qide); $j++) {

			$index = array_search($qide[$j], $quizoptions['qids']);

			$quespenttime = $quizoptions['qspenttime'][$index];

			if ($index == true)
				$a1 = $quizoptions['aids'][$index];

			else
				$a1 = 0;



			array_push($a, $a1);
		}



		$obj->checked = $a;



		if ($index == true) {

			if ($question_limit > 1) {
				$obj->quespenttime = 'empty';
			} else {
				$obj->quespenttime = count($qide) > 0 ? $quespenttime : 0;
			}

		}





		$livescore = $this->score();

		$obj->livescore = $livescore->score;
		$obj->maxscore = $livescore->maxscore;
		$obj->optiontypescore = $livescore->optiontypescore;


		// 已無題目
		if (empty($obj->item)) {
			$obj->limit = count($quizoptions['qids']);
			$livescore = $this->score();
			$obj->livescore = $livescore->score;
			$obj->maxscore = $livescore->maxscore;
			$obj->optiontypescore = $livescore->optiontypescore;

			$user = JFactory::getUser();
			$date = JFactory::getDate();
			$enddatetime = $date;


			$score = $livescore->score;
			$optiontypescore = $livescore->optiontypescore;

			if ($optiontypescore == 1)
				$maxscore = $livescore->maxscore;
			else
				$maxscore = 1;

			$obj->score = $score;

			if ($quizzesresult->optinscoretype == 2) {

				$query = 'select * from #__vquiz_score_message where quizid=' . $this->_db->quote($itemid);
				$this->_db->setQuery($query);
				$result = $this->_db->loadObject();

				if ($score <= $result->upto1)
					$obj->score_message = $result->text1;
				elseif ($score <= $result->upto2)
					$obj->score_message = $result->text2;
				elseif ($score <= $result->upto3)
					$obj->score_message = $result->text3;
				elseif ($score <= $result->upto4)
					$obj->score_message = $result->text4;
				else
					$obj->score_message = $result->text5;

				//$obj->score_message=$result->text1;
			}





			if ($session->has('fqid')) {
				$fqid = $session->get('fqid');

				for ($k = 0; $k < count($fqid['qid']); $k++) {

					$flagequestionid = $fqid['qid'][$k];

					$query = 'select flagcount from #__vquiz_question where id=' . $flagequestionid;

					$this->_db->setQuery($query);

					$t = $this->_db->loadResult();

					$totalflag = $t + 1;



					$query = 'update #__vquiz_question set flagcount=' . $totalflag . ' where id=' . $flagequestionid;

					$this->_db->setQuery($query);

					$this->_db->execute();
				}



				$total_flage = count($fqid['qid']);
			} else {
				$total_flage = 0;
			}

			if ($session->has('starttime')) {
				$enddatetime = JFactory::getDate();
				$starttime = $session->get('starttime');
				$d1 = $starttime;
				$d2 = $enddatetime->toUnix();
				$quiz_spentdtime = $d2 - $d1;
				$start_datetime = gmdate("H:i:s", $starttime);
			} else {
				$quiz_spentdtime = 0;
				$start_datetime = JFactory::getDate();
				$enddatetime = JFactory::getDate();
			}



			$quiz_questions = json_encode($quizoptions['qids']);
			$quiz_answers = json_encode($quizoptions['aids']);



			$q = 'insert into #__vquiz_quizresult(quiztitle,quizzesid,categoryid,userid,optiontypescore,score,maxscore,passed_score,startdatetime,enddatetime,quiz_spentdtime,quiz_questions,quiz_answers,flag,start_datetime) values(' . $this->_db->quote($quizzestitle) . ',' . $this->_db->quote($itemid) . ',' . $this->_db->quote($quiz_categoryid) . ',' . $this->_db->quote($user->id) . ',' . $this->_db->quote($optiontypescore) . ',' . $this->_db->quote($score) . ',' . $this->_db->quote($maxscore) . ',' . $this->_db->quote($passed_score) . ',' . $this->_db->quote($start_datetime) . ',' . $this->_db->quote($enddatetime) . ',' . $this->_db->quote($quiz_spentdtime) . ',' . $this->_db->quote($quiz_questions) . ',' . $this->_db->quote($quiz_answers) . ',' . $this->_db->quote($total_flage) . ',' . $this->_db->quote($date->toSQL()) . ')';



			$this->_db->setQuery($q);

			$this->_db->execute();



			if ($user->guest) {

				$session->clear('guest_result_id');

				$guest_result_id = $this->_db->insertid();

				$session->set('guest_result_id', $guest_result_id);
			}

			// 取舊的成績
			/* for comaparing score 
			$query = 'SELECT DISTINCT(score),totaluser from #__vquiz_answer  order by score asc';

			$this->_db->setQuery($query);

			$obj->oldscores = $this->_db->loadRowList();
			*/

			/*
			$query = 'select totaluser from #__vquiz_answer where score=' . $score;

			$this->_db->setQuery($query);

			$total = $this->_db->loadResult();



			$totaluser = $total + 1;



			$query = 'select score from #__vquiz_answer';

			$this->_db->setQuery($query);

			$column = $this->_db->loadColumn();





			if (!in_array($score, $column)) {

				$query = 'insert into #__vquiz_answer(score,totaluser) values(' . $this->_db->quote($score) . ', ' . $this->_db->quote($totaluser) . ')';

				$this->_db->setQuery($query);

				$this->_db->execute();

				$obj->newdata = 1;
			} else {

				$query = 'update #__vquiz_answer set totaluser=' . $this->_db->quote($totaluser) . ' where score=' . $score;

				$this->_db->setQuery($query);

				$this->_db->execute();

				$obj->newdata = 0;
			}

			*/



			if (!$this->_db->execute()) {

				$obj->error = $this->_db->getErrorMsg();

				return $obj;
			}




			/*	取全部的成績
			$query = 'SELECT DISTINCT (score),totaluser from #__vquiz_answer  order by score asc';

			$this->_db->setQuery($query);

			$obj->scores = $this->_db->loadRowList();
			 *
			 */


			/*	取個人舊的成績
			$query = 'SELECT date_format(start_datetime, "%e, %b") as start_datetime,score from #__vquiz_quizresult where userid=' . $this->_db->quote($user->id) . 'order by id asc';

			$this->_db->setQuery($query);

			$obj->singleuserscore = $this->_db->loadRowList();
			*/


			// 回傳統計圖表
			if ($display_userscore == 1) {
				// 統計結果分類
				unset($count_ids);
				if ($resultcats_id) {
					$query = 'SELECT a.id, a.quiztitle from #__vquiz_resultcats as a where a.parent_id = ' . $this->_db->quote($resultcats_id) . 'order by a.id asc';

					$this->_db->setQuery($query);

					$resultcats = $this->_db->loadObjectList();

					// 加總分類
					if ($quiz_opscore) {
						foreach ($quizoptions['resultcats_ids'] as $key => $resultcats_ids) {
							$ids = explode(",", $resultcats_ids);

							foreach ($ids as $id) {
								$count_ids[$id] += $quizoptions['scores_ids'][$key];
							}
						}
					} else {
						foreach ($quizoptions['resultcats_ids'] as $resultcats_ids) {
							$ids = explode(",", $resultcats_ids);

							foreach ($ids as $id) {
								$count_ids[$id] += 1;
							}
						}
					}

				}

				unset($chartdata);
				$chartdata = array();
				$total_val = 0;
				switch ($graph_type) {
					case "Doughnut":	// 環狀圖
					case "Pie":			// 圓餅圖
					case "PolarArea":	// 極地圖
						
						$colors = array("#FF8888", "#FFBB66", "#FFDD55", "#BBFF66", "#77FFCC", "#99BBFF", "#9F88FF", "#E38EFF", "#BB5500", "#AAAAAA");
						$highlight = array("#FF3333", "#FFAA33", "#FFCC22", "#99FF33", "#33FFAA", "#5599FF", "#7744FF", "#E93EFF", "#CC6600", "#CCCCCC");

						foreach ($resultcats as $key => $resultcat) {
							$val = ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
							array_push($chartdata, array("value" => $val, "color" => $colors[$key], "highlight" => $highlight[$key], "label" => $resultcat->quiztitle));

							$total_val += $val;
						}

						break;
					case "Line":	// 折線圖
					case "Bar":		// 柱狀圖
					case "Radar":	// 雷達圖
						foreach ($resultcats as $key => $resultcat) {
							$labels[] = $resultcat->quiztitle;

							$val = ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
							$data[] = $val;
						}

						$datasets["label"] = $graph_type;
						//$datasets["fillColor"] = "rgba(220,220,220,0.5)";
						//$datasets["highlightFill"] = "rgba(220,220,220,0.75)";
						//$datasets["highlightStroke"] = "rgba(220,220,220,1)";
						$datasets["fillColor"] = "#89C765";
						$datasets["highlightFill"] = "rgba(137,199,101,0.75)";
						$datasets["highlightStroke"] = "rgba(229,153,153,1)";
						$datasets["scaleFontFamily"] = "'微軟正黑體','PMingLiU','Arial'";
						$datasets["data"] = $data;

						$chartdata["labels"] = $labels;
						$chartdata["datasets"] = array($datasets);
					

						break;
					case "Bara":	// 柱狀圖(AVG)
					case "Radara":	// 雷達圖(AVG)
						foreach ($resultcats as $key => $resultcat) {
							$labels[] = $resultcat->quiztitle;

							//$val = ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
//							$val=$avg_ids[$resultcat->id];
//							$data[] = $val;
						}
						unset($total_count);
						unset($total);
						foreach (json_decode($quiz_answers) as $key => $quiz_answer) {
							$query = 'select resultcats_ids,options_score from #__vquiz_option where id = ' . $quiz_answer . ' order by id asc';
							$this->_db->setQuery($query);
							$option = $this->_db->loadObject();
							$total_count[$option->resultcats_ids]+=1;
							$total[$option->resultcats_ids] += $option->options_score;
						}
						foreach($total as $key=>$v){
							$val=$total[$key]/$total_count[$key];
							$data[] = $val;
						}
						$datasets["label"] = $graph_type;
						//$datasets["fillColor"] = "rgba(220,220,220,0.5)";
						//$datasets["highlightFill"] = "rgba(220,220,220,0.75)";
						//$datasets["highlightStroke"] = "rgba(220,220,220,1)";
						//$datasets["fillColor"] = "rgba(229,153,153,0.5)";
						$datasets["fillColor"] = "#89C765";
						$datasets["highlightFill"] = "rgba(229,153,153,0.75)";
						$datasets["highlightStroke"] = "rgba(229,153,153,1)";
						$datasets["scaleFontFamily"] = "'微軟正黑體','PMingLiU','Arial'";
						$datasets["data"] = $data;

						$chartdata["labels"] = $labels;
						$chartdata["datasets"] = array($datasets);
					

						break;
					case "Thermometer":		// 溫度計
						$total = 0;

						foreach ($resultcats as $key => $resultcat) {
							$count= ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
							$unit = (int) $resultcat->quiztitle;

							$total += ($unit * $count);
						}


						$levelNum = intval ($total/10)*10;
						//$text .= '<img src="'.JURI::base().'components/com_vquiz/assets/images/Thermometer/'. $total. '.png">';
						$text .= '<img src="'.JURI::base().'components/com_vquiz/assets/images/Thermometer/'. $levelNum . '.png">';

						if ($levelNum >= 70) {
							$text .=	 '<br><p style=" text-align: left;"><span style="font-size: 12pt;">恭喜您跟配偶的性生活感到滿足！</span></p>';
						} else if($levelNum <= 69 && $levelNum>=50) {
							$text .=	 '<br><p style=" text-align: left;"><span style="font-size: 12pt;">您跟配偶的性生活還有進步的空間，可以花點時間與配偶溝通。 </span></p>';
						} else {
							$text .=	 '<br><p style=" text-align: left;"><span style="font-size: 12pt;">您跟配偶的性生活不算滿意，可以花點時間與配偶溝通。</span></p>';
						}
						
						$chartdata = $text;

						break;

					case "Star":	// 五星級
						$total = 0;
						$total_count = 0;
						foreach ($resultcats as $key => $resultcat) {
							$count= ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
							$unit = (int) $resultcat->quiztitle;

							$total += ($unit * $count);
							$total_count += $count;
						}

						$start_num = floor($total / $total_count);
						$star_html = "";
						for ($s = 0; $s < $start_num; $s++) {
							$star_html .= '<img src="'.JURI::base().'components/com_vquiz/assets/images/star_icon.png">';
						}

						$text = '';
						switch ($start_num) {
							case 1:
								$text = "<strong>請注意，你一星級". $star_html. "守護幸福能力</strong>";
								$text .= "<br />   面對親密關係中的衝突，有很大的進步空間喔！會有強烈的情緒，表示你非常看重這段感情，不愉快的事情在發生時，先讓自己冷靜一下，並且向外求援，緩衝緊張氣氛吧！";
								break;
							case 2:
								$text = "<strong>你具備二星級". $star_html. "守護幸福能力</strong>";
								$text .= "<br />   面臨親密關係中的衝突處理，你有進步空間喔！<br />   親密關係中的衝突是否有時令你感到無力或火氣上揚？<br />   請持續加油，別放棄！&nbsp;<br />   人必定有差異，衝突更是戀愛時不可避免的，解決衝突是有方法的，運用健康的方式面對衝突，相信倆人的關係會越來越好。";
								break;
							case 3:
								$text = "<strong>恭喜你！具備三星級". $star_html. "守護幸福能力</strong>";
								$text .= "<br />   當和另一半有衝突時，有時候你處理的方式或態度，能磨合彼此差異。但是請小心，太情緒化或急就章的面對衝突，反而造成兩人更大的摩擦。加油，請努力持續摘星！";
								break;
							case 4:
								$text = "<strong>恭喜你！具備四星級". $star_html. "守護幸福能力</strong>";
								$text .= "<br />   親密關係遇到衝突時，大多時候有方法可以適當處理，不讓衝突越演越烈、或不牽扯無關的事物。加油，請往五星級邁進！";
								break;
							case 5:
								$text = "<strong>恭喜你！具備五星級". $star_html. "守護幸福能力</strong>";
								$text .= "<br />   在親密關係中，願意放下身段、與另一半建立處理衝突的共識，嘗試從衝突中，理解另一半的想法與立場。真是太棒了，請持續堅持唷！";
								break;
						}

						$chartdata = $text;

						break;
					case "LikeLove":	// 喜歡或愛
						if ($count_ids[39] > $count_ids[40]) {		// 愛情
							$obj->quiz_explanation = "「愛情指數」得分高者說明：<br />   當你和對方在一起時是否常常會有心動的感覺呢？你的視線總是繞著他轉，看到別人和他說話時會有酸酸的感覺，如果是，你可能是戀愛了，你對他感情以愛情成分居多喔！";
						}

						if ($count_ids[39] < $count_ids[40]) {		// 喜歡
							$obj->quiz_explanation = "「喜歡指數」得分高者說明：<br />   他對你而言是個談得來也很喜愛的朋友，他有許多值得你欣賞及學習，你們之間也歡迎其他朋友一起加入你們，你喜歡這位朋友，你對他的感情成分以喜歡居多喔！";
						}

						if ($count_ids[39] == $count_ids[40]) {		// 一樣
							$obj->quiz_explanation = "兩邊平衡代表你跟對方的感覺只是普通喔！";
						}
						break;
					
					case "m2f3":	// 共親職
						$total = 0;
						$total_count = 0;//print_r($resultcats);exit;
						//$quizzesid=$quiz_categoryid;
						foreach (json_decode($quiz_answers) as $key => $quiz_answer) {
							$query = 'select options_score from #__vquiz_option where id = ' . $quiz_answer . ' order by id asc';
							$this->_db->setQuery($query);
							$option = $this->_db->loadObject();
							$total_count += $option->options_score;							
						}

						$val1='';
						$val2='';
						$val3='';
						if($quizzesid==5){
							$val1='太太';
							$val2='父親';
							$val3='媽媽';
						}
						if($quizzesid==6){
							$val1='先生';
							$val2='母親';
							$val3='爸爸';
						}
						$text = '';
						if ($total_count>70) {
							$text = "<div style='font-size:16px; text-align:left;'>恭喜您！測驗結果發現：<br>"
							."夫妻倆人都很看重對方在孩子成長上的重要性，並且付出心力。您能感受到{$val1}的支持，她很看重您扮演{$val2}的角色。<br>"
							."近年來研究均發現：當夫妻彼此越認同並重視另一半當爸爸或媽媽的角色，不但能包容育兒的辛苦，讓孩子看見父母相互尊重的調整，對孩子的發展也有正向影響。祝福夫妻成為子女教養的好伙伴。<br>"
							."想想另一半平日對自己或孩子的付出，寄張感謝卡來表達愛意吧！</div><br>";
						}
						if($total_count>50 & $total_count<=70){
							$text = "<div style='font-size:16px; text-align:left;'>測驗結果：<br>"
							."夫妻倆人都願意為家庭、孩子付出，但是，有時{$val1}並不支持或是會反對自己教導孩子的方式。其實，育兒有苦有樂，難免有不一致的看法。請依{$val1}的個性，想一個好方法，看見與瞭解彼此習慣教育孩子的方式，對話對教養孩子的期待。<br>"
							."近年來研究均發現：當夫妻彼此越認同並重視另一半當爸爸或媽媽的角色，不但能包容育兒的辛苦，讓孩子看見父母相互尊重的調整，對孩子的發展也有正向影響。祝福夫妻成為子女教養的好伙伴。</div><br>";
						}
						if ($total_count<=50){
							$text = "<div style='font-size:16px; text-align:left;'>測驗結果：<br>"
							."照顧孩子過程中，{$val1}常讓您感覺自己不被信任、會干涉或批評自己管教孩子的方式，而讓您有所困擾。其實，{$val1}是一個很關心孩子的{$val3}，對於教養有想法與期望，使夫妻之間的認知與行動難免有落差。因此，找出彼此教養目標的共識，協調分工發揮各自擅長，成為目前重要的夫妻課題。<br>"
							."近年來研究均發現：當夫妻彼此越認同並重視另一半當爸爸或媽媽的角色，不但能包容育兒的辛苦，讓孩子看見父母相互尊重的調整，對孩子的發展也有正向影響。祝福夫妻成為子女教養的好伙伴。</div><br>";
						}
						$chartdata = $text;
						
						break;
				}
				
				$obj->chartdata = $chartdata;


				if ($resultcats_id == "23") {
					$text = '';
					foreach ($resultcats as $key => $resultcat) {
						$val = ($count_ids[$resultcat->id]) ? $count_ids[$resultcat->id] : 0;
						$item_num = round(($val * 100 ) / $total_val / 10);

						for ($p = 0; $p < $item_num; $p++) {
							$text .= '<img src="'.JURI::base().'components/com_vquiz/assets/images/icon_'. $resultcat->id. '.png">&nbsp;';
						}

					}
					$obj->chartdata_more = "您得到的建材：<br>". $text;
				}

			}

			// rene
			$quiz_explanation = str_replace('filesys', JURI::base().'filesys', $quiz_explanation);
			$quiz_explanation = str_replace('/images/', '/temp/', $quiz_explanation);
			$quiz_explanation = str_replace('images', JURI::base().'images', $quiz_explanation);
			$quiz_explanation = str_replace('/temp/', '/images/', $quiz_explanation);
			$obj->quiz_explanation .= $quiz_explanation;

			/*
			$query = 'select * from #__vquiz_configuration';

			$this->_db->setQuery($query);

			$confiresult = $this->_db->loadObject();



			if ($optiontypescore == 2) {
				$text = $confiresult->textformat2;
			} else {
				$text = $confiresult->textformat;
			}




			$query = 'select * from #__vquiz_quizresult where userid=' . $this->_db->quote($user->id) . 'order by id desc';

			$this->_db->setQuery($query);
			$quizesult = $this->_db->loadObject();


			$score = $quizesult->score;
			$maxscore = $quizesult->maxscore;
			$flag = $quizesult->flag;
			$stime = $quizesult->quiz_spentdtime;


			$h = floor($stime / 3600);
			$hr = $stime % 3600;
			$m = floor($hr / 60);
			$mr = $stime % 60;
			$s = floor($mr);


			$ht = $h > 0 ? $h . 'h' : '';
			$mt = $m > 0 ? $m . 'm' : '';
			$st = $s > 0 ? $s . 's' : '';

			$spenttime = $ht . '  ' . $mt . '  ' . $st;



			$quiztitle = $quizesult->quiztitle;
			$startdatetime = $quizesult->startdatetime;
			$enddatetime = $quizesult->enddatetime;
			$passed_score = $quizesult->passed_score;



			if ($maxscore > 0) {
				$persentagescore = round($score / $maxscore * 100, 2) > 100 ? 100 : round($score / $maxscore * 100, 2);
			} else {
				$persentagescore = $score;
			}



			if ($persentagescore >= $passed_score) {
				$passed_text = '<span style="color:green">Passed</span>';
			} else {
				$passed_text = '<span style="color:red">Failed</span>';
			}


			if ($user->get('guest')) {
				$username = 'Guest';
			} else {
				$username = $user->username;
			}


			if (strpos($text, '{username}') !== false) {
				$text = str_replace('{username}', $username, $text);
			}

			if (strpos($text, '{quizname}') !== false) {
				$text = str_replace('{quizname}', $quiztitle, $text);
			}

			if (strpos($text, '{userscore}') !== false) {
				$text = str_replace('{userscore}', $score, $text);
			}

			if (strpos($text, '{maxscore}') !== false) {
				$text = str_replace('{maxscore}', $maxscore, $text);
			}

			if (strpos($text, '{starttime}') !== false) {
				$text = str_replace('{starttime}', $startdatetime, $text);
			}

			if (strpos($text, '{endtime}') !== false) {
				$text = str_replace('{endtime}', $enddatetime, $text);
			}

			if (strpos($text, '{spenttime}') !== false) {
				$text = str_replace('{spenttime}', $spenttime, $text);
			}

			if (strpos($text, '{passedscore}') !== false) {
				$text = str_replace('{passedscore}', $passed_score, $text);
			}

			if (strpos($text, '{percentscore}') !== false) {
				$text = str_replace('{percentscore}', $persentagescore, $text);
			}

			if (strpos($text, '{passed}') !== false) {
				$text = str_replace('{passed}', $passed_text, $text);
			}

			if (strpos($text, '{flag}') !== false) {
				$text = str_replace('{flag}', $flag, $text);
			}


			$obj->text = $text;


			$obj->result_status = $persentagescore >= $passed_score ? 1 : 0;
			 *
			 */
			$obj->result = "endquiz";

			return $obj;
		}

		for ($j = 0; $j < count($obj->item); $j++) {
			$qustionid = $obj->item[$j]->id;
			$query = 'select id, qoption, resultcats_ids, options_score from #__vquiz_option where qid = ' . $this->_db->quote($qustionid) . ' order by id asc';
			$this->_db->setQuery($query);
			$obj->answer[$j] = $this->_db->loadObjectList();
		}
		$obj->result = "success";


		return $obj;
	}



	function backslide() {

		$session = JFactory::getSession();

		$obj = new stdClass();

		$obj->result = "error";

		$limit = JRequest::getInt('limit', 0);

		$itemid = JRequest::getInt('id', 0);



		if (!$session->has('quizoptions')) {

			$obj->error = JText::_('Please contact administrator');

			return $obj;
		}



		$quizoptions = $session->get('quizoptions');



		$query = ' SELECT * FROM #__vquiz_quizzes where id = ' . $this->_db->quote($itemid) . ' order by id asc';

		$this->_db->setQuery($query);

		$quizzesresult = $this->_db->loadObject();

		$paging = $quizzesresult->paging;

		$random_question = $quizzesresult->random_question;

		$question_limit = $quizzesresult->paging_limit > 1 ? $quizzesresult->paging_limit : 1;





		if ($session->has('random_quizoption')) {

			$random_quizoption = $session->get('random_quizoption');
		} else {

			$random_quizoption = array();

			$random_quizoption = $quizoptions['qids'];
		}



		$spenttimearray = $quizoptions['qspenttime'];

		$array2 = $random_quizoption;

		$qids = array_slice($array2, -$question_limit, $question_limit, true);



		if (!$limit == 0)
			$obj->first = 0;

		else
			$obj->first = 1;



		$query = 'select * from #__vquiz_question where quizzesid =' . $this->_db->quote($itemid) . ' and published = 1';



		if ($random_question == 1) {

			$query .=' and id IN (' . implode(',', $qids) . ')';
		}

		else
			$query .=' order by ordering asc limit ' . $limit . ',' . $question_limit;



		$this->_db->setQuery($query);

		$obj->item = $this->_db->loadObjectList();

		$qide = $this->_db->loadColumn();



		for ($j = 0; $j < count($obj->item); $j++) {



			$qustionid = $obj->item[$j]->id;

			$query = 'select * from #__vquiz_option where qid = ' . $this->_db->quote($qustionid) . ' order by id asc';

			$this->_db->setQuery($query);

			$obj->answer[$j] = $this->_db->loadObjectList();
		}



		$a = array();

		for ($k = 0; $k < count($qide); $k++) {

			$index = array_search($qide[$k], $quizoptions['qids']);

			$a1 = $quizoptions['aids'][$index];

			$quespenttime = $quizoptions['qspenttime'][$index];

			array_push($a, $a1);
		}



		if ($question_limit == 1) {



			if ($quespenttime == 0 or $quespenttime == -1) {



				$obj->skip_slide = 1;



				for ($j = $index; $j > 0; $j--) {

					if ($quizoptions['qspenttime'][$j] > 0 or $quizoptions['qspenttime'][$j] == -1)
						break;
				}



				$qid = $quizoptions['qids'][$j];



				if ($random_question == 1) {



					$index1 = array_search($qid, $random_quizoption);



					if ($index1 == false) {

						$randon_spenttime = array();

						for ($f = 0; $f < count($random_quizoption); $f++) {

							$indexes = array_search($random_quizoption[$f], $quizoptions['qids']);

							$spenttme = $quizoptions['qspenttime'][$indexes];

							array_push($randon_spenttime, $spenttme);
						}



						for ($k = count($random_quizoption) - 1; $k > 0; $k--) {

							if ($randon_spenttime[$k] > 0 or $randon_spenttime[$k] == -1)
								break;
						}



						$qid = $random_quizoption[$k];
					}
				}



				$query = 'select * from #__vquiz_question where  published = 1';

				$query .=' and id =' . $this->_db->quote($qid);

				$this->_db->setQuery($query);

				$obj->item = $this->_db->loadObjectList();

				$qide = $this->_db->loadColumn();



				for ($j = 0; $j < count($obj->item); $j++) {



					$qustionid = $obj->item[$j]->id;

					$query = 'select * from #__vquiz_option where qid = ' . $this->_db->quote($qustionid) . ' order by id asc';

					$this->_db->setQuery($query);

					$obj->answer[$j] = $this->_db->loadObjectList();
				}



				for ($s = 0; $s < count($qide); $s++) {

					$index = array_search($qide[$s], $quizoptions['qids']);

					$a = $quizoptions['aids'][$index];

					$quespenttime = $quizoptions['qspenttime'][$index];
				}



				$obj->limit = $index;



				if ($index == 0)
					$obj->first = 1;

				else
					$obj->first = 0;
			}
		}



		$obj->checked = $a;



		if ($question_limit > 1)
			$obj->quespenttime = 'empty';

		else
			$obj->quespenttime = $quespenttime;





		$livescore = $this->score();

		$obj->livescore = $livescore->score;

		$obj->maxscore = $livescore->maxscore;

		$obj->optiontypescore = $livescore->optiontypescore;



		if ($random_question == 1) {

			if ($question_limit == 1) {

				if ($quespenttime > 0 or $quespenttime == -1) {

					$index = array_search($qide[0], $random_quizoption);

					if ($index == true)
						array_splice($random_quizoption, $index, 1);
				}
			}else {

				for ($s = 0; $s < count($qide); $s++) {

					$index = array_search($qide[$s], $random_quizoption);

					if ($index == true)
						array_splice($random_quizoption, $index, 1);
				}
			}



			$session->set('random_quizoption', $random_quizoption);
		}



		$session->set('quizoptions', $quizoptions);

		$obj->result = "success";

		return $obj;
	}

	function showresult() {

		$obj = new stdClass();

		$obj->result = "error";

		$limit = JRequest::getInt('limit', 0);

		$itemid = JRequest::getInt('id', 0);

		$session = JFactory::getSession();

		$a = array();





		if (!$session->has('quizoptions')) {



			$obj->error = JText::_('Please contact administrator');

			return $obj;
		}

		$quizoptions = $session->get('quizoptions');



		$query = ' SELECT * FROM #__vquiz_quizzes where id = ' . $this->_db->quote($itemid);

		$this->_db->setQuery($query);

		$quizzesresult = $this->_db->loadObject();

		$paging = $quizzesresult->paging;

		$random_question = $quizzesresult->random_question;

		$optinscoretype = $quizzesresult->optinscoretype;

		$question_limit = $quizzesresult->paging_limit > 1 ? $quizzesresult->paging_limit : 1;







		$query = 'select qtitle,id,explanation from #__vquiz_question where quizzesid =' . $this->_db->quote($itemid) . ' and published = 1';

		$query .=' order by ordering asc limit ' . $limit . ',' . $question_limit;



		$this->_db->setQuery($query);

		$obj->item = $this->_db->loadObjectList();

		//$qide=$this->_db->loadColumn();



		for ($j = 0; $j < count($obj->item); $j++) {



			$qustionid = $obj->item[$j]->id;

			$query = 'select * from #__vquiz_option where qid = ' . $this->_db->quote($qustionid) . ' order by id asc';

			$this->_db->setQuery($query);

			$obj->answer[$j] = $this->_db->loadObjectList();



			$index = array_search($qustionid, $quizoptions['qids']);

			$a1 = $quizoptions['aids'][$index];

			array_push($a, $a1);
		}



		$obj->checked = $a;



		$obj->optiontypescore = $optinscoretype;

		$obj->result = "success";

		return $obj;
	}

	function quizzesdesc() {

		$user = JFactory::getUser();

		$date = JFactory::getDate();

		$quizzesid = JRequest::getInt('quizzesid');

		$obj = new stdClass();

		$obj->result = "error";



		$query = "select access,attemped_count,attemped_delay,delay_periods from #__vquiz_quizzes where id = " . $quizzesid . ' order by id asc';



		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		//$obj->desc = $result->description;

		$accessuser = $result->access;

		$attemped_count = $result->attemped_count;

		$attemped_delay = $result->attemped_delay;

		$delay_periods = $result->delay_periods;



		switch ($delay_periods) {

			case 'days':

				$days = 1;

				break;

			case 'week':

				$days = 7;

				break;

			case 'month':

				$days = 30;

				break;

			case 'hour':

				$days = 1 / 24;

				break;

			default:

				$days = 0;
		}





		$atteptdate = $attemped_delay * $days;

		$atteptdate_tounix = $atteptdate * 24 * 60 * 60;

		$nowdate = JFactory::getDate()->toUnix();

		$finaldate = $nowdate - $atteptdate_tounix;



		$query = 'select count(userid) from #__vquiz_quizresult where userid =' . $user->id . ' and UNIX_TIMESTAMP(start_datetime)<=' . $finaldate;

		$this->_db->setQuery($query);

		$attemptresult = $this->_db->loadResult();





		// $Levels = implode(',', $user->getAuthorisedViewLevels());

		$Levels = $user->getAuthorisedViewLevels();

		// $authorished=$user->getAuthorisedGroups();





		if (in_array($accessuser, $Levels)) {



			if ($attemped_count > 0) {

				if ($attemptresult <= $attemped_count) {

					$obj->access = 0;
				} else {

					$obj->access = 1;
				}
			}



			else
				$obj->access = 0;
		}



		else
			$obj->access = 1;



		$obj->link_url = JRoute::_('index.php?option=com_vquiz&view=quizmanager&id=' . $quizzesid);





		$obj->result = "success";

		return $obj;
	}

	function _buildQuery() {



		$modulorder = JRequest::getInt('modulorder');



		$query = 'select i.*';

		//most Played quizzes

		if ($modulorder == 2)
			$query .= ', count(r.id) as items';



		$query .=' from #__vquiz_quizzes as i left join #__vquiz_category as c ON i.quiz_categoryid = c.id';



		if ($modulorder == 2) {

			$query .=' left join #__vquiz_quizresult as r ON r.quizzesid=i.id';
		}



		//recent Played quizzes
		elseif ($modulorder == 3) {

			$query .=' left join ( select max(id) as id, quizzesid from #__vquiz_quizresult group by quizzesid) as r ON r.quizzesid=i.id';
		}



		return $query;
	}

	function _buildFilter() {



		$user = JFactory::getUser();

		$date = JFactory::getDate();

		$app = JFactory::getApplication();

		$lang = JFactory::getLanguage();



		$categoryid = JRequest::getInt('id');



		$query = 'select a.id';

		$query .=' from #__vquiz_category As a ';

		$query .=' LEFT join #__vquiz_category AS b ON a.lft > b.lft AND a.rgt < b.rgt';

		$where = array();

		if ($categoryid) {

			$query .=' LEFT join #__vquiz_category AS p ON p.id = ' . $categoryid;

			$where[] = ' NOT(a.lft <= p.lft AND a.rgt >= p.rgt) ';
		}

		$where[] = 'a.id !=1';

		$where[] = 'a.parent_id=' . $categoryid;

		$where[] = 'a.published=1';

		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		$query .= $filter;

		$query .=' group by a.id, a.quiztitle, a.level, a.lft, a.rgt, a.parent_id order by a.lft ASC';

		$this->_db->setQuery($query);

		$child_result = $this->_db->loadColumn();

		array_push($child_result, $categoryid);



		$featured = JRequest::getInt('featured');

		$modulorder = JRequest::getInt('modulorder');



		$where = array();



		$where[] = 'i.published =1';



		if ($featured == '1')
			$where[] = ' i.featured =1';

		//if($categoryid)
		// $where[] = ' i.quiz_categoryid ='.$this->_db->quote($categoryid);

		if ($child_result and !empty($ategoryid))
			$where[] = ' i.quiz_categoryid IN (' . implode(',', $child_result) . ')';



		$where[] = ' i.access in (' . implode(',', $user->getAuthorisedViewLevels()) . ')';

		$where[] = ' startpublish_date <= ' . $this->_db->quote($date->format('Y-m-d')) . ' and (endpublish_date >= ' . $date->format('Y-m-d') . ' or endpublish_date=' . ('0000-00-00') . ')';



		if ($app->getLanguageFilter()) {

			$where[] = 'i.language in (' . $this->_db->quote($lang->getTag()) . ', ' . $this->_db->Quote('*') . ')';
		}





		$filter = ' where ' . implode(' and ', $where);



		return $filter;
	}

	function _buildOrderBy() {



		$modulorder = JRequest::getInt('modulorder');

		//latest playes Quizzes

		if ($modulorder == 1) {

			$orderby = ' group by i.id order by i.created_date desc';
		}

		//most Played quizzes
		elseif ($modulorder == 2) {

			$orderby = ' group by i.id order by items desc';
		}



		//recent Played quizzes
		elseif ($modulorder == 3) {

			$orderby = ' order by r.id desc';
		}



		//random played quizzes
		elseif ($modulorder == 4) {

			$orderby = ' group by i.id ORDER BY RAND()';
		} else {

			$orderby = ' order by i.ordering asc';
		}



		return $orderby;
	}

	function getQuizzes() {



		if (empty($this->_data)) {

			$query = $this->_buildQuery();

			$query .= $this->_buildFilter();

			$query .= $this->_buildOrderBy();

			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}

	function getCategory_info() {



		$categoryid = JRequest::getInt('id');

		$query = 'select quiztitle,meta_desc,meta_keyword';

		$query .=' from #__vquiz_category where id=' . $categoryid;

		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		return $result;
	}

	function getDescription() {



		$user = JFactory::getUser();

		$date = JFactory::getDate();

		$app = JFactory::getApplication();

		$lang = JFactory::getLanguage();

		$id = JRequest::getInt('id');



		$query = 'select id,description,quizzes_title from #__vquiz_quizzes';



		$where = array();

		$where[] = ' id =' . $this->_db->quote($id);

		$where[] = ' access in (' . implode(',', $user->getAuthorisedViewLevels()) . ')';



		//$where[] =  ' startpublish_date <= '.$this->_db->quote($date->format('Y-m-d')).' and endpublish_date >= '.$this->_db->quote($date->format('Y-m-d'));

		$where[] = ' startpublish_date <= ' . $this->_db->quote($date->format('Y-m-d')) . ' and (endpublish_date >= ' . $date->format('Y-m-d') . ' or endpublish_date=' . ('0000-00-00') . ')';



		if ($app->getLanguageFilter()) {

			$where[] = 'language in (' . $this->_db->quote($lang->getTag()) . ', ' . $this->_db->Quote('*') . ')';
		}



		$query .= ' where ' . implode(' and ', $where);



		$query .= ' and  published =1';



		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		return $result;
	}

	function getShowresult() {



		$user = JFactory::getUser();

		$session = JFactory::getSession();



		if ($session->has('guest_result_id')) {

			$guest_result_id = $session->get('guest_result_id');
		}else
			$session->clear('guest_result_id');



		if ($user->guest and !empty($guest_result_id))
			$query = ' SELECT * FROM #__vquiz_quizresult  WHERE id = ' . $guest_result_id . ' order by id desc ';

		else
			$query = ' SELECT * FROM #__vquiz_quizresult  WHERE userid = ' . $user->id . ' order by id desc ';



		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();



		$quiz_questions = $result->quiz_questions;

		$quiz_answers = $result->quiz_answers;

		$optiontypescore = $result->optiontypescore;





		$question = json_decode($quiz_questions);

		$answers = json_decode($quiz_answers);





		$qusetion_item = new stdClass();

		for ($j = 0; $j < count($question); $j++) {





			$query = 'select qtitle from #__vquiz_question where id = ' . $this->_db->quote($question[$j]);

			$this->_db->setQuery($query);

			$qusetion_item->question[$j] = $this->_db->loadObject();



			$query = 'select * from #__vquiz_option where qid = ' . $this->_db->quote($question[$j]) . ' order by id asc';

			$this->_db->setQuery($query);

			$qusetion_item->options[$j] = $this->_db->loadObjectList();
		}





		$qusetion_item->givenanswer = $answers;

		$qusetion_item->question_array = $question;

		$qusetion_item->optiontypescore = $optiontypescore;









		return $qusetion_item;
	}


	// 計算分數
	function score() {

		$session = JFactory::getSession();

		$quizoptions = $session->get('quizoptions');

		$quizid = JRequest::getInt('id', 0);



		$query = 'select optinscoretype from #__vquiz_quizzes where id = ' . $quizid;

		$this->_db->setQuery($query);

		$optinscoretype = $this->_db->loadResult();



		$data = new stdClass();

		$data->maxscore = $data->score = 0;



		for ($i = 0; $i < count($quizoptions['qids']); $i++) {

			$countoption = false;

			$c = 0;



			if ($optinscoretype == 1) {

				$data->optiontypescore = 1;
				$data->maxscore += $quizoptions['score'][$i];

				$countoption = false;



				$query = 'select id from #__vquiz_option where qid = ' . $quizoptions['qids'][$i] . ' and correct_ans = 1 order by id asc';

				$this->_db->setQuery($query);

				$answers = $this->_db->loadColumn();


				$xx = explode(',', $quizoptions['aids'][$i]);


				for ($k = 0; $k < count($xx); $k++) {

					if (in_array($xx[$k], $answers) and count($xx) == count($answers)) {
						$countoption = true;

						$c = $c + 1;
					} else {
						$countoption = false;
					}

				}



				if ($countoption == true and $c == count($answers)) {
					$data->score +=$quizoptions['score'][$i] - $quizoptions['expire_timescore'][$i];
				} else{
					$data->score -=$quizoptions['penality'][$i];
				}

			} else if ($optinscoretype == 2) {



				$data->optiontypescore = 2;

				$query = 'select options_score from #__vquiz_option where id = ' . $quizoptions['aids'][$i];

				$this->_db->setQuery($query);

				$differnetscore = $this->_db->loadResult();



				$data->score +=$differnetscore;

				//$data->maxscore += $differnetscore;
			}
		}



		return $data;
	}



	function share_snapshot() {

		$obj = new stdClass();

		$obj->result = "error";

		$img_val = JRequest::getVar('img_data');



		$image_tmp = substr($img_val, strpos($img_val, ",") + 1);





		$foldername = JPATH_SITE . '/components/com_vquiz/assets/images/shareresult';

		if (!JFolder::exists($foldername)) {

			JFolder::create($foldername);
		}



		$image_name = 'Quizresult.png';

		$name = $foldername . '/' . $image_name;



		$unencodedData = base64_decode($image_tmp);





		//Save the image in folder

		file_put_contents($name, $unencodedData);



		$thumb = new Imagick();

		$thumb->readImage($name);

		$thumb->resizeImage(500, 300, Imagick::FILTER_LANCZOS, 1);

		$thumb->writeImage($name);

		$thumb->clear();

		$thumb->destroy();



		move_uploaded_file($img_resize, $name);



		//$obj->url =$name;

		$obj->result = "success";



		return $obj;
	}

	function getConfiguration() {

		$query = 'select * from #__vquiz_configuration';

		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		return $result;
	}


	function getResultcats() {

		$query = 'select a.id , a.quiztitle , a.level from #__vquiz_resultcats as a';

		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();

		return $result;
	}

}