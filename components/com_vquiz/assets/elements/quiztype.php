<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_menus
 * @since       1.6
 */
class JFormFieldQuiztype extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'Quiztype';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	/*protected function getInput()
	{return 'xyz';
		$html 		= array();
		$recordId	= (int) $this->form->getValue('id');
		$size		= ($v = $this->element['size']) ? ' size="' . $v . '"' : '';
		$class		= ($v = $this->element['class']) ? ' class="' . $v . '"' : 'class="text_area"';

		// Get a reverse lookup of the base link URL to Title
		$model 	= JModelLegacy::getInstance('menutypes', 'menusModel');
		$rlu 	= $model->getReverseLookup();

		switch ($this->value)
		{
			case 'url':
				$value = JText::_('COM_MENUS_TYPE_EXTERNAL_URL');
				break;

			case 'alias':
				$value = JText::_('COM_MENUS_TYPE_ALIAS');
				break;

			case 'separator':
				$value = JText::_('COM_MENUS_TYPE_SEPARATOR');
				break;

			case 'heading':
				$value = JText::_('COM_MENUS_TYPE_HEADING');
				break;

			default:
				$link	= $this->form->getValue('link');
				// Clean the link back to the option, view and layout
				$value	= JText::_(JArrayHelper::getValue($rlu, MenusHelper::getLinkKey($link)));
				break;
		}
		// Load the javascript and css
		JHtml::_('behavior.framework');
		JHtml::_('behavior.modal');

		$html[] = '<span class="input-append"><input type="text" disabled="disabled" readonly="readonly" id="' . $this->id . '" value="' . $value . '"' . $size . $class . ' /><a class="btn btn-primary" onclick="SqueezeBox.fromElement(this, {handler:\'iframe\', size: {x: 600, y: 450}, url:\''.JRoute::_('index.php?option=com_menus&view=menutypes&tmpl=component&recordId='.$recordId).'\'})"><i class="icon-list icon-white"></i> '.JText::_('JSELECT').'</a></span>';
		$html[] = '<input class="input-small" type="hidden" name="' . $this->name . '" value="'.htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '" />';

		return implode("\n", $html);
	}*/
	 protected function getInput()
  {
	  // Load modal behavior
	  JHtml::_('behavior.modal', 'a.modal');
 
	  // Build the script
	  
	//$js =' jQuery("#reset").click(function() { alert("reset"); });';
	//JFactory::getDocument()->addScriptDeclaration($js);
	  
	  $script = array();
	  $script[] = ' jQuery( document ).ready(function(){ jQuery("#reset").click(function() { ';
	  $script[] = '  document.id("'.$this->id.'_id").value ="";';
	  $script[] = '  document.id("'.$this->id.'_name").value ="Select Quizzes";';
	  $script[] = '});});';
	  $script[] = '  function jSelectQuizzes_'.$this->id.'(id, title, object) {';
	  $script[] = '  document.id("'.$this->id.'_id").value = id;';
	  $script[] = '  document.id("'.$this->id.'_name").value = title;';
	  $script[] = '  SqueezeBox.close();';
	  $script[] = '    }';
 
	  // Add to document head
	  JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
 
	  // Setup variables for display
	  $html = array();
	  $link = 'index.php?option=com_vquiz&amp;view=quizmanager'.
                  '&amp;tmpl=component&amp;function=jSelectQuizzes_'.$this->id;
 
	  $db = JFactory::getDbo();
	  $query = $db->getQuery(true);
	  $query->select('quizzes_title');
	  $query->from('#__vquiz_quizzes');
	  $query->where('id='.(int)$this->value);
	  $db->setQuery($query);
	  if (!$title = $db->loadResult()) {
		  JError::raiseWarning(500, $db->getErrorMsg());
	  }
	  if (empty($title)) {
		  $title = JText::_('SELECT_QUIZZES');
	  }
	  $title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
 
/*	  // The current book input field
	  $html[] = '<div class="fltlft" style="display:inline-flex;">';
	  $html[] = '  <input type="text" id="'.$this->id.'_name" value="'.$title.'" disabled="disabled" size="35" />';
	  $html[] = '</div>';
 
	  // The book select button
	  $html[] = '<div class="button2-left" style="display:inline-flex;>';
	  $html[] = '  <div class="blank">';
	  $html[] = '    <a class="modal" title="'.JText::_('SELECT_QUIZZES_TITLE').'" href="'.$link.
                         '" rel="{handler: \'iframe\', size: {x:800, y:450}}">'.
                         '<i class="icon-list icon-white"></i>Select Quizes</a>'.'</a>';
	  $html[] = '  </div>';
	  $html[] = '</div>';*/
	  
	  $html[] = '<span class="input-append"><input type="text" id="'.$this->id.'_name" value="'.$title.'" disabled="disabled" size="35" /><a class="btn btn-primary" onclick="SqueezeBox.fromElement(this, {handler:\'iframe\', size: {x: 800, y: 450}, url:\''.JRoute::_($link).'\'})"><i class="icon-list icon-white"></i> '.JText::_('JSELECT').'</a></span>';
	  $html[] = '<input class="btn" id="reset" type="button" name="reset" value="Reset" />';
	  $html[] = '<input class="input-small" id="'.$this->id.'_id" type="hidden" name="' . $this->name . '" value="'.htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '" />';
 
/*	  // The active book id field
	  if (0 == (int)$this->value) {
		  $value = '';
	  } else {
		  $value = (int)$this->value;
	  }
 
	  // class='required' for client side validation
	  $class = '';
	  if ($this->required) {
		  $class = ' class="required modal-value"';
	  }
 
	  $html[] = '<input type="hidden"  '.$class.' name="'.$this->name.'" value="'.$value.'" />';*/
 
	  return implode("\n", $html);
  }
}
