<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_menus
 * @since       1.6
 */
class JFormFieldCategorytype extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'Categorytype';

	 
	 protected function getInput()
  {
	  // Load modal behavior
	  JHtml::_('behavior.modal', 'a.modal');
 
	  $script = array();
	  $script[] = ' jQuery( document ).ready(function(){ jQuery("#categoryreset").click(function() { ';
	  $script[] = '  document.id("'.$this->id.'_id").value ="";';
	  $script[] = '  document.id("'.$this->id.'_name").value ="Select Category";';
	  $script[] = '});});';
	  $script[] = '  function jSelectCategory_'.$this->id.'(id, title, object) {';
	  $script[] = '  document.id("'.$this->id.'_id").value = id;';
	  $script[] = '  document.id("'.$this->id.'_name").value = title;';
	  $script[] = '  SqueezeBox.close();';
	  $script[] = '    }';
 
	  // Add to document head
	  JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
 
	  // Setup variables for display
	  $html = array();
	  $link = 'index.php?option=com_vquiz&amp;view=quizcategory'.
                  '&amp;tmpl=component&amp;function=jSelectCategory_'.$this->id;
 
	  $db = JFactory::getDbo();
	  $query = $db->getQuery(true);
	  $query->select('quiztitle');
	  $query->from('#__vquiz_category');
	  $query->where('id='.(int)$this->value);
	  $db->setQuery($query);
	  if (!$title = $db->loadResult()) {
		  JError::raiseWarning(500, $db->getErrorMsg());
	  }
	  if (empty($title)) {
		  $title = JText::_('Select Category');
	  }
	  $title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
 
 
	  
	  $html[] = '<span class="input-append"><input type="text" id="'.$this->id.'_name" value="'.$title.'" disabled="disabled" size="35" /><a class="btn btn-primary" onclick="SqueezeBox.fromElement(this, {handler:\'iframe\', size: {x: 800, y: 450}, url:\''.JRoute::_($link).'\'})"><i class="icon-list icon-white"></i> '.JText::_('JSELECT').'</a></span>';
	  $html[] = '<input class="btn" id="categoryreset" type="button" name="categoryreset" value="Reset" />';
	  $html[] = '<input class="input-small" id="'.$this->id.'_id" type="hidden" name="' . $this->name . '" value="'.htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '" />';
 
 
 
	  return implode("\n", $html);
  }
}
