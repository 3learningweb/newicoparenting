<?php
/*------------------------------------------------------------------------
# com_vquiz - vquiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class VquizControllerUsersquizzes extends VquizController
{

	function __construct()
	{
		parent::__construct();
		
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'orderup', 		'reorder' );
		$this->registerTask( 'orderdown', 		'reorder' );
		$this->registerTask( 'unfeatured','featured' );	
	
	}
	
	function edit()
	{
		JRequest::setVar( 'view', 'usersquizzes' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}
	
	
 	function publish()
	{
		$model = $this->getModel('usersquizzes');
		$msg = $model->publish();
			

		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
	}

		function featured()
	{
 
		
		$model = $this->getModel('usersquizzes');
		$msg = $model->featured();
		
		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
		
	}
			
	function reorder()
	{
	
		$model = $this->getModel('usersquizzes');
		$msg = $model->reorder();

		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
	
	}
	
	function drawChart()
	{  
		JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
		$model = $this->getModel('usersquizzes');
		$obj = $model->drawChart();	
		jexit(json_encode($obj));
	} 
	
 
	
 
	function saveOrder()
	{
	
		$model = $this->getModel('usersquizzes');
		$msg = $model->saveOrder();

	$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
	
	}
	 
	function save()
	{
		  $model = $this->getModel('usersquizzes');

		if($model->store()) {
			$msg = JText::_('QUIZZES_SAVED_SUCCESS');
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'));
		}
	
	}
	
	
	
	function apply()
	{
		$model = $this->getModel('usersquizzes');
		
 
		if($model->store()) {
			$msg = JText::_('QUIZZES_SAVED_SUCCESS');
			$this->setRedirect( JRoute::_('index.php?option=com_vquiz&view=usersquizzes&task=edit&cid[]='.JRequest::getInt('id', 0)), $msg );
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes&task=edit&cid[]='.JRequest::getInt('id', 0)) );
		}

	}

 
	
	function remove()
	{
		$model = $this->getModel('usersquizzes');
		
		if(!$model->delete()) 
		{
			$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
		} 
		else 
		{
			$msg = JText::_( 'Quizzes(s) Deleted' );
		}
		
		$this->setRedirect( JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
	}
	
	
					function cancel()
					{
							$msg = JText::_('CANCEL');
							$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
					}
					
	
 		 					function copyquestion()
							{
								$model = $this->getModel('usersquizzes');
									if(!$model->copyquestion()) 
									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
									else 	 
									$msg = JText::_( 'Copy Succesfully' );
									$this->setRedirect( 'index.php?option=com_vquiz&view=usersquizzes', $msg );
							}
							
						  function movequestion()
							{
								$model = $this->getModel('usersquizzes');
									if(!$model->movequestion()) 
									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
									else 	 
									$msg = JText::_( 'Move Succesfully' );
									$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=usersquizzes'), $msg );
							}
 
 
 
 
 
}