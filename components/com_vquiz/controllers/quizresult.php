<?php
/*------------------------------------------------------------------------
# com_vquiz - vquiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizControllerQuizresult extends VquizController
{

	function __construct()
	{
		parent::__construct();
		$this->registerTask( 'add'  , 	'edit' );
	}
	
	function edit()
	{
		JRequest::setVar( 'view', 'quizresult' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}
	
	function apply()
	{
		$model = $this->getModel('quizresult');
		if($model->store()) {
			$msg = JText::_( 'Greeting Save' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult');
		}	
	}
	
	function remove()
	{
		$model = $this->getModel('quizresult');
		if(!$model->delete()) 
		{
			$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
		} 
		else 
		{
			$msg = JText::_( 'Greeting(s) Deleted' );
		}
		$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
	
	}
	
	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
	}	
	
	function export()
	{
		$model = $this->getModel('quizresult');
		$model->getCsv();	
		//JPluginHelper::importPlugin('hexdata', $profile->plugin);
		$dispatcher = JDispatcher::getInstance();
		
		try{
			$dispatcher->trigger('startExport');
			jexit(/*JText::_('INTERNAL_SERVER_ERROR')*/);
		}catch(Exception $e){
			jerror::raiseWarning('', $e->getMessage());
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
		}
	
	}

}