<?php

/**
 * @version		: controller.php 2015-06-24 21:06:39$
 * @author		EFATEK 
 * @package		Choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * CHOOSE Component Controller
 */
class ChooseController extends JControllerLegacy {

	public function ajaxSetAge() {

		$app = JFactory::getApplication();
		$age = $app->input->getString('age');

        $session = JFactory::getSession();
        $registry=$session->get('registry',null);
        if(is_null($registry)) {
            $session->set('registry', new JRegistry('session'));
        }
		
		$app->setUserState("form.choose.age", $age);
        //$app->getUserStateFromRequest("form.choose.age", "age", '1');
		
		exit();
	}
}