<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$itemid = $jinput->getInt('Itemid');

$menu = $app->getMenu();
$menu_title = $menu->getActive()->title;
$dailyjobs =  $jinput->getString("dailyjobs");
$weeklyjobs = $jinput->getString("weeklyjobs");
$monthlyjobs = $jinput->getString("monthlyjobs");

$document = JFactory::getDocument();
$document->addCustomTag( '<meta property="og:url" content="'.JURI::root() .'爸媽加油站/協力育兒減家務" />' );
$document->addCustomTag( '<meta property="og:type" content="website" />' );
$document->addCustomTag( '<meta property="og:title" content="協力育兒減家務 | iCoparenting和樂共親職" />' );
$document->addCustomTag( '<meta property="og:description" content="有孩子之後，日常生活中有許多家務或育兒相關的事情需要完成，夫妻一起討論對於經營每日生活的想法及分工，達成共識，讓生活更舒適。" />' );
$document->addCustomTag( '<meta property="og:image" content="'.JURI::root().'images/fb/housework.jpg" />' );
$document->addCustomTag( '<meta property="og:image:width" content="736" />' );
$document->addCustomTag( '<meta property="og:image:height" content="464" />' );
?>
<link rel="stylesheet" href="components/com_choose/views/game3/tmpl/css/vnstyle.css" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script type="text/javascript" src="//code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="//www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="components/com_choose/views/game3/tmpl/js/hoursework_dropdown.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}
</style>
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div class="box">
      <div class="headertop">
        <div class="greenbox">
			<span class="tittlegreen">分享、分擔都是愛，家庭經營需要夫妻攜手協力完成</span>
            <br />
            <br />
            <span class="contentgray">每個人對家務及育兒工作的看法與標準不盡相同，「不同」的看法有時會引發衝突。只要放下事事皆需一百分的想法，靜心評估每件工作的重要程度及排序之，透過討論後的妥善分工，就能減少衝突。</span>
            <br />
            <br />
			<span class="tittleblue">評估每項工作的排序及分工時，可以先想想：</span>
            <br />
            <ul>
            	<li>每天都非做不可的工作？</li>
                <li>可以降低執行頻率的工作？</li>
                <li>可以藉由外部人力或器材簡化的工作？</li>
                <li>家中每個人擅長的工作?</li>
             </ul>
             <div class="clear"></div>
        </div>
        <div class="img"><img src="components/com_choose/views/game3/tmpl/images/img_03.png"/></div>
      </div>
      <div class="clear"></div>
      <div class="con_box">
        <div class="fistbox left">
          <h4>每天都要做</h4>
          <ul class="list01 ">
<?php		  
if(!empty($dailyjobs))
{
	$jobs_arr = json_decode($dailyjobs);
	foreach ($jobs_arr as $id => $job) 
	{
		echo "<li>".$job."</li>";
	}
}
?>
          </ul>
        </div> 
        <div class="secbox left">
          <h4>1週1~2次</h4>
          <ul class="list02">
<?php		  
if(!empty($weeklyjobs))
{
	$jobs_arr = json_decode($weeklyjobs);
	foreach ($jobs_arr as $id => $job) 
	{
		echo "<li>".$job."</li>";
	}
}
?>
          </ul>
        </div> 
        <div class="thrbox left">
          <h4>1個月1~2次</h4>
          <ul class="list03">

<?php		  
if(!empty($monthlyjobs))
{
	$jobs_arr = json_decode($monthlyjobs);
	foreach ($jobs_arr as $id => $job) 
	{
		echo "<li>".$job."</li>";
	}
}
?>
          </ul>
        </div> 
        <div class="clear"></div>
		<div class="noteTableExplain" style="font-size: large; padding-top: 10px; padding-bottom: 10px; line-height: 30.3999996185303px;">
		</br>
		<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_46.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;">
				<a onclick="window.open('https://icoparenting.moe.edu.tw/活動專區/活動列表/5-爸媽加油站/2-育兒路上，夫妻攜手相伴');">
				<span style="font-size: 12pt;">育兒路上，夫妻攜手相伴</span></a>
				</td>
				<td><a href="http://www.ece.moe.edu.tw/"><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" /></a>
				</td>
			</tr>
		</tbody>
		</table>
		<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_52.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;">
				<a onclick="window.open('https://icoparenting.moe.edu.tw/活動專區/活動列表/5-爸媽加油站/8-新手父母的家務分工');">
				<span style="font-size: 12pt;">新手父母的家務分工</span></a>
				</td>
				<td><a href="http://www.ece.moe.edu.tw/"><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" /></a>
				</td>
			</tr>
		</tbody>
		</table>
		</div>
      </div>  

        <div class="btn line">
        	<p>下載<span class="coblue">工作規劃清單</span>，進一步討論我們家的分工</p>
			
			<form id="output" name="output" method="post" action="<?php echo JURI::base(); ?>rd/export/print_housework.php">
				<input type="hidden" id="dailyjobs" name="dailyjobs" value='<?php echo $dailyjobs; ?>' />
				<input type="hidden" id="weeklyjobs" name="weeklyjobs" value='<?php echo $weeklyjobs; ?>' />
				<input type="hidden" id="monthlyjobs" name="monthlyjobs" value='<?php echo $monthlyjobs; ?>' />
				<button class="btnd">下載清單</button>
			</form>			
		
        </div>
	</div>

<script language="JavaScript">
	(function($) {
		// submit
		$(document).on("click", ".btnd", function() {			
				$("#output").submit();		
		}
	})(jQuery);
</script>