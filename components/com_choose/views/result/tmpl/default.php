<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

// 特質分配
$label_code[1] = 1; $label_code[2] = 4; $label_code[3] = 3; $label_code[4] = 1; 
$label_code[5] = 2; $label_code[6] = 3; $label_code[7] = 4; $label_code[8] = 2;
$label_code[9] = 1; $label_code[10] = 3; $label_code[11] = 1; $label_code[12] = 4;
$label_code[13] = 2; $label_code[14] = 4; $label_code[15] = 2; $label_code[16] = 3;

// 初始化
$cats[1] = 0; $cats[2] = 0; $cats[3] = 0; $cats[4] = 0;


$code_list = $app->input->getString('code_list');
$codes = explode(",", $code_list);

foreach ($codes as $code) {
	if ($code == "") {
		continue;
	}
	$cats[$label_code[$code]] ++;
}


?>
<script src="components/com_choose/assets/js/Chart.js"></script>
<script>
var Datas = {
    labels: ["精力資源", "時間資源", "溝通/關係資源", "金錢/財務資源"],
    datasets: [
        {
            label: "資源面向",
            fillColor: "rgba(229,153,153,0.5)",
            strokeColor: "rgba(229,153,153,0.8)",
            highlightFill: "rgba(229,153,153,0.75)",
            highlightStroke: "rgba(229,153,153,1)",
            data: [<?php echo ($cats[1]/4)*100; ?>, <?php echo ($cats[2]/4)*100; ?>, <?php echo ($cats[3]/4)*100; ?>, <?php echo ($cats[4]/4)*100; ?>]
        }
    ]
};

window.onload = function(){
    var ctx = document.getElementById("chart-area").getContext("2d");
    var myDoughnut = new Chart(ctx).Radar(Datas, {
        responsive : true,
        animationEasing: "easeOutQuart"
    });
    <?php if($this->fitem->quizzes->graph_type == "Radar" || $this->fitem->quizzes->graph_type == "Radara") { ?>
    myDoughnut.datasets[0].fillColor = "#89c765"; //改變雷達圖的顏色
    myDoughnut.datasets[0].strokeColor = "#89c765"; //改變雷達圖的顏色
    <?php } ?>

};

</script>

<style>
    .box {
		width:60%;
		margin: 0 auto;
	}
    .zone {
		width:300px; height:300px;
	}

	.cattb {
		width: 100%;
		margin-top: 20px;
		text-align: center;
		border: 1px solid #f89406;
	}

	.cattb th {
		background-color: #f89406;
		font-weight: bold;
		padding: 5px;
	}

	.cattb td {
		vertical-align: top;
	}

	.readmore {
		margin-top: 20px;
	}
</style>

<div class="com_choose">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div style="font-size: 16px;">
		生小孩是件重要的大事，需面對的是生心理與生活中全方面的改變，<br/>
		測驗結果可以幫助您了解在生育前，重視的資源為何?較少關注的資源為何？<br/>
		幫助你可以提早預備，及早尋得所需的資源喔！
	</div>
	<div class="component_intro">
		
		<div class="box">
			<canvas id="chart-area" class="zone"></canvas>
		</div>
		
		<div class="tb">
			<table border="1" class="listtable">
				<tbody>
					<tr>
						<th>精力資源</th>
						<td>育兒所需的付出較多的照顧心力，也會增加許多的家務、生理及情緒負擔，不但要共同討論規劃與分工，並須正視喘息與抒壓的需要，適當的提供紓解與支持。</td>
					</tr>
					<tr>
						<th>時間資源</th>
						<td>當時間不變，育兒的責任與壓力增加，將排擠到工作－家庭、夫妻相處與親密關係的時間，夫妻更需不斷營造親密互動的時間，找到工作與家庭的平衡點。</td>
					</tr>
					<tr>
						<th>溝通/關係資源</th>
						<td>當父母的角色出現，夫妻之間的關係須有改變調整，如能事先建立一致的教養策略、良好的衝突處理、溝通與親友關係，對於育兒準備十分有助益。</td>
					</tr>
					<tr>
						<th>金錢/財務資源</th>
						<td>孩子的食、衣、住、行、育、樂，甚至是未來發展，都會增添家庭的經濟負擔，因此重新規劃收支並事先規劃是相當重要的。</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div style="font-size: 16px;">關於育兒準備，夫妻之間想的不一定，邀請另一半來做做測驗，討論一下彼此的想法與需要，攜手面對育兒的需要與挑戰，為寶寶的到來多一分準備，多一分喜悅。</div>
		
		<div class="readmore">
			<div class="noteTableExplain" style="font-size: large; color: #333333; padding-top: 10px; padding-bottom: 10px; font-family: 微軟正黑體;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><img style="width: 42px; height: 44px;" src="filesys/files/images/speaker/Speaker_46.png" alt="" />
							</td>
							<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><a href="index.php?option=com_content&amp;view=article&amp;id=38&amp;Itemid=149" style="color: #333333; text-decoration: none;">當父母?攜手先學做好夫妻!!</a>
							</td>
							<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="21" height="44" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div style="text-align: right;">
			<img alt="" src="images/Owl.png" style="text-align: right; width: 188px; height: 164px;">
		</div>
	</div>
</div>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {
			
		});
	})(jQuery);
</script>