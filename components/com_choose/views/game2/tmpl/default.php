<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="//www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="components/com_choose/assets/js/choose2.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
<div class="com_choose">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	
	<div class="hint_msg" style="display: none;">此頁面請切換為電腦版，即可正常使用功能</div>

	<div class="component_intro">
		<div class="intro_text">
			親愛的準爸媽們，迎接新生命的來臨，生活的瑣事與勞務<br/>
			會大幅增加，夫妻雙方也需調整步調及角色，心裡一定感<br/>
			到又期待又緊張....<br/>
			如何在面對新階段的起步，提早規劃，事先準備，就讓我<br/>們共同來思考－為下一段旅程暖暖身吧！！
		</div>
		
		
		<div class="intro_image">
			<!-- <img id="choose_intro_icon" src="filesys/images/com_choose/intro_icon.png" alt="<?php echo $menu_title; ?>" /> -->
			<img id="choose_intro_image" src="filesys/images/com_choose/intro_image.png" alt="<?php echo $menu_title; ?>" />
		</div>
	</div>
	
	<form id="choose_form" name="choose_form" method="post" action="<?php echo JRoute::_("index.php?option=com_choose&view=result&Itemid={$itemid}", false); ?>">
		<div class="game2_block">
			<!-- 預備事項一覽表 -->
			<div class="title_icon">
				<div style="display: inline-block;"><img src="filesys/images/com_choose/title_icon.png" alt="" /></div>
				<div style="display:inline-block;">
					<span class="title_text">預備事項一覽表</span><br/>
					<span>您認為生小孩子前，最需要預備的事項是什麼<br/>請選出10項你覺得最重要的預備工作拖曳到下方的框框中吧！</span>
				</div>
				
			</div>
	
			<table>
				<tbody>
				<!-- 1~4 -->
				<tr>
					<td>
						<div class="block">
							<div class="choose_block" id="block_1">
								<div class="choose_item" id="choose_item1">
									1.<br/>
									托育安排(含臨時托育人力)
									<input type="hidden" class="item_code" value="1">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_2">
								<div class="choose_item" id="choose_item2">
									2.<br/>
									托育費用預備(保母、機構...)
									<input type="hidden" class="item_code" value="2">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_3">
								<div class="choose_item" id="choose_item3">
									3.<br/>
									與親友互動關係
									<input type="hidden" class="item_code" value="3">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_4">
								<div class="choose_item" id="choose_item4">
									4.<br/>
									家務分擔安排
									<input type="hidden" class="item_code" value="4">
								</div>
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 5~8 -->
				<tr>
					<td>
						<div class="block">
							<div class="choose_block" id="block_5">
								<div class="choose_item" id="choose_item5">
									5.<br/>
									緊急事件應變
									<input type="hidden" class="item_code" value="5">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_6">
								<div class="choose_item" id="choose_item6">
									6.<br/>
									一致教養策略
									<input type="hidden" class="item_code" value="6">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_7">
								<div class="choose_item" id="choose_item7">
									7.<br/>
									孩子的教育基金規劃
									<input type="hidden" class="item_code" value="7">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_8">
								<div class="choose_item" id="choose_item8">
									8.<br/>
									安排夫妻獨處時間
									<input type="hidden" class="item_code" value="8">
								</div>
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 9~12 -->
				<tr>
					<td>
						<div class="block">
							<div class="choose_block" id="block_9">
								<div class="choose_item" id="choose_item9">
									9.<br/>
									孩子照顧與分工
									<input type="hidden" class="item_code" value="9">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_10">
								<div class="choose_item" id="choose_item10">
									10.<br/>
									建立溝通模式與管道
									<input type="hidden" class="item_code" value="10">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_11">
								<div class="choose_item" id="choose_item11">
									11.<br/>
									情緒調適與舒壓
									<input type="hidden" class="item_code" value="11">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_12">
								<div class="choose_item" id="choose_item12">
									12.<br/>
									育兒日常支出(尿布、奶粉...)
									<input type="hidden" class="item_code" value="12">
								</div>
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 13~16 -->
				<tr>
					<td>
						<div class="block">
							<div class="choose_block" id="block_13">
								<div class="choose_item" id="choose_item13">
									13.<br/>
									工作與家庭時間協調
									<input type="hidden" class="item_code" value="13">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_14">
								<div class="choose_item" id="choose_item14">
									14.<br/>
									其他支出(玩具、衣物、設備...)
									<input type="hidden" class="item_code" value="14">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_15">
								<div class="choose_item" id="choose_item15">
									15.<br/>
									維持夫妻親密(性)生活
									<input type="hidden" class="item_code" value="15">
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="block">
							<div class="choose_block" id="block_16">
								<div class="choose_item" id="choose_item16">
									16.<br/>
									有效的衝突處理
									<input type="hidden" class="item_code" name value="16">
								</div>
							</div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
	
			<!-- 預備事項拖曳區塊 -->
			<div class="title_icon">
				<img src="filesys/images/com_choose/title_icon.png" alt="" />
				<span class="title_text">預備事項區塊</span>
			</div>		
			

			<div class="mychoose" id="choose1">
				<input type="hidden" class="code_list" name="code_list" value="">
			</div>

			
			<div class="choose_footer">
				<div class="reset_block"><input type="button" id="reset_btn" value="清除重填" /></div>
				<div class="submit_block"><input type="button" id="submit_btn" value="確認" /></div>	
			</div>
		</div>
	</form>
</div>