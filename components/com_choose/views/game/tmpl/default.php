<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$age = $app->getUserState("form.choose.age");

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<script type="text/javascript" src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="components/com_choose/assets/js/choose.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
	#temperature {
		display: none;
		position: relative;
		top: 60px; 
		right: 125px;"
	}
</style>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {	
			$('input[name=age]').change(function() {
				var age = $('input[name=age]:checked').val();
				jQuery.post('<?php echo JRoute::_('index.php?option=com_choose&task=ajaxSetAge&Itemid='. $itemid, false); ?>', { age: age}, function(data) {
					location.reload();
		   		});
			});
			
			if($('input[name=age]:checked').length > 0) {
				$(".game_block").show();
			}
		});
	})(jQuery);
</script>

<div class="com_choose">	
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="hint_msg" style="display: none;">此頁面請切換為電腦版，即可正常使用功能</div><br/>
	
	<div class="pretext">
		<div class="dialog_box">&nbsp;</div>
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				寶寶出生後，家裡的擺設開始要有一些改變了，或是要直接騰出一個房間作為嬰兒房。爸爸媽媽們心中應該都有一些想像，爸爸媽媽可以花一段時間，根據預算、家裡大小、家中寶寶數，一起好好討論與規劃！
還有什麼需要注意的細節呢？讓我們一起來模擬吧！
			</p>
		</div>
	</div>
	
	<!-- <div class="component_intro">
		<div class="intro_text">
			<?php echo JText::_("COM_CHOOSE_INTRO_TEXT"); ?>
		</div>		
		
		<div class="intro_image">
			<img id="choose_intro_icon" src="filesys/images/com_choose/intro_icon.png" alt="<?php echo $menu_title; ?>" />
			<img id="choose_intro_image" src="filesys/images/com_choose/intro_image.png" alt="<?php echo $menu_title; ?>" />
		</div>
	</div> -->
	
	<form id="choose_form" name="choose_form" method="post" action="<?php echo JRoute::_("index.php?option=com_choose&view=result&Itemid={$itemid}", false); ?>">
		<!-- 基本資料 -->
		<table class="profile_table">
			<tbody>
			
			<!-- 年齡 -->
			<tr>
				<th width="30%">寶寶幾個月了？</th>
				<td>
					<input type="radio" id="age0" name="age" value="1" <?php if($age == 1){echo "checked";} ?> />0-6個月 &nbsp;
					<input type="radio" id="age1" name="age" value="2" <?php if($age == 2){echo "checked";} ?> />7-12個月 &nbsp;
					<input type="radio" id="age2" name="age" value="3" <?php if($age == 3){echo "checked";} ?> />13-18個月 &nbsp;
					<input type="radio" id="age3" name="age" value="4" <?php if($age == 4){echo "checked";} ?> />19-24個月 &nbsp;<br/>
				</td>
			</tr>
			</tbody>
		</table>
		<br/><br/>

		<!-- 特質重視程度分類表 -->
		<!-- <div class="title_icon">
			<img src="filesys/images/com_choose/title_icon.png" alt="" />
			<span class="title_text">特質重視程度分類表</span>
			<span>（請拖曳上方特質一覽表中的編號，放置下方各分數區塊內)</span>
		</div>		 -->
		
		<div class="game_block" style="display: none;">
			<div class="game_page-header">
				<div class="title">
					甜蜜天使窩
				</div>
			</div>
				
			<div id="mychoose_block">
				<div class="mychoose" id="choose1">
					<input type="hidden" class="code_list" name="code_list" value="">
				</div>
			</div>
	
			<!-- 特質一覽表 -->
			<div class="title_icon">
				<span class="title_text">擺設物件</span>
			</div>
	
			<table>
				<tbody>
				<!-- 1~3 -->
				<tr>
					<td>
						<div class="choose_block" id="block_1">
							<div class="item_text">1.冷氣</div>
							<div class="choose_item" id="choose_item1" tag="1" >
								<img id="temperature" src="components/com_choose/assets/images/temperature.png" alt="溫度" />
								<img src="components/com_choose/assets/images/air_conditioner.png" alt="冷氣" />
								<input type="hidden" class="item_code" value="1">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_2">
							<div class="item_text">2.父母雙人床</div>
							<div class="choose_item" id="choose_item2" tag="2">
								<img src="components/com_choose/assets/images/parent_bed.png" alt="父母雙人床" />
								<input type="hidden" class="item_code" value="2">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_3">
							<div class="item_text">3.嬰兒床</div>
							<div class="choose_item" id="choose_item3" tag="3">
								<img src="components/com_choose/assets/images/baby_bed.png" alt="嬰兒床" />
								<input type="hidden" class="item_code" value="3">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_4">
							<div class="item_text">4.櫃子</div>
							<div class="choose_item" id="choose_item4" tag="4">
								<img src="components/com_choose/assets/images/cabinet.png" alt="櫃子" />
								<input type="hidden" class="item_code" value="4">
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 5~8 -->
				<tr>
					<td>
						<div class="choose_block" id="block_5">
							<div class="item_text">5.尿布臺</div>
							<div class="choose_item" id="choose_item5" tag="5">
								<img src="components/com_choose/assets/images/baby_changing_station.png" alt="尿布臺" />
								<input type="hidden" class="item_code" value="5">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_6">
							<div class="item_text">6.地墊</div>
							<div class="choose_item" id="choose_item6" tag="6">
								<img src="components/com_choose/assets/images/floor_mat.png" alt="地墊" />
								<input type="hidden" class="item_code" value="6">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_7">
							<div class="item_text">7.牆壁裝飾</div>
							<div class="choose_item" id="choose_item7" tag="7">
								<img src="components/com_choose/assets/images/decorations.png" alt="牆壁裝飾" />							<input type="hidden" class="item_code" value="7">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_8">
							<div class="item_text">8.椅子</div>
							<div class="choose_item" id="choose_item8" tag="8">
								<img src="components/com_choose/assets/images/chair.png" alt="椅子" />
								<input type="hidden" class="item_code" value="8">
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 9~10 -->
				<tr>
					<td>
						<div class="choose_block" id="block_9">
							<div class="item_text">9.插座蓋</div>
							<div class="choose_item" id="choose_item9" tag="9">
								<img src="components/com_choose/assets/images/socket.png" alt="插座蓋" />
								<input type="hidden" class="item_code" value="9">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_10">
							<div class="item_text">10.電風扇</div>
							<div class="choose_item" id="choose_item10" tag="10">
								<img src="components/com_choose/assets/images/fan.png" alt="電風扇" />
								<input type="hidden" class="item_code" value="10">
							</div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_58.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><span style="font-size: 12pt;"><a href="index.php?option=com_content&view=article&id=52&catid=2&Itemid=145" style="color: #333333;">打造全家人的安心窩</a></span>
				</td>
				<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" />
				</td>
			</tr>
		</tbody>
	</table>

		<!-- <div class="choose_footer">
			<img src="filesys/images/com_choose/person2.png" alt="" />
			<div class="submit_block"><input type="button" id="submit_btn" value="確定送出" /></div>
			<div class="reset_block"><input type="button" id="reset_btn" value="清空重填" /></div>
		</div> -->
	</form>
</div>

