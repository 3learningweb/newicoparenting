	(function($) {
		$(document).ready(function() {
			var limit = [];
			limit['choose1'] = 10;
			
			// drag 			
			$(".hwjob").each(function() {
				$(this).draggable({
					activeClass: 'droppable-active',
					containment: '#hw_scope',
					stack: '.hwjob',
					cursor: 'move',
					scroll: true,
					revert: true
				});
			});	
			$(".tckjob").each(function() {
				$(this).draggable({
					activeClass: 'droppable-active',
					containment: '#hw_scope',
					stack: '.tckjob',
					cursor: 'move',
					scroll: true,
					revert: true
				});
			});
			
			$(".housework").each(function() {  // 目的地
				$(this).droppable( {
			      	accept: '.hwjob',
			      	drop: limitOne
    			});
			});
			$(".takecarekids").each(function() {  // 目的地
				$(this).droppable( {
			      	accept: '.tckjob',
			      	drop: limitOne
    			});
			});
			
			$(".dailyjobs, .weeklyjobs, .monthlyjobs").each(function() {  // 目的地
				$(this).droppable( {
			      	accept: '.hwjob, .tckjob',
			      	drop: limitOne
    			});
			});
			
			
			function limitOne( event, ui ) {
				var size = $(this).children("div").length;
				if(size == 0)
					$(this).append($(ui.draggable))			
			}
			
			// submit
			$(document).on("click", "#submit_btn", function() {
				var dailyjobs = [];
				$(".dailyjobs").children("div").each(function() {
					dailyjobs.push($(this).text());
				});				
				jQuery('#dailyjobs').val(JSON.stringify(dailyjobs));
				
				var weeklyjobs = [];
				$(".weeklyjobs").children("div").each(function() {
					weeklyjobs.push($(this).text());
				});				
				jQuery('#weeklyjobs').val(JSON.stringify(weeklyjobs));
				
				var monthlyjobs = [];
				$(".monthlyjobs").children("div").each(function() {
					monthlyjobs.push($(this).text());
				});				
				jQuery('#monthlyjobs').val(JSON.stringify(monthlyjobs));
				
				$("#choose_form").submit();			
			});

			// reset
			$(document).on("click", "#reset_btn", function() {
				window.location.reload(false);
				return false;
			});
		});
	})(jQuery);