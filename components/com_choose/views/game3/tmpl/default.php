<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
$document = JFactory::getDocument();
$document->addCustomTag( '<meta property="og:url" content="'.JURI::root() .'爸媽加油站/協力育兒減家務" />' );
$document->addCustomTag( '<meta property="og:type" content="website" />' );
$document->addCustomTag( '<meta property="og:title" content="協力育兒減家務 | iCoparenting和樂共親職" />' );
$document->addCustomTag( '<meta property="og:description" content="有孩子之後，日常生活中有許多家務或育兒相關的事情需要完成，夫妻一起討論對於經營每日生活的想法及分工，達成共識，讓生活更舒適。" />' );
$document->addCustomTag( '<meta property="og:image" content="'.JURI::root().'images/fb/housework.jpg" />' );
$document->addCustomTag( '<meta property="og:image:width" content="736" />' );
$document->addCustomTag( '<meta property="og:image:height" content="464" />' );
?>
<link rel="stylesheet" href="components/com_choose/views/game3/tmpl/css/vnstyle.css" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="components/com_choose/views/game3/tmpl/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="components/com_choose/views/game3/tmpl/js/hoursework_dropdown.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}
</style>
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	
<div class="com_choose">
	<div class="component_intro">
        <div class="headertop">
          <div class="greenbox01">
             育兒階段的家庭生活需要做的事情種類繁多，有些工作每天一定要做，有些則不需要如此頻繁。夫妻共同討論，將工作項目拖曳至分類表格中，讓每日生活更有條理又舒適。</br></br>
提醒：如果每天都要做的事項超過可填入的欄位，建議夫妻共同思考經營家庭生活的期待，有時候，權衡之後的降低標準，能讓家庭生活更幸福，每日生活更餘裕。

               <div class="clear"></div>
          </div>
          <div class="img"><img src="components/com_choose/views/game3/tmpl/images/img_03.png"/></div>
        </div>
    	<div class="headertop01">
    	<div class="left"><img src="components/com_choose/views/game3/tmpl/images/icon_03.png"/></div>
        <div class="right">
        	<span class="tittlegreen">預備事項一覽表</span>
            <br />
            <span class="contentgray">育兒階段的家庭生活需要做的事情種類繁多，有些工作每天一定要做，有些則不需要如此頻繁。夫妻共同討論，將工作項目拖曳至分類表格中，讓每日生活更有條理又舒適。</span>
        </div>
        <div class="clear"></div>
         </div>
	
	<form id="choose_form" name="choose_form" method="post" action="<?php echo JRoute::_("index.php?option=com_choose&view=game3result&Itemid={$itemid}", false); ?>">
		
	<div id="hw_scope" class="box" class="block">
	    <div class="leftbox left">
       	<div class="green">家務工作</div>
            <div>
            	<div class="housework left" ><div id="hwjob_1"  class="hwjob oneline"><br />曬衣服</div></div>
            	<div class="housework right"><div id="hwjob_2"  class="hwjob twoline">擦拭<br />家具</div></div>
            	<div class="housework left" ><div id="hwjob_3"  class="hwjob twoline">準備<br />三餐</div></div>
            	<div class="housework right"><div id="hwjob_4"  class="hwjob twoline">採買<br />生活用品</div></div>
            	<div class="housework left" ><div id="hwjob_5"  class="hwjob oneline"><br />掃地</div></div>
            	<div class="housework right"><div id="hwjob_6"  class="hwjob oneline"><br />曬棉被</div></div>
                <div class="housework left" ><div id="hwjob_7"  class="hwjob oneline"><br />洗衣服</div></div>
            	<div class="housework right"><div id="hwjob_8"  class="hwjob twoline">清洗<br />廚房</div></div>
                <div class="housework left" ><div id="hwjob_9"  class="hwjob oneline"><br />洗碗</div></div>
            	<div class="housework right"><div id="hwjob_10" class="hwjob oneline"><br />拖地</div></div>
                <div class="housework left" ><div id="hwjob_11" class="hwjob oneline"><br />摺棉被</div></div>
            	<div class="housework right"><div id="hwjob_12" class="hwjob twoline">照顧<br />寵物</div></div>
                <div class="housework left" ><div id="hwjob_13" class="hwjob oneline"><br />摺衣服</div></div>
            	<div class="housework right"><div id="hwjob_14" class="hwjob twoline">清理<br />浴室</div></div>
                <div class="housework left" ><div id="hwjob_15" class="hwjob twoline">購買<br />食材</div></div>
            	<div class="housework right"><div id="hwjob_16" class="hwjob oneline"><br />倒垃圾</div></div>
                <div class="housework left" ><div id="hwjob_17" class="hwjob oneline"><br />擦窗戶</div></div>
            	<div class="housework right"><div id="hwjob_18" class="hwjob twoline">幫寵物<br />洗澡 </div></div>
        </div>
      </div>
        <div class="leftbox left">
        	<div class="chbox">
            	<p>每天都要做</p>
				<input type="hidden" id="dailyjobs" name="dailyjobs">
            	<div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
                <div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
                <div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
                <div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
                <div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
                <div class="dailyjobs left"></div>
            	<div class="dailyjobs right"></div>
            </div>
            <div class="chbox">
            	<p>1週1~2次</p>
				<input type="hidden" id="weeklyjobs" name="weeklyjobs">
            	<div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
                <div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
                <div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
                <div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
                <div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
                <div class="weeklyjobs left"></div>
            	<div class="weeklyjobs right"></div>
            </div>
            <div class="chbox">
            	<p>1個月1~2次</p>
				<input type="hidden" id="monthlyjobs" name="monthlyjobs">
            	<div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
                <div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
                <div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
                <div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
                <div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
                <div class="monthlyjobs left"></div>
            	<div class="monthlyjobs right"></div>
            </div>
        </div>
        <div class="leftbox right">
        	<div class="blue">照顧小孩</div>
                <div>
                    <div class="takecarekids left" ><div id="tckjob_1"  class="tckjob oneline"><br />泡奶</div></div>
                    <div class="takecarekids right"><div id="tckjob_2"  class="tckjob twoline">準備<br />副食品</div></div>
                    <div class="takecarekids left" ><div id="tckjob_3"  class="tckjob twoline">接送<br />孩子</div></div>
                    <div class="takecarekids right"><div id="tckjob_4"  class="tckjob twoline">幫孩子<br />洗澡</div></div>
                    <div class="takecarekids left" ><div id="tckjob_5"  class="tckjob twoline">帶孩子<br />打預防針</div></div>
                    <div class="takecarekids right"><div id="tckjob_6"  class="tckjob twoline">陪孩子<br />複習功課</div></div>
                    <div class="takecarekids left" ><div id="tckjob_7"  class="tckjob oneline"><br />餵奶</div></div>
                    <div class="takecarekids right"><div id="tckjob_8"  class="tckjob twoline">餵孩子<br />吃飯</div></div>
                    <div class="takecarekids left" ><div id="tckjob_9"  class="tckjob oneline"><br />陪孩子玩</div></div>
                    <div class="takecarekids right"><div id="tckjob_10" class="tckjob twoline">與孩子<br />共讀</div></div>
                    <div class="takecarekids left" ><div id="tckjob_11" class="tckjob twoline">陪孩子<br />練習才藝</div></div>
                    <div class="takecarekids right"><div id="tckjob_12" class="tckjob oneline">半夜起床<br />回應孩子<br />需求</div></div>
                    <div class="takecarekids left" ><div id="tckjob_13" class="tckjob twoline">整理<br />玩具</div></div>
                    <div class="takecarekids right"><div id="tckjob_14" class="tckjob twoline">哄孩子<br />睡覺</div></div>
                    <div class="takecarekids left" ><div id="tckjob_15" class="tckjob oneline"><br />換尿布</div></div>
                    <div class="takecarekids right"><div id="tckjob_16" class="tckjob twoline">帶孩子<br />看醫生</div></div>
                    <div class="takecarekids left" ><div id="tckjob_17" class="tckjob oneline">帶孩子<br />去戶外<br />郊遊</div></div>
                    <div class="takecarekids right"><div id="tckjob_18" class="tckjob oneline">帶孩子<br />拜訪<br />祖父母</div></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="btn">
        	<button class="btno" id="reset_btn">清除填寫</button>
        	<button class="btnb" id="submit_btn">完成</button>
        </div>
	</div>
	</form>
	</div>
</div>