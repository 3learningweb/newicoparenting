
	(function($) {
		$(document).ready(function() {
			
			var msg = []; // [age][tag]
			msg[1] = [];
			msg[1][1] = "應避免冷氣直吹床，造成嬰兒感冒。溫度維持攝氏26°C~28°C，並配合電風扇，可達到最適的效果。";
			msg[1][2] = "";
			msg[1][3] = "1.嬰兒床是必備家具，不可讓寶寶與父母同睡，以免不小心壓到寶寶。選擇合格的嬰兒床，避免選擇過軟的床，若幼兒翻身，易造成口鼻遮掩窒息。\n2. 床欄間距離約6公分以下寬度，不要讓孩子的手或頭有機會可以從欄杆中伸出去；床上最好不要放置物品。";;
			msg[1][4] = "除了可以用來儲物，也可以挑選矮一點的櫃子，同時能夠當作尿布台使用。";
			msg[1][5] = "不一定需要，可以矮櫃子替代";
			msg[1][6] = "使寶寶在地上爬行時較舒適，亦可隔絕地面寒氣（緩衝墊）";
			msg[1][7] = "應小心釘好，以免掉落砸傷寶寶，或是以貼壁紙或貼紙代替。可貼一些顏色較鮮明的圖案，刺激寶寶視覺。";
			msg[1][8] = "需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶不小心撞傷。";
			msg[1][9] = "";
			msg[1][10] = "應避免電風扇直吹頭部，造成寶寶不適。";
			
			msg[2] = [];
			msg[2][1] = "應避免冷氣直吹床，造成寶寶感冒。溫度維持攝氏26°C~28°C，並配合電風扇，可達到最適的效果。";
			msg[2][2] = "";
			msg[2][3] = "1.避免選擇過軟的床，若寶寶翻身，易造成口鼻遮掩窒息。\n2.床欄杆應加高，避免寶寶攀爬，不慎摔出。高度不宜過高，降低寶寶跌落地面造成的損傷";
			msg[2][4] = "1.需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。\n2.可固定櫃子，避免寶寶拖拉時翻倒或卡在夾縫中。\n3.抽屜可上鎖，避免寶寶玩耍時夾到手指。";
			msg[2][5] = "不一定需要，可以矮櫃子替代";
			msg[2][6] = "使寶寶在地上爬行時較舒適，亦可隔絕地面寒氣（緩衝墊）";
			msg[2][7] = "應小心釘好，以免掉落砸傷寶寶，或是以貼壁紙或貼紙代替。可貼一些顏色較鮮明的圖案，刺激寶寶視覺。";
			msg[2][8] = "需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。";
			msg[2][9] = "寶寶會爬行以後，開始會到處探索，家裡的插座一定要特別蓋上插座蓋，避免寶寶手指伸入。";
			msg[2][10] = "應避免電風扇直吹頭部，造成幼兒不適。";			
			
			msg[3] = [];
			msg[3][1] = "應避免冷氣直吹床，造成寶寶感冒。溫度維持攝氏26°C~28°C，並配合電風扇，可達到最適的效果。";
			msg[3][2] = "";
			msg[3][3] = "床欄杆應加高，避免寶寶攀爬，不慎摔出。高度不宜過高，降低寶寶跌落地面造成的損傷";
			msg[3][4] = "1.需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。特別是寶寶剛學會走路，還走不穩，容易跌倒，家中尖角的部分更要特別小心。\n2.可固定櫃子，避免寶寶拖拉時翻倒或卡在夾縫中。\n3.抽屜可上鎖，避免寶寶玩耍時夾到手指。";
			msg[3][5] = "不一定需要，可以矮櫃子替代";
			msg[3][6] = "寶寶跌倒時能減少傷害，亦可隔絕地面寒氣（緩衝墊）";
			msg[3][7] = "";
			msg[3][8] = "1. 需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。\n2. 應避免鋪桌布，以免寶寶拉扯物品掉落";
			msg[3][9] = "寶寶會爬行以後，開始會到處探索，家裡的插座一定要特別蓋上插座蓋，避免寶寶手指伸入。";
			msg[3][10] = "應避免電風扇直吹頭部，造成幼兒不適。";			
			
			msg[4] = [];
			msg[4][1] = "應避免冷氣直吹床，造成寶寶感冒。溫度維持攝氏26°C~28°C，並配合電風扇，可達到最適的效果。";
			msg[4][2] = "";
			msg[4][3] = "1.床欄杆應加高，避免寶寶攀爬，不慎摔出。高度不宜過高，降低寶寶跌落地面造成的損傷。\n2.或是直接換掉嬰兒床，改成後一點的床墊，直接放地面，讓寶寶可以自己自由上下床。";
			msg[4][4] = "1.需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。\n2.可固定櫃子，避免寶寶拖拉時翻倒或卡在夾縫中。\n3.抽屜可上鎖，避免寶寶玩耍時夾到手指。";
			msg[4][5] = "不一定需要，可以矮櫃子替代";
			msg[4][6] = "寶寶跌倒時能減少傷害，亦可隔絕地面寒氣（緩衝墊）";
			msg[4][7] = "";
			msg[4][8] = "1. 需特別注意尖角部分，可增添桌腳專用安全軟墊，避免寶寶撞傷。\n2. 應避免鋪桌布，以免寶寶拉扯物品掉落。";
			msg[4][9] = "寶寶會爬行以後，開始會到處探索，家裡的插座一定要特別蓋上插座蓋，避免寶寶手指伸入。";
			msg[4][10] = "應避免電風扇直吹頭部，造成幼兒不適。";	
			
			var limit = [];
			limit['choose1'] = 1; limit['choose2'] = 2; limit['choose3'] = 3;	limit['choose4'] = 4;
			limit['choose5'] = 4; limit['choose6'] = 3; limit['choose7'] = 2;	limit['choose8'] = 1;
			
			// drag 			
			$(".choose_item").each(function() {
				$(this).draggable({
					activeClass: 'droppable-active',
					containment: '#choose_form',
					stack: '.choose_item',
					cursor: 'move',
					scroll: true
					// revert: true
				});
			});
			
			// accept
			$(".choose_block").each(function() {  // 起始地
				var block = $(this).prop("id").split("_");
				$(this).droppable({
			      	accept: '#choose_item'+block[1],
			      	activeClass: 'choose_active',
			      	hoverClass: 'hovered',
			      	drop: returnBlock
    			});
			});
			
			$(".mychoose").each(function() {  // 目的地
				$(this).droppable({
			      	accept: '.choose_item',
			      	hoverClass: 'hovered',
			      	drop: handleCardDrop
    			});
			});

			function returnBlock( event, ui ) {
				var tag = $(ui.draggable).attr("tag");
				if(tag == 1) {
					jQuery("#temperature").hide();
				}
			}
			
			function handleCardDrop( event, ui ) {
				var age = jQuery('input[name=age]:checked').val();
				var tag = $(ui.draggable).attr("tag");
				var notice = msg[age][tag];
				
				if(tag == 1) {
					jQuery("#temperature").show();
				}
				
				if(notice) {
					alert(notice);
				}
				
				// if(!checkLimit(choose)){
					// return false;
				// }
				// $(this).append($(ui.draggable).css("position", "relative"));
			}
			
			
			// 限制個數
			function checkLimit(choose) {
				var key = choose.prop("id");
				var count = choose.children("div").length;
				/*if(limit[key] == count){
					alert("此區塊只能放"+limit[key]+"個");
					return false;
				}*/
				return true;
			}

			
			// submit
			$(document).on("click", "#submit_btn", function() {
				_count = 0;
				for (var i = 0; i < $(".mychoose").length; i++) {
					_val_str = "";
					$(".mychoose:eq(" + i + ") > .choose_item > .item_code").each(function () {
						_val_str += $(this).val() + ",";
						_count++;
					});
					$(".mychoose:eq(" + i + ") > .code_list").val(_val_str);

				}

				if (_count < 20) {
					alert("請將所有特質拖拉至分類表中。");
					return false;
				}

				$("#choose_form").submit();

			});

			// reset
			$(document).on("click", "#reset_btn", function() {
				location.reload();
			});
		});
	})(jQuery);
