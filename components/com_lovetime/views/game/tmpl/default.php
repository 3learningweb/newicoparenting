<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$ = jQuery;
	$(document).ready(function(){
		// date
		var opt={dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				 dayNamesMin:["日","一","二","三","四","五","六"],
				 monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 prevText:"上月",
				 nextText:"次月",
				 weekHeader:"週",
				 changeYear: true,
				 showMonthAfterYear:true,
				 dateFormat:"yy-mm-dd",
				 minDate: new Date(1930, 1, 1),
				 maxDate: new Date()
				 };

		$("#datepicker1").datepicker(opt);
		$("#datepicker2").datepicker(opt);
	});

	function send_date(_type) {
		$("#type").val(_type);

		$("#mainForm").submit();
	}
</script>

<div class="com_lovetime">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=game&layout=result&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">親子共讀趣</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveTop">
					<h6>親子共讀趣</h6>
				</div>
				<div class="loveCt">
					<div style="text-align: center">
						<span style="font-family:微軟正黑體,Trebuchet MS,Trebuchet,Arial,Verdana,sans-serif">
												寶寶總是會模仿自己最熟悉的人<br>
												因此想讓寶寶養成閱讀的好習慣<br>
												不是買了多少書給寶寶<br>
												而是陪著寶寶一起看書<br>
												雖然和寶寶一起閱讀是一種挑戰<br>
												但是這項挑戰的背後<br>
												會有甜蜜的時光<br>
												讓我們一起來看看<br>
												如何選擇適合書和寶寶一起享受共讀時間吧！</span></span></span></span></span></span></div>

					<div class="loveDate">
						請輸入寶寶的生日：

                       
							<input type="text" id="datepicker1" name="love_date" value="<?php echo date("Y-m-d"); ?>" size="15" maxlength="10">
							<div class="submit_block"><input type="button" value="開始計算" onClick="send_date('love')"></div>
						
					</div>
					
				</div>
			</div>
        </div>
        <div class="contentTableDown"></div>
    </div>
		<input type="hidden" name="type" id="type" value="">
	</form>
</div>