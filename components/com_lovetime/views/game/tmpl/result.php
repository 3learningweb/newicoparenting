<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$type = $app->input->getString('type');



if ($type == "love") {
	$start_date = $app->input->getString('love_date');
} else {
	$start_date = $app->input->getString('break_date');
}

$start_date = ($start_date) ? $start_date : date("Y-m-d");
$diff_days = ceil((time() - strtotime($start_date)) / 86400);


if ($type == "love") {
	$title = "寶寶的年齡";
	$subject = "<h6>恭喜你寶寶已經 ". $diff_days. " 天囉！</h6>";

	if ($diff_days <= 90) {
		$html_file = $type. "_1.html";
	} else if ($diff_days > 90 && $diff_days <= 540) {
		$html_file = $type. "_2.html";
	} else if ($diff_days > 540 && $diff_days <= 1095) {
		$html_file = $type. "_3.html";
	} else if ($diff_days > 1095 && $diff_days <= 2190) {
		$html_file = $type. "_4.html";
	} else {
		$subject = "<h6>WOW！已經 ". $diff_days. " 天囉！</h6>";
		$html_file = $type. "_5.html";
	}

} else {
	$title = "我們分手的那一天";
	$subject = "<h6>這是分手的第 ". $diff_days. " 天....</h6>";
	if ($diff_days <= 180) {
		$html_file = $type. "_1.html";
	} else {
		$html_file = $type. "_2.html";
	}

}


?>
<script>
	$ = jQuery;
	$(document).ready(function(){
		
	});
</script>

<div class="com_lovetime">
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=game&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">親子共讀趣</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveTop">
					<h6><?php echo $title; ?></h6>
				</div>
				<div class="loveCt">
					<?php echo $subject; ?>
					<br>
					<?php include('components/com_lovetime/assets/html/'. $html_file); ?>
				</div>
				<div class="submit_block" style="margin-top: 10px;"><input type="submit" value="重新計算"></div>
			</div>
			
	<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_52.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><span style="font-size: 12pt;"><a href="index.php?option=com_content&view=article&id=23&catid=4&Itemid=156" style="color: #333333;">跟寶寶一起「玩」閱讀</a></span>
				</td>
				<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" />
				</td>
			</tr>
		</tbody>
	</table>
        </div>
        <div class="contentTableDown"></div>
    </div>
	</form>
</div>
