<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$type = $app->input->getString('type');



if ($type == "love") {
	$start_date = $app->input->getString('love_date');
} else {
	$start_date = $app->input->getString('break_date');
}

$start_date = ($start_date) ? $start_date : date("Y-m-d");
$diff_days = ceil((time() - strtotime($start_date)) / 86400);
$diff_month=ceil($diff_days/30);

if ($type == "love") {
	$title = "愛的成長紀錄";
	$subject = "<h6>已經 ". $diff_month. " 個月囉！</h6>";

	if ($diff_days <= 30) {
		$html_file =  "record_1.html";
	} else if ($diff_days > 30 && $diff_days <= 60) {
		$html_file =  "record_2.html";
	} else if ($diff_days > 60 && $diff_days <= 90) {
		$html_file =  "record_3.html";
	} else if ($diff_days > 90 && $diff_days <= 120) {
		$html_file =  "record_4.html";
	} else if ($diff_days > 120 && $diff_days <= 150) {
		$html_file =  "record_5.html";
	} else if ($diff_days > 150 && $diff_days <= 180) {
		$html_file =  "record_6.html";
	} else if ($diff_days > 180 && $diff_days <= 210) {
		$html_file =  "record_7.html";
	} else if ($diff_days > 210 && $diff_days <= 240) {
		$html_file =  "record_8.html";
	} else if ($diff_days > 240 && $diff_days <= 270) {
		$html_file =  "record_9.html";
	} else if ($diff_days > 270 && $diff_days <= 300) {
		$html_file =  "record_10.html";
	} else {
		$subject = "<h6>WOW！已經 ". $diff_days. " 天囉！</h6>";
//		$html_file = $type. "_5.html";
	}

} else {
	$title = "我們分手的那一天";
	$subject = "<h6>這是分手的第 ". $diff_days. " 天....</h6>";
	if ($diff_days <= 180) {
		$html_file = $type. "_1.html";
	} else {
		$html_file = $type. "_2.html";
	}

}


?>
<script>
	$ = jQuery;
	$(document).ready(function(){
		
	});
</script>

<div class="com_lovetime">
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=record&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">愛的成長紀錄</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveTop">
					<h6><?php echo $title; ?></h6>
				</div>
				<div class="loveCt">
					<?php echo $subject; ?>
					<br>
					<?php include('components/com_lovetime/assets/html/'. $html_file); ?>
				</div>
				不論這是家中第幾個寶寶，準爸爸和準媽
媽在這一段時間都很辛苦，並且需要一段心理
調適的時間，更需要彼此更多的體諒。

          當對方情緒低落時，多給對方一些支持
與安慰，讓對方更多感受對自己與寶寶的愛
意，一起期待新成員來報到！<br>

				<div class="submit_block"><input type="submit" value="重新計算"></div>
			</div>
			
	<table border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td><img style="width: 49px; height: 44px;" src="filesys/files/images/speaker/Speaker_58.png" alt="Speaker 52" width="49" height="44" />
				</td>
				<td style="background: url('filesys/files/images/speaker/Speaker_95.png') repeat-x;"><span style="font-size: 12pt;"><a href="index.php?option=com_content&view=article&id=90&catid=2&Itemid=145" style="color: #333333;">產後護理好安心</a></span>
				</td>
				<td><img src="filesys/files/images/speaker/Speaker_90.png" alt="" width="16" height="44" />
				</td>
			</tr>
		</tbody>
	</table>
        </div>
        <div class="contentTableDown"></div>
    </div>
	</form>
</div>
