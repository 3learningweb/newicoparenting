<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$ = jQuery;
	$(document).ready(function(){
		// date
		var opt={dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				 dayNamesMin:["日","一","二","三","四","五","六"],
				 monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 prevText:"上月",
				 nextText:"次月",
				 weekHeader:"週",
				 changeYear: true,
				 showMonthAfterYear:true,
				 dateFormat:"yy-mm-dd",
				 minDate: new Date(1930, 1, 1),
				 maxDate: new Date()
				 };

		$("#datepicker1").datepicker(opt);
		$("#datepicker2").datepicker(opt);
	});

	function send_date(_type) {
		$("#type").val(_type);

		$("#mainForm").submit();
	}
</script>

<div class="com_lovetime">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=record&layout=result&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">愛的成長紀錄</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveCt">
	<div class="component_intro">
		<div class="intro_text">
			親愛的爸爸媽媽，剛得知懷孕的心情是如何呢？是不是<br/>
			又興奮又緊張呢？好想趕快看看寶寶現在肚子裡是什麼<br/>
			樣子？在做些什麼？這個時候爸爸媽媽該注意什麼呢?<br/>
			請在以下輸入媽媽最後一次月經來的日子，您就能得知<br/>
			懷孕這段時間的每個重要日子喔！讓我們一起期待寶寶<br/>
			的成長和出生吧！
		</div>
				
		<div class="intro_image">
			<img id="choose_intro_image" src="components/com_lovetime/assets/images/intro_image.png" alt="<?php echo $menu_title; ?>" />
		</div>
	</div>

					<div class="loveDate">                       
							<input type="text" id="datepicker1" name="love_date" value="<?php echo date("Y-m-d"); ?>" size="15" maxlength="10">
							<div class="submit_block"><input type="button" value="開始計算" onClick="send_date('love')"></div>	
					</div>
					
				</div>
			</div>
        </div>
        <div class="contentTableDown"></div>
    </div>
		<input type="hidden" name="type" id="type" value="">
	</form>
</div>