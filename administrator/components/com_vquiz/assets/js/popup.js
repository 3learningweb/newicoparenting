 window.document.onkeydown = function (e)
{
    if (!e){
        e = event;
    }
    if (e.keyCode == 27){
        lightbox_close();
    }
}
//Checkes if any key pressed. If ESC key pressed it calls the lightbox_close() function.
function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block';  
}
//This script makes light and fade divs visible by setting their display properties to block. Also it scrolls  the browser to top of the page to make sure, the popup will be on //middle of the screen.
function lightbox_close(){
    document.getElementById('light_import').style.display='none';
	document.getElementById('light_copy').style.display='none';
	document.getElementById('light_move').style.display='none';
    document.getElementById('fade').style.display='none';

}
function lightbox_close2(){
	document.getElementById('light_copy').style.display='none';
	document.getElementById('light_move').style.display='none';
    document.getElementById('fade').style.display='none';

}
function lightbox_close3(){
	document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';

}
//Checkes if any key pressed. If ESC key pressed it calls the lightbox_close() function.
function lightbox_copy(){
    window.scrollTo(0,0);
    document.getElementById('light_copy').style.display='block';
    document.getElementById('fade').style.display='block';  
}
function lightbox_move(){
    window.scrollTo(0,0);
    document.getElementById('light_move').style.display='block';
    document.getElementById('fade').style.display='block';  
}
function lightbox_import(){
    window.scrollTo(0,0);
    document.getElementById('light_import').style.display='block';
    document.getElementById('fade').style.display='block';  
}
 