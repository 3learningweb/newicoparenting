<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class VquizModelResultcats extends JModelList {

	var $_total = null;
	var $_pagination = null;

	function __construct() {
		parent::__construct();
		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.category.list.';
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest($context . 'limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');
		$filter_language = $mainframe->getUserStateFromRequest($context . 'filter_language', 'filter_language', '');
		//$akey			= $mainframe->getUserStateFromRequest( $context.'akey', 'akey', '',	'string' );
		//$akey			= JString::strtolower( $this->_akey );
		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);


		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);

		$array = JRequest::getVar('cid', 0, '', 'array');
		$this->setId((int) $array[0]);
	}

	function _buildQuery() {
		$db = JFactory::getDBO();
		$query = "SELECT i.*, count(u.quiz_categoryid) as totalquizzes FROM #__vquiz_resultcats as i LEFT JOIN #__vquiz_quizzes as u ON i.id = u.quiz_categoryid";

		return $query;
	}

	function setId($id) {
		// Set id and wipe data
		$this->_id = $id;
		$this->_data = null;
	}

	function &getItem() {
		// Load the data
		if (empty($this->_data)) {
			$query = ' SELECT * FROM #__vquiz_resultcats ' .
					'  WHERE id = ' . $this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
		}

		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->quiztitle = null;
			$this->_data->photopath = null;
			$this->_data->published = null;
			$this->_data->language = null;
			$this->_data->access = null;
			$this->_data->ordering = null;
			$this->_data->created_date = null;
			$this->_data->level = null;
			$this->_data->parent_id = null;
			$this->_data->alias = null;
			$this->_data->meta_desc = null;
			$this->_data->meta_keyword = null;
			$this->_data->path = null;
			$this->_data->lft = null;
			$this->_data->rgt = null;
			$this->_data->extension = null;
		}

		return $this->_data;
	}

	function &getItems() {
		// Lets load the data if it doesn't already exist
		if (empty($this->_data)) {
			$query = $this->_buildQuery();

			$filter = $this->_buildContentFilter();
			$orderby = $this->_buildItemOrderBy();

			$query .= $filter;
			$query .= $orderby;
			//$this->_data = $this->_getList( $query );
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}

	function getTotal() {

		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$query .= $this->_buildContentFilter();
			$query .= $this->_buildItemOrderBy();
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function _buildItemOrderBy() {
		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.category.list.';

		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$orderby = ' group by i.id,i.quiztitle, i.level, i.lft, i.rgt, i.parent_id order by ' . $filter_order . ' ' . $filter_order_Dir . ' ';
		//$orderby = ' group by i.id order by '.$filter_order.' '.$filter_order_Dir . ' ';

		return $orderby;
	}

	function getPagination() {
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_pagination;
	}

	function _buildContentFilter() {

		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.category.list.';
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');
		$search = JString::strtolower($search);

		$where = array();

		if ($publish_item) {

			if ($publish_item == 'p')
				$where[] = 'i.published= 1';

			else if ($publish_item == 'u')
				$where[] = 'i.published = 0';
		}

		if ($search) {
			if (is_numeric($search)) {
				$where[] = 'LOWER( i.id ) =' . $this->_db->Quote($this->_db->escape($search, true), false);
			} else {

				$where[] = 'i.quiztitle LIKE ' . $this->_db->Quote('%' . $this->_db->escape($search, true) . '%', false);
			}
		}

		$where[] = 'i.id !=1';

		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $filter;
	}

	function checkout($pk = null) {
		$table = $this->getTable();
		$pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');

		return $table->checkout($pk);
	}

	function reorder($ids, $inc) {
		$table = $this->getTable();

		return $table->reorder($ids, $inc);
	}

	function saveorder($idArray = null, $lft_array = null) {
		// Get an instance of the table object.
		$table = $this->getTable();

		if (!$table->saveorder($idArray, $lft_array)) {
			$this->setError($table->getError());
			return false;
		}

		// Clear the cache
		$this->cleanCache();

		return true;
	}

	function store() {
		$time = time();

		$row = $this->getTable();

		$data = JRequest::get('post');
		if (!$data["id"]) {
			$datee = JFactory::getDate();
			$data['created_date'] = $datee->toSQL();
		}
		$input = JFactory::getApplication()->input;
		$pk = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');

		// Load the row if saving an existing category.
		if ($pk > 0) {
			$row->load($pk);
			$isNew = false;
		}


		// Set the new parent id if parent id not matched OR while New/Save as Copy .
		if ($row->parent_id != $data['parent_id'] || $data['id'] == 0) {
			$row->setLocation($data['parent_id'], 'last-child');
		}

		$query = ' SELECT photopath from #__vquiz_resultcats WHERE id = ' . $data["id"];
		$this->_db->setQuery($query);
		$oldimage_path = $this->_db->loadResult();


		$data["alias"] = JFilterOutput::stringURLSafe($data['quiztitle']);



		//########## code for  pic upload. ###########

		$query = 'select categorythumbnailwidth,categorythumbnailheight from #__vquiz_configuration';
		$this->_db->setQuery($query);
		$configuration_img = $this->_db->loadObject();
		$configuration_width = $configuration_img->categorythumbnailwidth;
		$configuration_height = $configuration_img->categorythumbnailheight;

		$image = JRequest::getVar('photopath', null, 'FILES', 'array');
		$allowed = array('.jpg', '.jpeg', '.gif', '.png');

		$dirpath = JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/';
		$thumbPath = JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/thumbs/';

		jimport('joomla.filesystem.file');
		$image_name = str_replace(' ', '', JFile::makeSafe($image['name']));
		$image_tmp = $image['tmp_name'];
		$time = time();


		if ($image_name <> "") {

			$ext = strrchr($image_name, '.');
			if (!in_array($ext, $allowed)) {
				$msg_error = JText::_('This Image type is not allowed');
				$this->setError($msg_error);
				return false;
			}
			//=============================//

			$size = getimagesize($image_tmp);
			$src_w = $size[0];
			$src_h = $size[1];
			//set the height and width in proportions
			if ($src_w > $src_h) {
				if (!empty($configuration_width) and is_numeric($configuration_width))
					$width = $configuration_width;
				else
					$width = 125; //New width of image
				if (!empty($configuration_height) and is_numeric($configuration_height))
					$height = $configuration_height;
				else
					$height = $size[1] / $size[0] * $width; //This maintains proportions
				$width1 = 300; //New width of image
				$height1 = $size[1] / $size[0] * $width; //This maintains proportions
			}else {
				if (!empty($configuration_height) and is_numeric($configuration_height))
					$height = $configuration_height;
				else
					$height = 125;
				if (!empty($configuration_width) and is_numeric($configuration_width))
					$width = $configuration_width;
				else
					$width = $size[0] / $size[1] * $height; //This maintains proportions
				$height1 = 360;
				$width1 = $size[0] / $size[1] * $height; //This maintains proportions
			}


			// set image new width and height
			$width1 = 265; //New width of image
			$height1 = $size[1] / $size[0] * $width; //This maintains proportions

			$new_image = imagecreatetruecolor($width, $height);
			$new_image1 = imagecreatetruecolor($width1, $height1);

			if ($size['mime'] == "image/jpeg")
				$tmp = imagecreatefromjpeg($image_tmp);
			elseif ($size['mime'] == "image/gif")
				$tmp = imagecreatefromgif($image_tmp);
			else
				$tmp = imagecreatefrompng($image_tmp);
			imagecopyresampled($new_image, $tmp, 0, 0, 0, 0, $width, $height, $src_w, $src_h);


			if ($size['mime'] == "image/jpeg")
				imagejpeg($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);
			elseif ($size['mime'] == "image/gif")
				imagegif($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);
			else
				imagepng($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);
			echo "4";

			imagecopyresampled($new_image1, $tmp, 0, 0, 0, 0, $width1, $height1, $src_w, $src_h);


			if ($size['mime'] == "image/jpeg")
				imagejpeg($new_image1, $dirpath . $time . '_' . $image_name);
			elseif ($size['mime'] == "image/gif")
				imagegif($new_image1, $dirpath . $time . '_' . $image_name);
			else
				imagepng($new_image1, $dirpath . $time . '_' . $image_name);

			//======= end of thumbail code========//

			if (move_uploaded_file($image_tmp, $dirpath . $time . '_' . $image_name))
				$data['photopath'] = $time . '_' . $image_name;
		}

		if ($data['photopath']) {

			unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/' . $oldimage_path);
			unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/thumbs/' . 'thumb_' . $oldimage_path);
		}


		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}


		if (!$row->store()) {
			$this->setError($row->getErrorMsg());
			return false;
		}

		if (!$data['id'])
			JRequest::setVar('id', $row->id);


		return true;
	}

	function delete() {
		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$row = & $this->getTable();
		if (count($cids)) {
			foreach ($cids as $cid) {

				$query = ' SELECT photopath from #__vquiz_resultcats WHERE id = ' . $cid;
				$this->_db->setQuery($query);
				$oldimage_path = $this->_db->loadResult();
				if (!empty($oldimage_path)) {
					unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/' . $oldimage_path);
					unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/thumbs/' . 'thumb_' . $oldimage_path);
				}



				$query = ' SELECT id FROM #__vquiz_quizzes WHERE quiz_categoryid = ' . $cid;
				$this->_db->setQuery($query);
				$quizzes_id = $this->_db->loadColumn();

				if (!empty($quizzes_id)) {
					$query = ' SELECT id FROM #__vquiz_question WHERE quizzesid IN (' . implode(',', $quizzes_id) . ')';
					$this->_db->setQuery($query);
					$question_id = $this->_db->loadColumn();
				}



				$q1 = 'delete FROM #__vquiz_quizzes WHERE quiz_categoryid = ' . $cid;
				$this->_db->setQuery($q1);
				$this->_db->execute();

				if (!empty($quizzes_id)) {
					$q2 = 'delete FROM #__vquiz_question WHERE quizzesid IN (' . implode(',', $quizzes_id) . ')';
					$this->_db->setQuery($q2);
					$this->_db->execute();
				}

				if (!empty($question_id)) {
					$q3 = 'delete FROM #__vquiz_option WHERE qid IN (' . implode(',', $question_id) . ')';
					$this->_db->setQuery($q3);
					$this->_db->execute();
				}



				if (!$row->delete($cid)) {
					$this->setError($row->getErrorMsg());
					return false;
				}
			}
		}
		return true;
	}

	function getCsv() {
		$db = JFactory::getDbo();
		//get the column titles for heading row
		$query = 'show columns from #__vquiz_resultcats';
		$db->setQuery($query);
		$columnhead = $db->loadColumn();

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$query = 'select * from #__vquiz_resultcats WHERE id IN (' . implode(',', $cids) . ')';
		$db->setQuery($query);
		$data = $db->loadRowList();

		//push the heading row at the top
		array_unshift($data, $columnhead);

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=export.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		foreach ($data as $fields) {
			$f = array();
			foreach ($fields as $v)
				array_push($f, mb_convert_encoding($v, "UTF-8"));
			fputcsv($output, $f, ',', '"');
		}
		fclose($output);
		return true;
	}

	function publish() {
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		$task = JRequest::getCmd('task');
		$publish = ($task == 'publish') ? 1 : 0;
		$n = count($cid);
		if (empty($cid)) {
			return 'No item selected';
		}
		$cids = implode(',', $cid);
		//implode() convert array into string
		$query = 'UPDATE #__vquiz_resultcats SET published = ' . (int) $publish . ' WHERE id IN ( ' . $cids . ' )';
		$this->_db->setQuery($query);
		if (!$this->_db->query())
			return $this->_db->getErrorMsg();
		else
			return JText::_ ('category saved successfully');
	}

	function getParentcategory() {
		$query = 'select parent_id from #__vquiz_resultcats where id =' . $this->_id;
		$this->_db->setQuery($query);
		$parentid = $this->_db->loadResult();

		$query = 'select a.id , a.quiztitle , a.level ';
		$query .=' from #__vquiz_resultcats As a ';
		$query .=' LEFT join #__vquiz_resultcats AS b ON a.lft > b.lft AND a.rgt < b.rgt';

		$where = array();


		if ($parentid) {
			// Prevent parenting to children of this item.
			if ($id = $this->_id) {
				$query .=' LEFT join #__vquiz_resultcats AS p ON p.id = ' . $id;
				$where[] = ' NOT(a.lft >= p.lft AND a.rgt <= p.rgt) ';
			}
		}
		$where[] = 'a.id !=1';
		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
		$query .= $filter;
		$query .=' group by a.id, a.quiztitle, a.level, a.lft, a.rgt, a.parent_id order by a.lft ASC';

		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

}