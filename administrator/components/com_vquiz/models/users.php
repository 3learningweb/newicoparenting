<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class VquizModelUsers extends JModelLegacy
{
    
    var $_total = null;
	var $_pagination = null;
	
	function __construct()
	{
		parent::__construct();
 
        $mainframe = JFactory::getApplication();
		
		$context			= 'com_vquiz.users.list.'; 
        // Get pagination request variables
        $limit = $mainframe->getUserStateFromRequest($context.'limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
 
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}
	
	function _buildQuery()
	{
		$query = 'select i.* FROM #__users as i';

		return $query;
	}
	
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
	
	function getItem()
    {
		
		$query = ' SELECT c.profile_pic,c.country, i.id, i.name, i.email, i.username,  i.block FROM #__vquiz_users as c right join #__users as i on c.userid = i.id WHERE i.id = '.$this->_id;
		
		$this->_db->setQuery( $query );
		$item = $this->_db->loadObject();
		//$this->_db->getErrorMsg();
		
		if(empty($item))	{
			$item = new stdClass(); 
			$item->id = 0;
			$item->profile_pic = null;
			$item->email = null;
			$item->username = null;
			$item->name = null;
			$item->block = null;
			$item->country = null;
		}
				
		return $item;
    }
	
	function getItems()
    {
        if(empty($this->_data))	{
		
			$query = $this->_buildQuery();
			$filter = $this->_buildItemFilter();
			$query .= $filter;
			$orderby = $this->_buildItemOrderBy();
			$query .= $orderby;
			
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		
		}
		echo $this->_db->getErrorMsg();
        return $this->_data;
    }
	
	function getTotal()
  	{
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
			$filter = $this->_buildItemFilter();
			$query .= $filter;
			$orderby = $this->_buildItemOrderBy();
			$query .= $orderby;
            $this->_total = $this->_getListCount($query);    //echo $query; echo 'total='.$this->_total;
        }
        return $this->_total;
  	}
	
	function getPagination()
  	{
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
  	}
	
	function _buildItemOrderBy()
	{
        $mainframe = JFactory::getApplication();
		
		$context			= 'com_vquiz.users.list.';
 
        $filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'i.id', 'cmd' );
        $filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
 
        $orderby = ' group by i.id order by '.$filter_order.' '.$filter_order_Dir . ' ';
 
        return $orderby;
	}
	
	function _buildItemFilter()
	{
		$mainframe = JFactory::getApplication();
		$context	= 'com_vquiz.users.list.';

		$filter_order = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'i.id', 'cmd' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', '', 'word' );
		$search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '', 'string' );
		$search = JString::strtolower( $search );
		
 		$where =array();
		
		if ($search)
			$where[] = ' LOWER(i.name) LIKE '.$this->_db->Quote('%'.$search.'%');
		
		 $filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
    
		return $filter;
	}

	 
	function store()
	{
    	
		// Check for request forgeries		
		JRequest::checkToken() or jexit( JText::_('INVALID_TOKEN') );
				
		$mainframe = JFactory::getApplication();
		
		$id = JRequest::getInt('id', 0);
		
		$row =& $this->getTable('Users');
		
		jimport('joomla.user.helper');
		
		// Get required system objects
		$user = new JUser($id);
		$config		=  JFactory::getConfig();
		$params	= JComponentHelper::getParams('com_users');
		
		 
		
		$my =  JFactory::getUser();
		
		$iAmSuperAdmin	= $my->authorise('core.admin');
		
		$post = JRequest::get('post');
		
		$post['name']		= $post['name'];
		$post['username']	= $post['username'];
		$post['email']		= $post['email'];
		$post['password']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$post['password2']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
   		
		if ($post['block'] && $id == $my->id && !$my->block)
		{
			
			$this->setError(JText::sprintf('CANTDOYOURS', JText::_('BLOCK')));
			return false;
		}
		
		$allow	= $my->authorise('core.edit.state', 'com_users');
		// Don't allow non-super-admin to delete a super admin
		$allow = (!$iAmSuperAdmin && JAccess::check($id, 'core.admin')) ? false : $allow;
		
		if(!$id)	{
			// Get the default new user group, Registered if not specified.
			$system	= $params->get('new_usertype', 2);
	
			$post['groups'][] = $system;
			
		 
		
		}
		elseif($post['block'] and !$allow)	{
			
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
			return false;
		}
		
		
		
		if (!$user->bind( $post )) {
			
			$this->setError(JText::_($user->getError()));
			return false;
			
		}
 
		
		// Create the user table object
		$table = $this->getTable('user', 'JTable');
		//$this->params = (string) $this->_params;
		$this->params = (string) $user->params;
		$table->bind($user->getProperties());
  

		// Allow an exception to be thrown.
		try
		{
			// Check and store the object.
			if (!$table->check())
			{
				$this->setError($table->getError());

				return false;
			}

			// If user is made a Super Admin group and user is NOT a Super Admin

			// @todo ACL - this needs to be acl checked

			$my = JFactory::getUser();

			// Are we creating a new user
			$isNew = empty($user->id);

			// If we aren't allowed to create new users return
			if ($isNew && $updateOnly)
			{
				return true;
			}

			// Get the old user
			$oldUser = new JUser($user->id);

			// Access Checks

			// The only mandatory check is that only Super Admins can operate on other Super Admin accounts.
			// To add additional business rules, use a user plugin and throw an Exception with onUserBeforeSave.

			// Check if I am a Super Admin
			$iAmSuperAdmin = $my->authorise('core.admin');

			$iAmRehashingSuperadmin = false;

			if (($my->id == 0 && !$isNew) && $user->id == $oldUser->id && $oldUser->authorise('core.admin') && $oldUser->password != $user->password)
			{
				$iAmRehashingSuperadmin = true;
			}

			// We are only worried about edits to this account if I am not a Super Admin.
			if ($iAmSuperAdmin != true && $iAmRehashingSuperadmin != true)
			{
				// I am not a Super Admin, and this one is, so fail.
				if (!$isNew && JAccess::check($user->id, 'core.admin'))
				{
					throw new RuntimeException('User not Super Administrator');
				}

				if ($user->groups != null)
				{
					// I am not a Super Admin and I'm trying to make one.
					foreach ($user->groups as $groupId)
					{
						if (JAccess::checkGroup($groupId, 'core.admin'))
						{
							throw new RuntimeException('User not Super Administrator');
						}
					}
				}
			}

			// Fire the onUserBeforeSave event.
			JPluginHelper::importPlugin('user');
			$dispatcher = JEventDispatcher::getInstance();

			$result = $dispatcher->trigger('onUserBeforeSave', array($oldUser->getProperties(), $isNew, $user->getProperties()));

			if (in_array(false, $result, true))
			{
				// Plugin will have to raise its own error or throw an exception.
				return false;
			}

			// Store the user data in the database
			$result = $table->store();

			// Set the id for the JUser object in case we created a new user.
			if (empty($user->id))
			{
				$user->id = $table->get('id');
			}

			if ($my->id == $table->id)
			{
				$registry = new JRegistry;
				$registry->loadString($table->params);
				$my->setParameters($registry);
			}

			
			// Fire the onUserAfterSave event
			//$dispatcher->trigger('onUserAfterSave', array($this->getProperties(), $isNew, $result, $this->getError()));
			$dispatcher->trigger('onUserAfterSave', array($user->getProperties(), '', $result, $user->getError()));
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());

			return false;
		}

		// Reset the user object in the session on a successful save
		if ($result === true && JFactory::getUser()->id == $user->id)
		{
			JFactory::getSession()->set('user', $this);
		}
		 
		  
		
			
		$query = 'select count( * ) from #__vquiz_users where userid = '.$user->id;
		$this->_db->setQuery( $query );
		$count = $this->_db->loadResult();
		
		$insert = new stdClass();
		
		$insert->userid	 = $user->id;
		$insert->country = $post['country'];
		
		 //Users Pic Upload 
 
		jimport('joomla.filesystem.file');
		$image = JRequest::getVar("profile_pic", null, 'files', 'array');
 		$allowed = array('.jpg', '.jpeg', '.gif', '.png');
		$thumbPath = JPATH_ADMINISTRATOR.'/components/com_vquiz/assets/uploads/users/'; 
				 
		$image_name = str_replace(' ', '', JFile::makeSafe($image['name']));
		$image_tmp = $image['tmp_name'];
		$time = time();

 
			if($image_name <> "" )
			{
				$ext = strrchr($image_name, '.');
					if(!in_array($ext, $allowed)){
					$msg_error=JText::_('This Image type is not allowed');
						$this->setError($msg_error);
						return false;
					}
						
						$size = getimagesize($image_tmp);
						$src_w = $size[0];
						$src_h = $size[1];

 						$width = 125;    
 						//$height = $size[1]/$size[0]*$width; 
						$height =90;  
						$new_image = imagecreatetruecolor($width, $height);
						
 						if($size['mime'] == "image/jpeg")
 						$tmp = imagecreatefromjpeg($image_tmp);
 						elseif($size['mime'] == "image/gif")
 						$tmp = imagecreatefromgif($image_tmp);
 						else
 						$tmp = imagecreatefrompng($image_tmp);
						
 						imagecopyresampled($new_image, $tmp,0,0,0,0, $width, $height, $src_w, $src_h);
						
						if($size['mime'] == "image/jpeg")
							imagejpeg($new_image, $thumbPath.'thumb_'.$time.'_'.$image_name);
 						elseif($size['mime'] == "image/gif")
							imagegif($new_image, $thumbPath.'thumb_'.$time.'_'.$image_name);
						else
							imagepng($new_image, $thumbPath.'thumb_'.$time.'_'.$image_name);
 
							$insert->profile_pic = 'thumb_'.$time.'_'.$image_name;
				
				if(!empty($row->profile_pic) and is_file(JPATH_ADMINISTRATOR.'/components/com_vquiz/assets/uploads/users/'.$row->profile_pic))
				unlink(JPATH_ADMINISTRATOR.'/components/com_vquiz/assets/uploads/users/'.$row->profile_pic);
							 
					}
 
		
		if(!$count)	{
		
			$this->_db->insertObject('#__vquiz_users', $insert, 'userid');
			
			JRequest::setVar('id', $user->id);
				
		}
		else{
			
			$this->_db->updateObject('#__vquiz_users', $insert, 'userid');
					
		}
 
		return $user->id;
			
	}
	
	 
	
	 
	public function delete()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( JText::_('INVALID_TOKEN') );
		
		$user	= JFactory::getUser();
		$table		= JTable::getInstance('user');
		$pks 	= (array)JRequest::getVar( 'cid', array(), '', 'array' );
		
		// Check if I am a Super Admin
		$iAmSuperAdmin	= $user->authorise('core.admin');

		// Trigger the onUserBeforeSave event.
		JPluginHelper::importPlugin('user');
		$dispatcher = JDispatcher::getInstance();
		
		if(count($pks) < 1)	{
			$this->setError(JText::_('No Users selected'));
			return false;
		}

		if (in_array($user->id, $pks))
		{
			$this->setError( JText::_('You cannot delete yourself.') );
			return false;
		}

		// Iterate the items to delete each one.
		foreach ($pks as $i => $pk)
		{	
			if ($table->load($pk))
			{
				// Access checks.
				$allow = $user->authorise('core.delete', 'com_users');
				// Don't allow non-super-admin to delete a super admin
				$allow = (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;

				if ($allow)
				{
					// Get users data for the users to delete.
					$user_to_delete = JFactory::getUser($pk);
					
					try
					{
						// Fire the onUserBeforeDelete event.
						$dispatcher->trigger('onUserBeforeDelete', array($table->getProperties()));
	
						if (!$table->delete($pk))
						{
							$this->setError($table->getError());
							return false;
						}
						else
						{
							// Trigger the onUserAfterDelete event.

							$dispatcher->trigger('onUserAfterDelete', array($user_to_delete->getProperties(), true, $this->getError()));
							$db = JFactory::getDbo(); 
							
							$db->setQuery('DELETE FROM #__vquiz_users WHERE userid = '.$pk );
							if (!$db->query()) {									
								$this->setError($db->getErrorMsg());
								return false;
							}
						}
					}
					catch (Exception $e)
					{
						$this->setError($e->getMessage());

						return false;
					}
					
				}
				else
				{
					// Prune items that you can't change.
					unset($pks[$i]);
					$this->setError(JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
					return false;
				}
			}
			else
			{
				$this->setError($table->getError());
				return false;
			}

		}
		
		

		return true;
	}
	
	
	function publish()
	{
	
		JRequest::checkToken() or jexit( JText::_('INVALID_TOKEN') );
		
		// Initialise variables.
		$app		= JFactory::getApplication();
		$dispatcher	= JDispatcher::getInstance();
		$user		= JFactory::getUser();
		
		$iAmSuperAdmin	= $user->authorise('core.admin');
		$table		= JTable::getInstance('user');
		
		JPluginHelper::importPlugin('user');
		
		$pks		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$task		= JRequest::getCmd( 'task' );
		$value  	= $task == 'publish' ? 0 : 1;
		
		if(count($pks) < 1)	{
			$this->setError(JText::_('No Users selected'));
			return false;
		}
		
		// Access checks.
		foreach ($pks as $i => $pk)
		{
			
			if ($value == 1 && $pk == $user->get('id'))
			{
				// Cannot block yourself.
				unset($pks[$i]);
				$this->setError( 'You cannot block yourself.', JText::_('BLOCK') );
				return false;
			}
			elseif ($table->load($pk))
			{
				$old	= $table->getProperties();
				$allow	= $user->authorise('core.edit.state', 'com_users');
				// Don't allow non-super-admin to delete a super admin
				$allow = (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;

				// Prepare the logout options.
				$options = array(
					'clientid' => array(0, 1)
				);

				if ($allow)
				{
					// Skip changing of same state
					if ($table->block == $value)
					{
						unset($pks[$i]);
						continue;
					}

					$table->block = (int) $value;

					// Allow an exception to be thrown.
					try
					{
						if (!$table->check())
						{
							$this->setError($table->getError());
							return false;
						}

						// Trigger the onUserBeforeSave event.
						$result = $dispatcher->trigger('onUserBeforeSave', array($old, false, $table->getProperties()));
						if (in_array(false, $result, true))
						{
							// Plugin will have to raise it's own error or throw an exception.
							return false;
						}

						// Store the table.
						if (!$table->store())
						{
							$this->setError($table->getError());
							return false;
						}

						// Trigger the onAftereStoreUser event
						$dispatcher->trigger('onUserAfterSave', array($table->getProperties(), false, true, null));
					}
					catch (Exception $e)
					{
						$this->setError($e->getMessage());

						return false;
					}

					// Log the user out.
					if ($value)
					{
						$app->logout($table->id, $options);
					}
				}
				else
				{
					// Prune items that you can't change.
					unset($pks[$i]);
					$this->setError(JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
					return false;
				}
			}
		}
		
		return true;
	
	}
	
}

?>