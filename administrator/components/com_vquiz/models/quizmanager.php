<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class VquizModelQuizmanager extends JModelList {

	function __construct() {
		parent::__construct();
		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.quizmanager.list.';
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest($context . 'limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

		$filter_language = $mainframe->getUserStateFromRequest($context . 'filter_language', 'filter_language', '');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);


		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);

		$array = JRequest::getVar('cid', 0, '', 'array');
		$this->setId((int) $array[0]);
	}

	function _buildQuery() {
		$db = JFactory::getDBO();

		$query = "SELECT i.*, count(u.quizzesid) as totalquestion , x.quiztitle as categoryname FROM #__vquiz_quizzes as i left join #__vquiz_question as u ON i.id = u.quizzesid left join #__vquiz_category as x ON x.id = i.quiz_categoryid";

		return $query;
	}

	function setId($id) {
		$this->_id = $id;
		$this->_data = null;
	}

	function &getItem() {

		if (empty($this->_data)) {
			$query = ' SELECT * FROM #__vquiz_quizzes ' .
					'  WHERE id = ' . $this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
		}

		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->quizzes_title = null;
			$this->_data->quiz_categoryid = null;
			$this->_data->resultcats_id = null;
			$this->_data->startpublish_date = null;
			$this->_data->endpublish_date = null;
			$this->_data->question_categoryid = null;
			$this->_data->description = null;
			$this->_data->explanation = null;
			$this->_data->published = null;
			$this->_data->featured = null;
			$this->_data->total_timelimit = null;
			$this->_data->question_timelimit = null;
			$this->_data->passed_score = null;
			$this->_data->random_question = null;
			$this->_data->prev_button = null;
			$this->_data->skip_button = null;
			$this->_data->show_correctans = null;
			$this->_data->show_explanation = null;
			$this->_data->penality = null;
			$this->_data->paging = null;
			$this->_data->paging_limit = null;
			$this->_data->accessuser = null;
			$this->_data->attemped_count = null;
			$this->_data->attemped_delay = null;
			$this->_data->delay_periods = null;
			$this->_data->quiz_categoryname = null;
			$this->_data->graph_type = null;
			$this->_data->show_flage = null;
			$this->_data->show_livescore = null;
			$this->_data->optinscoretype = null;
			$this->_data->question_limit = null;
			$this->_data->meta_keyword = null;
			$this->_data->meta_desc = null;
			$this->_data->display_userscore = null;
			$this->_data->ordering = null;
			$this->_data->access = null;
			$this->_data->language = null;
		}

		return $this->_data;
	}

	function &getItems() {
		// Lets load the data if it doesn't already exist
		if (empty($this->_data)) {
			$query = $this->_buildQuery();
			$filter = $this->_buildContentFilter();
			$orderby = $this->_buildItemOrderBy();

			$query .= $filter;
			$query .= $orderby;
			//$this->_data = $this->_getList( $query );
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}



		return $this->_data;
	}

	function getTotal() {

		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$query .= $this->_buildContentFilter();
			$query .= $this->_buildItemOrderBy();
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function _buildItemOrderBy() {
		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.quizmanager.list.';

		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$orderby = ' group by i.id order by ' . $filter_order . ' ' . $filter_order_Dir . ' ';

		return $orderby;
	}

	function getPagination() {
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_pagination;
	}

	function _buildContentFilter() {

		$requestcategoryid = JRequest::getVar('qcategoryid');

		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.quizmanager.list.';
		$qcategoryid = $mainframe->getUserStateFromRequest($context . 'qcategoryid', 'qcategoryid', '', 'string');
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$categorysearch = $mainframe->getUserStateFromRequest($context . 'categorysearch', 'categorysearch', '', 'integer');
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');
		$search = JString::strtolower($search);

		$where = array();

		if ($publish_item) {

			if ($publish_item == 'p')
				$where[] = 'i.published= 1';

			else if ($publish_item == 'u')
				$where[] = 'i.published = 0';
		}

		if ($qcategoryid and $requestcategoryid) {
			$where[] = 'i.quiz_categoryid LIKE ' . $this->_db->Quote('%' . $this->_db->escape($qcategoryid, true) . '%', false);
		} else if ($categorysearch) {
			$where[] = 'i.quiz_categoryid=' . $this->_db->Quote($this->_db->escape($categorysearch, true), false);
		}


		if ($search) {
			if (is_numeric($search)) {
				$where[] = 'LOWER( i.id ) =' . $this->_db->Quote($this->_db->escape($search, true), false);
			} else {

				$where[] = 'i.quizzes_title LIKE ' . $this->_db->Quote('%' . $this->_db->escape($search, true) . '%', false);
			}
		}

		$where[] = 'i.quiz_categoryid !=1';
		if ($qcategoryid) {
			$where[] = 'i.quiz_categoryid LIKE ' . $this->_db->Quote('%' . $this->_db->escape($qcategoryid, true) . '%', false);
		}

		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $filter;
	}

	function reorder() {
		$mainframe = & JFactory::getApplication();
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		$task = JRequest::getCmd('task', '');
		$inc = ($task == 'orderup' ? -1 : 1);
		if (empty($cid)) {
			return JError::raiseWarning(500, 'No items selected');
		}
		$row = & $this->getTable();
		$row->load((int) $cid[0]);
		$row->move($inc);
		return 'reordered successfully!';
	}

	function saveOrder() {
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		if (empty($cid)) {
			return JError::raiseWarning(500, 'No items selected');
		}
		$total = count($cid);
		$row = & $this->getTable();
		$groupings = array();
		$order = JRequest::getVar('order', array(0), 'post', 'array');
		JArrayHelper::toInteger($order);
		// update ordering values
		for ($i = 0; $i < $total; $i++) {
			$row->load((int) $cid[$i]);
			// track postions
			$groupings[] = $row->position;
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					return JError::raiseWarning(500, $this->_db->getErrorMsg());
				}
			}
		}

		// execute updateOrder for each parent group
		$groupings = array_unique($groupings);
		foreach ($groupings as $group) {
			$row->reorder();
		}
		return 'New ordering saved';
	}

	function store() {
		$time = time();
		$user = JFactory::getUser();
		$row = & $this->getTable();
		$data = JRequest::get('post');
		$data['description'] = JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['explanation'] = JRequest::getVar('explanation', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['quiz_categoryname'] = JRequest::getVar('quiz_categoryname', '', 'post');
		$data["alias"] = JFilterOutput::stringURLSafe($data['quizzes_title']);

		if (!$data["id"]) {

			$datee = JFactory::getDate();
			$data['created_date'] = $datee->toSQL();
			$data['created_by'] = $user->id;
		}

		$query = ' SELECT image from #__vquiz_quizzes WHERE id = ' . $data["id"];
		$this->_db->setQuery($query);
		$oldimage_path = $this->_db->loadResult();

		//########## code for  pic upload. ###########

		$query = 'select categorythumbnailwidth,categorythumbnailheight from #__vquiz_configuration';
		$this->_db->setQuery($query);
		$configuration_img = $this->_db->loadObject();
		$configuration_width = $configuration_img->categorythumbnailwidth;
		$configuration_height = $configuration_img->categorythumbnailheight;

		$image = JRequest::getVar('image', null, 'FILES', 'array');
		$allowed = array('.jpg', '.jpeg', '.gif', '.png');

		$dirpath = JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/';
		$thumbPath = JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/';

		jimport('joomla.filesystem.file');
		$image_name = str_replace(' ', '', JFile::makeSafe($image['name']));
		$image_tmp = $image['tmp_name'];
		$time = time();


		if ($image_name <> "") {

			$ext = strrchr($image_name, '.');
			if (!in_array($ext, $allowed)) {
				$msg_error = JText::_('This Image type is not allowed');
				$this->setError($msg_error);
				return false;
			}
			//=============================//

			$size = getimagesize($image_tmp);
			$src_w = $size[0];
			$src_h = $size[1];
			//set the height and width in proportions
			if ($src_w > $src_h) {
				if (!empty($configuration_width) and is_numeric($configuration_width))
					$width = $configuration_width;
				else
					$width = 125; //New width of image
				if (!empty($configuration_height) and is_numeric($configuration_height))
					$height = $configuration_height;
				else
					$height = $size[1] / $size[0] * $width; //This maintains proportions
				$width1 = 300; //New width of image
				$height1 = $size[1] / $size[0] * $width; //This maintains proportions
			}else {
				if (!empty($configuration_height) and is_numeric($configuration_height))
					$height = $configuration_height;
				else
					$height = 125;
				if (!empty($configuration_width) and is_numeric($configuration_width))
					$width = $configuration_width;
				else
					$width = $size[0] / $size[1] * $height; //This maintains proportions
				$height1 = 360;
				$width1 = $size[0] / $size[1] * $height; //This maintains proportions
			}
			// set image new width and height
			$width1 = 265; //New width of image
			$height1 = $size[1] / $size[0] * $width; //This maintains proportions

			$new_image = imagecreatetruecolor($width, $height);
			$new_image1 = imagecreatetruecolor($width1, $height1);

			if ($size['mime'] == "image/jpeg")
				$tmp = imagecreatefromjpeg($image_tmp);
			elseif ($size['mime'] == "image/gif")
				$tmp = imagecreatefromgif($image_tmp);
			else
				$tmp = imagecreatefrompng($image_tmp);
			imagecopyresampled($new_image, $tmp, 0, 0, 0, 0, $width, $height, $src_w, $src_h);


			if ($size['mime'] == "image/jpeg")
				imagejpeg($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);

			elseif ($size['mime'] == "image/gif")
				imagegif($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);


			else
				imagepng($new_image, $thumbPath . 'thumb_' . $time . '_' . $image_name);

			imagecopyresampled($new_image1, $tmp, 0, 0, 0, 0, $width1, $height1, $src_w, $src_h);


			if ($size['mime'] == "image/jpeg")
				imagejpeg($new_image1, $dirpath . $time . '_' . $image_name);

			elseif ($size['mime'] == "image/gif")
				imagegif($new_image1, $dirpath . $time . '_' . $image_name);
			else
				imagepng($new_image1, $dirpath . $time . '_' . $image_name);

			//======= end of thumbail code========//
			if (move_uploaded_file($image_tmp, $dirpath . $time . '_' . $image_name))
				$data['image'] = $time . '_' . $image_name;
		}




		if ($data['image']) {

			unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/' . $oldimage_path);
			unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/' . 'thumb_' . $oldimage_path);
		}


		if (!$row->bind($data)) {

			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		// Make sure the wproperty record is valid
		if (!$row->check()) {

			$this->setError($this->_db->getErrorMsg());

			return false;
		}
		// Store the web link table to the database
		if (!$row->store()) {
			$this->setError($row->getErrorMsg());
			return false;
		}

		if (!$data['id'])
			JRequest::setVar('id', $row->id);
		return true;
	}

	function messagestore() {

		$session = JFactory::getSession();
		$quizid = JRequest::getInt('quizid');
		$data = JRequest::get('post');
		$data['text1'] = JRequest::getVar('text1', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['text2'] = JRequest::getVar('text2', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['text3'] = JRequest::getVar('text3', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['text4'] = JRequest::getVar('text4', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['text5'] = JRequest::getVar('text5', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['upto1'] = JRequest::getInt('upto1', '0', 'post');
		$data['upto2'] = JRequest::getInt('upto2', '0', 'post');
		$data['upto3'] = JRequest::getInt('upto3', '0', 'post');
		$data['upto4'] = JRequest::getInt('upto4', '0', 'post');
		$data['upto5'] = JRequest::getInt('upto5', '0', 'post');


		$insert = new stdClass();

		$insert->text1 = $data['text1'];
		$insert->text2 = $data['text2'];
		$insert->text3 = $data['text3'];
		$insert->text4 = $data['text4'];
		$insert->text5 = $data['text5'];
		$insert->upto1 = $data['upto1'];
		$insert->upto2 = $data['upto2'];
		$insert->upto3 = $data['upto3'];
		$insert->upto4 = $data['upto4'];
		$insert->upto5 = $data['upto5'];

		try {
			if ($quizid) {

				$query = 'select count(*) from #__vquiz_score_message where quizid=' . $quizid;
				$this->_db->setQuery($query);
				$rcout = $this->_db->loadResult();
				if (!empty($rcout)) {
					$insert->quizid = $quizid;
					$this->_db->updateObject('#__vquiz_score_message', $insert, 'quizid');
				} else {
					$insert->quizid = $quizid;
					$this->_db->insertObject('#__vquiz_score_message', $insert, 'id');
				}

				JRequest::setVar('id', $quizid);
				$session->clear('message_text');
			} else {

				$session->set('message_text', $data);
			}
		} catch (Exception $e) {
			$this->setError($e->getMessage());

			return false;
		}

		return true;
	}

	function getScoremessages() {
		$quizid = JRequest::getInt('id');
		$query = 'select * from #__vquiz_score_message where quizid=' . $quizid;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		return $result;
	}

	function publish() {

		$cid = JRequest::getVar('cid', array(), 'post', 'array');

		$task = JRequest::getCmd('task');

		$publish = ($task == 'publish') ? 1 : 0;

		$n = count($cid);



		if (empty($cid)) {

			return 'No item selected';
		}

		$cids = implode(',', $cid);

		//implode() convert array into string		

		$query = 'UPDATE #__vquiz_quizzes SET published = ' . (int) $publish . ' WHERE id IN ( ' . $cids . ' )';

		$this->_db->setQuery($query);


		if (!$this->_db->query())
			return $this->_db->getErrorMsg();

		else
			return ucwords($task) . 'ed successfully.';
	}

	function featured() {

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		$task = JRequest::getCmd('task');
		$featured = ($task == 'featured') ? 1 : 0;
		$n = count($cid);


		if (empty($cid)) {
			return 'No item selected';
		}

		$cids = implode(',', $cid);
		$query = 'UPDATE #__vquiz_quizzes SET featured = ' . (int) $featured . ' WHERE id IN ( ' . $cids . ' )';
		$this->_db->setQuery($query);

		if (!$this->_db->query())
			return $this->_db->getErrorMsg();
		else
			return ucwords($task) . 'ed successfully.';
	}

	function getCategory() {

		$query = 'select a.id , a.quiztitle , a.level ';
		$query .=' from #__vquiz_category As a ';
		$query .=' LEFT join #__vquiz_category AS b ON a.lft > b.lft AND a.rgt < b.rgt';

		$where = array();
		$where[] = 'a.id !=1';
		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
		$query .= $filter;
		$query .=' group by a.id, a.quiztitle, a.level, a.lft, a.rgt, a.parent_id order by a.lft ASC';

		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function delete() {

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');

		$row = & $this->getTable();

		if (count($cids)) {

			foreach ($cids as $cid) {

				$query = ' SELECT image from #__vquiz_quizzes WHERE id = ' . $cid;
				$this->_db->setQuery($query);
				$oldimage_path = $this->_db->loadResult();

				if (!empty($oldimage_path)) {
					unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/' . $oldimage_path);
					unlink(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/' . 'thumb_' . $oldimage_path);
				}

				if (!$row->delete($cid)) {

					$this->setError($row->getErrorMsg());

					return false;
				}

				if ($row->delete($cid)) {

					$query = ' SELECT id FROM #__vquiz_question WHERE quizzesid = ' . $cid;
					$this->_db->setQuery($query);
					$question_id = $this->_db->loadColumn();

					$q = 'delete FROM #__vquiz_question WHERE quizzesid = ' . $cid;
					$this->_db->setQuery($q);
					$this->_db->execute();

					if (!empty($question_id)) {

						$q = 'delete FROM #__vquiz_option WHERE qid IN (' . implode(',', $question_id) . ')';
						$this->_db->setQuery($q);
						$this->_db->execute();
					}
				}
			}
		}

		return true;
	}

	function getAccess() {
		$query = 'select * from #__usergroups order by id asc';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();

		return $result;
	}

	function movequestion() {

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$categoryid = JRequest::getInt('quizcategoryidmove');

		if (count($cids)) {
			foreach ($cids as $cid) {

				$query = 'UPDATE #__vquiz_quizzes SET quiz_categoryid = ' . $categoryid . ' WHERE id = ' . $cid;
				$this->_db->setQuery($query);

				if (!$this->_db->query()) {
					$msg = $this->setError($this->_db->stderr());
					$this->setRedirect('index.php?option=com_vquiz&view=quizmanager', $msg);
					return false;
				}
			}
		}
		return true;
	}

	function copyquestion() {

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$categoryid = JRequest::getInt('quizcategoryidcopy');

		if (count($cids)) {
			foreach ($cids as $cid) {

				$query = ' SELECT * FROM #__vquiz_quizzes WHERE id = ' . $cid;
				$this->_db->setQuery($query);
				$result = $this->_db->loadObject();

				$insert = new stdClass();
				$insert->id = null;
				$insert->quiz_categoryid = $categoryid;
				$insert->resultcats_id = $result->resultcats_id;
				$insert->quiz_categoryname = $result->quiz_categoryname;
				$insert->startpublish_date = $result->startpublish_date;
				$insert->endpublish_date = $result->endpublish_date;
				$insert->quizzes_title = $result->quizzes_title;
				$insert->alias = $result->alias;
				$insert->passed_score = $result->passed_score;
				$insert->total_timelimit = $result->total_timelimit;
				$insert->totaltime_parameter = $result->totaltime_parameter;
				$insert->random_question = $result->random_question;
				$insert->prev_button = $result->prev_button;
				$insert->skip_button = $result->skip_button;
				$insert->show_flage = $result->show_flage;

				$insert->show_livescore = $result->show_livescore;
				$insert->show_correctans = $result->show_correctans;
				$insert->show_explanation = $result->show_explanation;
				$insert->penality = $result->penality;
				$insert->display_userscore = $result->display_userscore;
				$insert->graph_type = $result->graph_type;
				$insert->paging = $result->paging;
				$insert->paging_limit = $result->paging_limit;
				$insert->accessuser = $result->accessuser;
				$insert->attemped_count = $result->attemped_count;
				$insert->attemped_delay = $result->attemped_delay;
				$insert->delay_periods = $result->delay_periods;
				$insert->description = $result->description;
				$insert->explanation = $result->explanation;
				$insert->created_date = $result->created_date;

				$insert->published = $result->published;
				$insert->featured = $result->featured;
				$insert->ordering = $result->ordering;
				$insert->language = $result->language;
				$insert->access = $result->access;

				if (!$this->_db->insertObject('#__vquiz_quizzes', $insert, 'id')) {
					$msg = $this->setError($this->_db->stderr());
					$this->setRedirect('index.php?option=com_vquiz&view=quizmanager', $msg);
					return false;
				}
				$queszid = $this->_db->insertid();

				$query = ' SELECT * FROM #__vquiz_question WHERE quizzesid = ' . $cid;
				$this->_db->setQuery($query);
				$question = $this->_db->loadObjectList();




				for ($i = 0; $i < count($question); $i++) {

					$insertquestion = new stdClass();
					$insertquestion->id = null;
					$insertquestion->quizzesid = $queszid;
					$insertquestion->questiontime_parameter = $question[$i]->questiontime_parameter;
					$insertquestion->question_timelimit = $question[$i]->question_timelimit;
					$insertquestion->optiontype = $question[$i]->optiontype;
					$insertquestion->qtitle = $question[$i]->qtitle;
					$insertquestion->explanation = $question[$i]->explanation;
					$insertquestion->scoretype = $question[$i]->scoretype;
					$insertquestion->score = $question[$i]->score;
					$insertquestion->expire_timescore = $question[$i]->expire_timescore;
					$insertquestion->penality = $question[$i]->penality;
					$insertquestion->created_date = $question[$i]->created_date;
					$insertquestion->modified_date = $question[$i]->modified_date;
					$insertquestion->published = $question[$i]->published;
					$insertquestion->featured = $question[$i]->featured;


					if (!$this->_db->insertObject('#__vquiz_question', $insertquestion, 'id')) {

						$msg = $this->setError($this->_db->stderr());
						$this->setRedirect('index.php?option=com_vquiz&view=quizquestion', $msg);
						return false;
					}


					$qid = $this->_db->insertid();

					$query = ' SELECT * FROM #__vquiz_option WHERE qid = ' . $question[$i]->id;
					$this->_db->setQuery($query);
					$option = $this->_db->loadObjectList();


					for ($j = 0; $j < count($option); $j++) {

						$insertoption = new stdClass();
						$insertoption->id = null;
						$insertoption->qid = $qid;
						$insertoption->qoption = $option[$j]->qoption;
						$insertoption->correct_ans = $option[$j]->correct_ans;
						$insertoption->options_score = $option[$j]->options_score;


						if (!$this->_db->insertObject('#__vquiz_option', $insertoption, 'id')) {

							$msg = $this->setError($this->_db->stderr());
							$this->setRedirect('index.php?option=com_vquiz&view=quizquestion', $msg);

							return false;
						}
					}
				}
			}
		}

		return true;
	}

	function drawChart() {


		$quizzesid = JRequest::getInt('quizzesid');
		$charttype = JRequest::getInt('charttype');
		$date = JFactory::getDate();
		$obj = new stdClass();
		$obj->result = "error";
		$obj->charttype = $charttype;

		$q = 'SELECT quiz_answers from #__vquiz_quizresult WHERE quizzesid =' . $quizzesid;
		$this->_db->setQuery($q);
		$result = $this->_db->loadRowList();

		$options = array();
		$countoption = array();
		$array = array();

		for ($i = 0; $i < count($result); $i++) {
			$xx = json_decode($result[$i][0]);

			for ($j = 0; $j < count($xx); $j++) {
				$zz = $xx[$j];
				for ($k = 0; $k < count($zz); $k++) {
					$kk = $zz[$k];
					array_push($countoption, $kk);
				}
			}
		}

		$choosedoption = array_count_values($countoption);
		$keyvalue = array_keys($choosedoption);


		$query = 'SELECT score,count(userid) as totaluser from #__vquiz_quizresult WHERE quizzesid =' . $quizzesid . ' group by score order by score asc';
		$this->_db->setQuery($query);
		$result = $this->_db->loadRowList();
		$obj->responce1 = $result;

		$maxoption = 0;
		$query = 'select id from #__vquiz_question where  quizzesid = ' . $this->_db->quote($quizzesid);
		$this->_db->setQuery($query);
		$loadcolumn = $this->_db->loadColumn();

		$query = 'select count(qid) from #__vquiz_option where qid in (' . implode(',', $loadcolumn) . ') group by qid';
		$this->_db->setQuery($query);
		$maxid = $this->_db->loadColumn();

		for ($ck = 0; $ck < count($maxid); $ck++) {
			$xx = $maxid[$ck];
			$maxoption = $xx > $maxoption ? $xx : $maxoption;
		}


		$query = 'select id,qtitle from #__vquiz_question where  quizzesid = ' . $this->_db->quote($quizzesid) . ' order by id asc';
		$this->_db->setQuery($query);
		$questions = $this->_db->loadObjectList();

		$arr = array();
		$qu = array('Question');
		$xx = array();
		for ($h = 0; $h < $maxoption; $h++) {
			array_push($xx, chr(65 + $h));
		}

		$merg = array_merge($qu, $xx);
		array_push($arr, $merg);


		for ($k = 0; $k < count($questions); $k++) {

			$query = 'select id from #__vquiz_option where qid = ' . $this->_db->quote($questions[$k]->id) . ' order by id asc';
			$this->_db->setQuery($query);
			$options = $this->_db->loadColumn();

			$oo = array();


			for ($q = 0; $q < $maxoption; $q++) {

				if (in_array($options[$q], $keyvalue)) {
					foreach ($choosedoption as $key => $value) {
						if ($key == $options[$q])
							array_push($oo, $value);
					}
				}
				else
					array_push($oo, 0);
			}

			$ques = array(strip_tags($questions[$k]->qtitle));
			$x[$k] = array_merge($ques, $oo);
			array_push($arr, $x[$k]);
		}

		$obj->responce2 = $arr;



		$obj->result = "success";

		return $obj;
	}
	
	
	function getResultcats() {

		$query = 'select a.id , a.quiztitle , a.level ';
		$query .=' from #__vquiz_resultcats As a ';
		$query .=' LEFT join #__vquiz_resultcats AS b ON a.lft > b.lft AND a.rgt < b.rgt';

		$where = array();
		$where[] = 'a.id !=1';
		$where[] = 'a.level =1';
		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
		$query .= $filter;
		$query .=' group by a.id, a.quiztitle, a.level, a.lft, a.rgt, a.parent_id order by a.lft ASC';

		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

}