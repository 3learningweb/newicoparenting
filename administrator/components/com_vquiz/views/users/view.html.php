<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class VquizViewUsers extends JViewLegacy
{
	
	public function display($tpl = null)
    {
		
		$mainframe = JFactory::getApplication();
		$context			= 'com_vquiz.users.list';
		$layout = JRequest::getCmd('layout', '');
		
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );
        $filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
		
		$search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '', 'string' );
		$search = JString::strtolower( $search );
        		
		if($layout == 'form')	{
        	
			$this->item = $this->get('Item');
			$isNew		= ($this->item->id < 1);
			
			if($isNew)
				JToolBarHelper::title( JText::_( 'USER' ).': '.JText::_('NEW').'', 'vquizusers' );
			
			else	{
				JToolBarHelper::title( JText::_( 'USER' ).': '.JText::_('EDIT').'', 'vquizusers' );
			}
			
			JToolBarHelper::apply();
			JToolBarHelper::save();
			JToolBarHelper::cancel();
			
		}
		
		else	{
		
			JToolBarHelper::title( JText::_( 'USERS' ), 'vquizusers' );
			JToolBarHelper::deleteList();
        	JToolBarHelper::editList();
        	JToolBarHelper::addNew();
			JToolbarHelper::publish('publish', JText::_( 'Activate' ));
			JToolbarHelper::unpublish('unpublish', JText::_( 'Block' ));
			
			$this->items = $this->get('Items');
			
			$this->pagination = $this->get('Pagination');
						
	
			// Table ordering.
			$this->lists['order_Dir'] = $filter_order_Dir;
			$this->lists['order']     = $filter_order;
			// search filter
			$this->lists['search']= $search;
			
		}
		
		parent::display($tpl);
        
    }
  
  
}
