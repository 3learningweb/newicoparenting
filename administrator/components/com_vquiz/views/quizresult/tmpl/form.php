<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addScript('components/com_vquiz/assets/js/script.js');
?>

<form action="index.php?option=com_vquiz" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <div class="col101 result_table">
                <fieldset class="adminform">
                    <legend><?php echo JText::_( 'DETAILS' ); ?></legend>
                            <div align="center">
                            </div>
                            <table width="100%">
                            <tbody>
                            <tr><th class="sectiontableheader" colspan="2"><?php echo JText::_( 'RESULT' ); ?></th></tr>
                            <tr>
                            <td width="50%"><label><?php echo JText::_( 'QUIZ_NAME' ); ?></label></td>
                            <td><?php  echo $this->item->quiztitle;?></td>
                            </tr>
                            <tr>
                             <tr>
                            <td><label><?php echo JText::_( 'USER_NAME' ); ?></label></td>
                            <td>	
							<?php 					
                            if($this->item->userid==0)
                            echo 'Guest';
                            else
							echo $this->item->username; 
                            ?> 
               				 </td>
 		                      </tr>
 					          <tr>
                            <td><label><?php echo JText::_( 'RESULT' ); ?></label></td>
                            <td><?php echo $this->item->score ?> / <?php echo $this->item->maxscore ?></td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('PERCENTAGE'); ?></label></td>
                            <td><?php echo ($this->item->score/$this->item->maxscore*100);?> %</td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('STATUS'); ?></label></td>
                            <td>           
							<?php if(($this->item->score/$this->item->maxscore*100)>=$this->item->passed_score)
                            echo '<span style="color:green">Passed</span>';	
                            else
                            echo '<span style="color:red">Failed</span>'; ?>
          					</td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('STARTTIME');?></label></td>
                            <td><?php echo $this->item->startdatetime; ?></td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('ENDTIME');?></label></td>
                            <td><?php echo $this->item->enddatetime; ?></td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('SPENT_TIME');?></label></td>
                            <td><?php echo $this->item->quiz_spentdtime;?> <?php echo JText::_( 'SECONDS' );?></td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('PASSED_PERCENTAGE');?></label></td>
                            <td><?php echo $this->item->passed_score;?>  %</td>
                            </tr>
                            <tr>
                            <td><label><?php echo JText::_('FLAG');?></label></td>
                            <td><?php echo $this->item->flag;?></td>
                            </tr>
                            </tbody>
                            </table>

                </fieldset>
                <div class="all_quiz">
					<?php for( $i=0;$i<count($this->showresult->question_array);$i++){
                    $given_answer=explode(',',$this->showresult->givenanswer[$i]);
                    ?>
                        <div class="result_quiz">
                        <div class="result_quiz_data">
                        <span class="qbx"><?php echo JText::_('QUESTION');echo $i+1; ?></span>
                        <h4><?php echo $this->showresult->question[$i]->qtitle;?> </h4>
                        </div>
                  <table border="0" width="100%" cellpadding="5" cellspacing="0">
                <tr>
                <th><?php echo JText::_('COUNT');?></th><th><?php echo JText::_('OPTIONS');?></th>
                
                <th>
				<?php
                 if($this->showresult->optiontypescore==1)
					 echo JText::_('YOUR_ANSWER');
				 elseif($this->showresult->optiontypescore==2){
					 echo JText::_("(Point)");
				 	 echo JText::_('YOUR_ANSWER');
				 }

					for($k=0;$k<count($given_answer);$k++){
						if ($given_answer[$k]==0)
						echo '<label class="skipquest">('.JText::_('SKIP_QUESTION').')</label>';
					}
				 ?>
                
                </th>
                </tr>
                
                
                <?php for( $j=0;$j<count($this->showresult->options[$i]);$j++){?>
                  
                    <tr>
                        <td><p><?php echo $j+1;?></p></td>
                        
                        <td><p><?php echo $this->showresult->options[$i][$j]->qoption;?></p></td>
                        
                        <td>
                        <p style="text-align:center;">
                         
                        
                        <?php 
						
						if($this->showresult->optiontypescore==1)
						{
 							if($this->showresult->options[$i][$j]->correct_ans==1){
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
							for($k=0;$k<count($given_answer);$k++){
							if ($given_answer[$k]==$this->showresult->options[$i][$j]->id and ($this->showresult->options[$i][$j]->correct_ans!=1) )
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/delete-icon.png" />';
							}
				 
						}
						
						elseif($this->showresult->optiontypescore==2)
						{
							for($k=0;$k<count($given_answer);$k++){
							echo $this->showresult->options[$i][$j]->options_score;
							if($this->showresult->options[$i][$j]->id==$given_answer[$k])
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
						}
                        ?>
                        
                        </p>
                        </td>
                        
                    </tr> 
                <?php }?>
            </table>    
             </div>    
             <?php }?>
            </div>

</div>
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="quizresult" />
</form>







