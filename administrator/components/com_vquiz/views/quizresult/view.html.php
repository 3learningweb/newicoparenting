<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );
class VquizViewQuizresult extends JViewLegacy
{ 
 
		function display($tpl = null)
 		{

	          $mainframe =JFactory::getApplication();
		      $context	= 'com_vquiz.result.list.';
			  $layout = JRequest::getCmd('layout', '');
			  $search = $mainframe->getUserStateFromRequest( $context.'search', 'search', '',	'string' );
		      $search = JString::strtolower( $search );
			  $startdatesearch = $mainframe->getUserStateFromRequest( $context.'startdatesearch', 'startdatesearch', '',	'string' );
			  $enddatesearch = $mainframe->getUserStateFromRequest( $context.'enddatesearch', 'enddatesearch', '',	'string' );
			  
			  $filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );
       		  $filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
 			  $this->config = $this->get('Config');	
 				if($layout == 'form')
 				{
 					$item =$this->get('Item');
					$this->assignRef( 'item', $item );
					$this->showresult=$this->get('Showresult');
					JToolBarHelper::title( JText::_( 'Details Quiz Result' ), 'quizresult.png' );	

					JToolBarHelper::cancel();
//					JToolBarHelper::help('help', true);

			}

			else	{

				JToolBarHelper::title( JText::_('QUIZRESULT'), 'quizresult.png' );
  				JToolBarHelper::deleteList('Do you want to delete the record(s)');
  				JToolBarHelper::custom( 'export', 'download', 'download', JText::_( 'CSV-Export' ), false );
				//JToolBarHelper::custom( 'sendmail', 'sendmail', 'sendmail', JText::_( 'Send Mail' ), false );
//  	            JToolBarHelper::help('help', true);
 				$items = $this->get('Items');
 				$this->assignRef( 'items', $items );
 				$this->pagination = $this->get('Pagination');
 				$lists['search']= $search;
				$lists['startdatesearch']= $startdatesearch;
				$lists['enddatesearch']= $enddatesearch;
				$this->assignRef( 'lists', $lists );
				
				// Table ordering.
				$this->lists['order_Dir'] = $filter_order_Dir;
				$this->lists['order']     = $filter_order;

  			   }
 
			 parent::display($tpl);
 
		 }
 
}

 