<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.html.list');

class VquizViewResultcats extends JViewLegacy {

	function getSortFields() {
		return array('i.lft' => JText::_('JGRID_HEADING_ORDERING'),
			'i.id' => JText::_('JGRID_HEADING_ID'));
	}

	function display($tpl = null) {


		$mainframe = JFactory::getApplication();
		$context = 'com_vquiz.category.list.';
		$layout = JRequest::getCmd('layout', '');
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$search = JString::strtolower($search);
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');

		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$this->config = $this->get('Config');


		if ($layout == 'form') {

			$this->parentcategory = $this->get('Parentcategory');
			$this->item = $this->get('Item');
			$isNew = ($this->item->id < 1);

			$this->lists['access'] = JHtml::_('access.assetgrouplist', 'access', $this->item->access);

			if ($isNew) {

				JToolBarHelper::title(JText::_('NEWCATEGORY'), 'quizcategories.png');
				JToolBarHelper::apply();
				JToolBarHelper::save();
				JToolBarHelper::cancel();
//						JToolBarHelper::help('help', true);
			} else {

				JToolBarHelper::title(JText::_('EDITCATEGORY'), 'quizcategories.png');
				JToolBarHelper::apply();
				JToolBarHelper::save();
				JToolBarHelper::cancel();
//						JToolBarHelper::help('help', true);
			}
		} else {

			JToolBarHelper::title(JText::_('RESULTCATS'), 'quizcategories.png');
			JToolBarHelper::addNew();
			JToolBarHelper::deleteList('Do you want to delete the record(s)');
			JToolBarHelper::editList();

			JToolBarHelper::publish();
			JToolBarHelper::unpublish();
//				JToolBarHelper::help('help', true);
			$items = $this->get('Items');
			$this->assignRef('items', $items);

			$this->pagination = $this->get('Pagination');

			$lists['search'] = $search;
			$lists['publish_item'] = $publish_item;
			$this->assignRef('lists', $lists);

			// Table ordering.
			$this->lists['order_Dir'] = $filter_order_Dir;
			$this->lists['order'] = $filter_order;

			$this->ordering = array();
			// Preprocess the list of items to find ordering divisions.
			foreach ($this->items as $item) {
				$this->ordering[$item->parent_id][] = $item->id;
			}
		}
		parent::display($tpl);
	}

}

