<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
JHTML::_('behavior.modal');

	$session=JFactory::getSession();
 
		
 	if(!empty($this->scoremessages)){ 
 
			$upto1=$this->scoremessages->upto1;
			$text1=$this->scoremessages->text1;
			$upto2=$this->scoremessages->upto2;
			$text2=$this->scoremessages->text2;
			$upto3=$this->scoremessages->upto3;
			$text3=$this->scoremessages->text3;
			$upto4=$this->scoremessages->upto4;
			$text4=$this->scoremessages->text4;
			$upto5=$this->scoremessages->upto5;
			$text5=$this->scoremessages->text5;
	}
	else if($session->has('message_text')){
			$message=$session->get('message_text');
				$upto1=$message['upto1'];
				$text1=$message['text1'];
				$upto2=$message['upto2'];
				$text2=$message['text2'];
				$upto3=$message['upto3'];
				$text3=$message['text3'];
				$upto4=$message['upto4'];
				$text4=$message['text4'];
				$upto5=$message['upto5'];
				$text5=$message['text5'];
 	}
	else{
			$text1=$text2=$text3=$text4=$text5='';
			$upto1=$upto2=$upto3=$upto4=$upto5='';
		}

 ?>
 
 <div id="toolbar" class="btn-toolbar">
<div id="toolbar-apply" class="btn-wrapper">
	<button class="btn btn-small btn-success" onclick="Joomla.submitbutton('savemessage')">
	<span class="icon-apply icon-white"></span>
	Save</button>
</div>
 
</div>

<form action="index.php?option=com_vquiz" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="cpanel-left">
<fieldset class="adminform">
	   <legend><?php echo JText::_('Messages' ); ?></legend>
	   
                <fieldset>
                <legend><?php echo JText::_('Text:1' ); ?></legend>
                <div>
                <?php echo JText::_('Upto  (Range)'); ?> 
                <input type="text" name="upto1" id="upto1" value="<?php echo $upto1;?>" />
                <?php 
                $editor = JFactory::getEditor();
                echo $editor->display("text1", $text1, "400", "100", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                ?> 
                </div> 
                </fieldset>

                <fieldset>
                <legend><?php echo JText::_('Text:2' ); ?></legend>
                <div>
                <?php echo JText::_('Upto  (Range)'); ?> 
                <input type="text" name="upto2" id="upto2" value="<?php echo $upto2;?>" />
                <?php 
                $editor = JFactory::getEditor();
                echo $editor->display("text2", $text2, "400", "100", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                ?> 
                </div> 
                </fieldset>
		
                <fieldset>
                <legend><?php echo JText::_('Text:3' ); ?></legend>
                <div>
                <?php echo JText::_('Upto  (Range)'); ?> 
                <input type="text" name="upto3" id="upto3" value="<?php echo $upto3;?>" />
                <?php 
                $editor = JFactory::getEditor();
                echo $editor->display("text3",$text3, "400", "100", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                ?> 
                </div> 
                </fieldset>
		
                <fieldset>
                <legend><?php echo JText::_('Text:4' ); ?></legend>
                <div>
                <?php echo JText::_('Upto  (Range)'); ?> 
                <input type="text" name="upto4" id="upto4" value="<?php echo $upto4;?>" />
                <?php 
                $editor = JFactory::getEditor();
                echo $editor->display("text4", $text4, "400", "100", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                ?> 
                </div> 
                </fieldset>
		
                <fieldset>
                <legend><?php echo JText::_('Text:5' ); ?></legend>
                <div>
                <?php echo JText::_('Upto  (Range)'); ?> 
                <input type="text" name="upto5" id="upto5" value="<?php echo $upto5;?>" />
                <?php 
                $editor = JFactory::getEditor();
                echo $editor->display("text5",$text5, "400", "100", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                ?> 
                </div> 
                </fieldset>

         

    </fieldset>

 
</div>
   
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="quizmanager" />
<input type="hidden" name="quizid" value="<?php echo JRequest::getInt('id');?>" />
</form>