<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class VquizViewQuizmanager extends JViewLegacy {

	function display($tpl = null) {

		$mainframe = JFactory::getApplication();
		$context = 'com_vquiz.quizmanager.list.';
		$layout = JRequest::getCmd('layout', '');
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$search = JString::strtolower($search);
		$qcategoryid = $mainframe->getUserStateFromRequest($context . 'qcategoryid', 'qcategoryid', '', 'string');
		$categorysearch = $mainframe->getUserStateFromRequest($context . 'categorysearch', 'categorysearch', '', 'string');
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');

		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$this->config = $this->get('Config');

		// 圖表清單
		$this->graph_types["Doughnut"] = "環狀圖";
		$this->graph_types["Pie"] = "圓餅圖";
		$this->graph_types["PolarArea"] = "極地圖";
		$this->graph_types["Bar"] = "柱狀圖";
		$this->graph_types["Bara"] = "柱狀圖(Avg)";
		$this->graph_types["Radar"] = "雷達圖";
		$this->graph_types["Radara"] = "雷達圖(Avg)";
		$this->graph_types["Line"] = "折線圖";
		$this->graph_types["Thermometer"] = "溫度計";
		$this->graph_types["Start"] = "五星級";
		$this->graph_types["LikeLove"] = "是愛還是喜歡";
		$this->graph_types["LoveBread"] = "愛情或麵包";
		$this->graph_types["m2f3"] = "共親職";
		
		if ($layout == 'form') {

			$this->access = $this->get('Access');
			$item = $this->get('Item');
			$isNew = ($item->id < 1);
			$this->assignRef('item', $item);
			$this->category = $this->get('Category');
			$this->resultcats = $this->get('Resultcats');

			$this->lists['access'] = JHtml::_('access.assetgrouplist', 'access', $this->item->access, 'size="5"', false);

			if ($isNew) {

				JToolBarHelper::title(JText::_('NEWQUIZ'), 'quizmanager.png');

				JToolBarHelper::apply();

				JToolBarHelper::save();

				JToolBarHelper::cancel();
//						JToolBarHelper::help('help', true);
			} else {

				JToolBarHelper::title(JText::_('EDITQUIZ'), 'quizmanager.png');
				JToolBarHelper::apply();
				JToolBarHelper::save();
				JToolBarHelper::cancel();
//						JToolBarHelper::help('help', true);
			}
		} else if ($layout == 'messages') {
			$this->scoremessages = $this->get('Scoremessages');
		} else {


			JToolBarHelper::title(JText::_('QUIZMANAGER'), 'quizmanager.png');
			JToolBarHelper::addNew();
			JToolBarHelper::deleteList('Do you want to delete the record(s)');
			JToolBarHelper::custom('move', 'move', 'move', JText::_('Move'), false);
			JToolBarHelper::custom('copy', 'copy', 'copy', JText::_('Copy'), false);
			JToolBarHelper::publish();
			JToolBarHelper::unpublish();

			JToolBarHelper::editList();


//				JToolBarHelper::help('help', true);

			$items = $this->get('Items');

			$this->assignRef('items', $items);

			$this->category = $this->get('Category');

			$this->pagination = $this->get('Pagination');

			$lists['search'] = $search;
			$lists['publish_item'] = $publish_item;


			$requestcategoryid = JRequest::getVar('qcategoryid');

			if ($qcategoryid and $requestcategoryid) {
				$lists['categorysearch'] = $qcategoryid;
			}
			else
				$lists['categorysearch'] = $categorysearch;



			$this->assignRef('lists', $lists);

			// Table ordering.
			$this->lists['order_Dir'] = $filter_order_Dir;
			$this->lists['order'] = $filter_order;
		}

		parent::display($tpl);
	}

}

