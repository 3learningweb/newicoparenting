<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class VquizViewQuizquestion extends JViewLegacy {

	function display($tpl = null) {

		$mainframe = JFactory::getApplication();
		$context = 'com_vquiz.question.list.';
		$layout = JRequest::getCmd('layout', '');
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$search = JString::strtolower($search);
		$quizzesid = $mainframe->getUserStateFromRequest($context . 'quizzesid', 'quizzesid', '', 'string');
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');



		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$this->config = $this->get('Config');


		if ($layout == 'form') {

			$item = $this->get('Item');

			$isNew = ($item->id < 1);

			$this->assignRef('item', $item);
			$this->optinscoretype = $this->get('Optinscoretype');
			$this->options = $this->get('Options');
			$this->quizes = $this->get('Quizes');

			$model = $this->getModel();
			$this->quiz = $model->getQuiz($quizzesid);
			$this->resultcats = $model->getResultcats($quizzesid);

			if ($isNew) {

				JToolBarHelper::title(JText::_('NEWQUESTION'), 'quizquestions.png');
				JToolBarHelper::apply();
				JToolBarHelper::save();

				JToolBarHelper::cancel();
//						JToolBarHelper::help('help', true);
			} else {

				JToolBarHelper::title(JText::_('EDITQUESTION'), 'quizquestions.png');

				JToolBarHelper::apply();
				JToolBarHelper::save();
				JToolBarHelper::cancel();
//					   JToolBarHelper::help('help', true);
			}
		} else {

			JToolBarHelper::title(JText::_('QUIZQUESTION'), 'quizquestions.png');

			JToolBarHelper::addNew();

			JToolBarHelper::deleteList('Do you want to delete the record(s)');

			JToolBarHelper::editList();
			JToolBarHelper::publish();
			JToolBarHelper::unpublish();
			JToolBarHelper::custom('move', 'move', 'move', JText::_('Move'), false);
			JToolBarHelper::custom('copy', 'copy', 'copy', JText::_('Copy'), false);
//			JToolBarHelper::custom('import', 'upload', 'upload', JText::_('CSV-Import'), false);
//			JToolBarHelper::custom('export', 'download', 'download', JText::_('CSV-Export'), false);

			$items = $this->get('Items');
//				JToolBarHelper::help('help', true);

			$this->assignRef('items', $items);
			$this->quizes = $this->get('Quizes');

			$this->pagination = $this->get('Pagination');

			$lists['search'] = $search;
			$lists['publish_item'] = $publish_item;
			$lists['quizzesid'] = $quizzesid;
			$this->assignRef('lists', $lists);




			// Table ordering.
			$this->lists['order_Dir'] = $filter_order_Dir;
			$this->lists['order'] = $filter_order;
		}

		parent::display($tpl);
	}

}

