<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.database.tablenested'); 
class TableQuizcategory extends JTableNested//JTable
{	 
			public $id = 0;
			public $quiztitle = null;
			public $alias = null;
			public $photopath= null;
			public $published= null;
			public $language= null;
			public $access= null;
			public $ordering= null;
			public $created_date= null;
			public $level= null;
			public $parent_id= null;
			public $path= null;
			public $extension= null;
			public $lft= null;
			public $rgt= null;

			
			function __construct(& $db) {
	
				parent::__construct('#__vquiz_category', 'id', $db);
		 
			}
		
			function delete($pk = null, $children = false)
				{
					return parent::delete($pk, $children);
				}
			
			function reorder($ids, $inc)
				{  				
					return parent::reorder($ids, $inc);
				}	
 	
				function  load($keys = null, $reset = true)
				{
					return parent::load($keys = null, $reset = true);
				}	
				
  				function move($delta, $where = '')
				{ 
					return parent::move($delta, $where = '');
				}
				
			   function checkOut($userId, $pk = null)
				{ 

					return parent::checkOut($userId, $pk = null);
				}
				
				function bind($array, $ignore = '')
				{ 

					return parent::bind($array, $ignore);
				}
      
		 function check()
			{
				// Check for a title.
				if (trim($this->quiztitle) == '')
				{
					$this->setError(JText::_('JLIB_DATABASE_ERROR_MUSTCONTAIN_A_TITLE_CATEGORY'));
		
					return false;
				}
		
			 	$this->alias = trim($this->alias);
  		
				if (empty($this->alias))
				{
					$this->alias = $this->quiztitle;
				}
		
				$this->alias = JApplication::stringURLSafe($this->alias);
		
				if (trim(str_replace('-', '', $this->alias)) == '')
				{
					$this->alias = JFactory::getDate()->format('Y-m-d-H-i-s');
				}
		
				return true;
			}
	
	  	  
	  
	    function store($updateNulls = false)
		{ 
		 
  			$date = JFactory::getDate();
			$user = JFactory::getUser();
	
 			$table = JTable::getInstance('Category', 'JTable', array('dbo' => $this->getDbo()));
			
			if ($table->load(array('alias' => $this->alias, 'parent_id' => $this->parent_id, 'extension' => $this->extension))
			&& ($table->id != $this->id || $this->id == 0))
			{
			$this->setError(JText::_('JLIB_DATABASE_ERROR_CATEGORY_UNIQUE_ALIAS'));
			
			return false;
			}
	
	
			return parent::store($updateNulls);
		}
	
}