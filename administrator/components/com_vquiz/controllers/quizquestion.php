<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

class VquizControllerQuizquestion extends VquizController
 
{
  				function __construct()
				{
					parent::__construct();
					$this->registerTask( 'add'  , 	'edit' );
					$this->registerTask( 'orderup', 		'reorder' );
					$this->registerTask( 'orderdown', 		'reorder' );
					$this->registerTask( 'unpublish',	'publish' );
				}
		
				function edit()
				{
		
				JRequest::setVar( 'view', 'quizquestion' );
				JRequest::setVar( 'layout', 'form'  );
				
				JRequest::setVar('hidemainmenu', 1);
				parent::display();	
				}
				
				function save()
				{	
		
					 $model = $this->getModel('quizquestion');
		
					if($model->store()) {
						$msg = JText::_('QUESTION_SAVE');
						$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
		
					} else {
		
						jerror::raiseWarning('', $this->model->getError());
		
						$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion');
		
					}
				}
		
		
				function apply()
		
				{
		
		 
					$model = $this->getModel('quizquestion');
		 
					if($model->store()) {
		
					$msg = JText::_('QUESTION_SAVE');
		
					$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion&task=edit&cid[]='.JRequest::getInt('id', 0), $msg );
					} else {
		
					jerror::raiseWarning('', $this->model->getError());
		
						$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion&task=edit&cid[]='.JRequest::getInt('id', 0) );
					}
		
		 
				}
		
		 
				function remove()
		 
				{
		
				$model = $this->getModel('quizquestion');
		
					if(!$model->delete()) 
		
				{
					
				$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
		 
				} 
		
				else 
		
		 
					{
		
					$msg = JText::_( 'Question(s) Deleted' );
					}
					$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
			   
				}
				
				function publish()
				{
				
				$model = $this->getModel('quizquestion');
				$msg = $model->publish();
				$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
				}
		
			function cancel()
				{
		
				$msg = JText::_('CANCELLED');
				$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
		
				}
		function reorder()
				{ 
					$model = $this->getModel('quizquestion');
					$msg = $model->reorder();
			
					$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
				
				}
							
			function saveOrder()
				{
				
					$model = $this->getModel('quizquestion');
					$msg = $model->saveOrder();
			
				$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
				
				}

			  function importquestioncsv()
				{
										
					jimport('joomla.filesystem.file');
					 $db =JFactory::getDBO();
					 					 
					$questioncsv = JRequest::getVar("questioncsv", null, 'files', 'array');
					$quizid = JRequest::getInt('quizid');


					$questioncsv['questioncsv']=str_replace(' ', '', JFile::makeSafe($csv['name']));	
					$temp=$questioncsv["tmp_name"];
								

						if(is_file($temp))	{
 						$fp = fopen($temp, "r");
 						$count = 0;
						while(($data = fgetcsv($fp, 100000, ",", '"')) !== FALSE)
						{
					 
 							$insert = new stdClass();
							$insert->id = null;
						    $insert->quizzesid =$data[1];
							$insert->questiontime_parameter = $data[2];
							$insert->question_timelimit = $data[3];
							$insert->optiontype = $data[4];
							$insert->flagcount = $data[5];
							$insert->qtitle = $data[6];
							$insert->explanation = $data[7];
							$insert->scoretype = $data[8];
							$insert->score = $data[9];
							$insert->expire_timescore = $data[10];
							$insert->penality = $data[11];
							$insert->ordering = $data[12];
							$insert->created_date = $data[13];
							$insert->modified_date = $data[14];
							$insert->published = $data[15];
  																		
							if(!$db->insertObject('#__vquiz_question', $insert, 'id'))	{
								
								$msg = $this->setError($db->stderr());
				                $this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
								return false;
							}
							
							
							$id = $db->insertid();
						  	
							$correct=array();
							$correct=explode(',',$data[16]);
							$insertoption->options_score =$data[17];
							
							for($n=0,$i=18;$i<count($data);$i++,$n++)	{	
							$insertoption = new stdClass();
							$insertoption->id = null;
							$insertoption->qid = $id;
							$insertoption->qoption =$data[$i];
							$insertoption->correct_ans = in_array($n,$correct)?1:0;
							
								
								if(!$db->insertObject('#__vquiz_option', $insertoption, 'id'))	{
								
									$msg = $this->setError($db->stderr());
									$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
									
									return false;
								}
								
							}
						    
							$count++;
						}
						fclose($fp);
						$msg = JText::_('CSV_IMPORTED');
				        $this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
					}
				
			}
			
			
						function export()
							{
								$model = $this->getModel('quizquestion');
								$model->getCsv();	
  								$dispatcher = JDispatcher::getInstance();
								
								try{
									$dispatcher->trigger('startExport');
									jexit(/*JText::_('INTERNAL_SERVER_ERROR')*/);
								}catch(Exception $e){
									jerror::raiseWarning('', $e->getMessage());
									 $this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
								}
							
							
							}
							
							
							
							
						function checktotaltime()
						{ 
 						   // Check for request forgeries
							JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
 							$model = $this->getModel('quizquestion');
 							// ansswer querty and jo send karna hai sab ko
							$obj = $model->checktotaltime();		
							
							jexit(json_encode($obj));
						   
						   
						} 
		 
		 
		 
		 					function copyquestion()
							{
								$model = $this->getModel('quizquestion');
									if(!$model->copyquestion()) 
									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
									else 	 
									$msg = JText::_( 'Copy Succesfully' );
									$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
							}
							
						  function movequestion()
							{
								$model = $this->getModel('quizquestion');
									if(!$model->movequestion()) 
									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
									else 	 
									$msg = JText::_('MOVED_SUCCESS');
									$this->setRedirect( 'index.php?option=com_vquiz&view=quizquestion', $msg );
							}
							
							
							
}