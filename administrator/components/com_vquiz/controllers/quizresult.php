<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizControllerQuizresult extends VquizController
{

		function __construct()
		{
			parent::__construct();
			$this->registerTask( 'add'  , 	'edit' );

		}
 
		function edit()
		{
 
			JRequest::setVar( 'view', 'quizresult' );
 			JRequest::setVar( 'layout', 'form'  );
 			JRequest::setVar('hidemainmenu', 1);
 			parent::display();
 		}

 		function apply()

 		{

 				  $model = $this->getModel('quizresult');
 					if($model->store()) {
 						$msg = JText::_( 'Greeting Save' );
 					$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
 					} else {
 					jerror::raiseWarning('', $this->model->getError());
 					$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult');
 		  		}


		}

 			function remove()

 			{
 				$model = $this->getModel('quizresult');
 					if(!$model->delete()) 

 						{

 					$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
 						} 
 					else 
   					{
 					$msg = JText::_( 'Greeting(s) Deleted' );
 					}
 					$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
 		
					}

 
		function cancel()
 			{
 				$msg = JText::_('CANCELLED');

 				$this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
 
			}
			
			
			
			function export()
				{
					$model = $this->getModel('quizresult');
					$model->getCsv();	
 					$dispatcher = JDispatcher::getInstance();
					
					try{
						$dispatcher->trigger('startExport');
						jexit(/*JText::_('INTERNAL_SERVER_ERROR')*/);
					}catch(Exception $e){
						jerror::raiseWarning('', $e->getMessage());
						 $this->setRedirect( 'index.php?option=com_vquiz&view=quizresult', $msg );
					}
				
 				}
				
		function sendmail()
		{ 
			$user = JFactory::getUser();
			$session = JFactory::getSession();
			$userid= JRequest::getInt('userid');
			$resultid= JRequest::getInt('resultid');
			$date =JFactory::getDate();
			$datetime = $date->toSQL();
			$db = JFactory::getDbo();
			$mailer = JFactory::getMailer();
			$config = JFactory::getConfig();
			
			$obj = new stdClass();
			$obj->result = "error";
			
			$query = 'select * from #__vquiz_users where userid='.$userid;
			$db->setQuery( $query );
			$users_result =$db->loadObject();
			$email = $users_result->email; 
 
			$mailer->setSender($config->get('mailfrom'));
	
			$query = 'select * from #__vquiz_configuration';
 			$db->setQuery( $query );
 			$confiresult =$db->loadObject();

			
			$mailsubject = $confiresult->subject;
			
			$query = 'select * from #__vquiz_quizresult where id='.$resultid;
			$db->setQuery( $query );
			$quizesult =$db->loadObject();
			
			$score=$quizesult->score;
			$maxscore=$quizesult->maxscore;
			$spenttime=$quizesult->quiz_spentdtime;
			$quiztitle=$quizesult->quiztitle;
			$startdatetime=$quizesult->startdatetime;
			$enddatetime=$quizesult->enddatetime;
			$passed_score=$quizesult->passed_score;
			$flag=$quizesult->flag;
						
			if($quizesult->optiontypescore==2)
			$text = $confiresult->mailformat2;
			else
			$text = $confiresult->mailformat;
			
			if($maxscore>0)
				$persentagescore=round($score/$maxscore*100,2)>100?100:round($score/$maxscore*100,2);
			else
			$persentagescore=$score;


					if($persentagescore>=$passed_score)
					$passed_text='<span style="color:green">Passed!!</span>';	
					else
					$passed_text='<span style="color:red">Failed!!</span>';
					
					$username=$users_result->username;
					
					if(strpos($text, '{username}')!== false)
					{
					$text = str_replace('{username}', $username, $text);
					}
				    if(strpos($text, '{quizname}')!== false)
					{
					$text = str_replace('{quizname}', $quiztitle, $text);
					}
					if(strpos($text, '{userscore}')!== false)
					{
					$text = str_replace('{userscore}', $score, $text);
					}
					if(strpos($text, '{maxscore}')!== false)
					{
					$text = str_replace('{maxscore}', $maxscore, $text);
					}
				    if(strpos($text, '{starttime}')!== false)
					{
					$text = str_replace('{starttime}', $startdatetime, $text);
					}
				    if(strpos($text, '{endtime}')!== false)
					{
					$text = str_replace('{endtime}', $enddatetime, $text);
					}
					if(strpos($text, '{spenttime}')!== false)
					{
					$text = str_replace('{spenttime}', $spenttime, $text);
					}
					if(strpos($text, '{passedscore}')!== false)
					{
					$text = str_replace('{passedscore}', $passed_score, $text);
					}
					if(strpos($text, '{percentscore}')!== false)
					{
					$text = str_replace('{percentscore}', $persentagescore, $text);
					}
				    if(strpos($text, '{passed}')!== false)
					{
					$text = str_replace('{passed}', $passed_text, $text);
					}
					if(strpos($text, '{flag}')!== false)
					{
					$text = str_replace('{flag}', $flag, $text);
					}

			$body = $text;
			
			$mailer->setSubject($mailsubject);

			$mailer->setBody($body);

			$recievermail = $email;

            $mailer->addRecipient($recievermail);
			$mailer->IsHTML(true);
		    $send = $mailer->send();

		 if($send==true)
				$obj->result =1;
			else 
				$obj->result =0;

			jexit(json_encode($obj)); 

		}	 

		

 
}