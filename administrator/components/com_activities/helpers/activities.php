<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Activities helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'items')
	{
		// multi langeage setting
		// $append = JFactory::getLanguageId(true);

		$user = JFactory::getUser();
		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		
		$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
		$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');	// 決賽
		$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教
		$check = JComponentHelper::getParams('com_activities')->get('check_access');  // 台師大

		if(in_array($rank, $level)) {
			JHtmlSidebar::addEntry(
				"初賽評選排名",
				'index.php?option=com_activities&view=phase1rank',
				$vName == 'phase1rank'
			);
			JHtmlSidebar::addEntry(
				"總得分排名",
				'index.php?option=com_activities&view=export',
				$vName == 'export'
			);
			JHtmlSidebar::addEntry(
				"作品資料查詢",
				'index.php?option=com_activities&view=search',
				$vName == 'search'
			);
			
		}elseif(in_array($preliminary, $level)){
			JHtmlSidebar::addEntry(
				JText::_('COM_ACTIVITIES_SUBMENU_PHASE1AS'),
				'index.php?option=com_activities&view=phase1as',
				$vName == 'phase1as'
			);
			JHtmlSidebar::addEntry(
				"總得分排名",
				'index.php?option=com_activities&view=export',
				$vName == 'export'
			);
			
		}elseif(in_array($rematch, $level)){
			JHtmlSidebar::addEntry(
				"決賽評選",
				'index.php?option=com_activities&view=rematch',
				$vName == 'rematch'
			);
			JHtmlSidebar::addEntry(
				"總得分排名",
				'index.php?option=com_activities&view=export',
				$vName == 'export'
			);
			
		}elseif(in_array($check, $level)) {
			// JHtmlSidebar::addEntry(
				// "活動評選參數設定",
				// 'index.php?option=com_activities&view=peferences'.$append,
				// $vName == 'peferences'
			// );

			JHtmlSidebar::addEntry(
				"中央作品檢核",
				'index.php?option=com_activities&view=phase2check',
				$vName == 'phase2check'
			);
			
			JHtmlSidebar::addEntry(
				"總得分排名",
				'index.php?option=com_activities&view=export',
				$vName == 'export'
			);

			JHtmlSidebar::addEntry(
				"作品資料查詢",
				'index.php?option=com_activities&view=search',
				$vName == 'search'
			);

			// JHtmlSidebar::addEntry(
				// JText::_('COM_ACTIVITIES_SUBMENU_CATEGORIES'),
				// 'index.php?option=com_categories&extension=com_activities'.$append,
				// $vName == 'categories'
			// );
			// if ($vName == 'categories')
			// {
				// JToolbarHelper::title(
					// JText::sprintf('COM_CATEGORIES_CATEGORIES_TITLE', JText::_('com_activities')),
					// 'expert-categories');
			// }
		}else{
			JHtmlSidebar::addEntry(
				"活動評選參數設定",
				'index.php?option=com_activities&view=peferences',
				$vName == 'peferences'
			);
			
			JHtmlSidebar::addEntry(
				JText::_('COM_ACTIVITIES_SUBMENU_PHASE1AS'),
				'index.php?option=com_activities&view=phase1as',
				$vName == 'phase1as'
			);
			
			JHtmlSidebar::addEntry(
				"初賽評選排名",
				'index.php?option=com_activities&view=phase1rank',
				$vName == 'phase1rank'
			);

			JHtmlSidebar::addEntry(
				"中央作品檢核",
				'index.php?option=com_activities&view=phase2check',
				$vName == 'phase2check'
			);

			JHtmlSidebar::addEntry(
				"決賽評選",
				'index.php?option=com_activities&view=rematch',
				$vName == 'rematch'
			);

			JHtmlSidebar::addEntry(
				"總得分排名",
				'index.php?option=com_activities&view=export',
				$vName == 'export'
			);
			
			JHtmlSidebar::addEntry(
				"作品資料查詢",
				'index.php?option=com_activities&view=search',
				$vName == 'search'
			);
			
			JHtmlSidebar::addEntry(
				JText::_('COM_ACTIVITIES_SUBMENU_CATEGORIES'),
				'index.php?option=com_categories&extension=com_activities',
				$vName == 'categories'
			);
			if ($vName == 'categories')
			{
				JToolbarHelper::title(
					JText::sprintf('COM_CATEGORIES_CATEGORIES_TITLE', JText::_('com_activities')),
					'expert-categories');
			}
		}

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId))
		{
			$assetName = 'com_activities';
			$level = 'component';
		}
		else
		{
			$assetName = 'com_activities.category.'.(int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_activities', $level);

		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}
}
