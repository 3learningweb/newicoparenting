<?php 
	define( '_JEXEC', 1 );
	define('JPATH_BASE', '../../../../../');
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	
	$mainframe =& JFactory::getApplication('administrator');
	$mainframe->initialise();
	
	$app = JFactory::getApplication();
	$ranks = array();
	$ranks = (array) $app->getUserState('form.activities.ranks');
	
	$post = $app->input->getArray($_POST);
	$item_id = explode("_", $post['item_id']);
	$id = $item_id[1];
	$value = $post['value'];
	
	if($value == "0"){
		if(array_key_exists($id, $ranks)) {
			unset($ranks[$id]);
		}
	}else{
		// if(!array_key_exists($id, $ranks)) {
			// $ranks[$id] = $value;
		// }
		$ranks[$id] = $value;
	}

	$app->setUserState('form.activities.ranks', $ranks);
	
	echo implode(",", $ranks); die();
?>