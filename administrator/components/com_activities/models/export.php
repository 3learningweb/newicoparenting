<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelExport extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'state', 'a.state',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'ordering', 'a.ordering',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id', '');
		$this->setState('filter.category_id', $categoryId);

		$title = $this->getUserStateFromRequest($this->context . '.filter.title', 'filter_title', '');
		$this->setState('filter.title', $title);

		// rene
		$city = $this->getUserStateFromRequest($this->context . '.filter_city_id', 'filter_city_id', '');
		$this->setState('filter.city_id', $city);
		

		// Load the parameters.
		$params = JComponentHelper::getParams('com_activities');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.category_id');
		$id .= ':' . $this->getState('filter.language');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery()
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$catid = $app->getUserState('form.activities.catid');
		$type = $app->getUserState("form.activities.type");
		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		
		$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
		$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');	// 決賽
		$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教
		$check = JComponentHelper::getParams('com_activities')->get('check_access');  // 台師大

		$db		= $this->getDbo();
		$query	= $db->getQuery(true);		

		if(in_array($rank, $level)) {	// 家教
			$self_gps = JUserHelper::getUserGroups($user->get('id'));
			$city =  "'" . implode("','", $self_gps) . "'";
			
			$query->select("s.ContributeListCde, s.total as score");
			$query->from($db->quoteName('#__preliminary_decision', 's'));
			
			$query->select('c.Name, c.Title, c.PicPath, c.Introduction, c.CreateTime, c.UserID, c.Tel, c.Mobile, c.Email')
				  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = s.ContributeListCde');
			
			$query->select('u.title AS city')
				  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');
			
			$query->where("s.catid = '{$catid}'");
			$query->where("c.state = '1'");
			$query->where("c.city IN ({$city})");
			
			$query->order("score DESC");

		}elseif(in_array($preliminary, $level)){	// 初賽 地方評審
			$self_gps = JUserHelper::getUserGroups($user->get('id'));
			$city = implode(",", $self_gps);
			
			$query->select("s.ContributeListCde, sum(s.Score) as score");
			$query->from($db->quoteName('#__preliminary_score', 's'));
			
			$query->select('c.Name, c.Title, c.PicPath, c.Introduction')
				  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = s.ContributeListCde');
			
			$query->select('u.title AS city')
				  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');
			
			$query->where("s.catid = '{$catid}'");
			$query->where("c.state = '1'");
			$query->where("c.city = '{$city}'");
			$query->group("s.ContributeListCde");
			
			$query->order("score DESC");
						
		}elseif(in_array($rematch, $level)){	// 決賽 決賽評審
			$query->select("s.ContributeListCde, sum(s.Score) as score");
			$query->from($db->quoteName('#__rematch_score', 's'));
			
			$query->select('c.Name, c.Title, c.PicPath, c.Introduction')
				  ->join('LEFT', $db->quoteName('#__contribute_list'). ' AS c ON c.id = s.ContributeListCde');
			
			$query->select('u.title AS city')
				  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');		
	
			$query->where("s.catid = '{$catid}'");
			$query->where("c.Flg = '1'");
			$query->where("s.ScoreUsr = '{$user->get('id')}'");
			$query->group("s.ContributeListCde");
	
			$query->order("score DESC");
		
		}elseif(in_array($check, $level)){	// 台師大

			if($type == "rank") {
				$query->select("s.ContributeListCde, s.total as score");
				$query->from($db->quoteName('#__preliminary_decision', 's'));
				
				$query->select('c.Name, c.Title, c.PicPath, c.Introduction, c.CreateTime, c.UserID, c.Tel, c.Mobile, c.Email')
					  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = s.ContributeListCde');
				
				$query->select('u.title AS city')
					  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');
				
				$query->where("c.state = '1'");	
							
			}elseif($type == "preliminary") {
				$query->select("s.ContributeListCde, sum(s.Score) as score");
				$query->from($db->quoteName('#__preliminary_score', 's'));
				
				$query->select('c.Name, c.Title, c.PicPath, c.Introduction, c.CreateTime, c.UserID, c.Tel, c.Email')
					  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = s.ContributeListCde');
				
				$query->select('u.title AS city')
					  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');
				
				$query->where("c.state = '1'");
				$query->group("s.ContributeListCde");
				
			}elseif($type == "rematch") {
				$query->select("s.ContributeListCde, sum(s.Score) as score");
				$query->from($db->quoteName('#__rematch_score', 's'));
				
				$query->select('c.Name, c.Title, c.PicPath, c.Introduction, c.CreateTime, c.UserID, c.Tel, c.Mobile, c.Email')
					  ->join('LEFT', $db->quoteName('#__contribute_list'). ' AS c ON c.id = s.ContributeListCde');
				
				$query->select('u.title AS city')
					  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');		
		
				$query->where("c.Flg = '1'");
				$query->group("s.ContributeListCde");

			}else{  // 預設
				$query->select("s.ContributeListCde, sum(s.Score) as score");
				$query->from($db->quoteName('#__preliminary_score', 's'));
				
				$query->select('c.Name, c.Title, c.PicPath, c.Introduction, c.CreateTime, c.UserID, c.Tel, c.Mobile, c.Email')
					  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = s.ContributeListCde');
				
				$query->select('u.title AS city')
					  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = c.city');
				
				$query->where("c.state = '1'");
				$query->group("s.ContributeListCde");
			}

			// Filter
			$city_id = $this->getState('filter.city_id');
			if(is_numeric($city_id)) {
				$query->where("c.city = '{$city_id}'");
			}

			$query->where("s.catid = '{$catid}'");
			$query->order("score DESC");		
		}

        return $query;
	
	}

	public function getExportItems() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query = self::getListQuery();
		$db->setQuery($query);
		$items = $db->loadObjectList();
		
		return $items;
	}

	public function getPeference() {		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__activities_set', 'a'));
		$query->where("a.state = '1'");

		$query->select('c.title AS category_title')
			->join('LEFT', $db->quoteName('#__categories'). ' AS c ON c.id = a.catid');
		
		$db->setQuery($query);
		$items = $db->loadObjectList();		

		array_unshift($items, "--請選擇活動--");

		return $items;
	}
	
	public function getCity() {
		$db = $this->getDBO();
		
		$query = $db->getQuery(true);
		$query->select("id, title");
		$query->from($db->quoteName('#__usergroups'));
		$query->where("parent_id = '10'");
		$query->order("id asc");
		
		$db->setQuery($query);
		$city = $db->loadObjectList();
		
		return $city;
	}

}
