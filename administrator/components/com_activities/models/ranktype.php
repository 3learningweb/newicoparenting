<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelRanktype extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{

		parent::__construct($config);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getRankType()
	{
		$app = JFactory::getApplication();
		$item_id	= $app->input->getInt('item_id');
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query->select("*");
		$query->from($db->quoteName('#__activities_ranktype'));
		$query->where("item_id = '{$item_id}'");

		$db->setQuery($query);
		$type = $db->loadObject();

		return $type;
	}
	
	public function getPFields($id = '') {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__activities_ranktype_field'));
		$query->where("type_id = '{$id}'");
		$query->where("type = '0'");
		
		$db->setQuery($query);
		$pfields = $db->loadObjectList();
		
		return $pfields;		
	}
	
	public function getRFields($id = '') {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__activities_ranktype_field'));
		$query->where("type_id = '{$id}'");
		$query->where("type = '1'");
		
		$db->setQuery($query);
		$rfields = $db->loadObjectList();
		
		return $rfields;
	}
}
