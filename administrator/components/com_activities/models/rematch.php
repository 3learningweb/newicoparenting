<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelRematch extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
//				'serial_num', 'a.serial_num',
				'state', 'a.state'
			);
		}

		parent::__construct($config);
	}


	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);


		// Load the parameters.
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getListQuery()
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		
		// catid
		$peference_id = $app->getUserState("form.activities.peference");	
		$peferences = self::getPeference();
		
		
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
			)
		);

		$query->from($db->quoteName('#__preliminary_decision') . ' AS a ');
		
		// rene
		$query->where("a.catid = '{$peferences[$peference_id]->catid}'");
		$query->where("a.Flg = '1'");
		
		// join profile
		$query->select('c.id, c.Name, c.Title, c.PicPath, c.Introduction')
			  ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = a.ContributeListCde');
		$query->where("c.state = '1'");	
		
		$query->order("a.total DESC");
		
		return $query;
	}

	public function getAllItems() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query = self::getListQuery();

		$db->setQuery($query);
		$items = $db->loadObjectList(id);
		
		return $items;
	}
	
	public function getPeference() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__activities_set', 'a'));
		$query->where("a.state = '1'");

		$query->select('c.title AS category_title')
			->join('LEFT', $db->quoteName('#__categories'). ' AS c ON c.id = a.catid');
		
		$db->setQuery($query);
		$items = $db->loadObjectList();		

		array_unshift($items, "--請選擇活動--");

		return $items;
	}
	
	public function getSelected($catid = '') {
		$user = JFactory::getUser();
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("l.*, s.*");
		$query->from($db->quoteName('#__contribute_list', 'l'));
		$query->leftJoin($db->quoteName('#__rematch_score', 's') . " ON s.ContributeListCde = l.id");
		$query->where("l.state = '1'");
		$query->where("l.Flg = '1'");
		$query->where("l.catid = '{$catid}'");
		$query->where("s.ScoreUsr = '{$user->get('id')}'");
		
		$db->setQuery($query);
		$selected = $db->loadObjectList("id");

		return $selected;
	}
	
	// 判斷是否已進入評分階段 	
	public function getCheck() {
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$catid = $app->getUserState('form.activities.catid');

		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("COUNT(*)");
		$query->from($db->quoteName('#__rematch_score'));
		$query->where("ScoreUsr = '{$user->get("id")}'");
		$query->where("catid = '{$catid}'");

		$db->setQuery($query);
		$check = $db->loadResult();

		return $check;
	}
	
	// 判斷是否已完成決賽評分
	public function getRematchCheck() {		
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("COUNT(*)");
		$query->from($db->quoteName('#__rematch_check'));
		$query->where("UsrID = '{$user->get('id')}'");
		$query->where("catid = '{$catid}'");
		$query->where("IsCheck = '1'");
		
		$db->setQuery($query);
		$check = $db->loadResult();
		
		return $check;
	}

	// 取得分數的排序
	public function getSort() {
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");

		$user = JFactory::getUser();
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("l.id, l.PicPath, (s.Score1*0.5)+(s.Score2*0.3)+(s.Score3*0.2) as Score");
		$query->from($db->quoteName('#__contribute_list', 'l'));
		$query->leftJoin($db->quoteName('#__rematch_score', 's') . " ON s.ContributeListCde = l.id");
		$query->where("l.state = '1'");
		$query->where("l.Flg = '1'");
		$query->where("l.catid = '{$catid}'");
		$query->where("s.ScoreUsr = '{$user->get('id')}'");
		$query->order("Score DESC");
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		return $rows;		
	}
	
}
