<?php if($this->rows) { ?>
	<table class="datatable" width="800">
		<thead>
		<tr>
			<th width="5%" align="center">#</th>
			<th width="20%" align="center">姓名</th>
			<th width="20%" align="center">Email</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach( $this->rows as $key => $row ):?>
		<tr>
			<td align="center"><?php echo $key+1; ?></td>
			<td align="center"><?php echo $row->name; ?></td>
			<td align="center"><?php echo $row->email; ?></td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php }else{ ?>
	所有專家評審皆已完成評選
<?php } ?>

<style>
	.contentpane {
		padding-top: 0px;
	}
	
	.datatable th {
		padding: 5px;		
	}
	
	.datatable td {
		line-height: 25px;
		border-top: 1px solid #ddd;
	}
</style>
