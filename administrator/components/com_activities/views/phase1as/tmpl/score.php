<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();

$catid = $app->getUserState("form.activities.catid");
$save_arr = array();

$user		= JFactory::getUser();
$userId		= $user->get('id');
?>

<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$("#toolbar-save button").attr("onclick", "");
			
			$("#toolbar-save button").on("click", function() {
				if(confirm("分數上傳後則不可再修改，是否確定送出？")){
					Joomla.submitform("phase1as.score_save", document.getElementById('adminForm'))
				}
			});
		
			var first = 0.5;
			var second = 0.3;
			var third = 0.2;
			var temp_score;
			var check = 1;
		
			$("#toolbar-save").hide();
			$(".score").each(function() {
				if(!$(this).val()){
					check = 0;
				}
			});
			
			if(check == 1) {
				$("#toolbar-save").show();
			}


			$(".item_img img").fancybox();

			// $("#sort").fancybox();
			
			$(".score").focusout(function() {
				var id = $(this).attr("id").split("_");
				var score = $(this).val();
				var total_score = $("#total_"+id[1]);
					check = 1;
				
				if(score < 0 || score > 100) {
					alert("分數請填入0~100區間。");
					$(this).val("").focus();
					return false;
				}
				
								
				switch(id[2]) {
					case "1":
						var score2 = $("#score_"+id[1]+"_2").val() * second;
						var score3 = $("#score_"+id[1]+"_3").val() * third;

						total_score.val((score * first) + score2 + score3);
						break;
						
					case "2":
						var score1 = $("#score_"+id[1]+"_1").val() * first;
						var score3 = $("#score_"+id[1]+"_3").val() * third;

						total_score.val((score * second) + score1 + score3);
						break;
						
					case "3":
						var score1 = $("#score_"+id[1]+"_1").val() * first;
						var score2 = $("#score_"+id[1]+"_2").val() * second;

						total_score.val((score * third) + score1 + score2);
						break;
				}
				
				// 判斷是否都有填寫分數
				$(".score").each(function() {
					if(!$(this).val()){
						check = 0;
					}
				});
				
				if(check == 1) {
					$("#toolbar-save").show();
				}else{
					$("#toolbar-save").hide();
				}
				
			});
			
			$(".temp_btn").on('click', function() {
				if(confirm("是否確定暫時儲存")) {
					var item_block = $(this).parent().parent();
					var item_id = $(this).attr("id").split("_");
					var score1 = $("#score_"+item_id[1]+"_1").val();
					var score2 = $("#score_"+item_id[1]+"_2").val();
					var score3 = $("#score_"+item_id[1]+"_3").val();
					var url = "components/com_activities/assets/ajax/temp_save.php";
					$.post(url, {id:item_id[1], catid:<?php echo $catid; ?>, score1:score1, score2:score2, score3:score3}, function(data) {
						if(data) {
							alert("暫時儲存成績完成");
							item_block.css("border-color", "#0088CC");
						}else{
							alert("暫時儲存成績失敗");
						}
			        });		        			
				}
			});

		});
		
	})(jQuery);
</script>
<form action="<?php echo JRoute::_('index.php?option=com_activities&view=phase1as'.$append); ?>" method="post" name="adminForm" id="adminForm">
	<!-- 左方選單 -->
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
	
	<!-- <input type="button" class="btn" id="sort" href="index.php?option=com_activities&view=phase1as&layout=sort&tmpl=component" value="分數排行" />(使用此功能請暫時儲存分數)<br/><br/> -->
	
	<div class="notice_text">若尚未評分完成可點選「暫時儲存」，已評分完成後請點選「確定送出」；請注意確定送出後即不可再次修改任何資料。</div>
	
	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', '作品評分'); ?>
				<?php foreach($this->selected as $key => $select): 
					$score = ($select->Score1 * 0.5) + ($select->Score2 * 0.3) + ($select->Score3 * 0.2); ?>
					<div class="item_block">
						<div class="item_id">作品編號：<?php echo $key; ?></div>
						<!-- <div class="item_name">投稿人姓名：<?php echo $select->Name; ?></div> -->
						<div class="item_img">
							<img src="../<?php echo $select->PicPath; ?>" href="../<?php echo $select->PicPath; ?>" title="<?php echo $select->Introduction; ?>" />
						</div>
						<!-- <div class="item_title">主題：<?php echo $select->Title; ?></div> -->
						<br/>
						<!-- 評分項目 -->
						<div>主題符合&nbsp;&nbsp;佔50%：
							<input type="text" class="score" id="score_<?php echo $key; ?>_1" name="score_<?php echo $key; ?>_1" value="<?php echo $select->Score1; ?>" placeholder="1~100分" />
						</div>
						<div>照片構圖&nbsp;&nbsp;佔30%：
							<input type="text" class="score" id="score_<?php echo $key; ?>_2" name="score_<?php echo $key; ?>_2" value="<?php echo $select->Score2; ?>" placeholder="1~100分" />
						</div>
						<div>整體創意&nbsp;&nbsp;佔20%：
							<input type="text" class="score" id="score_<?php echo $key; ?>_3" name="score_<?php echo $key; ?>_3" value="<?php echo $select->Score3; ?>" placeholder="1~100分" />
						</div>
						
						<div>分數：<input class="total_score" id="total_<?php echo $key; ?>" value="<?php echo ($score) ? $score : "0.0"; ?>" readonly /></div>
						<div class="temp_save">
							<input type="hidden" name="id_arr[]" value="<?php echo $key; ?>" />
							<input type="button" class="btn temp_btn" id="tsitem_<?php echo $key; ?>" value="暫時儲存" />
						</div>
					</div>
				<?php endforeach; ?>

			<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		<input type="hidden" name="catid" value="<?php echo $catid; ?>" />
		<input type="hidden" name="user" value="<?php echo $userId; ?>" />
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
</form>

<style>
	.item_block {
		float: left;
		width: 250px;
		margin: 10px;
		padding: 10px;
		border: 2px solid;
		min-height: 485px;
		text-align: center
	}
	
	.item_img img:hover {
		cursor: pointer;
	}

	.item_img img {
		max-height: 220px;
	}
	
	.temp_save {
		margin: 5px;
		text-align: center;
	}
	
	.pagination-toolbar {
		clear: both;
	}
	
	.total_score {
		color: red;
		border: 0;
	}
	
	.notice_text {
		color: red;
	}
</style>