<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$selected = (array) $app->getUserState('form.activities.primaries');
$preliminary_limit = $app->getUserState('form.activities.preliminary_limit');

$user		= JFactory::getUser();
$userId		= $user->get('id');

?>

<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">	
	(function($) {
		$(document).ready(function() {			
			$(".item_img img").fancybox();
			
			if("<?php echo $this->pre_check; ?>" == 1) {
				$("#toolbar-pencil-2").hide();
			}
			
			$("#toolbar-pencil-2 button").attr("onclick", "");
			$("#toolbar-pencil-2 button").on("click", function() {
				if(confirm("下一步後即不能再更改已入選的作品，是否確定送出？")){
					Joomla.submitform("phase1as.score", document.getElementById('adminForm'))
				}
			});
			
			$(".select_btn").on("click", function() {
				var item_id = $(this).attr("id");
		        var url = "components/com_activities/assets/ajax/select_item.php";

		        if($("#selected .item_block").length < "<?php echo $preliminary_limit; ?>") {
		        	$(this).attr('disabled', true);
			        $.post(url, {item_id:item_id}, function(data) {
			        	$("#selected").html(data);
			        	var num = $("#selected .item_block").length;
			            $(".num").text(num);
			            alert("成功選入作品");
			            $(".item_img img").fancybox();
			        });		        	
		        }else{
		        	alert("入選件數不能超過<?php echo $preliminary_limit; ?>件");
		        	return false;
		        }

			});
			
			$(document).on("click", ".delete_btn", function() {
				if(confirm("是否確定刪除")) {
					var item_id = $(this).attr("id");
					var url = "components/com_activities/assets/ajax/delete_item.php";
					$(this).attr('disabled', true);
					
					$.post(url, {item_id:item_id}, function(data) {
						var num = data;
						var id = item_id.split("_");
						$("#"+item_id).parent().parent(".item_block").remove();
						$(".num").text(num);
						$("#item_"+id[1]).attr('disabled', false);
					});	
				}else{
					return false;
				}
			});
			
			$("#peference").change(function() {
				var peference = $(this).val();
				var url = "<?php echo JRoute::_('index.php?option=com_activities&task=phase1as.savepeference&peference=', false); ?>" + peference;
				document.location.href = url;
			});
		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=phase1as'.$append); ?>" method="post" name="adminForm" id="adminForm">
	<!-- 左方選單 -->
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
	
	<?php
	if(!$this->pre_check) { 
		echo JHtml::_('select.genericlist ', $this->peferences , 'peference', 'class="peference"', 'id', 'title', $this->peference_id);
	
		if($this->peference_id && $this->items) {
	?>
	<br/><br/>
	<div class="notice_text">選擇1~<?php echo $preliminary_limit; ?>張入選參賽作品後點選[下一步：評分]，需注意點選下一步後即不能再更改已入選的作品。</div>
	
	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', '參賽作品('.count($this->allitems).'件)'); ?>
				<?php foreach($this->items as $i => $item): ?>
					<div class="item_block">
						<div class="item_id">作品編號：<?php echo $item->id; ?></div>
						<!-- <div class="item_name">投稿人姓名：<?php echo $item->Name; ?></div> -->
						<div class="item_img">
							<img src="../<?php echo $item->PicPath; ?>" href="../<?php echo $item->PicPath; ?>" title="<?php echo $item->Introduction; ?>" />
						</div>
						<!-- <div class="item_title">主題：<?php echo $item->Title; ?></div> -->
						<br/>
						<div class="item_select">
							<input type="button" class="btn select_btn" id="item_<?php echo $item->id; ?>" value="選入" <?php if(in_array($item->id, $selected)) {echo "disabled";}  ?> />
						</div>
					</div>
				<?php endforeach; ?>
				<?php echo $this->pagination->getListFooter(); ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'selected', '已選作品(<span class="num">'.count($selected).'</span>/'.$preliminary_limit.')'); ?>
				<?php foreach($selected as $key => $select): ?>
					<div class="item_block">
						<div class="item_id">作品編號：<?php echo $this->allitems[$select]->id; ?></div>
						<!-- <div class="item_name">投稿人姓名：<?php echo $this->allitems[$select]->Name; ?></div> -->
						<div class="item_img">
							<img src="../<?php echo $this->allitems[$select]->PicPath; ?>" href="../<?php echo $this->allitems[$select]->PicPath; ?>" title="<?php echo $this->allitems[$select]->Introduction; ?>" />
						</div>
						<!-- <div class="item_title">主題：<?php echo $this->allitems[$select]->Title; ?></div> -->
						<br/>
						<div class="item_delete">
							<input type="button" class="btn delete_btn" id="ditem_<?php echo $this->allitems[$select]->id; ?>" value="刪除" />
						</div>
					</div>
				<?php endforeach; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
	<?php }else{ ?>
		<br/><br/>
		<div>
			<?php 
				if(!$this->items && $this->peference_id) {
					echo "尚無投稿資料";
				}
			?>
		</div>
	<?php 
		}
	}else{
		echo "您已完成初賽評選";
	}
 	?>
</form>

<style>
	.item_block {
		float: left;
		width: 250px;
		margin: 10px;
		padding: 10px;
		border: 2px solid;
		min-height: 290px;
		text-align: center
	}
	
	.item_img img:hover {
		cursor: pointer;
	}

	.item_img img {
		max-height: 220px;
	}
	
	.item_select,
	.item_delete {
		text-align: center;
	}
	
	.pagination-toolbar {
		clear: both;
	}
	
	.notice_text {
		color: red;
	}
</style>