<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of phase1rank.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewPhase1rank extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$app = JFactory::getApplication();
		$layout = $app->input->getString('layout');
		$this->peference_id = $app->getUserState("form.activities.peference");
		$this->peferences	= $this->get('Peference');
		$catid = $this->peferences[$this->peference_id]->catid;
		
		if($catid) {
			$this->state		= $this->get('State');
			$this->items 		= $this->get('Items');
			$this->pagination	= $this->get('Pagination');
			$this->cnt			= $this->get('Cnt');
			$app->setUserState("form.activities.preliminary_rank_limit", $this->peferences[$this->peference_id]->preliminary_rank_limit);
			$app->setUserState("form.activities.catid", $catid);

			$model = $this->getModel();
			$this->score		= $this->get('Score');
			$this->fields 		= $model->getFields($this->peferences[$this->peference_id]->id, $this->peferences[$this->peference_id]->preliminary_rank_limit);
			
			$this->pre_check	= $this->get('PreliminaryRankCheck');	
			
			if($layout == 'undone') {
				$this->rows = $this->get('Undone');
			}
		}
		

		ActivitiesHelper::addSubmenu('phase1rank');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';
		
		$app = JFactory::getApplication();
		
		$state	= $this->get('State');
		$canDo	= ActivitiesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		$layout = $app->input->getString("layout");

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_activities');
		}

		JToolbarHelper::title(JText::_('COM_ACTIVITIES') . "-初賽評選排名", 'appform.png');
		
		if($this->peference_id && !$this->pre_check) {
			JToolbarHelper::custom('phase1rank.save', 'save', '', '確定送出', false, true );
		}
		
		JHtmlSidebar::setAction('index.php?option=com_activities&view=phase1rank');

	}


	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			// 'a.state' => '處理狀態',
			// 'a.doc_num' => '案件編號',
			// 'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}

}
