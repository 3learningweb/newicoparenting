<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_exportdata
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$user = JFactory::getUser();
$level = JAccess::getAuthorisedViewLevels($user->get('id'));
$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');	// 決賽
$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$filename = '總得分排名';

ob_start();

if(!(in_array($preliminary, $level) && !in_array($rank, $level) or in_array($rematch, $level))) {
	echo "作品編號,";
	echo "投稿人姓名,";
	echo "身分證字號,";
	echo "聯絡電話,";
	echo "行動電話,";
	echo "Email,";	
	echo "縣市,";
	echo "作品照片,";
	echo "文字介紹,";
	echo "總得分,";
	echo "投稿時間";
}else if(in_array($rematch, $level)){
	echo "作品編號,";
	echo "得分,";
	echo "縣市,";
	echo "作品照片,";
	echo "文字介紹";
}else{
	echo "作品編號,";	
	echo "縣市,";
	echo "作品照片,";
	echo "文字介紹";
}

echo "\r\n";

if(!(in_array($preliminary, $level) && !in_array($rank, $level) or in_array($rematch, $level))) {
	foreach($this->data as $key => $item){
		echo $item->ContributeListCde . ","; 
		echo trim(str_replace(",", "，", $item->Name)) . ",";
		echo trim($item->UserID) . ",";
		echo trim($item->Tel) . ",";
		echo trim($item->Mobile) . ",";
		echo trim($item->Email) . ",";			
		echo $item->city . ",";
		echo "=HYPERLINK(\"" . JURI::root() . $item->PicPath . "\"，\"照片\"),";
		echo trim(str_replace(array("\r", "\n", "\r\n", "\n\r"), ' ', $item->Introduction)) . ",";
		echo round($item->score, 1) . ",";
		echo $item->CreateTime;
		echo "\r\n";
	}
}else if(in_array($rematch, $level)) {
	foreach($this->data as $key => $item){
		echo $item->ContributeListCde . ","; 
		echo $item->score . ",";	
		echo $item->city . ",";
		echo "=HYPERLINK(\"" . JURI::root() . $item->PicPath . "\"，\"照片\"),";
		echo trim(str_replace(array("\r", "\n", "\r\n", "\n\r"), ' ', $item->Introduction));
		echo "\r\n";
	}		
}else{
	foreach($this->data as $key => $item){
		echo $item->ContributeListCde . ","; 	
		echo $item->city . ",";
		echo "=HYPERLINK(\"" . JURI::root() . $item->PicPath . "\"，\"照片\"),";
		echo trim(str_replace(array("\r", "\n", "\r\n", "\n\r"), ' ', $item->Introduction));
		echo "\r\n";
	}	
}

$output = ob_get_contents();
ob_end_clean();

header("Content-type: text/x-csv");

header("Content-Disposition: inline; filename=\"" . $filename . ".csv\"");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

echo mb_convert_encoding($output, "Big5" , "UTF-8");
jexit();
?>
