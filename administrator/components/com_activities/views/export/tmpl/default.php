<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();

$user = JFactory::getUser();
$level = JAccess::getAuthorisedViewLevels($user->get('id'));
$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');	// 決賽
$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教
$check = JComponentHelper::getParams('com_activities')->get('check_access');  // 台師大

$type = $app->getUserState("form.activities.type");
?>
<script type="text/javascript">
	function check_export() {
		jQuery("#adminForm").prop("action", "<?php echo JRoute::_('index.php?option=com_activities&view=export&layout=exportdata', false); ?>");
		jQuery("#adminForm").submit();
		jQuery("#adminForm").prop("action", "<?php echo JRoute::_('index.php?option=com_activities&view=export', false); ?>");
	}
		
	(function($) {
		$(document).ready(function() {			
			$("#peference").change(function() {
				var peference = $(this).val();
				var url = "<?php echo JRoute::_('index.php?option=com_activities&task=export.savepeference&peference=', false); ?>" + peference;
				document.location.href = url;
			});

			$("#type").change(function() {
				var type = $(this).val();
				var url = "<?php echo JRoute::_('index.php?option=com_activities&task=export.savetype&type=', false); ?>" + type;
				document.location.href = url;
			});

		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=export'.$append); ?>" method="post" name="adminForm" id="adminForm">
	<!-- 左方選單 -->
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>

	<?php echo JHtml::_('select.genericlist ', $this->peferences , 'peference', 'class="peference"', 'id', 'title', $this->peference_id); ?>
	
	<?php if(in_array($check, $level)) { ?>
		<select id="type" name="type" class="type">
			<option value="0">--請選擇類別--</option>
			<option value="preliminary" <?php if($type == "preliminary") { echo "selected"; } ?>>初賽評選排行</option>
			<option value="rank" <?php if($type == "rank") { echo "selected"; } ?>>縣市家教評選排行</option>
			<option value="rematch" <?php if($type == "rematch") { echo "selected"; } ?>>決賽賽評選排行</option>
		</select>
	<?php } ?>
	
<?php if($this->peference_id && $this->items) { ?>		
	<!-- button-->
	<div><input type="button" id="submit_export" class="btn" value="匯出CSV檔案" onclick="check_export()" /></div>
	
	<!-- 輸出 -->
	<div class="data_list">	
		<table width="100%" class="table table-striped" cellspacing="1">
			<thead>
			<tr>			
				<?php if(!(in_array($preliminary, $level) && !in_array($rank, $level) or in_array($rematch, $level))) { $colspan = 10; ?>
					<th>作品編號</th>
					<th>投稿人姓名</th>
					<th>身分證字號</th>
					<th>聯絡電話</th>
					<th>行動電話</th>
					<th>Email</th>
					<th>縣市</th>
					<th>作品照片</th>
					<th>文字介紹</th>
					<th>總得分</th>
					<th>投稿時間</th>
				<?php }else if(in_array($rematch, $level)){ $colspan = 5; ?>
					<th>作品編號</th>
					<th>分數</th>
					<th>縣市</th>
					<th>作品照片</th>
					<th>文字介紹</th>										
				<?php }else{ $colspan = 5; ?>
					<th>作品編號</th>
					<th>縣市</th>
					<th>作品照片</th>
					<th>文字介紹</th>
				<?php } ?>
			</tr>
			</thead>
			
			<tbody>
			<?php foreach($this->items as $key => $item){ ?>
				<tr>
					<!-- <td><?php echo $this->pagination->limitstart + ($key+1); // 流水號 ?></td> -->
					<?php if(!(in_array($preliminary, $level) && !in_array($rank, $level) or in_array($rematch, $level))){ ?>
						<td><?php echo $item->ContributeListCde; ?></td>
						<td><?php echo $item->Name; ?></td>
						<td><?php echo $item->UserID; ?></td>
						<td><?php echo $item->Tel; ?></td>
						<td><?php echo $item->Mobile; ?></td>
						<td><?php echo $item->Email; ?></td>
						<td><?php echo $item->city; ?></td>
						<td><img src="../<?php echo $item->PicPath; ?>" /></td>
						<td><?php echo $item->Introduction; ?></td>
						<td><?php echo round($item->score, 1); ?></td>
						<td><?php echo $item->CreateTime; ?></td>
					<?php }else if(in_array($rematch, $level)){ ?>
						<td><?php echo $item->ContributeListCde; ?></td>
						<td><?php echo $item->score; ?></td>
						<td><?php echo $item->city; ?></td>
						<td><img src="../<?php echo $item->PicPath; ?>" /></td>
						<td><?php echo $item->Introduction; ?></td>								
					<?php }else{ ?>
						<td><?php echo $item->ContributeListCde; ?></td>
						<td><?php echo $item->city; ?></td>
						<td><img src="../<?php echo $item->PicPath; ?>" /></td>
						<td><?php echo $item->Introduction; ?></td>					
					<?php } ?>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="<?php echo $colspan; ?>" style="text-align: center;">
						<label id="display_num">顯示數目：</label>
						<?php echo $this->pagination->getLimitBox(); ?>
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>	
<?php } ?>
</form>

<style>
	.filter {
		margin-bottom: 5px;
	}
	
	.data_list {
		margin-top: 10px;
	}
	
	.data_list table {
		border-collapse: separate;
  		border-spacing: 2px;
	}
	
	.data_list th{
		background: #D7D7D7;
	}
	
	#display_num, #limit_chzn, .pagination-toolbar {
		display: inline-block;
	}
	
	#display_num {
		position: relative;
  		top: -5px;
	}
	
	#submit_export {
		margin-top: 10px;
	}
	
	.data_list img {
		max-height: 220px;
		max-width: 220px;
	}
	
	.data_list img:hover {
		/*cursor: pointer;*/
	}
</style>