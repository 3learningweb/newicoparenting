<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of activities.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewExport extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$app = JFactory::getApplication();
		$this->state = $this->get('State');
		
		$layout = $app->input->getString('layout');
		$this->peference_id = $app->getUserState("form.activities.peference");
		$this->peferences	= $this->get('Peference');
		$catid = $this->peferences[$this->peference_id]->catid;

		if($catid) {
			$app->setUserState("form.activities.catid", $catid);
			
			$this->items		= $this->get('Items');
			$this->pagination	= $this->get('Pagination');

			if($layout == 'exportdata') {
				$this->data = $this->get('ExportItems');
			}else{
				$this->data = $this->get('Items');
			}
		}
		$this->city 		= $this->get('City');
		
		ActivitiesHelper::addSubmenu('export');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';

		$state	= $this->get('State');
		$canDo	= ActivitiesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_ACTIVITIES') . "-總得分排名", 'activities.png');
		if ($canDo->get('core.create'))
//		if (count($user->getAuthorisedCategories('com_activities', 'core.create')) > 0)
		{
			// JToolbarHelper::addNew('export.add');
		}
		if ($canDo->get('core.edit'))
		{
			// JToolbarHelper::editList('export.edit');
		}

		JHtmlSidebar::setAction('index.php?option=com_activities&view=export');
		
		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		$check = JComponentHelper::getParams('com_activities')->get('check_access');  // 台師大
		if(in_array($check, $level)) {
			JHtmlSidebar::addFilter(
				JText::_('- 選擇縣市 -'),
				'filter_city_id',
				JHtml::_('select.options', $this->city, 'id', 'title', $this->state->get('filter.city_id'))
			);
		}
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.state' => JText::_('JSTATUS'),
			'a.title' => JText::_('JGLOBAL_TITLE'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
