<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View to edit a item.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewPeference extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		// Since we don't track these assets at the item level, use the category id.
		$canDo		= ActivitiesHelper::getActions($this->item->catid, 0);

		JToolbarHelper::title(JText::_('COM_ACTIVITIES_MANAGER_TITLE'), 'activities.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_activities', 'core.create')))))
		{
			JToolbarHelper::apply('peference.apply');
			JToolbarHelper::save('peference.save');
		}
		if (!$checkedOut && (count($user->getAuthorisedCategories('com_activities', 'core.create')))){
			JToolbarHelper::save2new('peference.save2new');
		}
		// If an existing item, can save to a copy.
		if (!$isNew && (count($user->getAuthorisedCategories('com_activities', 'core.create')) > 0))
		{
			JToolbarHelper::save2copy('peference.save2copy');
		}
		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('peference.cancel');
		}
		else
		{
			JToolbarHelper::cancel('peference.cancel', 'JTOOLBAR_CLOSE');
		}

		
	}
}
