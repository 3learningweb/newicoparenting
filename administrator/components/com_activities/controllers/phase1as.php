<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * phase1as list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerPhase1as extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Guide', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}
	
	function savepeference() {
		$app = JFactory::getApplication();
		$peference_id = $app->input->getInt('peference');
		
		if(!$peference_id) {
			$msg = "請選擇活動";
		}
		
		$app->setUserState("form.activities.peference", $peference_id);
		
		$link = JRoute::_("index.php?option=com_activities&view=phase1as", false);
		$this->setRedirect($link, $msg, 'error');
		
		return;
	}
	
	function score() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$peference_id = $app->input->getInt('peference');
		$catid = $app->getUserState('form.activities.catid');
		$primaries = $app->getUserState('form.activities.primaries');
		$preliminary_limit = $app->getUserState('form.activities.preliminary_limit');

		if(count($primaries) < 1 or count($primaries) > $preliminary_limit) {
			$msg = "入選件數不得少於或超過限制";
			$link = JRoute::_("index.php?option=com_activities&view=phase1as", false);
			$this->setRedirect($link, $msg, 'error');
			return;
		}
	
		$id_arr =  implode("','", $primaries);
		
		$db = JFactory::getDBO();
		
		// 更新作品為入選
		$query = $db->getQuery(true);
		$query->update($db->quoteName('#__contribute_list'));
		$query->set("state = '1'");
		$query->where("id IN ('{$id_arr}') ");
		
		$db->setQuery($query);
		
		if(!$db->execute()) {
			$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
			$link = JRoute::_("index.php?option=com_activities&view=phase1as", false);
			$this->setRedirect($link, $msg);
			return;
		}
		
		
		// 寫入#__preliminary_score 初賽評審分數資料表
		$date = JFactory::getDate();
		$datetime = $date->toSql();
		foreach($primaries as $primarie) {
			$columns = array('ContributeListCde', 'ScoreUsr', 'CreateTime', 'catid');
			$values = array(
					"'{$primarie}'",
					"'{$user->get('id')}'",
					"'{$datetime}'",
					"'{$catid}'"
			);
			
			$query_check = $db->getQuery(true);
			$query_check->insert($db->quoteName('#__preliminary_score'));
			$query_check->columns($columns);
			$query_check->values(implode(',', $values));

			$db->setQuery($query_check);
			if(!$db->execute()) {
				$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
				$link = JRoute::_("index.php?option=com_activities&view=phase1as", false);
				$this->setRedirect($link, $msg);
				return;
			}	
		}
		
		
		$app->setUserState("form.activities.primaries", null);
		
		$link = JRoute::_("index.php?option=com_activities&view=phase1as&layout=score", false);
		$this->setRedirect($link);
	}


	function score_save() {	
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$post = $app->input->getArray($_POST);

		$date = JFactory::getDate();
		$datetime = $date->toSql();

		// city
		$self_gps = JUserHelper::getUserGroups($user->get('id'));
		$city = implode(",", $self_gps);

		$db = JFactory::getDBO();
		$ids = $post['id_arr'];

		
		// 存分數
		foreach($ids as $key => $id) {
			$score1 = $post["score_{$id}_1"];
			$score2 = $post["score_{$id}_2"];
			$score3 = $post["score_{$id}_3"];
			$score = ($score1*0.5)+($score2*0.3)+($score3*0.2);
			
			$query = $db->getQuery(true);
			$query->update($db->quoteName('#__preliminary_score'));
			$query->set("Score = '{$score}'");
			$query->set("Score1 = '{$score1}'");
			$query->set("Score2 = '{$score2}'");
			$query->set("Score3 = '{$score3}'");
			$query->set("ModifyTime = '{$datetime}'");
			$query->where("ScoreUsr = '{$post['user']}'");
			$query->where("catid = '{$post['catid']}'");
			$query->where("ContributeListCde = '{$id}'");	
			
			$db->setQuery($query);
			if(!$db->execute()) {
				$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
				$link = JRoute::_("index.php?option=com_activities&view=phase1as&layout=score", false);
				$this->setRedirect($link, $msg);
				return;
			}	
		}
		
		// 記錄此評審已評完
		$columns = array('UsrID', 'City', 'catid', 'IsCheck');
		$values = array(
				$user->get('id'),
				$city,
				$post['catid'],
				1
		);
		
		$query_check = $db->getQuery(true);
		$query_check->insert($db->quoteName('#__preliminary_check'));
		$query_check->columns($columns);
		$query_check->values(implode(',', $values));
		
		$db->setQuery($query_check);
		
		if(!$db->execute()) {
			$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
			$link = JRoute::_("index.php?option=com_activities&view=phase1as&layout=score", false);
			$this->setRedirect($link, $msg);
			return;
		}		
		
		$msg = "您已完成評選，資料已送出";
		$link = JRoute::_("index.php?option=com_activities&view=phase1as", false);
		$this->setRedirect($link, $msg);
		
	}
	
}
