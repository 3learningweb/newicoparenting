<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * activities list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerRanktype extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Ranktype', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	function getData() {
		$this->setRedirect("index.php?option=com_activities&view=ranktype");
	}
	
	function cancel() {
		$this->setRedirect("index.php?option=com_activities&view=peferences");
	}
	
	function save() {
		$app 	 = JFactory::getApplication();
		$post 	 = $app->input->getArray($_POST);

		$db = JFactory::getDBO();
		$query_type = $db->getQuery(true);
		$id = $post['id'];
		if($id) {
			$query_type->update($db->quoteName('#__activities_ranktype'));
			$query_type->set("preliminary_type = '{$post['preliminary_type']}'");
			$query_type->set("rematch_type = '{$post['rematch_type']}'");
			$query_type->where("id = '{$post['id']}' AND item_id = '{$post['item_id']}'");
			
			$db->setQuery($query_type);
			if(!$db->execute()) {
				$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
				$link = JRoute::_("index.php?option=com_activities&view=ranktype&item_id={$post['item_id']}", false);
				$this->setRedirect($link, $msg);
				return;
			}
			
		}else{
			$columns = array('item_id', 'preliminary_type', 'rematch_type');
			$values = array(
						$post['item_id'],
						$post['preliminary_type'],
						'0'
					);
			$query_type->insert($db->quoteName('#__activities_ranktype'));
			$query_type->columns($columns);
			$query_type->values(implode(',', $values));
			
			$db->setQuery($query_type);
			if(!$db->execute()) {
				$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
				$link = JRoute::_("index.php?option=com_activities&view=ranktype&item_id={$post['item_id']}", false);
				$this->setRedirect($link, $msg);
				return;
			}
			
			$id = $db->insertid();
		}
		
		
		$query_del = $db->getQuery(true);
		$query_del->delete($db->quoteName('#__activities_ranktype_field'));
		$query_del->where("type_id = '{$id}'");
		$db->setQuery($query_del);
		$db->execute();

		if($post['preliminary_type'] == "0") {
			foreach($post['ptext'] as $key => $text) {
				if(trim($text) == "") {
					continue;
				}
				
				$query_field = $db->getQuery(true);
				$num = $post['pnum'][$key];
				$columns = array('type_id', 'type', 'text', num);
				$values = array(
							$id,
							0,
							"'{$text}'",
							"'{$num}'"
						);
				$query_field->insert($db->quoteName('#__activities_ranktype_field'));
				$query_field->columns($columns);
				$query_field->values(implode(',', $values));
				
				$db->setQuery($query_field);
				if(!$db->execute()) {
					$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
					$link = JRoute::_("index.php?option=com_activities&view=ranktype&item_id={$post['item_id']}", false);
					$this->setRedirect($link, $msg);
					return;
				}
			}
		}

		if($post['rematch_type'] == "0") {
			foreach($post['rtext'] as $text) {
				if(trim($text) == "") {
					continue;
				}
				
				$query_field = $db->getQuery(true);
				$columns = array('type_id', 'type', 'text');
				$values = array(
							$id,
							1,
							"'{$text}'"
						);
				$query_field->insert($db->quoteName('#__activities_ranktype_field'));
				$query_field->columns($columns);
				$query_field->values(implode(',', $values));
				
				$db->setQuery($query_field);
				if(!$db->execute()) {
					$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
					$link = JRoute::_("index.php?option=com_activities&view=ranktype&item_id={$post['item_id']}", false);
					$this->setRedirect($link, $msg);
					return;
				}
			}
		}		
		
		$msg = "活動排名設定完成";
		$link = JRoute::_("index.php?option=com_activities&view=peferences", false);
		$this->setRedirect($link, $msg);
	}
}
