<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;


// Get template params
$sitetitle = $app->getTemplate(true)->params->get('sitetitle');
$home_ids = explode(",", $app->getTemplate(true)->params->get('home_id'));

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');


//rename title (if home page)
$doc->setTitle($doc->getTitle() . " | " . $sitetitle);


// Remove meta generator tag
$this->setGenerator($sitetitle);

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

$filter = JFilterInput::getInstance();
$uri = JURI::getInstance();
$uri = $filter->clean($uri, 'string');
$uri = htmlspecialchars($uri);

// 活動itemid
$menu = $app->getMenu() ;
$menuItem = $menu ->getItems( 'link', 'index.php?option=com_activities&view=activity', true );
$activity_id = $menuItem->id;

$home_itemid = implode(",", $home_ids);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<jdoc:include type="head" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<?php
		// Add CSS & JS
		?>
		
		<link rel="stylesheet" href="templates/system/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/site.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/layout.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/style.css" type="text/css" />
		<link rel="stylesheet" href="templates/3C3T/css/style.css" type="text/css" />
		<link rel="stylesheet" href="templates/3C3T/css/site.css" type="text/css" />
		<link rel="stylesheet" href="templates/3C3T/css/layout.css" type="text/css" />
  		<link rel="stylesheet" href="templates/3C3T/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
 		<link rel="stylesheet" href="templates/3C3T/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
  		<link rel="stylesheet" href="templates/3C3T/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
  		<link rel="stylesheet" href="modules/mod_tabs/assets/css/tabs.css" type="text/css" />
  		<link rel="stylesheet" href="modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />		
		
		<!--[if lt IE 9]>
			<script src="templates/system/js/html5shiv.js"></script>
			<script src="/media/jui/js/html5.js"></script>
		<![endif]-->
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="media/jui/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		
		<script src="media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
		<script src="media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
		<script src="media/system/js/caption.js" type="text/javascript"></script>
		<script src="media/jui/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="templates/ch/js/template.js" type="text/javascript"></script>
		<script src="templates/system/js/respond.min.js" type="text/javascript"></script>
		<script src="modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
		<script src="modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>
	</head>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-77270035-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
	
	<script>
		// rene
		jQuery(document).ready(function() {
			jQuery("#remainmenu .parent").each(function(){
				jQuery(this).children().children("a").attr("href", "#");
			});		
			
			jQuery(".mainlevel2").hide();
			jQuery("#remainmenu .active").find("li").show();
			
			jQuery(".mainlevel1").on("click", function() {
				jQuery(".mainlevel1").removeClass("active");
				jQuery(this).addClass("active");
				jQuery(".mainlevel2").hide();
				jQuery(this).find("li").show();
			});
			
			jQuery(".item-134 .itemlevel1 a").attr("href", "<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>");
			jQuery(".item-135 .itemlevel1 a").attr("href", "<?php echo JRoute::_("index.php?option=com_activities&view=activity&Itemid={$activity_id}", false); ?>");
		});
		(function($) {
			window.setInterval(function(){var r;try{r=window.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP")}catch(e){}if(r){r.open("GET.html","index.html",true);r.send(null)}},3600000);
			jQuery(document).ready(function(){$=jQuery;var tabs_num=jQuery('#tabs_announcement .tab-title').length;jQuery('#tabs_announcement .tab-separator').each(function(index,obj){var offset=0+((index+1)*120.5);var top=8;jQuery(obj).css('left',offset+'px');jQuery(obj).css('top',top+'px');});jQuery('#tabs_announcement .tab-link').each(function(index,obj){jQuery(obj).bind('click focus',function(){jQuery('#tabs_announcement  .tab-link').removeClass("active");jQuery(this).addClass("active");jQuery('#tabs_announcement .tab-content').hide().eq(index).show();});});jQuery('#tabs_announcement .tab-link').eq(0).click();$('#tabs_announcement .tab-link').mouseover(function(){$(this).addClass('hover');}).mouseout(function(){$(this).removeClass('hover');});});
			;(function(window,$){var $mainmenu;var $mainmainmenu;var $sfmenu;var $menuLinks;$(function(){$sfmenu=$('ul.sf-menu');$menuLinks=$('.menu_link_1');$submenu_warpper=$('.submenu_warpper');$sfmenu.superfish({autoArrows:false,dropShadows:false});$mainmenu=$('#open-mainmenu');$mainmainmenu=$('.mainmainmenu');$mainmenu.bind('click',function(){$('#topmenu').hide();$mainmainmenu.toggle({speed:500});});initMenuState();$(document).bind('responsive',initMenuState);});var initMenuState=function(){$mainmainmenu.show();$sfmenu.find('ul').css('position','absolute');$menuLinks.unbind('click');}})(window,jQuery);
			;(function(window,$){$(function(){var remainmenu=$('#remainmenu');var openremainmenu=$('#open-remainmenu');openremainmenu.bind('click',function(){if(remainmenu.is(':hidden')){$('.menu').hide();}
			remainmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){remainmenu.hide();}else{remainmenu.show();}}})(window,jQuery);
			;(function(window,$){$(function(){var retopmenu=$('#retopmenu');var openretopmenu=$('#open-retopmenu');openretopmenu.bind('click',function(){if(retopmenu.is(':hidden')){$('.menu').hide();}
			retopmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){retopmenu.hide();}else{retopmenu.show();}}})(window,jQuery);
		})(jQuery);
	</script>
	
	<body class="<?php
		echo $option
		. ' view-' . $view
		. ($layout ? ' layout-' . $layout : ' no-layout')
		. ($task ? ' task-' . $task : ' no-task')
		. ($itemid ? ' itemid-' . $itemid : '');
		?>">
		<!-- Body -->
		<div class="all">
			<div class="container">
				<div class="theme_top">
					<!-- Header -->
					<div class="header">
						<div class="header-logo">
							<div class="custom">
								<p><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>"><img src="templates/3C3T/images/system/logo.png" alt="logo" /></a></p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="head_banner" id="home_banner">
					<div class="bannergroup" style="background-image: url('templates/3C3T/images/banner/3c3t.jpg');"></div>

				</div>
			</div>

			<div class="home">
				<div class="home_middle">
					<div class="custom_quicklink"  >
						<div class="quick_link">
							<div class="item_block">
								<div class="record_item item quick_left">
									<div class="icon"><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid=314", false); ?>"><img src="templates/3C3T/images/system/3c3t.png" alt="3c3t2016國際家庭日"  title="3c3t2016國際家庭日"/></a>
									</div>
								</div>
								<div class="relation_item item quick_right">
									<div class="icon"><a href="https://icoparenting.moe.edu.tw/event" target="_blank"><img src="templates/3C3T/images/system/related2.png" alt="999個親子心動時光"  title="999個親子心動時光"/></a>
									</div>
								</div>
								<div class="activity_item item quick_left">
									<div class="icon"><a href="http://is.gd/N5PER0" target="_blank"><img src="templates/3C3T/images/system/quick_activity.png" alt="與愛同行迷你馬拉松" title="與愛同行迷你馬拉松" /></a>
									</div>
								</div>
								<div class="library_item last_item quick_right">
									<div class="icon"><a href="http://www.ntl.edu.tw/" target="_blank"><img src="templates/3C3T/images/system/quick_library.png" alt="幸福家庭樂書香"  title="幸福家庭樂書香"/></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php 
					if (in_array($itemid, $home_ids)) :	// 首頁 item id
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/home.php");
					else :								// 內頁
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/tmpl.php");
					endif;
				?>
				<!-- Footer -->
				<div class="footer clear" id="footer_home">
					<div class="foot_main">
						<div class="custom_footerintro"  >
							<p>建議使用瀏覽器Google Chrome、Firefox或IE10以上版本，瀏覽解析度1024x768以上，以獲得最佳瀏覽模式</p>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</body>
</html>
