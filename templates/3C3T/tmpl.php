<!-- <div class="contentarea" id="contentarea_block"> -->
	<div class="contentarea inner">
		<div id="left_menu">
			<div class="custom_iquick">
				<a href="http://www.un.org/en/events/familyday/" target="_blank" title="聯合國國際家庭日"><img src="templates/3C3T/images/system/banne01.jpg" alt="聯合國國際家庭日" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="https://moe.familyedu.moe.gov.tw/" target="_blank" title="教育部家庭教育網"><img src="templates/3C3T/images/system/banne02.jpg" alt="教育部家庭教育網" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="https://ilove.moe.edu.tw/" target="_blank" title="iLove 戀愛時光地圖"><img src="templates/3C3T/images/system/banne03.jpg" alt="iLove 戀愛時光地圖" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="https://icoparenting.moe.edu.tw/" target="_blank" title="iCoparenting 和樂共親職"><img src="templates/3C3T/images/system/banne04.jpg" alt="iCoparenting 和樂共親職" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="https://imyfamily.moe.edu.tw/" target="_blank" title="iMyfamily 愛我們的家"><img src="templates/3C3T/images/system/banne05.jpg" alt="iMyfamily 愛我們的家" width="240" height="70" /></a>
			</div>
		</div>
		<div id="content">	
			<div id="content_title">
				<div class="mod_sectiontitle"></div>
				<div id="content_block">
					<div class="com_gathercard">	
						<div class="game_block">
							<div class="game_page-header">
								<div class="title" style="text-align:center; font-size:24px;">
								105年515國際家庭日<BR>
				「善用3C 幸福3T」家庭教育宣導系列活動
				
								</div>
							</div>
								
							<div>
								<div class="intro_text">
									
									<p style="font-size:20px; font-weight:bold;"><strong>推廣「善用數位科技與社群媒體」的正向力量</strong></p>
									<p> 　　有鑑於科技進步及網路、智慧型手機的普及，網路、社群媒體（social media）成為全球發展最快速的媒體。曾有人稱這一代的年輕孩子為「滑世代」，事實上，現在已是「滑年代」！幾乎是不分世代、全民皆「滑」。
								    </p>
									<p style="font-size:20px; font-weight:bold;"><br>
								    <strong>「善用3C 幸福3T」:家庭教育宣導系列活動</strong></p>
									<p>　　教育部基於推動終身學習及家庭教育的立場，思考「滑年代」的現象如何影響著家庭以及人際間的互動關係，並將關注或討論的範疇，延伸至「正向使用」數位科技及社群媒體的力量，於105年515國際家庭日，推出「善用3C 幸福3T」的正向理念。</p>
									<p><br>
									  　　所謂「善用3C」，意在強調身處在數位時代的父母，在教養上一定要做到：「瞭解數位科技與社群媒體」、「設定合宜的監控機制」及「力行健康的使用型態」這三件事；並由「增進家庭凝聚力」的目標出發，提出「幸福3T」－「全家共讀、同樂、一起動一動」(Reading together、Playing together、Running together) 的愛家行動，推出「幸福家庭樂書香」的圖書館家庭共學活動、「999個親子心動時光」照片徵文及「與愛同行-Mini馬」等家庭教育宣導系列活動，鼓勵民眾於生活中建立家庭共學的習慣，營造家人美好的相聚時光，一起維護家人的身心健康。
								    </p>
								</div>
							
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
<!-- </div> -->