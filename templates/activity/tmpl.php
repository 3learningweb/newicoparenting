<div class="contentarea" id="contentarea_block">
	<div class="contentarea inner">
		<div id="left_menu">
			<div class="custom_iquick">
				<a href="https://icoparenting.moe.edu.tw/3C3T" target="_blank" title="3c3t 2016國際家庭日" ><img src="templates/activity/images/system/banne01.jpg" alt="3c3t 2016國際家庭日" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="http://ks.familyedu.moe.gov.tw/SubSites/Pages/Detail.aspx?site=2963aef4-cd1f-4ed2-a6d8-bc525f4bde19&nodeid=445&pid=6868" target="_blank" title="高雄市家庭教育中心與愛同行迷你馬"><img src="templates/activity/images/system/banne02.jpg" alt="高雄市家庭教育中心與愛同行迷你馬" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="http://www.ntl.edu.tw/" target="_blank" title="國立台灣圖書館 幸福家庭樂書香"><img src="templates/activity/images/system/banne03.jpg" alt="國立台灣圖書館 幸福家庭樂書香" width="240" height="70" /></a>
			</div>
		</div>
		<div id="content">	
			<ul class="breadcrumb">
				<li class="active"><span class="divider"></span></li>
				<li>
					<a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>" class="pathway">活動首頁</a>
					<span class="divider">&gt;</span>
					<?php
						if($layout != "") {
							echo "我要投稿";
						}elseif($option == "com_content") {
							if($itemid == 315) {
								echo "999活動得獎公告";	
							}else{
								echo "最新消息";	
							}
						}else{
							echo "活動辦法";
						} 
					?>
				</li>
			</ul>

			<div id="content_title">
				<div class="mod_sectiontitle">
					<div class="section_title">
					<?php
						if($layout != "") {
							echo "我要投稿";
						}elseif($option == "com_content") {
							if($itemid == 315) {
								echo "999活動得獎公告";	
							}else{
								echo "最新消息";	
							}
						}else{
							echo "活動辦法";
						} 
					?>
					</div>
				</div>
				
				<div id="content_block">
					<jdoc:include type="message" />
					<jdoc:include type="component" />
				</div>
			</div>
		</div>
	</div>
</div>