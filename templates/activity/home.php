<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76545260-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="home">
	<div class="home_area">
		<div class="left">
			<div id="tabs_announcement">
				<div class="tab-box">
					<div class="tab-item">
			    		<div class="tab-title tab-title_announcement tab-1st">
    						<a class="tab-link tab-link_announcement"><span class="tab-link-span tab-link_announcement">網站公告</span></a>
						</div>
			    		<div class="tab-content">
    						<div class="mod_announcement">
								<div class="listline">
									<div class="datablock">
										<span class="title_icon">•</span>
										<div class="rtitle">
											<a href="<?php echo JRoute::_("index.php?option=com_content&view=category&id=38&Itemid=312", false); ?>" title="最新消息">最新消息</a>
										</div>
									</div>
									<div class="datablock">
										<span class="title_icon">•</span>
										<div class="rtitle">
											<a href="https://icoparenting.moe.edu.tw/最新消息/99-客服資訊" title="客服資訊">客服資訊</a>
										</div>
									</div>
								</div>
							</div>
    					</div>
    					<!--照片-->
    					<img src="templates/activity/images/system/photo.gif" alt="999親子愛的時光" title="999親子愛的時光" width="200" height="150" style="margin:50px 20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div class="middle">
			<div class="newsflash">
				<h5 class="newsflash-title">我要投稿</h5>
				<!--投稿內容-->
				<table style="width: 100%;" border="0" cellspacing="2" cellpadding="5" align="center">
					<tbody>
					<tr>
						<td align="left" style="font-weight: bolder;">請自行選擇投稿縣市</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">
							<img src="templates/activity/images/system/photo1.png" alt="im 005" width="200" height="130" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200"><font size="1";>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=keelung_city&Itemid={$activity_id}"); ?>">基隆市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=taipei_city&Itemid={$activity_id}"); ?>">臺北市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=new_taipei_city&Itemid={$activity_id}"); ?>">新北市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=taoyuan_city&Itemid={$activity_id}"); ?>">桃園市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=hsinchu_city&Itemid={$activity_id}"); ?>">新竹市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=hsinchu_county&Itemid={$activity_id}"); ?>">新竹縣</a>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200" height="0">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							<img src="templates/activity/images/system/photo2.png" alt="im 006" width="200" height="130" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=miaoli_county&Itemid={$activity_id}"); ?>">苗栗縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=taichung_city&Itemid={$activity_id}"); ?>">臺中市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=nantou_county&Itemid={$activity_id}"); ?>">南投縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=changhua_county&Itemid={$activity_id}"); ?>">彰化縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=yunlin_county&Itemid={$activity_id}"); ?>">雲林縣</a>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" height="-1">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							<img src="templates/activity/images/system/photo3.png" alt="im 007" width="200" height="130" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=chiayi_county&Itemid={$activity_id}"); ?>">嘉義縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=chiayi_city&Itemid={$activity_id}"); ?>">嘉義市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=tainan_city&Itemid={$activity_id}"); ?>">臺南市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=kaohsiung_city&Itemid={$activity_id}"); ?>">高雄市</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=pingtung_county&Itemid={$activity_id}"); ?>">屏東縣</a>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" height="1">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">
							<img src="templates/activity/images/system/photo4.png" alt="im 008" width="200" height="130" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=yilan_county&Itemid={$activity_id}"); ?>">宜蘭縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=hualien_county&Itemid={$activity_id}"); ?>">花蓮縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=taitung_county&Itemid={$activity_id}"); ?>">臺東縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=penghu_county&Itemid={$activity_id}"); ?>">澎湖縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=kinmen_county&Itemid={$activity_id}"); ?>">金門縣</a>
							<font size="1";>●</font><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&layout=lienchiang_county&Itemid={$activity_id}"); ?>">連江縣</a>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="200">&nbsp;</td>
					</tr>
					</tbody>
				</table>
				<!--投稿內容結束-->
			</div>
		</div>
		<div class="right">
			<div class="custom_iquick">
				<a href="https://icoparenting.moe.edu.tw/3C3T" target="_blank" title="3c3t 2016國際家庭日" ><img src="templates/activity/images/system/banne01.jpg" alt="3c3t 2016國際家庭日" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="http://ks.familyedu.moe.gov.tw/SubSites/Pages/Detail.aspx?site=2963aef4-cd1f-4ed2-a6d8-bc525f4bde19&nodeid=445&pid=6868" target="_blank" title="高雄市家庭教育中心與愛同行迷你馬"><img src="templates/activity/images/system/banne02.jpg" alt="高雄市家庭教育中心與愛同行迷你馬" width="240" height="70" /></a>
			</div>
			<div class="custom_iquick">
				<a href="http://www.ntl.edu.tw/" target="_blank" title="國立台灣圖書館 幸福家庭樂書香"><img src="templates/activity/images/system/banne03.jpg" alt="國立台灣圖書館 幸福家庭樂書香" width="240" height="70" /></a>
			</div>
		</div>
	</div>
</div>