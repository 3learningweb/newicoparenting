<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;


// Get template params
$sitetitle = $app->getTemplate(true)->params->get('sitetitle');
$home_ids = explode(",", $app->getTemplate(true)->params->get('home_id'));

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');


//rename title (if home page)
$doc->setTitle($doc->getTitle() . " | " . $sitetitle);


// Remove meta generator tag
$this->setGenerator($sitetitle);

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

$filter = JFilterInput::getInstance();
$uri = JURI::getInstance();
$uri = $filter->clean($uri, 'string');
$uri = htmlspecialchars($uri);

// 活動itemid
$menu = $app->getMenu() ;
$menuItem = $menu ->getItems( 'link', 'index.php?option=com_activities&view=activity', true );
$activity_id = $menuItem->id;

$home_itemid = implode(",", $home_ids);

// 得獎公告 link
$menu = $app->getMenu() ;
$result_link = $menu->getItem(315)->link . "&Itemid=315";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<jdoc:include type="head" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<?php
		// Add CSS & JS
		?>
		
		<link rel="stylesheet" href="templates/system/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/site.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/layout.css" type="text/css" />
		<link rel="stylesheet" href="templates/system/css/style.css" type="text/css" />
		<link rel="stylesheet" href="templates/activity/css/style.css" type="text/css" />
		<link rel="stylesheet" href="templates/activity/css/site.css" type="text/css" />
		<link rel="stylesheet" href="templates/activity/css/layout.css" type="text/css" />
  		<link rel="stylesheet" href="templates/activity/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
 		<link rel="stylesheet" href="templates/activity/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
  		<link rel="stylesheet" href="templates/activity/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
  		<link rel="stylesheet" href="modules/mod_tabs/assets/css/tabs.css" type="text/css" />
  		<link rel="stylesheet" href="modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />		
		
		<!--[if lt IE 9]>
			<script src="templates/system/js/html5shiv.js"></script>
			<script src="/media/jui/js/html5.js"></script>
		<![endif]-->
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="media/jui/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		
		<script src="media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
		<script src="media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
		<script src="media/system/js/caption.js" type="text/javascript"></script>
		<script src="media/jui/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="templates/ch/js/template.js" type="text/javascript"></script>
		<script src="templates/system/js/respond.min.js" type="text/javascript"></script>
		<script src="modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
		<script src="modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>
	</head>
	
	<script>
		// rene
		jQuery(document).ready(function() {
			jQuery("#remainmenu .parent").each(function(){
				jQuery(this).children().children("a").attr("href", "#");
			});		
			
			jQuery(".mainlevel2").hide();
			jQuery("#remainmenu .active").find("li").show();
			
			jQuery(".mainlevel1").on("click", function() {
				jQuery(".mainlevel1").removeClass("active");
				jQuery(this).addClass("active");
				jQuery(".mainlevel2").hide();
				jQuery(this).find("li").show();
			});
			
			jQuery(".item-134 .itemlevel1 a").attr("href", "<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>");
			jQuery(".item-135 .itemlevel1 a").attr("href", "<?php echo JRoute::_("index.php?option=com_activities&view=activity&Itemid={$activity_id}", false); ?>");
			jQuery(".item-136 .itemlevel1 a").attr("href", "<?php echo JRoute::_("{$result_link}", false); ?>");
		});
		(function($) {
			window.setInterval(function(){var r;try{r=window.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP")}catch(e){}if(r){r.open("GET.html","index.html",true);r.send(null)}},3600000);
			jQuery(document).ready(function(){$=jQuery;var tabs_num=jQuery('#tabs_announcement .tab-title').length;jQuery('#tabs_announcement .tab-separator').each(function(index,obj){var offset=0+((index+1)*120.5);var top=8;jQuery(obj).css('left',offset+'px');jQuery(obj).css('top',top+'px');});jQuery('#tabs_announcement .tab-link').each(function(index,obj){jQuery(obj).bind('click focus',function(){jQuery('#tabs_announcement  .tab-link').removeClass("active");jQuery(this).addClass("active");jQuery('#tabs_announcement .tab-content').hide().eq(index).show();});});jQuery('#tabs_announcement .tab-link').eq(0).click();$('#tabs_announcement .tab-link').mouseover(function(){$(this).addClass('hover');}).mouseout(function(){$(this).removeClass('hover');});});
			;(function(window,$){var $mainmenu;var $mainmainmenu;var $sfmenu;var $menuLinks;$(function(){$sfmenu=$('ul.sf-menu');$menuLinks=$('.menu_link_1');$submenu_warpper=$('.submenu_warpper');$sfmenu.superfish({autoArrows:false,dropShadows:false});$mainmenu=$('#open-mainmenu');$mainmainmenu=$('.mainmainmenu');$mainmenu.bind('click',function(){$('#topmenu').hide();$mainmainmenu.toggle({speed:500});});initMenuState();$(document).bind('responsive',initMenuState);});var initMenuState=function(){$mainmainmenu.show();$sfmenu.find('ul').css('position','absolute');$menuLinks.unbind('click');}})(window,jQuery);
			;(function(window,$){$(function(){var remainmenu=$('#remainmenu');var openremainmenu=$('#open-remainmenu');openremainmenu.bind('click',function(){if(remainmenu.is(':hidden')){$('.menu').hide();}
			remainmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){remainmenu.hide();}else{remainmenu.show();}}})(window,jQuery);
			;(function(window,$){$(function(){var retopmenu=$('#retopmenu');var openretopmenu=$('#open-retopmenu');openretopmenu.bind('click',function(){if(retopmenu.is(':hidden')){$('.menu').hide();}
			retopmenu.toggle({speed:500});});$(document).bind('responsive',initMenuState);});var initMenuState=function(){if(window.responsive.platformId==3){retopmenu.hide();}else{retopmenu.show();}}})(window,jQuery);
		})(jQuery);
	</script>
	
	<body class="<?php
		echo $option
		. ' view-' . $view
		. ($layout ? ' layout-' . $layout : ' no-layout')
		. ($task ? ' task-' . $task : ' no-task')
		. ($itemid ? ' itemid-' . $itemid : '');
		?>">
		<!-- Body -->
		<div class="all">
			<div class="container">
				<div class="theme_top">
					<!-- Header -->
					<div class="header">
						<div class="header-logo">
							<div class="custom"  >
								<p><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>"><img src="templates/activity/images/system/logo.png" alt="999個親子心動時光" title="999個親子心動時光" /></a></p>
							</div>
						</div>
						<div class="header-menu">
							<ul class="nav menu_topmenu" id="topmenu">
								<li class="item-126 current active"><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>" >活動首頁</a></li>
								<li class='separator'><span>．</span></li>
								<li class="item-128"><a href="https://ilove.moe.edu.tw/" target="_blank" >iLove</a></li>
								<li class='separator'><span>．</span></li>
								<li class="item-129"><a href="https://icoparenting.moe.edu.tw/" target="_blank" >iCoparenting</a></li>
								<li class='separator'><span>．</span></li>
								<li class="item-132"><a href="https://imyfamily.moe.edu.tw/"target="_blank" >iMyfamily</a></li>
							</ul>
							<div id="open-retopmenu" style="display: none;"><a href="javascript:void(0);" alt="上方選單" title="上方選單"><span>上方選單</span></a></div>
							<ul class="menu" id="retopmenu">
								<li class="item-126 current active mainlevel1"><div class="itemlevel1"><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>" >活動首頁</a></div></li>
								<li class="item-128 mainlevel1"><div class="itemlevel1"><a href="https://ilove.moe.edu.tw/" target="_blank" >iLove</a></div></li>
								<li class="item-129 mainlevel1"><div class="itemlevel1"><a href="https://icoparenting.moe.edu.tw/" target="_blank" >iCoparenting</a></div></li>
								<li class="item-132 mainlevel1"><div class="itemlevel1"><a href="https://imyfamily.moe.edu.tw/"target="_blank" >iMyfamily</a></div></li>
							</ul>
						</div>
					</div>
					
					<div class="mainmenu">		
						<ul class="sf-menu sf-js-enabled menu" id="mainmenu">
							<li id="item-134" class="parent"><a class="menu_link_1" title="我要投稿" href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>" ><span>我要投稿</span></a></li>
							<li id="item-135" class="parent"><a class="menu_link_1" title="活動辦法" href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&Itemid={$activity_id}", false); ?>" ><span>活動辦法</span></a></li>
							<li id="item-136" class="parent"><a class="menu_link_1" title="得獎公告" href="<?php echo JRoute::_("{$result_link}", false); ?>" ><span>得獎公告</span></a></li>
							<li id="item-138" class="parent"><a class="menu_link_1" title="親子學習趣" href="#" ><span>親子學習趣</span></a>
								<ul class='submenu_warpper' style='float: none; width: 220px; position: absolute;'>
									<div class='submenu_table'>
										<div class='twoblock'>
											<div class='submenu_list'>
												<span id="level_tow_178" class="submenu_1"><a class="menu_link_2" title="iLove" href="https://ilove.moe.edu.tw/"  target="_blank" ><span class='submenu_icon'>•</span><span>iLove 戀愛時光地圖</span></a></span>
											</div>
										</div>
										<div class='twoblock'>
											<div class='submenu_list'>
												<span id="level_tow_302" class="submenu_1"><a class="menu_link_2" title="iCoparenting" href="https://icoparenting.moe.edu.tw/"  target="_blank" ><span class='submenu_icon'>•</span><span>iCoparenting 和樂共親職</span></a></span>
											</div>
										</div>
										<div class='twoblock'>
											<div class='submenu_list'>
												<span id="level_tow_303" class="submenu_1"><a class="menu_link_2" title="iMyfamily" href="https://imyfamily.moe.edu.tw/"  target="_blank" ><span class='submenu_icon'>•</span><span>iMyfamily 愛我們的家</span></a></span>
											</div>
										</div>
									</div>
								</ul>
							</li>
						</ul>       
						<div id="open-remainmenu" style="display: none;">
							<a href="javascript:void(0);" alt="主要選單" title="主要選單"><span>主要選單</span></a>
						</div>
						<ul class="menu" id="remainmenu">
							<li class="item-134 deeper parent mainlevel1"><div class="itemlevel1"><a href="<?php echo JRoute::_("index.php?option=com_content&view=featured&Itemid={$home_itemid}", false); ?>" >我要投稿</a></div></li>
							<li class="item-135 deeper parent mainlevel1"><div class="itemlevel1"><a href="<?php echo JRoute::_("index.php?option=com_activities&view=activity&Itemid={$activity_id}", false); ?>" >活動辦法</a></div></li>
							<li class="item-136 deeper parent mainlevel1"><div class="itemlevel1"><a href="<?php echo JRoute::_("{$result_link}", false); ?>" >得獎公告</a></div></li>
							<li class="item-138 deeper parent mainlevel1"><div class="itemlevel1"><a href="#" >親子學習趣</a></div>
								<ul>
									<li class="item-178 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span><a href="https://ilove.moe.edu.tw/"  target="_blank" >iLove 戀愛時光地圖</a></div></li>
									<li class="item-302 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span><a href="https://icoparenting.moe.edu.tw/" target="_blank" >iCoparenting 和樂共親職</a></div></li>
									<li class="item-303 mainlevel2"><div class="itemlevel2"><span class='submenu_icon'>•</span><a href="https://imyfamily.moe.edu.tw/" target="_blank" >iMyfamily 愛我們的家</a></div></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="head_banner" id="home_banner">		
						<div class="bannergroup" style="background-image: url('templates/activity/images/banner/imy.jpg');"></div>
					</div>
				</div>
				<?php 
					if (in_array($itemid, $home_ids)) :	// 首頁 item id
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/home.php");
					else :								// 內頁
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/tmpl.php");
					endif;
				?>
				
				<!-- Footer -->
				<div class="footer clear" id="footer_home">
					<div class="foot_main">
						<div class="custom_footerintro"  >
							<p style="height:30px;">主辦單位：教育部終身教育司&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;承辦單位：桃園市政府教育局、桃園市政府家庭教育中心</p>
    						<p style="height:60px;">建議使用瀏覽器Google Chrome、Firefox或IE10以上版本，瀏覽解析度1024x768以上，以獲得最佳瀏覽模式</p>
    					</div>
						<div class="footergroup" style="background-image: url('templates/activity/images/footer/footer_img.jpg');"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
