<?php 
// no direct access
defined('_JEXEC') or die;
?>
<script type="text/javascript">
var dir = "<?php echo JURI::root(); ?>";
var moodzt = "0";
var http_request = false;
var delay = 0;

jQuery(document).ready(function () {
	// for category tab 切換頁籤投票時，把已投票判斷歸零
	jQuery(".tab").click(function() {
		moodzt = "0";
	});
});

function makeRequest(url, functionName, httpType, sendData) 
{
	http_request = false;
	if(!httpType) httpType = "GET";
	if(window.XMLHttpRequest) {  // Non-IE...
		http_request = new XMLHttpRequest();
		if(http_request.overrideMimeType) {
			http_request.overrideMimeType('text/plain');
		}
	} else if(window.ActiveXObject) { // IE
		try{
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {}
		}
	}
	
	if(!http_request) {
	    alert("Cannot send an XMLHTTP request");
	    return false;
	}
	
	var changefunc = "http_request.onreadystatechange = " + functionName;
	eval (changefunc);
	http_request.open(httpType, url, true);
	http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	http_request.send(sendData);
}

function $() 
{
	var elements = new Array();
	
	for(var i=0; i<arguments.length; i++) {
		var element = arguments[i];
		if(typeof element == 'string')
		    element = document.getElementById(element);
		
		if(arguments.length == 1)
		    return element;
		
		elements.push(element)
	}
	
	return elements;	
}

jQuery(document).on("click", "#mood_link", function() {
	var mood_id = jQuery(this).attr("class"); 
	var infoid = jQuery(this).parent().parent().find("#infoid").val();
	var classid = jQuery(this).parent().parent().find("#classid").val();
	
	remood(infoid, classid); // 第一次投票(資料庫還未建資料)使用
	
	if(moodzt == "1") {
		alert("您已經投過票，請勿重複投票。");
	} else {
		//alert("投票成功");  // sleep
		setTimeout(function(){
				delay = 1;
				url = dir + "plugins/content/mood/xinqing.php?action=mood&classid="+classid+"&id="+infoid+"&typee="+mood_id+"&m=" + Math.random();
			    makeRequest(url, 'return_review1', 'GET', '');
			    moodzt = "1";
		},500);
	}
	
});

function remood(infoid, classid) 
{
	url = dir + "plugins/content/mood/xinqing.php?action=show&id="+infoid+"&classid="+classid+"&m=" + Math.random();
	makeRequest(url, 'return_review1', 'GET', '');
}

function return_review1(ajax) 
{
	if(http_request.readyState == 4) {
		if(http_request.status == 200) {
			var str_error_num = http_request.responseText;
			// alert(str_error_num);
			if(str_error_num == "error") {
				alert("訊息不存在");
			} else if(str_error_num == 0) {
				alert("您已經投過票，請勿重複投票。");
			} else {
				if(delay == 1){
					moodinner(str_error_num);
				}
			}
		} else {
			alert('There was a problem with the request.');
		}
	}
}

function moodinner(moodtext) 
{
	var tab = jQuery(".tab.active").find("a").attr("class");
	if(tab) {
		var tab_index = tab.split("_");
	}
	
	var imga = dir+"/plugins/content/mood/images/pre_02.gif";
	var imgb = dir+"/plugins/content/mood/images/pre_01.gif";
	var color1 = "#666666";
	var color2 = "#EB610E";
	var heightz = "80";
	var hmax = 0;
	var hmaxpx = 0;
	var heightarr = new Array();
	var moodarr = moodtext.split(",");
	var moodzs = 0;
	
	for(k=0; k<8; k++) {
		moodarr[k] = parseInt(moodarr[k]);
		moodzs += moodarr[k];
	}
	
	for(i=0; i<8; i++) {
		heightarr[i] = Math.round(moodarr[i]/moodzs*heightz);
		if(heightarr[i]<1) heightarr[i]=1;
		if(moodarr[i]>hmaxpx) {
			hmaxpx = moodarr[i];
		}
	}
	
	var lang = document.getElementById("lang").value;
	if(lang == "zh-TW"){
		if(tab) {
			jQuery(".tab_content_"+tab_index[1]+" #moodtitle").html("<span style='color: #555555;'>目前已有<font color='#FF0000'>"+moodzs+"</font>人發表心情</span>");
		}else{
			jQuery("#moodtitle").html("<span style='color: #555555;'>目前已有<font color='#FF0000'>"+moodzs+"</font>人發表心情</span>");
		}		
	}else{
		
	}
	
	for(j=0; j<8; j++) {
		var num = (moodarr[j]/moodzs)*100;
		
		if(moodarr[j] == hmaxpx && moodarr[j] != 0) {
//			百分比顯示
			if(tab) {
				jQuery(".tab_content_"+tab_index[1]+" #moodinfo"+j).html("<span style='color: "+color2+";'>"+num.toFixed(1)+"%</span>");
			}else{
				jQuery("#moodinfo"+j).html("<span style='color: "+color2+";'>"+num.toFixed(1)+"%</span>");
			}
		} else {
//			百分比顯示
			if(tab) {
				jQuery(".tab_content_"+tab_index[1]+" #moodinfo"+j).html("<span style='color: "+color1+";'>"+num.toFixed(1)+"%</span>");
			}else{
				jQuery("#moodinfo"+j).html("<span style='color: "+color1+";'>"+num.toFixed(1)+"%</span>");
			}
		}
	}
}

</script>