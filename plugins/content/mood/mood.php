<?php
defined('_JEXEC') or die;

//$mainframe->registerEvent( 'onAfterDisplayContent', 'plgContentMood' );

class plgContentMood extends JPlugin {

//function plgContentMood($row, $params, $page=0)
public function onContentAfterDisplay($context, &$row, &$params, $page=0)
{
	$lang = JFactory::getLanguage();
	// $langId = JFactory::getLanguageId();
	
	$input =JFactory::getApplication()->input;
	$tmpl=$input->get('tmpl');
	include_once JPATH_ROOT.DIRECTORY_SEPARATOR. "plugins/content/mood/mood_ajax.php";
	// get mood params	
	$plugin = JPluginHelper::getPlugin('content', 'mood');
	$pluginparams = new JRegistry(); 
	$pluginparams->loadString($plugin->params);
	
	// 過濾不要顯示的文章 id 2014-04-22 (mnp005) rene
	$filter = $pluginparams->get('filter');
	
	if($filter) {
		if(strpos($filter, ",")) {
			$filter_arr = explode(",", $filter);
			foreach($filter_arr as $key => $f) {
				if($f == $row->id) {
					$hidden = 1;
					break;
				}
			}
		}else{
			if($filter == $row->id) {
				$hidden = 1;
			}	
		}		
	}

	// 過濾掉友善列印等頁面 2015-03-02 tsg012 zoe
	if($tmpl){
		$hidden = 1;
	}


	if($hidden != 1) {
//		if($lang->getTag() == "zh-TW") {$title_icon = "plugins/content/mood/images/nmb003n.png";} else {$title_icon = "plugins/content/mood/images/nmb003en.png";}
		if($lang->getTag() == "zh-TW") {$title_name = "看完您覺得";} else {$title_name = "What do you think?";}
	
		$view = JRequest::getString('view');
		$layout = JRequest::getString('layout');
		
		if($view == 'article' or ($view = 'article' and ($layout = 'tabs' or $layout = 'mtabs'))):

		// display
		for($i=1 ; $i<=8 ; $i++){
			if($pluginparams->get("mood".$i."_display") == "1") {
				$display[$i] = "";
			}else{
				$display[$i] = "none";
			}
		}

		// icon
		if($pluginparams->get('mood4_icon')){$mood4_icon = "filesys/image/" . $pluginparams->get('mood4_icon');}else{$mood4_icon = "plugins/content/mood/images/3.gif";}
		if($pluginparams->get('mood3_icon')){$mood3_icon = "filesys/image/" . $pluginparams->get('mood3_icon');}else{$mood3_icon = "plugins/content/mood/images/2.gif";}
		if($pluginparams->get('mood1_icon')){$mood1_icon = "filesys/image/" . $pluginparams->get('mood1_icon');}else{$mood1_icon = "plugins/content/mood/images/0.gif";}
		if($pluginparams->get('mood6_icon')){$mood6_icon = "filesys/image/" . $pluginparams->get('mood6_icon');}else{$mood6_icon = "plugins/content/mood/images/5.gif";}
		if($pluginparams->get('mood8_icon')){$mood8_icon = "filesys/image/" . $pluginparams->get('mood8_icon');}else{$mood8_icon = "plugins/content/mood/images/7.gif";}
		if($pluginparams->get('mood2_icon')){$mood2_icon = "filesys/image/" . $pluginparams->get('mood2_icon');}else{$mood2_icon = "plugins/content/mood/images/1.gif";}
		if($pluginparams->get('mood5_icon')){$mood5_icon = "filesys/image/" . $pluginparams->get('mood5_icon');}else{$mood5_icon = "plugins/content/mood/images/4.gif";}
		if($pluginparams->get('mood7_icon')){$mood7_icon = "filesys/image/" . $pluginparams->get('mood7_icon');}else{$mood7_icon = "plugins/content/mood/images/6.gif";}
	
		// ch_name
		if($pluginparams->get('mood4_ch_text')){$mood4_ch_text = $pluginparams->get('mood4_ch_text');}else{$mood4_ch_text = "很棒";}
		if($pluginparams->get('mood3_ch_text')){$mood3_ch_text = $pluginparams->get('mood3_ch_text');}else{$mood3_ch_text = "支持";}
		if($pluginparams->get('mood1_ch_text')){$mood1_ch_text = $pluginparams->get('mood1_ch_text');}else{$mood1_ch_text = "驚訝";}
		if($pluginparams->get('mood6_ch_text')){$mood6_ch_text = $pluginparams->get('mood6_ch_text');}else{$mood6_ch_text = "搞笑";}
		if($pluginparams->get('mood8_ch_text')){$mood8_ch_text = $pluginparams->get('mood8_ch_text');}else{$mood8_ch_text = "不解";}
		if($pluginparams->get('mood2_ch_text')){$mood2_ch_text = $pluginparams->get('mood2_ch_text');}else{$mood2_ch_text = "欠揍";}
		if($pluginparams->get('mood5_ch_text')){$mood5_ch_text = $pluginparams->get('mood5_ch_text');}else{$mood5_ch_text = "憤怒";}
		if($pluginparams->get('mood7_ch_text')){$mood7_ch_text = $pluginparams->get('mood7_ch_text');}else{$mood7_ch_text = "噁心";}
	
		// en_name
		if($pluginparams->get('mood4_en_text')){$mood4_en_text = $pluginparams->get('mood4_en_text');}else{$mood4_en_text = "Great";}
		if($pluginparams->get('mood3_en_text')){$mood3_en_text = $pluginparams->get('mood3_en_text');}else{$mood3_en_text = "Support";}
		if($pluginparams->get('mood1_en_text')){$mood1_en_text = $pluginparams->get('mood1_en_text');}else{$mood1_en_text = "Amazed";}
		if($pluginparams->get('mood6_en_text')){$mood6_en_text = $pluginparams->get('mood6_en_text');}else{$mood6_en_text = "Funny";}
		if($pluginparams->get('mood8_en_text')){$mood8_en_text = $pluginparams->get('mood8_en_text');}else{$mood8_en_text = "puzzled";}
		if($pluginparams->get('mood2_en_text')){$mood2_en_text = $pluginparams->get('mood2_en_text');}else{$mood2_en_text = "Annoying";}
		if($pluginparams->get('mood5_en_text')){$mood5_en_text = $pluginparams->get('mood5_en_text');}else{$mood5_en_text = "Angry";}
		if($pluginparams->get('mood7_en_text')){$mood7_en_text = $pluginparams->get('mood7_en_text');}else{$mood7_en_text = "Nausea";}	
	
		// 取消圖片顯示 2014-04-01 (mnp005) rene
		$html = '';
		$html .= '<div align="center" class="mood-jquery">'.
	         	 '<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size:12px; margin: 20px 0;">'.
	         	 '<tr style="border: solid 0px;"><td></td><td style="border: solid 0px;" colspan="8"></td></tr>'.
	         	 // '<tr><td></td><td colspan="8"><td></tr>'.
	         	 // '<tr><td></td><td colspan="8"><td></tr>'.
             	 '<tr style="border: solid 0px; background-color: #FFFFFF;">'.
             	 '<td width="20%" style="border: solid 0px;" align="center">'.
//	change image to text for RWD by Zoe
//             	 '<img style="position: relative; display: block;" alt="'.JText::_('心情投票').'" title="'.JText::_('心情投票').'" src="' . $title_icon . '" />' .
	'<div class="title">'.$title_name . '<div class="arrow"></div></div>' .
             	 '</td>' .
             	 // '<tr style="border: solid 0px;" align="center" valign="middle">'.
   	         	 '<input id="infoid" type="hidden" value="' . $row->id . '">'.
   	         	 '<input id="lang" type="hidden" value="' . $lang->getTag() . '">'.
   	         	 '<input id="classid" type="hidden" value="' . $row->catid . '">';
   	         	 // '<td style="border: solid 0px; display: ' . $display[4] . '"><input type="image" onclick="get_mood(\'mood4\')" src="' . $mood4_icon . '" width="40" height="40"></td>'.
   	         	 // '<td style="border: solid 0px; display: ' . $display[3] . '"><input type="image" onclick="get_mood(\'mood3\')" src="' . $mood3_icon . '" width="40" height="40"></td>'.
   	         	 // '<td style="border: solid 0px; display: ' . $display[1] . '"><input type="image" onclick="get_mood(\'mood1\')" src="' . $mood1_icon . '" width="40" height="40"></td>'.
   	         	 // '<td style="border: solid 0px; display: ' . $display[6] . '"><input type="image" onclick="get_mood(\'mood6\')" src="' . $mood6_icon . '" width="40" height="40"></td>'.
   	         	 // '<td style="border: solid 0px; display: ' . $display[8] . '"><input type="image" onclick="get_mood(\'mood8\')" src="' . $mood8_icon . '" width="40" height="40"></td>'.        
   	         	// '<td style="border: solid 0px; display: ' . $display[2] . '"><input type="image" onclick="get_mood(\'mood2\')" src="' . $mood2_icon . '" width="40" height="40"></td>'.
   	         	// '<td style="border: solid 0px; display: ' . $display[5] . '"><input type="image" onclick="get_mood(\'mood5\')" src="' . $mood5_icon . '" width="40" height="40"></td>'.
   	         	// '<td style="border: solid 0px; display: ' . $display[7] . '"><input type="image" onclick="get_mood(\'mood7\')" src="' . $mood7_icon . '" width="40" height="40"></td>';
		
		if($lang->getTag() == "zh-TW") {

		$html .= '<td style="border: solid 0px; display: ' . $display[4] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood4">' . $mood4_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[3] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood3">' . $mood3_ch_text . '</a></td>'.
   	      	  	 '<td style="border: solid 0px; display: ' . $display[1] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood1">' . $mood1_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[6] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood6">' . $mood6_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[8] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood8">' . $mood8_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[2] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood2">' . $mood2_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[5] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood5">' . $mood5_ch_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[7] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood7">' . $mood7_ch_text . '</a></td>';

		} elseif($lang->getTag() == "en-GB") {

    	$html .= '<td style="border: solid 0px; display: ' . $display[4] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood4">' . $mood4_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[3] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood3">' . $mood3_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[1] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood1">' . $mood1_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[6] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood6">' . $mood6_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[8] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood8">' . $mood8_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[2] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood2">' . $mood2_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[5] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood5">' . $mood5_en_text . '</a></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[7] . '" align="center" class="hui"><a href="javascript:void(0);" id="mood_link" class="mood7">' . $mood7_en_text . '</a></td>';
		}
	$html .='</tr>';
	$html .='<tr style="border: solid 0px;" align="center" valign="bottom">' .
             	 '<td id="moodtitle"></td>' .
   	         	 '<td style="border: solid 0px; display: ' . $display[4] . '" height="30" id="moodinfo3"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[3] . '" height="30" id="moodinfo2"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[1] . '" height="30" id="moodinfo0"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[6] . '" height="30" id="moodinfo5"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[8] . '" height="30" id="moodinfo7"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[2] . '" height="30" id="moodinfo1"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[5] . '" height="30" id="moodinfo4"></td>'.
   	         	 '<td style="border: solid 0px; display: ' . $display[7] . '" height="30" id="moodinfo6"></td>'.
             	 '</tr>' ;
	$html .='</table>'.
	        	 '</div>';
		$html .= '<style>
					#mood_link{
						text-decoration: none;
						cursor: pointer;
					}
			 	</style>';
			
  		return $html;
		endif;
	}
}
}
?>